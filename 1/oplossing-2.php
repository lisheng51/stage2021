<?php
$url = "https://bloemendaalconsultancy.nl/funny_lab/sort/Home/json";
$json = file_get_contents($url);
$gegevens = json_decode($json, true);

$new = [];
foreach($gegevens['listdb'] as $rs)
{
    $new[] = $rs["province"]."#".$rs["city"];
}
$newU = array_unique($new);

$itemlist = [];
foreach($newU as $d){
    $dList = explode('#', $d);
    $item = [];
    $item['Provincie'] = $dList[0];
    $item['Naam'] = $dList[1];
    array_push($itemlist, $item);
}

$provincies = [];
foreach($itemlist as $i)
{
    if(!array_key_exists($i['Provincie'], $provincies))
    {
        $provincies[$i['Provincie']] = [];
        array_push($provincies[$i['Provincie']], $i['Naam']);
    }
    else{
        array_push($provincies[$i['Provincie']], $i['Naam']);
    }
    sort($provincies[$i['Provincie']]);
}
ksort($provincies);
var_dump($provincies);

exit;

