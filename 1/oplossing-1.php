<?php 

$url = "https://bloemendaalconsultancy.nl/funny_lab/sort/Home/json";
$json = file_get_contents($url);
$gegevens = json_decode($json, true);

// provincies in array
$provincies = [];
foreach($gegevens['listdb'] as $city)
{
    $provincie = $city['province'];
    if(!in_array($provincie, $provincies)){
        array_push($provincies, $provincie);
    }
}
sort($provincies);

$eindstand = array();
foreach($provincies as $provincie)
{
    $tijdelijkeArray = array();
    $tijdelijkeArray['provincie'] = $provincie;
    $steden = array();
    foreach($gegevens['listdb'] as $city)
    {
        if($provincie === $city['province'])
        {
            if(!in_array($city['city'], $steden)){
                array_push($steden, $city['city']);
            }
        }
    }
    sort($steden);
    $tijdelijkeArray['steden'] = $steden;
    array_push($eindstand, $tijdelijkeArray);
}

var_dump($eindstand);
echo "<br>";

foreach($eindstand as $provincie)
{
    echo $provincie['provincie'];
    echo "<br>";
    foreach($provincie['steden'] as $stad){
        echo "<li> " . $stad . "</li>";
    }
}

?>