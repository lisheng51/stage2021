function show_google_map(distance_now)
{
    var mypostion = new google.maps.LatLng($('#geo_lat').val(), $('#geo_lng').val());
    var mapProp = {
        center: mypostion,
        zoom: 8,
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    $("#googleMap").css("width", '100%').css("height", 600);

    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

    var stylez = [
        {
            featureType: "all",
            elementType: "all",
            stylers: [
                {saturation: -100} // <-- THIS
            ]
        }
    ];

    var mapType = new google.maps.StyledMapType(stylez, {name: "Grayscale"});
    map.mapTypes.set('tehgrayz', mapType);
    map.setMapTypeId('tehgrayz');

    var marker = new google.maps.Marker({
        position: mypostion,
        map: map,
        title: 'Ik ben hier!'
    });

    var radius;
    if (distance_now > 99) {
        radius = 0;
    } else {
        radius = distance_now * 1000;
    }
    var circle = new google.maps.Circle({
        center: mypostion,
        radius: radius,
        strokeColor: "#0000FF",
        fillColor: "#0000FF",
        map: map
    });


    $("#max_distance").slider({
        formatter: function (v) {
            if (v > 99) {
                $('.slider-selection').css('background', '#b52b27');  //red
                $("#amount").text('onbeperk');
                circle.setRadius(0);
                return 'Onbeperk';
            } else if (v > 60) {
                $('.slider-selection').css('background', '#f0ad4e');  //yellow
                $("#amount").text(v);
                circle.setRadius(v * 1000);
                return v;
            } else {
                $('.slider-selection').css('background', '#5cb85c');  //green
                $("#amount").text(v);
                circle.setRadius(v * 1000);
                return v;
            }
        }
    });
}

function max_distance()
{

    $("#max_distance").slider({
        formatter: function (v) {
            if (v > 99) {
                $('.slider-selection').css('background', '#b52b27');  //red
                $("#amount").text('onbeperk');
                return 'Onbeperk';
            } else if (v > 60) {
                $('.slider-selection').css('background', '#f0ad4e');  //yellow
                $("#amount").text(v);
                return v;
            } else {
                $('.slider-selection').css('background', '#5cb85c');  //green
                $("#amount").text(v);
                return v;
            }
        },
        step: 1, min: 10, max: 100
    });

    $('a[href="#distancedata"]').on('shown.bs.tab', function () {
        if ($('#geo_lat').val() > 0 && $('#geo_lng').val() > 0) {
            show_google_map($("#max_distance").val());
        } else {
            switch_tab_box("distancedata", "profile");
            handle_info_box("error", "Adres informatie is niet correct");
        }
    });
}

function switch_tab_box(from, to)
{
    $("li#tab_" + from + "").removeClass("active");
    $("div#" + from + "").removeClass("active");
    $("li#tab_" + to + "").addClass("active");
    $("div#" + to + "").addClass("active");
}