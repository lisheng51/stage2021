<?php

class Mail extends Ajax_Controller
{

    public function view()
    {
        $id = $this->input->post("mail_id");
        $type = $this->input->post("type");

        switch ($type) {
            case "sys":
                $arr_db = $this->sendmail_model->get_one_by_id(intval($id));
                break;
            default:
                $arr_db = [];
                break;
        }
        if (empty($arr_db) === false) {
            $json["msg"] = preg_replace("/<img[^>]+\>/i", "", $arr_db["message"]);
            $json["status"] = "info";
            exit(json_encode($json));
        }

        $json["msg"] = "Het mail is niet gevonden!";
        $json["status"] = "error";
        exit(json_encode($json));
    }

    public function send()
    {
        $id = $this->input->post("mail_id");
        $type = $this->input->post("type");
        $status = false;
        //$this->sendmail_model->alwaysSend = true;
        switch ($type) {
            case "sys":
                $arr_mail = $this->sendmail_model->get_one_by_id(intval($id));
                $sendstatus = $this->sendmail_model->send_action($arr_mail);
                if ($sendstatus === true) {
                    $data["is_send"] = 1;
                    $data["send_date"] = date('Y-m-d H:i:s');
                    $this->sendmail_model->edit($arr_mail[$this->sendmail_model->primary_key], $data);
                    $status = true;
                }
                break;
            default:
                break;
        }

        if ($status === true) {
            $json["msg"] = "Een mail is naar dit adres gestuurd!";
            $json["status"] = "good";
            exit(json_encode($json));
        }

        $json["msg"] = "Het mail is niet verzonden!";
        $json["status"] = "error";
        exit(json_encode($json));
    }

    public function preview()
    {
        $mail_content_js = $this->input->post("mail_content_js");
        $mail_title_js = $this->input->post("mail_title_js");
        $mail_to_address = $this->input->post("mail_to_address") ?? c_key('webapp_master_email_address');
        if (empty($mail_content_js) === true) {
            $json["msg"] = "Het voorbeeld email bestaat niet!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $this->ajaxck_model->ck_value('email', $mail_to_address);

        $status = $this->sendmail_model->preview($mail_content_js, $mail_title_js, $mail_to_address);
        if ($status === true) {
            $json["msg"] = "Het voorbeeld email is naar uw mailbox verzonden!";
            $json["status"] = "good";
            exit(json_encode($json));
        }

        $json["msg"] = "Het voorbeeld email verzending is mislukt";
        $json["status"] = "error";
        exit(json_encode($json));
    }
}
