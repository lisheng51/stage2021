<?php

class Permission extends Ajax_Controller
{

    public function checkForUser()
    {
        $pathurl = $this->input->post('pathurl');
        if ($this->login_model->user_id() <= 0) {
            $json["type_done"] = "redirect";
            $json["redirect_url"] = login_url('?redirect_url=' . site_url($pathurl));
            $json["msg"] = anchor($json["redirect_url"], 'Uw sessie is verlopen!');
            $json["status"] = "error";
            exit(json_encode($json));
        }
        if ($this->login_model->user_id() !== $this->user_model->secure_id) {
            $userDB = $this->user_model->get_one_by_id($this->login_model->user_id());
            $ids = $userDB["permission_group_ids"];

            $listdbGroup = $this->permission_group_model->get_all_group($ids);
            $stringCompares = array_column($listdbGroup, 'permission_ids');

            $permisson = $permissionArray = $all_permissions = $permissions = [];
            foreach ($stringCompares as $stringCompare) {
                $permissionArray[] = explode(',', $stringCompare);
            }
            $all_permissions = array_reduce($permissionArray, 'array_merge', []);
            $all_permissions = array_unique($all_permissions);

            if (empty($all_permissions) === false) {
                $this->permission_model->sql_where = [$this->module_model->table . ".is_active" => 1];
                $this->permission_model->sql_where_in = [$this->permission_model->table . "." . $this->permission_model->primary_key => $all_permissions];
                $listdb = $this->permission_model->get_all();
                foreach ($listdb as $rsdb) {
                    $path = $rsdb['use_path'] > 0 ? $rsdb['path'] . '/' : null;
                    $permissions[] = $path . $this->access_check_model->backPath . '/' . $rsdb['object'] . '/' . $rsdb['method'] . config_item('url_suffix');
                }
            }

            $permisson = array_unique($permissions);

            $ckPermission = in_array($pathurl, $permisson, true);
            if ($ckPermission === false) {
                $json["msg"] = 'U bent niet gemachtigd om hier te komen!';
                $json["status"] = "error";
                exit(json_encode($json));
            }
        }

        $json["msg"] = 'U bent gemachtigd om hier te komen!';
        $json["status"] = "good";
        exit(json_encode($json));
    }
}
