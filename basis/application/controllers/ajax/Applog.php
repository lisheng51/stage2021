<?php

class Applog extends Ajax_Controller
{

    public function json_data()
    {
        $search = $this->input->post("search");
        $length = $this->input->post("length");
        $start = $this->input->post("start");
        $draw = $this->input->post("draw");
        $order = $this->input->post("order");
        $columns = $this->input->post("columns");

        if (empty($order) === true) {
            $order[0]['column'] = 0;
            $order[0]['dir'] = 'desc';
        }

        $totaldata = $this->applog_model->get_total();

        $arrcolumns = array(
            0 => 'log_id',
            1 => 'date',
            2 => 'user.emailaddress',
            3 => 'description',
            4 => 'path'
        );

        //        if (empty($search) === false) {
        //            $data_or_like = [];
        //            foreach ($arrcolumns as $value) {
        //                $data_or_like[$value] = $search['value'];
        //            }
        //            $this->applog_model->sql_or_like = $data_or_like;
        //        }

        if (empty($columns) === false) {
            $data_where = [];
            $data_like = [];
            foreach ($columns as $value) {
                $valuesearch = $value["search"]['value'];
                $field = $value['data'];
                if (empty($valuesearch) === false) {
                    switch ($field) {
                        case 'emailaddress':
                            $data_like[$this->user_model->table . '.emailaddress'] = $valuesearch;
                            break;
                        case 'date':
                            $obj = date_create(trim($valuesearch));
                            if ($obj !== false) {
                                $data_where[$this->applog_model->table . ".date >="] = date_format($obj, 'Y-m-d H:i:s');
                                $data_where[$this->applog_model->table . ".date <="] = date_format($obj, 'Y-m-d 23:59:59');
                            }
                            break;
                        default:
                            $data_like[$field] = $valuesearch;
                            break;
                    }
                }
            }
            $this->applog_model->sql_where = $data_where;
            $this->applog_model->sql_like = $data_like;
        }

        $limit = empty($length) === true ? 10 : $length;
        $page = empty($start) === true ? 0 : $start;
        $order_by_field = $arrcolumns[$order[0]['column']];
        $order_by_type = $order[0]['dir'];

        $this->applog_model->sql_order_by = array($order_by_field => $order_by_type);

        //debug_as_file($this->applog_model->sql_like);
        $totalfiltered = $this->applog_model->get_total();
        $controller_url = $this->access_check_model->backPath . '/Applog';

        $arr_result = [];
        $listdb = $this->applog_model->get_list($limit, $page);
        foreach ($listdb as $rs) {
            $rs["date"] = date_format(date_create($rs["date"]), 'd-m-Y H:i:s');
            $rs["del_url"] = site_url($controller_url . "/del");
            $rs["view_url"] = site_url($controller_url . "/view/" . $rs["log_id"]);
            $rs["button"] = '<button type="button" class="btn btn-info btn-sm" data-view_link="' . $rs["view_url"] . '" data-toggle="modal" data-target="#Modal_view_detail_app_log" ><i class="fas fa-eye fa-fw"></i></button> ' . '<button type="button" class="btn btn-danger btn-sm delButton" data-search_data ="' . $rs["log_id"] . '" data-del_link="' . $rs["del_url"] . '"><i class="fas fa-times fa-fw"></i></button>';
            $arr_result[] = $rs;
        }

        $json = array(
            "draw" => intval($draw),
            "recordsTotal" => intval($totaldata),
            "recordsFiltered" => intval($totalfiltered),
            "data" => $arr_result
        );

        exit(json_encode($json));
    }
}
