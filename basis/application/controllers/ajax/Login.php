<?php

class Login extends Ajax_Controller
{

    public function fail_check()
    {
        if ($this->login_model->is_double_login() === true) {
            $this->session->sess_destroy();
            $json["status"] = "error";
            $json["redirect_url"] = login_url();
            $json["msg"] = "Het sessie is verlopen";
            exit(json_encode($json));
        }

        $arr_session = $this->session->userdata($this->login_model->session_key);
        if (empty($arr_session) === true) {
            $json["status"] = "error";
            $json["redirect_url"] = login_url();
            $json["msg"] = "Het sessie is verlopen";
            exit(json_encode($json));
        }
        $json["status"] = "good";
        exit(json_encode($json));
    }

    public function sessionmaxlivetime()
    {
        if ($this->login_model->user_id() <= 0) {
            $json["type_done"] = "redirect";
            $json["redirect_url"] = login_url('?redirect_url=' . current_url());
            $json["msg"] = anchor($json["redirect_url"], 'Uw sessie is verlopen!');
            $json["status"] = "error";
            $json["timenow"] = "00:00:00";
            exit(json_encode($json));
        }

        //$val = $_SESSION['__ci_last_regenerate'];
        //$this->telegram_model->default_chat_id = 273053602;
        //$this->telegram_model->log(__METHOD__ . PHP_EOL . date('d-m-Y H:i:s', $val) . PHP_EOL);
        //$valnew = $val + time() - $val;
        $_SESSION['__ci_last_regenerate'] = time();
        //$this->telegram_model->log(__METHOD__ . PHP_EOL . date('d-m-Y H:i:s') . PHP_EOL);


        $json["timenow"] = date('H:i:s', $this->config->item('sess_expiration') - 3600);
        $json["msg"] = "De sessie is verlengd";
        $json["status"] = "good";
        exit(json_encode($json));
    }
}
