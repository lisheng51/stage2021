<?php

class Upload extends Ajax_Controller
{

    public function tinymce()
    {
        $origin_filename = $this->input->post("name") ?? time() . '.png';
        $ext = pathinfo($origin_filename, PATHINFO_EXTENSION);
        $user_id = $this->input->post("user_id") ?? 0;
        $base64 = $this->input->post("base64") ?? "";
        $name = time() . '.' . $ext;
        if (empty($base64) === true) {
            $json["result"] =  "";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $filename = $this->upload_type_model->show_dir("", $user_id) . $name;
        file_put_contents($filename, base64_decode($base64));
        $filenameUrl = str_replace('\\', '/', $filename);
        $json["result"] =  base_url($filenameUrl);
        $json["status"] = "good";
        exit(json_encode($json));
    }
}
