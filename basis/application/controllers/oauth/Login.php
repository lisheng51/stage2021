<?php

class Login extends MX_Controller
{

    public function index()
    {
        $hasGetToken = $this->input->get('token') ?? null;
        if ($hasGetToken !== null) {
            exit(rawurlencode($hasGetToken));
        }

        $code = $this->input->get('code') ?? null;
        $this->ajaxck_model->ck_value('code', $code);
        $apiId = $this->encryption->decrypt(rawurldecode($code));

        if (empty($apiId) === true) {
            $json["msg"] = "Code is niet juist";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        if ($this->login_model->user_id() > 0) {
            $this->load->database();
            $arr_user = $this->login_model->getByField($this->login_model->primary_key, $this->login_model->user_id());
            $data["expires"] = $this->api_model->fetchTokenMin($apiId);
            $data[$this->user_model->primary_key] = $this->login_model->user_id();
            $data[$this->api_model->primary_key] = $apiId;
            $data["ip_address"] = $arr_user["ip_address"];
            $data["browser"] = $arr_user["browser"];
            $data["platform"] = $arr_user["platform"];
            $data["password_date"] = $arr_user['password_date'];

            $valuestring = json_encode($data);
            $tokenString = $this->encryption->encrypt($valuestring);
            $token = rawurlencode($tokenString);
            $getRedirectUrl = $this->input->get('redirectUrl') ?? current_url();
            $redirect_url = $getRedirectUrl . '?token=' . $token;
            redirect($redirect_url);
        }

        $this->language_model->set_language();
        $lang = $this->language_model->get_language();
        $this->module_model->language($lang);
        if ($this->input->post()) {
            $this->indexAction($apiId);
        }

        $data["title"] = "Oauth login";
        $this->load->view("oauth/index", $data);
    }

    private function indexAction(int $apiId = 0)
    {
        $username = $this->input->post('username') ?? "";
        $password = $this->input->post('password') ?? "";

        $json = [];
        $this->ajaxck_model->ck_value('Gebruikersnaam', $username);
        $this->ajaxck_model->ck_value('Wachtwoord', $password);

        $this->load->database();
        $arr_user = $this->login_model->get_one($username, $password);
        if (empty($arr_user) === true) {
            $json["msg"] = "Verkeerde gebruikersnaam of wachtwoord!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $onecode = $this->input->post('webapp_one_code') ?? "";
        if ($this->login_model->check2FA($arr_user, $onecode) === false) {
            $json["msg"] = "2FA code is niet juist!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        if ($this->login_model->check_data($username, $password) === true) {
            $this->login_model->update_data($arr_user['user_id']);
            $this->api_log_model->add(__METHOD__, $apiId);
            $data["expires"] = $this->api_model->fetchTokenMin($apiId);
            $data[$this->user_model->primary_key] = $arr_user[$this->user_model->primary_key];
            $data[$this->api_model->primary_key] = $apiId;
            $data["ip_address"] = $this->input->ip_address();
            $data["browser"] = $this->agent->browser();
            $data["platform"] = $this->agent->platform();
            $data["password_date"] = $arr_user['password_date'];

            $valuestring = json_encode($data);
            $tokenString = $this->encryption->encrypt($valuestring);
            $token = rawurlencode($tokenString);
            $getRedirectUrl = $this->input->get('redirectUrl') ?? current_url();
            $redirectUrl = $getRedirectUrl . '?token=' . $token;
            $json["msg"] = "Even geduld aub... U wordt automatisch doorgeschakeld";
            $json["status"] = "good";
            $json["type_done"] = "redirect";
            $json["redirect_url"] = $redirectUrl;
            exit(json_encode($json));
        }

        $json["msg"] = "Verkeerde gebruikersnaam of wachtwoord!";
        $json["status"] = "error";
        exit(json_encode($json));
    }
}
