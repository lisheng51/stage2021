<?php

class Aes extends API_Controller
{

    private $defaultTypeFun = 'usingSystem';

    private function usingGlobal(string $valuestring = "", string $type = "encrypt"): string
    {
        if ($type === 'encrypt') {
            return $this->aes_model->encrypt($valuestring) ?? "";
        }
        return $this->aes_model->decrypt($valuestring) ?? "";
    }

    private function usingSystem(string $valuestring = "", string $type = "encrypt"): string
    {
        if ($type === 'encrypt') {
            return $this->encryption->encrypt($valuestring);
        }
        return $this->encryption->decrypt($valuestring);
    }

    public function encrypt()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $data = $this->input->post() ?? null;
        $data[$this->api_model->primary_key] = $this->apiId;
        $typeFun = $this->input->post('typeFun') ?? $this->defaultTypeFun;

        if (empty($typeFun) === true) {
            $this->api_model->outNOK(99, "Geen typeFun gevonden!", $apiLogId);
        }

        $valuestring = json_encode($data);
        if (empty($valuestring) === true) {
            $this->api_model->outNOK(99, "Geen data gevonden!", $apiLogId);
        }

        $tokenString = call_user_func_array([$this, $typeFun], [$valuestring, __FUNCTION__]);
        if (empty($tokenString) === true) {
            $this->api_model->outNOK(99, "Geen data gevonden!", $apiLogId);
        }

        $token = rawurlencode($tokenString);
        $this->api_model->outOK($token, $apiLogId);
    }

    public function decrypt()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $code = $this->input->post('code') ?? null;
        $typeFun = $this->input->post('typeFun') ?? $this->defaultTypeFun;

        if (empty($typeFun) === true) {
            $this->api_model->outNOK(99, "Geen typeFun gevonden!", $apiLogId);
        }

        if (empty($code) === true) {
            $this->api_model->outNOK(99, "Geen code gevonden!", $apiLogId);
        }
        $valuestring = rawurldecode($code);
        $json = call_user_func_array([$this, $typeFun], [$valuestring, __FUNCTION__]);
        if (empty($json) === true) {
            $this->api_model->outNOK(99, "Geen data gevonden!", $apiLogId);
        }
        $this->api_model->outOK($json, $apiLogId);
    }

    public function info()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $json["password"] = $this->aes_model->password;
        $json["key"] = $this->aes_model->key;
        $this->api_model->outOK($json, $apiLogId);
    }

    public function infoToken()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $sec = $this->input->post('sec') ?? 60;
        $json["password"] = random_string();
        $json["key"] = random_string('sha1');
        $json["datetime"] = date('Y-m-d H:i:s', time() + $sec);

        $valuestring = json_encode($json);
        $encrypted_string = $this->encryption->encrypt($valuestring);
        $hashkey = rawurlencode($encrypted_string);
        $json["token"] = $hashkey;

        $this->api_model->outOK($json, $apiLogId);
    }
}
