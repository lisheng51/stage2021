<?php

class Token extends API_Controller
{

    protected $onlyMethodPost = true;
    public function refresh()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $dataUser = $this->api_model->checkFetchByJWT(false);
        $data["expires"] = $this->api_model->fetchTokenMin($this->apiId);
        $data[$this->user_model->primary_key] = $dataUser[$this->user_model->primary_key];
        $data[$this->api_model->primary_key] = $this->apiId;
        $data["ip_address"] = $dataUser["ip_address"];
        $data["browser"] = $dataUser["browser"];
        $data["platform"] = $dataUser["platform"];
        $data["password_date"] = $dataUser['password_date'];

        $valuestring = json_encode($data);
        $tokenString = $this->encryption->encrypt($valuestring);
        $token = rawurlencode($tokenString);
        $this->api_model->out($token, 100, $apiLogId);
    }

    public function access()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $postData = empty($this->arrPost) === true ? $this->input->post() : $this->arrPost;

        $username = $postData["username"] ?? null;
        $password = $postData["password"] ?? null;

        if ($username === null || empty($username) === true) {
            $this->api_model->out('Geen gebruikersnaam gevonden', 98);
        }

        if ($password === null || empty($password) === true) {
            $this->api_model->out('Geen wachtwoord gevonden', 98);
        }

        $arr_user = $this->login_model->get_one($username, $password);
        if (empty($arr_user) === true) {
            $this->api_model->out('Geen account gevonden', 94);
        }

        $this->login_model->update_data($arr_user['user_id']);
        $data["expires"] = $this->api_model->fetchTokenMin($this->apiId);
        $data[$this->user_model->primary_key] = $arr_user[$this->user_model->primary_key];
        $data[$this->api_model->primary_key] = $this->apiId;
        $data["ip_address"] = $this->input->ip_address();
        $data["browser"] = $this->agent->browser();
        $data["platform"] = $this->agent->platform();
        $data["password_date"] = $arr_user['password_date'];

        $valuestring = json_encode($data);
        $tokenString = $this->encryption->encrypt($valuestring);
        $token = rawurlencode($tokenString);
        $this->api_model->out($token, 100, $apiLogId);
    }
}
