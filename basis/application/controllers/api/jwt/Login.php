<?php

class Login extends API_Controller
{

    public function windows()
    {
        $this->addLog(__METHOD__);
        $dataUser = $this->api_model->checkFetchByJWT();
        $sess_array[$this->user_model->primary_key] = $dataUser[$this->user_model->primary_key];
        $sess_array['redirect_url'] = $dataUser["redirect_url"];
        $this->login_model->login_user_data = $sess_array;
        $this->login_model->add_user_session();
        redirect($this->access_check_model->redirect_url());
    }

    public function index()
    {
        $this->addLog(__METHOD__);
        $dataUser = $this->api_model->checkFetchByJWT();
        $base_url = $this->input->post('base_url');
        if (empty($base_url) === true) {
            $this->api_model->outNOK(98, 'base_url is niet gevonden');
        }
        $sec = $this->input->post('sec') ?? 10;
        $data[$this->user_model->primary_key] = $dataUser[$this->user_model->primary_key];
        $data["datetime"] = date('Y-m-d H:i:s', time() + $sec);
        $valuestring = json_encode($data);
        $encrypted_string = $this->encryption->encrypt($valuestring);
        $hashkey = rawurlencode($encrypted_string);
        $message = $base_url . ENVIRONMENT_ACCESS_URL . "/Login/directEasy?key=$hashkey";
        $this->api_model->outOK($message);
    }

    public function password()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $postData = empty($this->arrPost) === true ? $this->input->post() : $this->arrPost;

        $username = $postData["username"] ?? null;
        if ($username === null || empty($username) === true) {
            $this->api_model->out('Geen gebruikersnaam gevonden', 98);
        }

        $access_code = $postData["access_code"] ?? null;
        if ($access_code === null || empty($access_code) === true) {
            $this->api_model->out('Geen code gevonden', 98);
        }

        $arr_rs = $this->login_model->get_one_by_username_email($username);

        if (empty($arr_rs) === true || empty($arr_rs["access_code"]) === true) {
            $this->api_model->out('Er is geen gebruiker gevonden', 94);
        }

        if (date("Y-m-d H:i:s") > $arr_rs["access_code_date"]) {
            $this->api_model->out('Code is verlopen', 95);
        }

        if ($access_code !== $arr_rs["access_code"]) {
            $this->api_model->out('Code is niet juist', 97);
        }

        $password1 = $postData["password1"];
        $password2 = $postData["password2"];

        if (empty($password1) === true || empty($password2) === true) {
            $this->api_model->out('Geen wachtwoord gevonden', 98);
        }

        if ($password1 !== $password2) {
            $this->api_model->out('Wachtwoorden zijn niet gelijk', 98);
        }

        $uid = $arr_rs[$this->user_model->primary_key];
        $data_login["password"] = password_hash($password1, PASSWORD_DEFAULT);
        $data_login["password_date"] = date('Y-m-d H:i:s');
        $data_login["password_reset_date"] = null;
        $this->login_model->edit($uid, $data_login);

        $json["msg"] = "Wachtwoord is bijgewerkt";
        if ($arr_rs["is_active"] == 0) {
            $data["is_active"] = 1;
            $this->user_model->edit($uid, $data);
            $json["msg"] = "Uw account is nu geactiveerd";
        }

        $json["status"] = "good";
        $this->api_model->out($json, 100, $apiLogId);
    }


    public function register()
    {

        $apiLogId = $this->addLog(__METHOD__);
        $postData = empty($this->arrPost) === true ? $this->input->post() : $this->arrPost;

        $username = $postData["username"] ?? null;
        if ($username === null || empty($username) === true) {
            $this->api_model->out('Geen gebruikersnaam gevonden', 98);
        }

        $arr_user = $this->login_model->get_one_by_username_email($username);
        if (empty($arr_user) === false && $arr_user["is_del"] == 1) {
            $this->user_model->edit($arr_user[$this->user_model->primary_key], ["is_del" => 0]);
        }

        if (empty($arr_user) === false && $arr_user["is_active"] > 0) {
            $this->api_model->out('Deze gebruikersnaam bestaat al', 98);
        }

        $access_code = $postData["access_code"] ?? null;
        if (empty($access_code) === true) {
            $this->api_model->outNOK(99, 'Code is niet gevonden');
        }

        $code = $postData["access_code_cookie"] ?? null;
        if (empty($code) === true) {
            $this->api_model->outNOK(99, 'Code is niet gevonden');
        }
        $key = rawurldecode($code);
        $arrTokenValue = json_decode($this->encryption->decrypt($key), true);
        if ($arrTokenValue['access_code'] != $access_code) {
            $this->api_model->outNOK(97, "Code kan niet verifiëren!");
        }

        $token_expires = $arrTokenValue['datetime'] ?? null;
        if (date("Y-m-d H:i:s") > $token_expires) {
            $this->api_model->outNOK(95, "Code is verlopen!");
        }

        $password1 = $postData["password1"];
        $password2 = $postData["password2"];

        if (empty($password1) === true || empty($password2) === true) {
            $this->api_model->out('Geen wachtwoord gevonden', 98);
        }

        if ($password1 !== $password2) {
            $this->api_model->out('Wachtwoorden zijn niet gelijk', 98);
        }

        $data["emailaddress"] = strtolower($username);
        $data["permission_group_ids"] = 2;

        $uid = $this->user_model->add($data);
        if ($uid > 0) {
            $data_login["user_id"] = $uid;
            $data_login["username"] = $data["emailaddress"];
            $data_login["redirect_url"] = '';
            $data_login["password"] = password_hash($password1, PASSWORD_DEFAULT);
            $data_login["password_date"] = date('Y-m-d H:i:s');
            $data_login["password_reset_date"] = null;
            $this->login_model->add($data_login);
            $data_newpic["createdby"] = $uid;
            $this->user_model->edit($uid, $data_newpic);
            $this->upload_type_model->make_dir($uid);
            $json["msg"] = "Account is aangemaakt";
            $json["status"] = "good";
            $json["id"] = $uid;
            $this->api_model->out($json, 100, $apiLogId);
        }

        $this->api_model->out('Account is niet aangemaakt', 93);
    }
}
