<?php

class Message extends API_Controller
{

    public function index()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $dataUser = $this->api_model->checkFetchByJWT();
        $start = $this->input->get("start");
        $end = $this->input->get("end");
        if (empty($start) === false) {
            $data_where["date >="] = date_format(date_create(trim($start)), 'Y-m-d H:i:s');
        }

        if (empty($end) === false) {
            $data_where["date <="] = date_format(date_create(trim($end)), 'Y-m-d 23:59:59');
        }

        $isopen = $this->input->get("is_open");
        $data_where["to_user_id"] = $dataUser[$this->user_model->primary_key];
        $data_where["is_del"] = 0;
        if (empty($isopen) === false) {
            if ($isopen === 'yes') {
                $data_where["is_open"] = 1;
            }

            if ($isopen === 'no') {
                $data_where["is_open"] = 0;
            }
        }

        $this->message_model->sql_where = $data_where;
        $listdb = $this->message_model->get_all();
        $arr_result = [];
        if (empty($listdb) === false) {
            foreach ($listdb as $rs) {
                $rs["from"] = $this->user_model->fetch_name($rs["from_user_id"]);
                unset($rs['from_user_id'], $rs['to_user_id'], $rs['is_del']);
                $arr_result[] = $rs;
            }
        }
        $this->api_model->outOK($arr_result, $apiLogId);
    }

    public function open()
    {
        $this->addLog(__METHOD__);
        $dataUser = $this->api_model->checkFetchByJWT();
        $messageId = $this->input->post("message_id") ?? 0;
        $rsdb = $this->message_model->get_one_by_id(intval($messageId));
        if (empty($rsdb) === true || $messageId <= 0) {
            $this->api_model->outNOK();
        }

        if ($dataUser[$this->user_model->primary_key] == $rsdb["to_user_id"]) {
            $this->message_model->edit($messageId, array("is_open" => 1));
            $this->api_model->outOK();
        }

        $this->api_model->outNOK();
    }

    public function del()
    {
        $this->addLog(__METHOD__);
        $dataUser = $this->api_model->checkFetchByJWT();
        $messageId = $this->input->post("message_id") ?? 0;
        $rsdb = $this->message_model->get_one_by_id(intval($messageId));
        if (empty($rsdb) === true || $messageId <= 0) {
            $this->api_model->outNOK();
        }

        if ($dataUser[$this->user_model->primary_key] == $rsdb["to_user_id"]) {
            $this->message_model->edit($messageId, array("is_del" => 1));
            $this->api_model->outOK();
        }

        $this->api_model->outNOK();
    }
}
