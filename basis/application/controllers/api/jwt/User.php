<?php

class User extends API_Controller
{

    public function index()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $dataUser = $this->api_model->checkFetchByJWT();
        $this->api_model->outOK($dataUser, $apiLogId);
    }

    public function permission()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $userDB = $this->api_model->checkFetchByJWT();
        $ids = $userDB["permission_group_ids"];
        $permisson = [];
        if (empty($ids) === false) {
            $listdbGroup = $this->permission_group_model->get_all_group($ids);
            $stringCompares = array_column($listdbGroup, 'permission_ids');

            $permisson = $permissionArray = $all_permissions = $permissions = [];
            foreach ($stringCompares as $stringCompare) {
                $permissionArray[] = explode(',', $stringCompare);
            }
            $all_permissions = array_reduce($permissionArray, 'array_merge', []);
            $all_permissions = array_unique($all_permissions);

            if (empty($all_permissions) === false) {
                $this->permission_model->sql_where = [$this->module_model->table . ".is_active" => 1];
                $this->permission_model->sql_where_in = [$this->permission_model->table . "." . $this->permission_model->primary_key => $all_permissions];
                $listdb = $this->permission_model->get_all();

                foreach ($listdb as $rsdb) {
                    $path = $rsdb['use_path'] > 0 ? $rsdb['path'] : null;
                    $permissions[] = $path . '.' . $rsdb['object'] . '.' . $rsdb['method'];
                }
            }
            $permisson = array_unique($permissions);
        }
        $this->api_model->outOK($permisson, $apiLogId);
    }
}
