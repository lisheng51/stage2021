<?php

class Log extends Cron_Controller
{

    public function delAll()
    {
        $this->delAppLog();
        $this->delSendmail();
        //$this->telegram_model->log(__METHOD__);
    }

    private function delAppLog()
    {
        $date = date("Y-m-d H:i:s", strtotime("-3 month"));
        $data_where[$this->applog_model->table . ".date <="] = $date;
        $this->applog_model->sql_where = $data_where;
        $total = $this->applog_model->get_total();
        if ($total > 0) {
            $this->db->from($this->applog_model->table);
            foreach ($data_where as $field => $value) {
                $this->db->where($field, $value);
            }
            $this->db->delete();
        }
    }

    private function delSendmail()
    {
        $date = date("Y-m-d H:i:s", strtotime("-3 month"));
        $data_where[$this->sendmail_model->table . ".created_at <="] = $date;
        $this->sendmail_model->sql_where = $data_where;
        $total = $this->sendmail_model->get_total();
        if ($total > 0) {
            $this->db->from($this->sendmail_model->table);
            foreach ($data_where as $field => $value) {
                $this->db->where($field, $value);
            }
            $this->db->delete();
        }
    }
}
