<?php

class Mail extends Cron_Controller
{

    public function send()
    {
        $this->benchmark->mark('code_start');

        $ck_file_exist_path = $this->config->item('cache_path') . 'cron_mail_send.txt';
        if (file_exists($ck_file_exist_path) === false) {
            write_file($ck_file_exist_path, 'klaar');
        }

        $status = file_get_contents($ck_file_exist_path);
        if ($status !== 'klaar') {
            $this->telegram_model->log(__METHOD__ . PHP_EOL . 'Overlapping uitvoeren');
            exit;
        }

        write_file($ck_file_exist_path, 'bezig');

        $total_send = 0;
        $total_error = 0;

        $this->config_model->set_session();

        $arrSys = $this->sendmail_model->send_action_bulk(5);
        $message = 'Sys ok: ' . $arrSys["total_send_ok"] . PHP_EOL;
        $message .= 'Sys error: ' . $arrSys["total_send_error"] . PHP_EOL;
        $message .= 'Sys batch: ' . $arrSys["total_batch"] . PHP_EOL;
        $total_send += $arrSys["total_send_ok"] + $arrSys["total_send_error"];
        $total_error += $arrSys["total_send_error"];

        write_file($ck_file_exist_path, 'klaar');

        $this->benchmark->mark('code_end');
        if ($total_error > 0) {
            $message .= 'Total Execution Time:' . $this->benchmark->elapsed_time('code_start', 'code_end');
            $this->telegram_model->log(__METHOD__ . PHP_EOL . $message);
        }
    }
}
