<?php

class Database extends Cron_Controller
{

    public function optimize()
    {
        $this->load->dbutil();
        $this->dbutil->optimize_database();
        //$this->telegram_model->log(__METHOD__);
    }

    public function backup(string $datbase = "")
    {
        if (empty($datbase) === true) {
            $datbase = $this->db->database;
        }
        $this->db->db_select($datbase);
        $this->load->dbutil($this->db);

        if ($this->dbutil->database_exists($datbase) === true) {
            $path = FCPATH . 'database' . DIRECTORY_SEPARATOR . date('Ymd') . DIRECTORY_SEPARATOR;
            if (is_dir($path) === false) {
                mkdir($path, 0755, true);
            }
            $ck_file_exist_path = $path . $datbase . '.gz';
            set_time_limit(0);
            ini_set('memory_limit', '-1');
            $backup = $this->dbutil->backup();
            write_file($ck_file_exist_path, $backup);
            //$this->telegram_model->log(__METHOD__ . ' => ' . $datbase);
        }
    }

    public function restore(string $datbase = "", string $datefolder = "")
    {
        if (empty($datbase) === true) {
            $datbase = $this->db->database;
        }

        if (empty($datefolder) === true) {
            $datefolder = date('Ymd');
        }
        $this->db->db_select($datbase);
        $this->load->dbutil($this->db);

        $path = FCPATH . 'database' . DIRECTORY_SEPARATOR . $datefolder . DIRECTORY_SEPARATOR;
        $ck_file_exist_path = $path . $datbase . '.gz';
        if (file_exists($ck_file_exist_path) === true && $this->dbutil->database_exists($datbase) === true) {
            $templine = '';
            $lines = gzfile($ck_file_exist_path);
            set_time_limit(0);
            ini_set('memory_limit', '-1');
            foreach ($lines as $line) {
                if (substr($line, 0, 2) == '--' || $line == '') {
                    continue;
                }
                $templine .= $line;
                if (substr(trim($line), -1, 1) == ';') {
                    $this->db->simple_query($templine);
                    $templine = '';
                }
            }
            //$this->telegram_model->log(__METHOD__ . ' => ' . $datbase);
        }
    }
}
