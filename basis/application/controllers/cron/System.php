<?php

class System extends Cron_Controller
{

    protected $site_url = 'https://live.com';
    public function resetSession()
    {
        $path = $this->config->item('sess_save_path');
        delete_files($path);
        //$this->telegram_model->log(__METHOD__);
    }

    public function index()
    {
        exit(ENVIRONMENT_UPLOAD_PATH . '|' . ENVIRONMENT_DATABASE);
    }
}
