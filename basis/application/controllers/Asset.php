<?php

class Asset extends MX_Controller
{

    private $path = 'assets';

    public function index(string $name = '')
    {
        $output = "";
        $site_url = site_url('/');
        $module_url = site_url($name . '/');
        $session_id = session_id();
        $csrf_token_name = $this->security->get_csrf_token_name();
        $csrf_hash =  $this->security->get_csrf_hash();

        $message_ajax_fail = ENVIRONMENT === 'development' ? '' : 'Er is momenteel storing, probeert u het later nog eens!';
        $output .= "var site_url = '$site_url'; var message_ajax_fail = '$message_ajax_fail'; var module_url = '$module_url';var session_id = '$session_id'; var csrf_hash = '$csrf_hash';var csrf_token_name = '$csrf_token_name'";
        $this->output->set_content_type('text/javascript')->set_output($output);
    }


    public function load()
    {
        $part = explode("/", str_replace("asset/load/", "", uri_string()), 2);
        if (isset($part[0]) === false) {
            exit;
        }

        $hasGetPath = $this->input->get('path') ?? "";
        if (empty($hasGetPath) === false) {
            $this->path = $this->input->get('path');
        }

        $module = $part[0];
        $filenamePre = APPPATH . $this->path . DIRECTORY_SEPARATOR;
        if ($module !== $this->path && isset($part[1]) === true) {
            $filenamePre = APPPATH . 'modules' . DIRECTORY_SEPARATOR . $part[0] . DIRECTORY_SEPARATOR . $this->path . DIRECTORY_SEPARATOR;
        }
        $filename = $filenamePre . $part[1];

        if (file_exists($filename) === false) {
            exit;
        }

        $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
        if ($ext === 'js') {
            $output = file_get_contents($filename);
            $this->output->set_content_type('text/javascript')->set_output($output);
        }

        if ($ext === 'css' || $ext === 'less') {
            $output = file_get_contents($filename);
            $this->output->set_content_type('text/css')->set_output($output);
        }

        if ($ext === 'png' || $ext === 'gif' || $ext === 'jpeg' || $ext === 'jpg') {
            $mime = mime_content_type($filename);
            header('Content-Length: ' . filesize($filename));
            header("Content-Type: $mime");
            readfile($filename);
            exit;
        }

        if ($ext === 'mp3' || $ext === 'ogg' || $ext === 'wav') {
            $mime = mime_content_type($filename);
            header('Content-Length: ' . filesize($filename));
            header("Content-Type: $mime");
            readfile($filename);
            exit;
        }

        if ($ext === 'woff' || $ext === 'ttf' || $ext === 'woff2' || $ext === 'svg' || $ext === 'otf') {
            $mime = mime_content_type($filename);
            header('Content-Length: ' . filesize($filename));
            header("Content-Type: $mime");
            readfile($filename);
            exit;
        }
    }
}
