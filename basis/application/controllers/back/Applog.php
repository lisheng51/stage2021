<?php

class Applog extends Back_Controller
{

    protected static $title_module = 'Logging - systeem';

    public function index_2()
    {
        $data["title"] = self::$title_module . " overzicht";
        $this->view_layout("index_2", $data);
    }

    public function widget_last()
    {
        $limit = $this->input->get("limit") ?? 20;
        $data["listdb"] = $this->applog_model->get_list($limit);
        $data["title"] = 'Laatste logs';
        $content = $this->view_layout_return("widget/last", $data);
        exit($content);
    }

    public function index()
    {
        $reportrange = $this->input->post("reportrange");
        if (empty($reportrange) === false) {
            $arr_range = explode("t/m", $reportrange);
            $data_where[] = [$this->applog_model->table . ".date >=" => date_format(date_create(trim($arr_range[0])), 'Y-m-d 00:00:00')];
            $data_where[] = [$this->applog_model->table . ".date <=" => date_format(date_create(trim($arr_range[1])), 'Y-m-d 23:59:59')];
        }

        $data_where[] = setFieldAndOperator('display_info', $this->user_model->table . '.display_info');
        $data_where[] = setFieldAndOperator('description', $this->applog_model->table . '.description');
        $data_where[] = setFieldAndOperator('path', $this->applog_model->table . '.path');

        $this->applog_model->setSqlWhere($data_where);
        $this->applog_model->sql_order_by = setFieldOrderBy();

        $total = $this->applog_model->get_total();
        $data["listdb"] = $this->getData();
        $data["total"] = $total;
        $data["pagination"] = $this->global_model->show_page($total);
        $data["ajax_batch_del_url"] = site_url($this->controller_url . "/batch_del");
        $data["result"] = $this->view_layout_return("ajax_list", $data);
        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }

        $data["title"] = self::$title_module . " overzicht";
        $this->view_layout("index", $data);
    }

    private function getData()
    {
        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;

        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;

        $arr_result = [];
        $listdb = $this->applog_model->get_list($limit, $page);
        foreach ($listdb as $rs) {
            $rs["del_url"] = site_url($this->controller_url . "/del");
            $rs["view_url"] = site_url($this->controller_url . "/view/" . $rs["log_id"]);
            $arr_result[] = $rs;
        }
        return $arr_result;
    }

    public function view($id = 0)
    {
        $rsdb = $this->applog_model->get_one_by_id(intval($id));
        if (empty($rsdb) === true) {
            $json["msg"] = "Geen details gevonden!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $rsdb["date"] = date_format(date_create($rsdb["date"]), 'd-m-Y H:i:s');

        $this->user_model->sql_where = [$this->user_model->primary_key => $rsdb["user_id"]];
        $rsdb_user = $this->user_model->get_one();

        $arr_result = array_merge($rsdb, $rsdb_user);

        exit(json_encode($arr_result));
    }

    public function batch_del()
    {
        $arr_ids = $this->input->post("log_id");
        if (empty($arr_ids) === true) {
            $json["msg"] = self::$title_module . " kan niet worden verwijderd!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        foreach ($arr_ids as $id) {
            $this->applog_model->del($id);
        }
        $json["type_done"] = "redirect";
        $json["redirect_url"] = site_url($this->controller_url);
        $json["msg"] = self::$title_module . " is verwijderd!";
        $json["status"] = "good";
        exit(json_encode($json));
    }

    public function del()
    {
        $id = $this->input->post("del_id");
        $rsdb = $this->applog_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            $json["msg"] = self::$title_module . " kan niet worden verwijderd!";
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $this->applog_model->del($id);
        $json["msg"] = self::$title_module . " is verwijderd!";
        $json["status"] = "good";
        exit(json_encode($json));
    }
}
