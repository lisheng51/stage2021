<?php

class Api extends Back_Controller
{
    use DataFormat;
    protected static $title_module = 'API';

    public function add()
    {
        if ($this->input->post()) {
            $this->addAction();
        }
        $data["title"] = self::$title_module . " toevoegen";
        $data["rsdb"] = null;
        $data["event_result_box"] = $this->global_model->event_result_box($data["title"]);
        $this->view_layout("edit", $data);
    }

    public function del()
    {
        $id = $this->input->post("del_id");
        if (empty($id) === true) {
            redirect($this->controller_url);
        }
        $rsdb = $this->api_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            $json["msg"] = self::$title_module . " kan niet worden verwijderd!";
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $this->api_model->del($id);
        $json["msg"] = self::$title_module . " is verwijderd!";
        $json["status"] = "good";
        $json["type_done"] = "redirect";
        $json["redirect_url"] = site_url($this->controller_url);
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $this->editAction();
        }

        $rsdb = $this->api_model->get_one_by_id(intval($id));
        if (empty($rsdb) === true) {
            redirect($this->controller_url);
        }

        $data["rsdb"] = $rsdb;
        $data["title"] = self::$title_module . " wijzigen";
        $data["event_result_box"] = $this->global_model->event_result_box($data["title"]);
        $this->view_layout("edit", $data);
    }

    public function download(string $type = "json")
    {
        $data_where[] = [$this->api_model->table . '.' . $this->api_model->_field_is_deleted => 0];
        $this->api_model->setSqlWhere($data_where);
        $json = $jsonData = [];
        $listdb = $this->api_model->get_all();
        foreach ($listdb as $rs) {
            $jsonData["apiid"] = $rs[$this->api_model->primary_key];
            $jsonData["apikey"] = $rs["secret"];
            $jsonData["name"] =  $rs["name"];
            $json[] = $jsonData;
        }

        if ($type === 'json') {
            force_download('api.json', $this->asJson($json));
        }
        force_download('api.xml', $this->asXml($json));
    }

    public function index()
    {
        $data_where[] = [$this->api_model->table . '.' . $this->api_model->_field_is_deleted => 0];
        $data_where[] = setFieldAndOperator('secret', $this->api_model->table . '.secret');
        $data_where[] = setFieldAndOperator($this->permission_group_model->primary_key, $this->api_model->table . '.permission_group_ids');
        $data_where[] = setFieldAndOperator($this->api_model->primary_key, $this->api_model->table . '.' . $this->api_model->primary_key);
        $data_where[] = setFieldAndOperator('name', $this->api_model->table . '.name');
        $this->api_model->setSqlWhere($data_where);
        $this->api_model->sql_order_by = setFieldOrderBy();
        $total = $this->api_model->get_total();
        $data["listdb"] = $this->getData();
        $data["total"] = $total;
        $data["pagination"] = $this->global_model->show_page($total);
        $data["result"] = $this->view_layout_return("ajax_list", $data);
        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }

        $data["title"] = self::$title_module . ' overzicht';
        $data["addButton"] = addButton($this->controller_name . '.add', $this->controller_url . '/add');
        $dataBox["body"] = '<a href=' . site_url($this->controller_url . '/download') . ' class="btn btn-info btn-sm ">JSON</a> | <a href=' . site_url($this->controller_url . '/download/xml') . ' class="btn btn-info btn-sm ">XML</a>';
        $data["event_result_box"] = $this->global_model->event_result_box($dataBox["body"], 'Data downloaden');
        $this->view_layout("index", $data);
    }


    public function editInline()
    {
        $id = $this->input->post("editid") ?? 0;
        $rsdb = $this->api_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            $json["msg"] = self::$title_module . ' is niet gevonden!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $field = $this->input->post("field") ?? "";
        $fieldvalue = $this->input->post("fieldvalue") ?? "";

        if (empty($field) === true) {
            $json["msg"] = self::$title_module . ' is niet bijgewerkt!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $data[$field] = $fieldvalue;

        if ($field === 'name') {
            $existdb = $this->api_model->get_one_by_field($field, $data[$field]);
            if (empty($existdb) === false && $id != $existdb[$this->api_model->primary_key]) {
                $json["msg"] = self::$title_module . ' bestaat al!';
                $json["status"] = "error";
                exit(json_encode($json));
            }
        }

        $this->api_model->edit($id, $data);
        $json["msg"] = self::$title_module . ' is bijgewerkt';
        $json["status"] = "good";
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }


    private function getData()
    {
        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;

        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;

        $arr_result = [];
        $listdb = $this->api_model->get_list($limit, $page);
        foreach ($listdb as $rs) {
            $rs['permissionGroupDb'] = $this->permission_group_model->get_all_group($rs['permission_group_ids'] ?? "");
            $rs["name"] = editInlineButton($this->controller_name . '.editInline', $rs[$this->api_model->primary_key], 'name', $rs['name']);
            $rs["editButton"] = editButton($this->controller_name . '.edit', $this->controller_url . "/edit/" . $rs[$this->api_model->primary_key]);
            $arr_result[] = $rs;
        }

        return $arr_result;
    }

    private function addAction()
    {
        $data = $this->getPostdata();
        $check_double = $this->api_model->get_one_by_name($data["name"]);
        if (empty($check_double) === false) {
            $json["msg"] = self::$title_module . " bestaat al!";
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $insert_id = $this->api_model->add($data);
        if ($insert_id > 0) {
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            $json["msg"] = self::$title_module . " is aangemaakt!";
            $json["status"] = "good";
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    private function editAction()
    {
        $data = $this->getPostdata();
        $id = $this->input->post($this->api_model->primary_key);
        $existdb = $this->api_model->get_one_by_name($data["name"]);
        if (empty($existdb) === false && $id != $existdb[$this->api_model->primary_key]) {
            $json["msg"] = self::$title_module . ' bestaat al!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $rsdb = $this->api_model->get_one_by_id($id);
        if (empty($rsdb) === false) {
            $this->api_model->edit($id, $data);
            $json["msg"] = self::$title_module . " is bijgewerkt!";
            $json["status"] = "good";
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    private function getPostdata()
    {
        if ($this->input->post("del_id")) {
            $this->del();
        }
        $data = $this->api_model->getPostdata();
        return $data;
    }

    private function myprint_r($my_array)
    {
        $html = "";
        if (is_array($my_array)) {
            foreach ($my_array as $k => $v) {
                $html .= $k . ': ' . $v . "<hr>";
            }
        }
        return $html;
    }

    public function log()
    {
        $data_where[] = setFieldAndOperator($this->api_model->primary_key, $this->api_log_model->table . '.' . $this->api_model->primary_key);
        $data_where[] = setFieldAndOperator('msg', $this->api_log_model->table . '.msg');
        $data_where[] = setFieldAndOperator('path', $this->api_log_model->table . '.path');
        $data_where[] = setFieldAndOperator('ip_address', $this->api_log_model->table . '.ip_address');
        $data_where[] = setFieldAndOperator('browser', $this->api_log_model->table . '.browser');
        $data_where[] = setFieldAndOperator('platform', $this->api_log_model->table . '.platform');
        $data_where[] = setFieldAndOperator('post_value', $this->api_log_model->table . '.post_value');
        $data_where[] = setFieldAndOperator('get_value', $this->api_log_model->table . '.get_value');
        $data_where[] = setFieldAndOperator('header_value', $this->api_log_model->table . '.header_value');
        $data_where[] = setFieldAndOperator('out_value', $this->api_log_model->table . '.out_value');

        $reportrange = $this->input->post("reportrange");
        if (empty($reportrange) === false) {
            $arr_range = explode("t/m", $reportrange);
            $data_where[] = [$this->api_log_model->table . ".datetime >=" => date_format(date_create(trim($arr_range[0])), 'Y-m-d 00:00:00')];
            $data_where[] = [$this->api_log_model->table . ".datetime <=" => date_format(date_create(trim($arr_range[1])), 'Y-m-d 23:59:59')];
        }

        $reportrange_end = $this->input->post("reportrange_end");
        $reportrange_start = $this->input->post("reportrange_start");
        if (empty($reportrange_start) === false && empty($reportrange_end) === true) {
            $data_where[] = [$this->api_log_model->table . ".datetime >=" => date_format(date_create(trim($reportrange_start)), 'Y-m-d H:i:s')];
        }

        if (empty($reportrange_end) === false && empty($reportrange_start) === true) {
            $data_where[] = [$this->api_log_model->table . ".datetime <=" => date_format(date_create(trim($reportrange_end)), 'Y-m-d H:i:s')];
        }

        if (empty($reportrange_start) === false && empty($reportrange_end) === false) {
            $data_where[] = [$this->api_log_model->table . ".datetime >=" => date_format(date_create(trim($reportrange_start)), 'Y-m-d H:i:s')];
            $data_where[] = [$this->api_log_model->table . ".datetime <=" => date_format(date_create(trim($reportrange_end)), 'Y-m-d H:i:s')];
        }

        $this->api_log_model->setSqlWhere($data_where);
        $this->api_log_model->sql_order_by = setFieldOrderBy();

        $total = $this->api_log_model->get_total();
        $data["listdb"] = $this->log_list();
        $data["total"] = $total;
        $data["pagination"] = $this->global_model->show_page($total);
        $data["result"] = $this->view_layout_return("ajax_list_log_new", $data);
        if ($this->input->post()) {
            $this->global_model->savePostGet();
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }
        $data["title"] = self::$title_module . " log overzicht";
        $this->view_layout("log", $data);
    }

    private function log_list()
    {
        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;
        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;
        return $this->api_log_model->get_list($limit, $page);
        //        $listdb = $this->api_log_model->get_list($limit, $page);
        //        $arr_result = [];
        //        foreach ($listdb as $rs) {
        //            $rs["header_value"] = $this->myprint_r(json_decode($rs["header_value"], true));
        //            $rs["post_value"] = $this->myprint_r(json_decode($rs["post_value"], true));
        //            $rs["get_value"] = $this->myprint_r(json_decode($rs["get_value"], true));
        //            $arr_result[] = $rs;
        //        }
        //
        //        return $arr_result;
    }
}
