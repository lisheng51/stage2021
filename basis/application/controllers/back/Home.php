<?php

class Home extends Back_Controller
{

    protected static $title_module = 'Dashboard';

    public function index()
    {
        $data["title"] = self::$title_module;
        $data["breadcrumb"] = "";
        $data["event_result_box"] = "";
        $moduleWidget = $this->module_model->get_one_by_path("widget");
        if (empty($moduleWidget) === false) {
            redirect('widget/back/Home/dashboard');
        }
        $this->view_layout("index", $data);
    }
}
