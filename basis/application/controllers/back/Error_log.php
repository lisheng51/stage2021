<?php

class Error_log extends Back_Controller
{

    protected static $title_module = 'Error log';

    public function index()
    {
        $reportrange = $this->input->post("reportrange") ?? "";
        $result = $this->getData($reportrange);
        $data["total"] = $result["total"];
        $data["pagination"] = $this->global_model->show_page($data["total"]);
        $data["listdb"] = $result["listdb"];
        $data["result"] = $this->view_layout_return("ajax_list", $data);
        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }

        $data["title"] = self::$title_module . ' overzicht';
        $data["select_page_limit"] = select_page_limit();
        $this->view_layout("index", $data);
    }

    private function getData(string $reportrange = "")
    {
        $date_start = "";
        $date_end = "";

        if (empty($reportrange) === false) {
            $arr_range = explode("t/m", $reportrange);
            $date_start = strtotime(trim($arr_range[0]));
            $date_end = strtotime(trim($arr_range[1]));
        }

        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;

        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;

        $arrResult = $arr = $temp = [];
        $listdb = directory_map(FCPATH . 'logs');
        if (empty($listdb) === true) {
            $data["total"] = 0;
            $data["listdb"] = $arrResult;
            return $data;
        }

        $log_file_extension = config_item('log_file_extension');
        foreach ($listdb as $value) {
            preg_match('/log-(.+?).' . $log_file_extension . '/i', $value, $arr);
            if (empty($arr) === true) {
                continue;
            }
            $filename = end($arr);
            $timestamp = strtotime($filename);
            $temp["active"] = '';
            $temp["date"] = $filename;
            if ($filename === date('Y-m-d')) {
                $temp["active"] = 'info';
            }
            $temp["viewButton"] = viewButton($this->controller_name . '.view', $this->controller_url . "/view/" . $filename, 'Download');
            if (empty($date_start) === false && $timestamp < $date_start || empty($date_end) === false && $timestamp > $date_end) {
                continue;
            }

            $arrResult[] = $temp;
        }

        usort($arrResult, function ($b, $a) {
            return strtotime($a["date"]) - strtotime($b["date"]);
        });

        $data["total"] = count($arrResult);
        $data["listdb"] = array_slice($arrResult, $page, $limit);
        return $data;
    }

    public function view(string $date = "")
    {
        if (empty($date) === true) {
            $date = date('Y-m-d');
        }
        $fileName = 'log-' . $date . '.' . config_item('log_file_extension');
        $file = config_item('log_path') . $fileName;
        if (file_exists($file) === false) {
            redirect($this->controller_url);
        }

        force_download($fileName, file_get_contents($file));
    }
}
