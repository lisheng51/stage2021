<?php

class Spamword extends Back_Controller
{

    protected static $title_module = 'Spamword';

    public function add()
    {
        if ($this->input->post()) {
            $this->addAction();
        }
        $data["rsdb"] = null;
        $data["title"] = self::$title_module . ' toevoegen';
        $data["delButton"] = null;
        $data["event_result_box"] = "";
        $this->view_layout("edit", $data);
    }

    public function del()
    {
        $id = $this->input->post("del_id");
        if (empty($id) === true) {
            redirect($this->controller_url);
        }
        $rsdb = $this->spamword_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            $json["msg"] = self::$title_module . ' is niet gevonden!';
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $this->spamword_model->del($id);
        $json["msg"] = self::$title_module . ' is verwijderd!';
        $json["status"] = "good";
        $json["type_done"] = "redirect";
        $json["redirect_url"] = site_url($this->controller_url);
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $this->editAction();
        }

        $data["rsdb"] = $this->spamword_model->get_one_by_id($id);
        if (empty($data["rsdb"]) === true) {
            redirect($this->controller_url);
        }

        $data["delButton"] = delButton($this->controller_name . '.del', $id);
        $data["title"] = self::$title_module . ' wijzigen';
        $data["event_result_box"] = "";
        $this->view_layout("edit", $data);
    }

    public function editInline()
    {
        $id = $this->input->post("editid") ?? 0;
        $rsdb = $this->spamword_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            $json["msg"] = self::$title_module . ' is niet gevonden!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $field = $this->input->post("field") ?? "";
        $fieldvalue = $this->input->post("fieldvalue") ?? "";

        if (empty($field) === true) {
            $json["msg"] = self::$title_module . ' is niet bijgewerkt!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $data[$field] = $fieldvalue;

        if ($field === 'word') {
            $existdb = $this->spamword_model->get_one_by_field($field, $data[$field]);
            if (empty($existdb) === false && $id != $existdb[$this->spamword_model->primary_key]) {
                $json["msg"] = self::$title_module . ' bestaat al!';
                $json["status"] = "error";
                exit(json_encode($json));
            }
        }

        $this->spamword_model->edit($id, $data);
        $json["msg"] = self::$title_module . ' is bijgewerkt';
        $json["status"] = "good";
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function index()
    {
        $data_where[] = [$this->spamword_model->table . '.' . $this->spamword_model->_field_is_deleted => 0];
        $data_where[] = setFieldAndOperator('word', $this->spamword_model->table . '.word');
        $this->spamword_model->setSqlWhere($data_where);
        $this->spamword_model->sql_order_by = setFieldOrderBy();
        $total = $this->spamword_model->get_total();
        $data["listdb"] = $this->getList();

        $data["total"] = $total;
        $data["pagination"] = $this->global_model->show_page($total);
        $data["result"] = $this->view_layout_return("ajax_list", $data);
        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }
        $data["event_result_box"] = "";
        $data["title"] = self::$title_module . ' overzicht';
        $data["addButton"] = addButton($this->controller_name . '.add', $this->controller_url . '/add');
        $this->view_layout("index", $data);
    }

    private function getList()
    {
        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;

        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;

        $arr_result = [];
        $listdb = $this->spamword_model->get_list($limit, $page);
        foreach ($listdb as $rs) {
            $rs["word"] = editInlineButton($this->controller_name . '.editInline', $rs[$this->spamword_model->primary_key], 'word', $rs['word']);
            $rs["editButton"] = editButton($this->controller_name . '.edit', $this->controller_url . "/edit/" . $rs[$this->spamword_model->primary_key]);
            $arr_result[] = $rs;
        }

        return $arr_result;
    }

    private function addAction()
    {
        $data = $this->getPostdata();
        $check_double = $this->spamword_model->get_one_by_word($data["word"]);
        if (empty($check_double) === false && $check_double[$this->spamword_model->_field_is_deleted] > 0) {
            $data_reset["is_del"] = 0;
            $this->spamword_model->edit($check_double[$this->spamword_model->primary_key], $data_reset);
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            $json["msg"] = self::$title_module . ' is toegevoegd';
            $json["status"] = "good";
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }

        if (empty($check_double) === false) {
            $json["msg"] = self::$title_module . ' bestaat al!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $insert_id = $this->spamword_model->add($data);
        if ($insert_id > 0) {
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            $json["msg"] = self::$title_module . ' is toegevoegd';
            $json["status"] = "good";
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    private function editAction()
    {
        $data = $this->getPostdata();
        $id = $this->input->post($this->spamword_model->primary_key);
        $rsdb = $this->spamword_model->get_one_by_id($id);
        $existdb = $this->spamword_model->get_one_by_word($data["word"]);

        if (empty($existdb) === false && $id != $existdb[$this->spamword_model->primary_key]) {
            $json["msg"] = self::$title_module . ' bestaat al!';
            $json["status"] = "error";
            exit(json_encode($json));
        }
        if (empty($rsdb) === false) {
            $this->spamword_model->edit($id, $data);
            $json["msg"] = self::$title_module . ' is bijgewerkt';
            $json["status"] = "good";
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    private function getPostdata()
    {
        if ($this->input->post("del_id")) {
            $this->del();
        }
        $data = $this->spamword_model->getPostdata();
        return $data;
    }
}
