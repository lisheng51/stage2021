<?php

class Permission_group extends Back_Controller
{

    protected static $title_module = 'Toestemming groep';

    public function add()
    {
        if ($this->input->post()) {
            $this->addAction();
        }
        $data["rsdb"] = null;
        $data["title"] = self::$title_module . ' toevoegen';
        $data["delButton"] = null;
        $data["permissions"] = $this->getPermission();
        $this->view_layout("edit", $data);
    }

    public function del()
    {
        $id = $this->input->post("del_id");
        if (empty($id) === true) {
            redirect($this->controller_url);
        }
        $rsdb = $this->permission_group_model->get_one_by_id($id);
        if (empty($rsdb) === true || $rsdb['is_lock'] > 0) {
            $json["msg"] = self::$title_module . ' is niet gevonden!';
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $data["is_del"] = 1;
        $this->permission_group_model->edit($id, $data);
        $json["msg"] = self::$title_module . ' is verwijderd!';
        $json["status"] = "good";
        $json["type_done"] = "redirect";
        $json["redirect_url"] = site_url($this->controller_url);
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $this->editAction();
        }

        $rsdb = $this->permission_group_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            redirect($this->controller_url);
        }
        $data["rsdb"] = $rsdb;
        $data["delButton"] = delButton($this->controller_name . '.del', $id);
        $data["permissions"] = $this->getPermission($rsdb["permission_ids"]);
        $data["title"] = self::$title_module . ' wijzigen';
        $this->view_layout("edit", $data);
    }

    public function editInline()
    {
        $id = $this->input->post("editid") ?? 0;
        $rsdb = $this->permission_group_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            $json["msg"] = self::$title_module . ' is niet gevonden!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $field = $this->input->post("field") ?? "";
        $fieldvalue = $this->input->post("fieldvalue") ?? "";

        if (empty($field) === true) {
            $json["msg"] = self::$title_module . ' is niet bijgewerkt!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $data[$field] = $fieldvalue;

        if ($field === 'name') {
            $existdb = $this->permission_group_model->get_one_by_field($field, $data[$field]);
            if (empty($existdb) === false && $id != $existdb[$this->permission_group_model->primary_key]) {
                $json["msg"] = self::$title_module . ' bestaat al!';
                $json["status"] = "error";
                exit(json_encode($json));
            }
        }

        $this->permission_group_model->edit($id, $data);
        $json["msg"] = self::$title_module . ' is bijgewerkt';
        $json["status"] = "good";
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function index()
    {
        $data_where[] = [$this->permission_group_model->table . '.' . $this->permission_group_model->_field_is_deleted => 0];
        $data_where[] = setFieldAndOperator('name', $this->permission_group_model->table . '.name');
        $data_where[] = setFieldAndOperator($this->permission_group_type_model->primary_key, $this->permission_group_model->table . '.' . $this->permission_group_type_model->primary_key);
        $this->permission_group_model->setSqlWhere($data_where);
        $this->permission_group_model->sql_order_by = setFieldOrderBy();
        $total = $this->permission_group_model->get_total();
        $data["listdb"] = $this->getData();

        $data["total"] = $total;
        $data["pagination"] = $this->global_model->show_page($total);
        $data["result"] = $this->view_layout_return("ajax_list", $data);
        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }
        $data["title"] = self::$title_module . ' overzicht';
        $data["addButton"] = addButton($this->controller_name . '.add', $this->controller_url . '/add');
        $this->view_layout("index", $data);
    }

    private function getData()
    {
        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;

        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;

        $this->permission_model->sql_where = [$this->module_model->table . ".is_active" => 1];
        $listdbPermission = $this->permission_model->get_all();

        $arr_result = [];
        $listdb = $this->permission_group_model->get_list($limit, $page);
        foreach ($listdb as $rs) {
            $rs["listdbPermission"]  = $this->getPermission($rs["permission_ids"], $listdbPermission, true);
            $rs["type_name"] = $this->permission_group_type_model->fetchField($rs[$this->permission_group_type_model->primary_key]);
            $rs["name"] = editInlineButton($this->controller_name . '.editInline', $rs[$this->permission_group_model->primary_key], 'name', $rs['name']);
            $rs["editButton"] = editButton($this->controller_name . '.edit', $this->controller_url . "/edit/" . $rs[$this->permission_group_model->primary_key]);
            $arr_result[] = $rs;
        }

        return $arr_result;
    }

    private function addAction()
    {
        $data = $this->getPostdata();
        $check_double = $this->permission_group_model->get_one_by_name($data["name"]);
        if (empty($check_double) === false && $check_double["is_del"] > 0) {
            $data_reset["is_del"] = 0;
            $this->permission_group_model->edit($check_double[$this->permission_group_model->primary_key], $data_reset);
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            $json["msg"] = self::$title_module . ' is toegevoegd';
            $json["status"] = "good";
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }

        if (empty($check_double) === false) {
            $json["msg"] = self::$title_module . ' bestaat al!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $insert_id = $this->permission_group_model->add($data);
        if ($insert_id > 0) {
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            $json["msg"] = self::$title_module . ' is toegevoegd';
            $json["status"] = "good";
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    private function editAction()
    {
        $data = $this->getPostdata();
        $id = $this->input->post($this->permission_group_model->primary_key);

        $existdb = $this->permission_group_model->get_one_by_name($data["name"]);
        if (empty($existdb) === false && $id != $existdb[$this->permission_group_model->primary_key]) {
            $json["msg"] = self::$title_module . ' bestaat al!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $rsdb = $this->permission_group_model->get_one_by_id($id);
        if (empty($rsdb) === false) {
            $this->permission_group_model->edit($id, $data);
            $json["msg"] = self::$title_module . ' is bijgewerkt';
            $json["status"] = "good";
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    private function getPostdata()
    {
        if ($this->input->post("del_id")) {
            $this->del();
        }
        $data = $this->permission_group_model->getPostdata();
        return $data;
    }

    private function getPermission(string $permission_ids = "", array $listdb = [], bool $onlyHas = false)
    {
        $hasIds = explode(',', $permission_ids);
        $arr_result = [];
        if (empty($listdb) === true) {
            $this->permission_model->sql_where = [$this->module_model->table . ".is_active" => 1];
            $listdb = $this->permission_model->get_all();
        }

        foreach ($listdb as $rs) {
            if ($onlyHas === true && in_array($rs[$this->permission_model->primary_key], $hasIds) === false) {
                continue;
            }
            $labelExtra = $rs["link_title"] !== "" ? '(' . $rs["link_title"] . ')' : null;
            $label = $rs["link_dir"] . '.' . $rs["object"] . '.' . $rs["method"] . $labelExtra;
            $rs["checkbox_label"] = $label;
            $rs["checkbox_value"] = in_array($rs[$this->permission_model->primary_key], $hasIds) === true ? "checked" : "";
            $arr_result[$rs["path_description"] . '#_#' . $rs[$this->module_model->primary_key]][] = $rs;
        }
        return $arr_result;
    }
}
