<?php

class Module extends Back_Controller
{
    protected static $title_module = 'Module';

    public function sortList()
    {
        $order_num = $this->input->post('sort_list');
        foreach ($order_num as $id => $value) {
            $data['sort_list'] = $value;
            $this->module_model->edit($id, $data);
        }
        $json["status"] = "good";
        $json['msg'] = 'Volgorde is verwerkt!';
        exit(json_encode($json));
    }

    public function update()
    {
        $moduleId = $this->input->post('id') ?? 0;
        $arrModule = $this->module_model->get_one_by_id(intval($moduleId));
        if (empty($arrModule) === true) {
            $json["status"] = "error";
            $json['msg'] = 'Geen module gevonden!';
            exit(json_encode($json));
        }
        $data = $this->module_model->getInfo($arrModule["path"]);
        if (empty($data) === true) {
            $json["status"] = "error";
            $json['msg'] = 'Geen info gevonden!';
            exit(json_encode($json));
        }

        $this->module_model->check_exist($data);
        $this->module_model->update($data);
        $this->module_model->setup($data);
        $json["msg"] = "Module " . $data["path_description"] . " is bijgewerkt!";
        $json["status"] = "good";
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function add()
    {
        $path = $this->input->post('path') ?? "";
        $data = $this->module_model->getInfo($path);
        if (empty($data) === true) {
            $json["status"] = "error";
            $json['msg'] = 'Geen info gevonden!';
            exit(json_encode($json));
        }

        $this->module_model->check_exist($data);
        $this->module_model->update($data);
        $this->module_model->setup($data);
        $json["msg"] = "Module " . $data["path_description"] . " is geïnstalleerd!";
        $json["status"] = "good";
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function del()
    {
        $id = $this->input->post("del_id");
        if (empty($id) === true) {
            redirect($this->controller_url);
        }
        $rsdb = $this->module_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            $json["msg"] = self::$title_module . ' is niet gevonden!';
            $json["status"] = "error";
            exit(json_encode($json));
        }
        //$data["is_active"] = 0;
        //$this->module_model->edit($id, $data);
        $this->module_model->del($id);
        $json["msg"] = self::$title_module . ' is verwijderd!';
        $json["status"] = "good";
        $json["type_done"] = "redirect";
        $json["redirect_url"] = site_url($this->controller_url);
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function index()
    {
        $data_where[] = setFieldAndOperator('path_description', $this->module_model->table . '.path_description');
        $is_active = $this->input->post_get("is_active") ?? "";
        if ($is_active != "") {
            $is_active = intval($is_active);
            $data_where[] = [$this->module_model->table . '.' . "is_active" => $is_active];
        }

        $this->module_model->setSqlWhere($data_where);
        $this->module_model->sql_order_by = setFieldOrderBy('sort_list#asc');
        $total = $this->module_model->get_total();
        $data["listdb"] = $this->getList();
        $data["total"] = $total;
        $data["result"] = $this->view_layout_return("ajax_list", $data);
        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }
        $data["title"] = self::$title_module . ' overzicht';
        $data["new"] = $this->getNew($data["listdb"]);
        $this->view_layout("index", $data);
    }

    private function getList()
    {
        $arr_result = [];
        $listdb = $this->module_model->get_all();
        foreach ($listdb as $rs) {
            $rs["editButton"] = editButton($this->controller_name . '.edit', $this->controller_url . "/edit/" . $rs[$this->module_model->primary_key]);
            $rs["changelog_url"] = site_url($this->path_name . "/Change_log/index/{$rs[$this->module_model->primary_key]}");
            $arr_result[] = $rs;
        }

        return $arr_result;
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $this->editAction();
        }

        $rsdb = $this->module_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            redirect($this->controller_url);
        }
        $data["rsdb"] = $rsdb;
        $data["delButton"] = delButton($this->controller_name . '.del', $id);
        $data["title"] = self::$title_module . ' wijzigen';
        $this->view_layout("edit", $data);
    }

    private function editAction()
    {
        if ($this->input->post("del_id")) {
            $this->del();
        }
        $data = $this->module_model->get_postdata();
        $id = $this->input->post($this->module_model->primary_key);
        $rsdb = $this->module_model->get_one_by_id($id);
        if (empty($rsdb) === false) {
            $this->module_model->edit($id, $data);
            $json["msg"] = self::$title_module . ' is bijgewerkt';
            $json["status"] = "good";
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    private function getNew(array $listdbnow = []): array
    {
        $modulesmap = array_merge(directory_map(APPPATH . DIRECTORY_SEPARATOR . 'modules_core' . DIRECTORY_SEPARATOR, 1), directory_map(APPPATH . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR, 1));
        $arrResultNew = $arrResultNow = [];
        foreach ($modulesmap as $module) {
            $arrResultNew[] = rtrim($module, DIRECTORY_SEPARATOR);
        }

        if (empty($arrResultNew) === true) {
            return [];
        }

        foreach ($listdbnow as $module) {
            $arrResultNow[] = $module["path"];
        }

        $result = array_diff($arrResultNew, $arrResultNow);
        $listdb = [];
        $arr = [];
        foreach ($result as $key => $module) {
            $data = $this->module_model->getInfo($module);
            if (isset($data['path'])) {
                $listdb[$key]["path"] = $data["path"];
                $listdb[$key]["path_description"] = $data["path_description"];
            }
        }

        $arr["total"] = count($listdb);
        $arr["listdb"] = $listdb;
        return $arr;
    }
}
