<?php

class Visitor extends Back_Controller
{

    protected static $title_module = 'Logging - bezoeker';

    public function index()
    {
        $reportrange = $this->input->post("reportrange");
        if (empty($reportrange) === false) {
            $arr_range = explode("t/m", $reportrange);
            $data_where[] = ["datetime >=" => date_format(date_create(trim($arr_range[0])), 'Y-m-d 00:00:00')];
            $data_where[] = ["datetime <=" => date_format(date_create(trim($arr_range[1])), 'Y-m-d 23:59:59')];
        }

        $data_where[] = setFieldAndOperator('path', $this->visitor_model->table . '.path');
        $data_where[] = setFieldAndOperator('browser', $this->visitor_model->table . '.browser');
        $data_where[] = setFieldAndOperator('platform', $this->visitor_model->table . '.platform');
        $data_where[] = setFieldAndOperator('ip_address', $this->visitor_model->table . '.ip_address');
        $data_where[] = setFieldAndOperator('emailaddress', $this->user_model->table . '.emailaddress');

        $this->visitor_model->setSqlWhere($data_where);
        $this->visitor_model->sql_order_by = setFieldOrderBy();
        $total = $this->visitor_model->get_total();
        $data["listdb"] = $this->getData();
        $data["total"] = $total;
        $data["pagination"] = $this->global_model->show_page($total);
        $data["result"] = $this->view_layout_return("ajax_list", $data);
        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }
        $data["title"] = self::$title_module;
        $this->view_layout("index", $data);
    }

    private function getData()
    {
        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;
        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;
        return $this->visitor_model->get_list($limit, $page);
    }
}
