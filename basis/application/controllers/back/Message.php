<?php

class Message extends Back_Controller
{

    protected static $title_module = 'Bericht';

    public function add()
    {
        $data_where[] = [$this->user_model->table . '.' . $this->user_model->_field_is_deleted => 0];
        $data_where[] = setFieldAndOperator('emailaddress', $this->user_model->table . '.emailaddress');
        $data_where[] = [$this->user_model->table . '.' . $this->user_model->primary_key . '!=' => $this->arr_userdb["user_id"]];

        $this->user_model->setSqlWhere($data_where);
        $this->user_model->sql_order_by = setFieldOrderBy();
        $total = $this->user_model->get_total();
        $data["listdb"] = $this->get_user_list();
        $data["total"] = $total;
        $data["pagination"] = $this->global_model->show_page($total);
        $data["result"] = $this->view_layout_return("user_ajax_list", $data);

        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }
        $data["title"] = 'Gebruiker overzicht';
        $this->view_layout("add", $data);
    }

    private function get_user_list()
    {
        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;

        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;

        $arr_result = [];
        $listdb = $this->user_model->get_list($limit, $page);
        foreach ($listdb as $rs) {
            $rs["addButton"] = addButton($this->controller_name . '.addFrom', $this->controller_url . "/addFrom/" . $rs["user_id"]);
            $arr_result[] = $rs;
        }

        return $arr_result;
    }

    public function index()
    {
        $reportrange = $this->input->post("reportrange");
        if (empty($reportrange) === false) {
            $arr_range = explode("t/m", $reportrange);
            $data_where[] = [$this->message_model->table . ".date >=" => date_format(date_create(trim($arr_range[0])), 'Y-m-d 00:00:00')];
            $data_where[] = [$this->message_model->table . ".date <=" => date_format(date_create(trim($arr_range[1])), 'Y-m-d 23:59:59')];
        }

        $is_open = $this->input->post_get("is_open");
        if (empty($is_open) === false) {
            $is_open_value = $is_open == "yes" ? 1 : 0;
            $data_where[] = [$this->message_model->table . '.is_open' => $is_open_value];
        }


        $data_where[] = [$this->message_model->table . '.' . $this->message_model->_field_is_deleted => 0];
        $data_where[] = setFieldAndOperator('title', $this->message_model->table . '.title');
        $data_where[] = setFieldAndOperator('content', $this->message_model->table . '.content');
        $data_where[] = [$this->message_model->table . '.to_user_id' => $this->arr_userdb["user_id"]];

        $this->message_model->setSqlWhere($data_where);
        $this->message_model->sql_order_by = setFieldOrderBy();

        $total = $this->message_model->get_total();
        $data["listdb"] = $this->getData();
        $data["pagination"] = $this->global_model->show_page($total);
        $data["total"] = $total;
        $data["result"] = $this->view_layout_return("ajax_list", $data);
        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }
        $data["addButton"] = addButton($this->controller_name . '.add', $this->controller_url . '/add');
        $data["title"] = self::$title_module . ' overzicht (postvak in)';
        $data["select_open_status"] = $this->message_model->select_open_status($is_open);
        $this->view_layout("index", $data);
    }

    private function getData()
    {
        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;

        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;

        $arr_result = [];
        $listdb = $this->message_model->get_list($limit, $page);
        foreach ($listdb as $rs) {
            $rs["title"] = empty($rs["is_open"]) === true ? "<strong>{$rs["title"]}</strong>" : $rs["title"];
            $rs["from_user_name"] = $this->user_model->fetch_name($rs["from_user_id"]);
            $rs["to_user_name"] = $this->user_model->fetch_name($rs["to_user_id"]);
            $rs["date"] = date_format(date_create($rs["date"]), 'd-m-Y H:i:s');
            $rs["viewButton"] = viewButton($this->controller_name . '.view', $this->controller_url . '/view/' . $rs["message_id"]);
            $arr_result[] = $rs;
        }

        return $arr_result;
    }

    public function view($id = 0)
    {
        $rsdb = $this->message_model->get_one_by_id(intval($id));
        if (empty($rsdb) === true) {
            redirect($this->controller_url);
        }

        $supervisor = false;
        if ($this->arr_userdb["user_id"] == $this->user_model->secure_id) {
            $supervisor = true;
        }

        if ($this->arr_userdb["user_id"] != $rsdb["to_user_id"] && $this->arr_userdb["user_id"] != $rsdb["from_user_id"] && $supervisor === false) {
            $this->session->set_flashdata('page_error_type', "no_authorized");
            redirect(error_url());
        }

        if ($this->input->post()) {
            $this->addFrom();
        }

        if ($this->arr_userdb["user_id"] == $rsdb["to_user_id"]) {
            if ($rsdb['open_at'] === null) {
                $dataEdit['open_at'] = date('Y-m-d H:i:s');
                $dataEdit['is_open'] = 1;
                $this->message_model->edit($id, $dataEdit);
            }
        }

        $rsdb["from_user_name"] = $this->user_model->fetch_name($rsdb["from_user_id"]);
        $rsdb["to_user_name"] = $this->user_model->fetch_name($rsdb["to_user_id"]);
        $rsdb["date"] = F_datetime::convert_datetime($rsdb["date"]);

        $data["rsdb"] = $rsdb;
        $data["reply_status"] = '';
        $data["delButton"] = delButton($this->controller_name . '.del', $id);
        if ($this->arr_userdb["user_id"] == $rsdb["from_user_id"]) {
            $data["reply_status"] = 'invisible';
        }
        $data["title"] = self::$title_module . " informatie";
        $this->view_layout("view", $data);
    }

    public function addFrom($to_user_id = 0)
    {
        if ($this->input->post()) {
            $this->addFromAction();
        }
        $arr_user = $this->user_model->get_one_by_id(intval($to_user_id));
        if (empty($arr_user) === true) {
            redirect($this->controller_url);
        }
        $data["title"] = self::$title_module . ' toevoegen';

        $data["to_user_id"] = $to_user_id;
        $data["to_user_name"] = $arr_user["display_info"];
        $this->view_layout("addFrom", $data);
    }

    private function addFromAction()
    {
        $data = $this->get_postdata();
        $messageId = $this->message_model->add($data);
        if ($messageId > 0) {
            $this->sendmail_model->message_copie($messageId, $this->user_model->get_one_by_id($data["to_user_id"]));
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            $json["msg"] = self::$title_module . " is verzonden!";
            $json["status"] = "good";
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    public function del()
    {
        $id = $this->input->post("del_id");
        if (empty($id) === true) {
            redirect($this->controller_url);
        }
        $rsdb = $this->message_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            $json["msg"] = self::$title_module . " kan niet worden verwijderd!";
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $data["is_del"] = 1;
        $this->message_model->edit($id, $data);
        $json["msg"] = self::$title_module . " is verwijderd!";
        $json["status"] = "good";
        $json["type_done"] = "redirect";
        $json["redirect_url"] = site_url($this->controller_url);
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    private function get_postdata(): array
    {
        if ($this->input->post("del_id")) {
            $this->del();
        }

        $data = $this->message_model->get_postdata();
        return $data;
    }

    public function my()
    {
        $reportrange = $this->input->post("reportrange");
        if (empty($reportrange) === false) {
            $arr_range = explode("t/m", $reportrange);
            $data_where[] = [$this->message_model->table . ".date >=" => date_format(date_create(trim($arr_range[0])), 'Y-m-d 00:00:00')];
            $data_where[] = [$this->message_model->table . ".date <=" => date_format(date_create(trim($arr_range[1])), 'Y-m-d 23:59:59')];
        }

        $is_open = $this->input->post_get("is_open");
        if (empty($is_open) === false) {
            $is_open_value = $is_open == "yes" ? 1 : 0;
            $data_where[] = [$this->message_model->table . '.is_open' => $is_open_value];
        }


        $data_where[] = [$this->message_model->table . '.' . $this->message_model->_field_is_deleted => 0];
        $data_where[] = setFieldAndOperator('title', $this->message_model->table . '.title');
        $data_where[] = setFieldAndOperator('content', $this->message_model->table . '.content');
        $data_where[] = [$this->message_model->table . '.from_user_id' => $this->arr_userdb["user_id"]];

        $this->message_model->setSqlWhere($data_where);
        $this->message_model->sql_order_by = setFieldOrderBy();

        $total = $this->message_model->get_total();
        $data["listdb"] = $this->getData();
        $data["pagination"] = $this->global_model->show_page($total);
        $data["total"] = $total;
        $data["result"] = $this->view_layout_return("ajax_list", $data);
        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }

        $data["addButton"] = addButton($this->controller_name . '.add', $this->controller_url . '/add');
        $data["title"] = self::$title_module . ' overzicht (postvak out)';
        $data["select_open_status"] = $this->message_model->select_open_status($is_open);
        $this->view_layout("index", $data);
    }
}
