<?php

class Mail_template_info extends Back_Controller
{

    protected static $title_module = 'Mail template';

    public function index()
    {
        $data["title"] = self::$title_module . ' overzicht';
        $moduleName = $this->input->get('moduleName') ?? "";
        $data["listdb"] = $this->mail_template_model->selectFolder($moduleName);
        $data['edit_url'] = $this->controller_url . '/edit/';
        if (empty($moduleName) === false) {
            $data['edit_url'] = $this->controller_url . '/editModule/' . $moduleName . '/';
            $data["title"] = self::$title_module . ' - ' . $moduleName . ' overzicht';
        }
        $data["selectModule"] = $this->module_model->selectModule($moduleName);
        $data["event_result_box"] = "";
        $this->view_layout("index", $data);
    }

    public function editModule(string $moduleName = "", string $langname = "")
    {
        if ($this->input->post()) {
            $this->editModuleAction($moduleName, $langname);
        }
        $arrResult = $this->mail_template_model->moduleFetchfolderFiles($langname, $moduleName);
        if (empty($arrResult) === true) {
            redirect($this->controller_url);
        }

        $listdb = $arrResult;
        $config = $ul = [];
        foreach ($listdb as $filename) {
            $configValue = $this->mail_template_model->moduleFetchfolderFile($langname, $moduleName, $filename);
            if (empty($configValue) === true) {
                continue;
            }
            $ul[] = $filename;
            $config[][$filename] = $configValue;
        }
        $data["title"] = self::$title_module . ' - ' . $langname . ' - ' . $moduleName;
        $data['ul'] = $ul;
        $data['listdb'] = $config;
        $this->view_layout("edit", $data);
    }

    private function editModuleAction(string $moduleName = "", string $langname = "")
    {
        $postData = $this->input->post();
        foreach ($postData as $filename => $data) {
            $this->mail_template_model->moduleSaveFolderFile($data, $langname, $moduleName, $filename);
        }

        $json["msg"] = self::$title_module . " is bijgewerkt!";
        $json["status"] = "good";
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function edit(string $langname = "")
    {
        if ($this->input->post()) {
            $this->editAction($langname);
        }
        $arrResult = $this->mail_template_model->fetchfolderFiles($langname);
        if (empty($arrResult) === true) {
            redirect($this->controller_url);
        }

        $listdb = $arrResult;
        $config = $ul = [];
        foreach ($listdb as $filename) {
            $configValue = $this->mail_template_model->fetchfolderFile($langname, $filename);
            if (empty($configValue) === true) {
                continue;
            }
            $ul[] = $filename;
            $config[][$filename] = $configValue;
        }
        if (empty($ul) === true) {
            redirect($this->controller_url);
        }
        $data["title"] = self::$title_module . ' - ' . $langname;
        $data['ul'] = $ul;
        $data['listdb'] = $config;
        $this->view_layout("edit", $data);
    }

    private function editAction(string $langname = "")
    {
        $postData = $this->input->post();
        foreach ($postData as $filename => $data) {
            $this->mail_template_model->saveFolderFile($data, $langname, $filename);
        }

        $json["msg"] = self::$title_module . " is bijgewerkt!";
        $json["status"] = "good";
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }
}
