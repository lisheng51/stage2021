<?php

class Config extends Back_Controller
{

    protected static $title_module = 'Instelling';

    public function index()
    {
        if ($this->input->post()) {
            $this->indexAction();
        }
        $data["title"] = self::$title_module;
        $this->config_model->configNow = $this->config_model->get_all();
        $data["webapp_smtp_pass"] = "";
        if (empty($this->config_model->configNow["webapp_smtp_pass"]) === false) {
            $data["webapp_smtp_pass"] = $this->config_model->decryptData($this->config_model->configNow["webapp_smtp_pass"]);
        }
        $this->view_layout("index", $data);
    }

    private function indexAction()
    {
        $webdbs = $this->input->post();
        $webdbs["webapp_smtp_pass"] = $this->config_model->encryptData($webdbs["webapp_smtp_pass"]);
        $status = $this->config_model->update($webdbs);
        if ($status === true) {
            $json["msg"] = self::$title_module . ' is bijgewerkt!';
            $json["status"] = "good";
            add_app_log($json["msg"]);
            $this->config_model->set_session();
            exit(json_encode($json));
        }
        $json["msg"] = self::$title_module . ' is niet bijgewerkt!';
        $json["status"] = "error";
        exit(json_encode($json));
    }

    public function sendMailTest()
    {
        $to_email = $this->input->post("to_email") ?? "";
        $from_email = $this->input->post("user") ?? "";

        if (empty($to_email) || empty($from_email)) {
            $json["msg"] = "Email is niet verzonden";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $mailConfig["Username"] = $from_email;
        $mailConfig["Name"] = $this->input->post("name") ?? "";
        $mailConfig["Host"] = $this->input->post("host") ?? "";
        $mailConfig["Password"] = $this->input->post("pass") ?? "";
        $mailConfig["SMTPSecure"] = $this->input->post("crypto") ?? "";
        $mailConfig["Port"] = $this->input->post("port") ?? "";

        $subject = self::$title_module;
        $message = __FUNCTION__;

        $mail = new \PHPMailer\PHPMailer\PHPMailer(true);
        try {
            $mail->isSMTP();
            $mail->SMTPAuth = true;
            $mail->Host = $mailConfig['Host'];
            $mail->Username = $mailConfig['Username'];
            $mail->Password = $mailConfig['Password'];
            $mail->SMTPSecure = $mailConfig['SMTPSecure'];
            $mail->Port = $mailConfig['Port'];
            $mail->setFrom($mailConfig['Username'], $mailConfig['Name']);
            $mail->addAddress($to_email);
            $mail->Subject = $subject;
            $mail->Body = $message;

            $status = $mail->send();
            if ($status === true) {
                $json["msg"] = "Email is verzonden";
                $json["status"] = "good";
                exit(json_encode($json));
            }
        } catch (\PHPMailer\PHPMailer\Exception $e) {
            $debug_result = $from_email . ': ' . $mail->ErrorInfo;
            $json["msg"] = $debug_result;
            $json["status"] = "error";
            exit(json_encode($json));
        }
    }
}
