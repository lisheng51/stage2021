<?php

class Language_info extends Back_Controller
{

    protected static $title_module = 'Taal tekst';

    public function index()
    {
        $data["title"] = self::$title_module;
        $moduleName = $this->input->get('moduleName') ?? "";
        $data["listdb"] = $this->language_model->selectList($moduleName);
        $data['edit_url'] = $this->controller_url . '/edit/';
        if (empty($moduleName) === false) {
            $data['edit_url'] = $this->controller_url . '/editModule/' . $moduleName . '/';
            $data["title"] = self::$title_module . ' - ' . $moduleName;
        }
        $data["selectModule"] = $this->module_model->selectModule($moduleName);
        $data["event_result_box"] = "";
        $this->view_layout("index", $data);
    }

    public function editModule(string $moduleName = "", string $langname = "")
    {
        if ($this->input->post()) {
            $this->editModuleAction($moduleName, $langname);
        }
        $arrResult = $this->language_info_model->moduleFiles($langname, $moduleName);
        if (empty($arrResult) === true) {
            redirect($this->controller_url);
        }

        $listdb = $arrResult;
        $config = $ul = [];
        foreach ($listdb as $filename) {
            $configValue = $this->language_info_model->moduleFetch($langname, $moduleName, $filename);
            if (empty($configValue) === true) {
                continue;
            }
            $ul[] = $filename;
            $config[][$filename] = $configValue;
        }
        if (empty($ul) === true) {
            redirect($this->controller_url);
        }

        $data["title"] = self::$title_module . ' - ' . $langname . ' - ' . $moduleName;
        $data['ul'] = $ul;
        $data['listdb'] = $config;
        $data["event_result_box"] = "";
        $this->view_layout("edit", $data);
    }

    private function editModuleAction(string $moduleName = "", string $langname = "")
    {
        $postData = $this->input->post();
        foreach ($postData as $filename => $data) {
            $this->language_info_model->moduleSave($data, $langname, $moduleName, $filename);
        }

        $json["msg"] = self::$title_module . " is bijgewerkt!";
        $json["status"] = "good";
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function edit(string $langname = "")
    {
        if ($this->input->post()) {
            $this->editAction($langname);
        }
        $arrResult = $this->language_info_model->files($langname);
        if (empty($arrResult) === true) {
            redirect($this->controller_url);
        }

        $listdb = $arrResult;
        $config = $ul = [];
        foreach ($listdb as $filename) {
            $configValue = $this->language_info_model->fetch($langname, $filename);
            if (empty($configValue) === true) {
                continue;
            }
            $ul[] = $filename;
            $config[][$filename] = $configValue;
        }
        if (empty($ul) === true) {
            redirect($this->controller_url);
        }

        $data["title"] = self::$title_module . ' - ' . $langname;
        $data['ul'] = $ul;
        $data['listdb'] = $config;
        $data["event_result_box"] = "";
        $this->view_layout("edit", $data);
    }

    private function editAction(string $langname = "")
    {
        $postData = $this->input->post();
        foreach ($postData as $filename => $data) {
            $this->language_info_model->save($data, $langname, $filename);
        }

        $json["msg"] = self::$title_module . " is bijgewerkt!";
        $json["status"] = "good";
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }
}
