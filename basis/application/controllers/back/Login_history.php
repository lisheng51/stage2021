<?php

class Login_history extends Back_Controller
{

    protected static $title_module = 'Login geschiedenis';

    public function index()
    {
        $reportrange = $this->input->post("reportrange");
        if (empty($reportrange) === false) {
            $arr_range = explode("t/m", $reportrange);
            $data_where[] = [$this->login_history_model->table . ".created_at >=" => date_format(date_create(trim($arr_range[0])), 'Y-m-d 00:00:00')];
            $data_where[] = [$this->login_history_model->table . ".created_at <=" => date_format(date_create(trim($arr_range[1])), 'Y-m-d 23:59:59')];
        }

        $data_where[] = [$this->user_model->primary_key => $this->arr_userdb["user_id"]];
        $data_where[] = setFieldAndOperator('emailaddress', $this->user_model->table . '.emailaddress');
        $this->login_history_model->setSqlWhere($data_where);
        $this->login_history_model->sql_order_by = setFieldOrderBy();

        $total = $this->login_history_model->get_total();
        $data["listdb"] = $this->getData();
        $data["total"] = $total;
        $data["pagination"] = $this->global_model->show_page($total);
        $data["ajax_batch_del_url"] = site_url($this->controller_url . "/batch_del");
        $data["result"] = $this->view_layout_return("ajax_list", $data);
        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }

        $data["title"] = self::$title_module . " overzicht";
        $this->view_layout("index", $data);
    }

    private function getData()
    {
        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;

        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;

        $arr_result = [];
        $listdb = $this->login_history_model->get_list($limit, $page);
        foreach ($listdb as $rs) {
            $rs["del_url"] = site_url($this->controller_url . "/del");
            $rs["date"] = date_format(date_create($rs["created_at"]), 'd-m-Y H:i:s');
            $arr_result[] = $rs;
        }
        return $arr_result;
    }

    public function batch_del()
    {
        $arr_ids = $this->input->post("ids");
        if (empty($arr_ids) === true) {
            $json["msg"] = self::$title_module . " kan niet worden verwijderd!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        foreach ($arr_ids as $id) {
            $this->login_history_model->del($id);
        }
        $json["type_done"] = "redirect";
        $json["redirect_url"] = site_url($this->controller_url);
        $json["msg"] = self::$title_module . " is verwijderd!";
        $json["status"] = "good";
        exit(json_encode($json));
    }

    public function del()
    {
        $id = $this->input->post("del_id");
        $rsdb = $this->login_history_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            $json["msg"] = self::$title_module . " kan niet worden verwijderd!";
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $this->login_history_model->del($id);
        $json["msg"] = self::$title_module . " is verwijderd!";
        $json["status"] = "good";
        exit(json_encode($json));
    }
}
