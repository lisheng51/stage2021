<?php

class File extends Back_Controller
{

    protected static $title_module = 'Bestand';

    public function demo()
    {
        if ($this->input->post()) {
            $uploadData = [];
            $filesCount = count($_FILES['userFiles']['name']);
            for ($i = 0; $i < $filesCount; $i++) {
                $name = $_FILES['userFiles']['name'][$i];
                $tmp_name = $_FILES['userFiles']['tmp_name'][$i];
                $dataFile['decode_string'] = base64_encode(file_get_contents($tmp_name));
                $dataFile['file_name'] = $name;
                $uploadData[] = $dataFile;
            }
            dump($uploadData);

            foreach ($uploadData as $data) {
                $decode_string = $data['decode_string'];
                file_put_contents($this->upload_model->root_folder . DIRECTORY_SEPARATOR . $data['file_name'], base64_decode($decode_string));
            }
        }
        $data["title"] = self::$title_module . ' toevoegen';
        $this->view_layout("demo", $data);
    }

    public function add()
    {
        if ($this->input->post()) {
            $this->addAction();
        }
        $data["rsdb"] = null;
        $data["title_status"] = 'd-none';
        $data["input_file"] = '<input type="file" class="form-control" name="userFiles[]" multiple/>';
        $data["delButton"] = null;
        $data["title"] = self::$title_module . ' toevoegen';
        $data["select_type"] = $this->upload_type_model->select();
        $this->view_layout("edit", $data);
    }

    public function del()
    {
        $id = $this->input->post("del_id");
        if (empty($id) === true) {
            redirect($this->controller_url);
        }
        $rsdb = $this->upload_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            $json["msg"] = self::$title_module . ' is niet gevonden!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $path_name = $this->upload_type_model->fetch_path_name($rsdb[$this->upload_type_model->primary_key]);
        $file_path = $this->upload_type_model->show_dir($path_name) . DIRECTORY_SEPARATOR . $rsdb['file_name'];
        if (unlink($file_path) === true) {
            $this->upload_model->del($id);
            $json["msg"] = self::$title_module . ' is verwijderd!';
            $json["status"] = "good";
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $this->editAction();
        }

        $rsdb = $this->upload_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            redirect($this->controller_url);
        }

        $data["rsdb"] = $rsdb;
        $path_name = $this->upload_type_model->fetch_path_name($rsdb[$this->upload_type_model->primary_key]);
        $data["title_status"] = null;
        $path = base_url($this->upload_type_model->show_dir($path_name) . DIRECTORY_SEPARATOR . $data["rsdb"]['file_name']);
        $showPath = str_replace('\\', '/', $path);
        $data["input_file"] = '<p>' . $showPath . '</p>';
        $data["delButton"] = delButton($this->controller_name . '.del', $id);
        $data["title"] = self::$title_module . ' wijzigen';
        $data["select_type"] = $this->upload_type_model->select($data["rsdb"]["type_id"]);
        $this->view_layout("edit", $data);
    }

    public function make_dir()
    {
        $this->upload_type_model->make_dir($this->arr_userdb["user_id"]);
        redirect($this->controller_url);
    }

    public function index()
    {
        $reportrange = $this->input->post("reportrange");
        if (empty($reportrange) === false) {
            $arr_range = explode("t/m", $reportrange);
            $data_where[] = [$this->upload_model->table . ".created_at >=" => date_format(date_create(trim($arr_range[0])), 'Y-m-d 00:00:00')];
            $data_where[] = [$this->upload_model->table . ".created_at <=" => date_format(date_create(trim($arr_range[1])), 'Y-m-d 23:59:59')];
        }

        $data_where[] = setFieldAndOperator('title', $this->message_model->table . '.title');
        $data_where[] = [$this->upload_model->table . '.createdby' => $this->arr_userdb["user_id"]];
        $data_where[] = setFieldAndOperator($this->upload_type_model->primary_key, $this->upload_model->table . '.' . $this->upload_type_model->primary_key);
        $this->upload_model->setSqlWhere($data_where);
        $this->upload_model->sql_order_by = setFieldOrderBy();

        $total = $this->upload_model->get_total();
        $data["listdb"] = $this->getData();
        $data["total"] = $total;
        $data["pagination"] = $this->global_model->show_page($total);
        $data["result"] = $this->view_layout_return("ajax_list", $data);
        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }

        $data["title"] = self::$title_module . ' overzicht';
        $data["addButton"] = addButton($this->controller_name . '.add', $this->controller_url . '/add');
        $this->view_layout("index", $data);
    }

    private function getData()
    {
        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;

        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;

        $arr_result = [];
        $listdb = $this->upload_model->get_list($limit, $page);
        foreach ($listdb as $rs) {
            $rs["editButton"] = editButton($this->controller_name . '.edit', $this->controller_url . "/edit/" . $rs["upload_id"]);
            $rs["type_name"] = $this->upload_type_model->fetch_path_name($rs[$this->upload_type_model->primary_key], 'name');
            $rs["created_at"] = date_format(date_create($rs["created_at"]), 'd-m-Y H:i:s');
            $rs["path"] = base_url($this->upload_type_model->show_dir($this->upload_type_model->fetch_path_name($rs[$this->upload_type_model->primary_key])) . DIRECTORY_SEPARATOR . $rs['file_name']);
            $arr_result[] = $rs;
        }

        return $arr_result;
    }

    private function getPostdata()
    {
        if ($this->input->post("del_id")) {
            $this->del();
        }
        $data = $this->upload_model->get_postdata();
        return $data;
    }

    private function addAction()
    {
        if ($_FILES['userFiles']['error'] === 4) {
            $json["msg"] = "Geen bestand gevonden!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $type_id = $this->input->post($this->upload_type_model->primary_key);

        $path_name = $this->upload_type_model->fetch_path_name($type_id);
        $allowed_types = $this->upload_type_model->fetchData($type_id, 'allowed_types');

        if (empty($path_name) === true) {
            $json["msg"] = "Geen type gevonden!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $uploadData = [];
        $filesCount = count($_FILES['userFiles']['name']);
        for ($i = 0; $i < $filesCount; $i++) {
            $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
            $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
            $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
            $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
            $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];

            $config['upload_path'] = $this->upload_type_model->show_dir($path_name);
            $config['allowed_types'] = $allowed_types;
            $config['overwrite'] = true;
            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('userFile') === false) {
                $json["msg"] = $this->upload->display_errors();
                $json["status"] = "error";
                exit(json_encode($json));
            }

            $fileData = $this->upload->data();
            $data['file_name'] = $fileData['file_name'];
            $this->upload_model->sql_where = $data;
            $check_double = $this->upload_model->get_one();
            if (empty($check_double) === true) {
                $uploadData['file_name'] = $fileData['file_name'];
                $uploadData[$this->upload_type_model->primary_key] = $type_id;
                $uploadData['title'] = $uploadData[$this->upload_type_model->primary_key] . '_' . $uploadData['file_name'];
                $this->upload_model->add($uploadData);
            }
        }

        $json["type_done"] = "redirect";
        $json["redirect_url"] = site_url($this->controller_url);
        $json["msg"] = self::$title_module . ' is toegevoegd!';
        $json["status"] = "good";
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    private function editAction()
    {
        $data = $this->getPostdata();
        $id = $this->input->post($this->upload_model->primary_key);
        $rsdb = $this->upload_model->get_one_by_id($id);
        $existdb = $this->upload_model->get_one_by_title($data["title"]);
        if (empty($existdb) === false && $id != $existdb[$this->upload_model->primary_key]) {
            $json["msg"] = self::$title_module . " bestaat al!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $this->rename_file($data[$this->upload_type_model->primary_key], $rsdb[$this->upload_type_model->primary_key], $rsdb['file_name']);

        if (empty($rsdb) === false) {
            $this->upload_model->edit($id, $data);
            $json["msg"] = self::$title_module . ' is bijgewerkt!';
            $json["status"] = "good";
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    private function rename_file($new_type_id = 0, $old_type_id = 0, $file_name = "")
    {
        if ($new_type_id != $old_type_id) {
            $path_name_new = $this->upload_type_model->fetch_path_name($new_type_id);
            $path_name_old = $this->upload_type_model->fetch_path_name($old_type_id);

            $allowed_types = $this->upload_type_model->fetchData($new_type_id, 'allowed_types');

            $file_path_old = $this->upload_type_model->show_dir($path_name_old) . '/' . $file_name;
            $file_path_new = $this->upload_type_model->show_dir($path_name_new) . '/' . $file_name;

            $path_parts = pathinfo($file_path_old);
            $allow_types = explode('|', $allowed_types);

            if (in_array(strtolower($path_parts['extension']), $allow_types) === false) {
                $json["msg"] = "Bestand type is niet juist!";
                $json["status"] = "error";
                exit(json_encode($json));
            }

            rename($file_path_old, $file_path_new);
        }
    }
}
