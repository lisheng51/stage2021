<?php

class Sendmail extends Back_Controller
{

    protected static $title_module = 'Sendmail';

    public function index()
    {
        $reportrange = $this->input->post("reportrange");
        if (empty($reportrange) === false) {
            $arr_range = explode("t/m", $reportrange);
            $data_where[] = ["send_date >=" => date_format(date_create(trim($arr_range[0])), 'Y-m-d 00:00:00')];
            $data_where[] = ["send_date <=" => date_format(date_create(trim($arr_range[1])), 'Y-m-d 23:59:59')];
        }

        $data_where[] = setFieldAndOperator('to_email', $this->sendmail_model->table . '.to_email');
        $data_where[] = setFieldAndOperator('from_email', $this->sendmail_model->table . '.from_email');
        $data_where[] = setFieldAndOperator('subject', $this->sendmail_model->table . '.subject');
        $this->sendmail_model->setSqlWhere($data_where);
        $this->sendmail_model->sql_order_by = setFieldOrderBy();

        $total = $this->sendmail_model->get_total();
        $data["listdb"] = $this->getList();
        $data["total"] = $total;
        $data["pagination"] = $this->global_model->show_page($total);
        $data["result"] = $this->view_layout_return("ajax_list", $data);
        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }
        $data["title"] = self::$title_module . ' overzicht';
        $this->view_layout("index", $data);
    }

    private function getList()
    {
        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;

        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;

        $arr_result = [];
        $listdb = $this->sendmail_model->get_list($limit, $page);
        foreach ($listdb as $rs) {
            $rs["del_url"] = site_url($this->controller_url . "/del");
            $rs["send_date_view"] = F_datetime::convert_datetime($rs["send_date"]);
            $rs["open_date_view"] = F_datetime::convert_datetime($rs["open_date"]);
            $rs["view_url"] = $this->sendmail_model->create_event($rs, 'sys');
            $reply_to_json = json_decode($rs["reply_to_json"], true) ?? [];
            $cc_json = json_decode($rs["cc_json"], true) ?? [];
            $bcc_json = json_decode($rs["bcc_json"], true) ?? [];
            $rs["attach"] = $this->showAttach($rs['attach']);
            $rs["file"] = $this->showFile($rs[$this->sendmail_model->primary_key]);
            $rs["reply"] = implode(", ", array_keys($reply_to_json));
            $rs["cc"] = implode(", ", array_keys($cc_json));
            $rs["bcc"] = implode(", ", array_keys($bcc_json));
            $rs["to_email"] = editInlineButton($this->controller_name . '.editInline', $rs[$this->sendmail_model->primary_key], 'to_email', $rs['to_email']);
            $arr_result[] = $rs;
        }
        return $arr_result;
    }

    public function editInline()
    {
        $id = $this->input->post("editid") ?? 0;
        $rsdb = $this->sendmail_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            $json["msg"] = self::$title_module . ' is niet gevonden!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $field = $this->input->post("field") ?? "";
        $fieldvalue = $this->input->post("fieldvalue") ?? "";

        if (empty($field) === true) {
            $json["msg"] = self::$title_module . ' is niet bijgewerkt!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $data[$field] = $fieldvalue;

        if ($field !== 'to_email') {
            $json["msg"] = self::$title_module . ' is niet bijgewerkt!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $this->sendmail_model->edit($id, $data);
        $json["msg"] = self::$title_module . ' is bijgewerkt';
        $json["status"] = "good";
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }


    public function downloadFile(int $id = 0)
    {
        $rsdb = $this->send_mail_file_model->get_one_by_id($id);
        if (empty($rsdb) === false) {
            $file_content = base64_decode($rsdb["base64"]);
            $filename = $rsdb["file_name"];
            force_download($filename, $file_content);
        }
    }

    private function showFile(int $mail_id = 0): string
    {
        $content = "";
        if ($mail_id > 0) {
            $files = $this->send_mail_file_model->get_all_by_field($this->sendmail_model->primary_key, $mail_id);
            if (empty($files) === false) {
                foreach ($files as $rsdb) {
                    $link = site_url($this->controller_url . "/downloadFile/" . $rsdb[$this->send_mail_file_model->primary_key]);
                    $filename = $rsdb["file_name"];
                    $content .= '<a href="' . $link . '" class="btn btn-info btn-sm mt-1">' . $filename . '</a> ';
                }
            }
        }
        return $content;
    }

    private function showAttach($attach = null): string
    {
        $content = "";
        if (empty($attach) === false) {
            $multiAttach = explode(",", $attach);
            foreach ($multiAttach as $key => $path) {
                $key++;
                $content .= '<a target="_blank" href="' . base_url($path) . '" class="btn btn-info btn-sm mt-1">' . $key . '</a> ';
            }
        }
        return $content;
    }

    public function del()
    {
        $id = $this->input->post("del_id");
        $rsdb = $this->sendmail_model->get_one_by_id(intval($id));
        if (empty($rsdb) === true) {
            $json["msg"] = "Deze kan niet worden verwijderd!";
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $this->sendmail_model->del(intval($id));
        $json["msg"] = self::$title_module . " is verwijderd!";
        $json["status"] = "good";
        exit(json_encode($json));
    }
}
