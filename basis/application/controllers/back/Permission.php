<?php

class Permission extends Back_Controller
{

    protected static $title_module = 'Toestemming';

    public function sortList()
    {
        $order_num = $this->input->post('sort_list');
        foreach ($order_num as $id => $value) {
            $data['order_num'] = $value;
            $this->permission_model->edit($id, $data);
        }
        $json["status"] = "good";
        $json['msg'] = 'Volgorde is verwerkt!';
        exit(json_encode($json));
    }

    public function editInline()
    {
        $id = $this->input->post("editid") ?? 0;
        $rsdb = $this->permission_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            $json["msg"] = self::$title_module . ' is niet gevonden!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $field = $this->input->post("field") ?? "";
        $fieldvalue = $this->input->post("fieldvalue") ?? "";

        if (empty($field) === true) {
            $json["msg"] = self::$title_module . ' is niet bijgewerkt!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $data[$field] = $fieldvalue;
        $this->permission_model->edit($id, $data);
        $json["msg"] = self::$title_module . ' is bijgewerkt';
        $json["status"] = "good";
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function add()
    {
        if ($this->input->post()) {
            $this->addAction();
        }
        $data["title"] = self::$title_module . ' toevoegen';
        $this->view_layout("add", $data);
    }

    public function del()
    {
        $id = $this->input->post("del_id");
        if (empty($id) === true) {
            redirect($this->controller_url);
        }
        $rsdb = $this->permission_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            $json["msg"] = self::$title_module . ' is niet gevonden!';
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $this->permission_model->del($id);
        $json["msg"] = self::$title_module . ' is verwijderd!';
        $json["status"] = "good";
        $json["type_done"] = "redirect";
        $json["redirect_url"] = site_url($this->controller_url);
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $this->editAction();
        }

        $rsdb = $this->permission_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            redirect($this->controller_url);
        }
        $data["rsdb"] = $rsdb;
        $data["delButton"] = delButton($this->controller_name . '.del', $id);
        $data["title"] = self::$title_module . ' wijzigen';
        $this->view_layout("edit", $data);
    }

    public function index()
    {
        $data_where[] = setFieldAndOperator($this->module_model->primary_key, $this->permission_model->table . '.' . $this->module_model->primary_key);
        $data_where[] = setFieldAndOperator('description', $this->permission_model->table . '.description');
        $this->permission_model->setSqlWhere($data_where);
        $this->permission_model->sql_order_by = setFieldOrderBy();
        $total = $this->permission_model->get_total();
        $data["listdb"] = $this->getData();
        $data["total"] = $total;
        $data["pagination"] = null; //$this->global_model->show_page($total);;
        $data["result"] = $this->view_layout_return("ajax_list", $data);
        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }
        $data["title"] = self::$title_module . ' overzicht';
        $this->view_layout("index", $data);
    }

    private function getData()
    {
        $arr_result = [];
        $listdb = $this->permission_model->get_all();
        foreach ($listdb as $rs) {
            $rs["editButton"] = editButton($this->controller_name . '.edit', $this->controller_url . "/edit/" . $rs[$this->permission_model->primary_key]);
            $rs["link_title"] = editInlineButton($this->controller_name . '.editInline', $rs[$this->permission_model->primary_key], 'link_title', $rs['link_title']);
            $rs["description"] = editInlineButton($this->controller_name . '.editInline', $rs[$this->permission_model->primary_key], 'description', $rs['description']);
            $arr_result[] = $rs;
        }

        return $arr_result;
    }

    private function addAction()
    {
        $data = $this->getPostdata();
        $data[$this->module_model->primary_key] = $this->input->post($this->module_model->primary_key);
        $data["link_dir"] = $this->input->post("link_dir");

        $ckData["method"] = $data["method"];
        $ckData["object"] = $data["object"];
        $ckData["link_dir"] = $data["link_dir"];
        $ckData[$this->module_model->primary_key] = $data[$this->module_model->primary_key];
        $this->permission_model->sql_where = $ckData;
        $check_double = $this->permission_model->get_one();

        if (empty($check_double) === false) {
            $json["msg"] = self::$title_module . ' bestaat al!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        if (empty($data["description"]) === true) {
            $data["description"] = $data[$this->module_model->primary_key] . '.' . $data["link_dir"] . '.' . $data["object"] . '.' . $data["method"];
        }

        $insert_id = $this->permission_model->add($data);
        if ($insert_id > 0) {
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            $json["msg"] = self::$title_module . ' is toegevoegd';
            $json["status"] = "good";
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    private function editAction()
    {
        $data = $this->getPostdata();
        $id = $this->input->post($this->permission_model->primary_key);
        $rsdb = $this->permission_model->get_one_by_id($id);
        if (empty($rsdb) === false) {
            if (empty($data["description"]) === true) {
                $data["description"] = $rsdb[$this->module_model->primary_key] . '.' . $rsdb["link_dir"] . '.' . $rsdb["object"] . '.' . $rsdb["method"];
            }
            $this->permission_model->edit($id, $data);
            $json["msg"] = self::$title_module . ' is bijgewerkt';
            $json["status"] = "good";
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    private function getPostdata()
    {
        if ($this->input->post("del_id")) {
            $this->del();
        }
        $data = $this->permission_model->getPostdata();
        return $data;
    }
}
