<?php

class Redirect extends Site_Controller {

    private $using_view = true;

    public function index(int $sec = 10) {
        $this->load->database();
        $redirect_url = $this->input->get('redirect_url');
        $url = site_url();
        if (empty($redirect_url) === false) {
            $url = $redirect_url;
        }
        if ($this->using_view === false) {
            redirect($url);
        }
        $this->language_model->set_language();
        $data["url"] = $url;
        $data["sec"] = $sec;
        $data["title"] = lang('redirect_title');
        $this->view_layout("index", $data);
    }

}
