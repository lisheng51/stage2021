<?php

class Setup extends Site_Controller
{

    public function index()
    {
        if ($this->input->post()) {
            $this->indexAction();
        }

        if ($this->login_model->user_id() > 0) {
            redirect($this->access_check_model->redirect_url());
        }

        $dataLayout["title"] = "Setup";
        $dataLayout["module_url"] = $this->module_url;
        $data["h4"] = "Maak admin aan";
        $dataLayout["content"] = $this->load->view('site/setup/index', $data, true);
        $this->load->view('site/_global/layout', $dataLayout);
    }

    private function insertSqlFile()
    {
        $fileList = [];
        $arrSqlFile = directory_map(APPPATH . '/sql/');
        if (empty($arrSqlFile) === false) {
            foreach ($arrSqlFile as $name) {
                $fileList[] = basename($name, ".sql");
            }

            foreach ($fileList as $tableName) {
                if ($this->db->table_exists($tableName) === false) {
                    $templine = '';
                    $lines = file(APPPATH . "sql/" . $tableName . '.sql');
                    foreach ($lines as $line) {
                        if (substr($line, 0, 2) == '--' || $line == '') {
                            continue;
                        }
                        $templine .= $line;
                        if (substr(trim($line), -1, 1) == ';') {
                            $this->db->query($templine);
                            $templine = '';
                        }
                    }
                }
            }
        }
    }

    private function indexAction()
    {
        $this->load->database();
        $databasename = $this->db->database;
        $this->load->dbutil();
        if ($this->dbutil->database_exists($databasename) === false) {
            $this->load->dbforge();
            if ($this->dbforge->create_database($databasename) === false) {
                $json["msg"] = "Database bestaat nog niet!";
                $json["status"] = "error";
                exit(json_encode($json));
            }
        }
        $this->insertSqlFile();
        $rsdb = $this->user_model->get_one_by_id($this->user_model->secure_id);
        if (empty($rsdb) === false) {
            $json["msg"] = "Installatie is al geweest!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $this->db->truncate($this->login_model->table);
        $this->db->truncate($this->user_model->table);
        $this->db->truncate($this->applog_model->table);
        //$this->db->truncate($this->api_model->table);
        $this->db->truncate($this->api_log_model->table);
        $this->db->truncate($this->failcheck_model->table);
        $this->db->truncate($this->config_model->table);
        $this->db->truncate($this->sendmail_model->table);
        $this->db->truncate($this->message_model->table);
        $this->db->truncate($this->upload_model->table);
        $this->db->truncate($this->history_url_model->table);

        $email = $this->input->post("email");
        $password = $this->input->post("password");
        $this->ajaxck_model->ck_value('email', $email);
        $this->ajaxck_model->ck_value('password', $password);
        $data["emailaddress"] = $email;
        $data["is_active"] = 1;
        $data["display_info"] = 'Web master';
        $uid = $this->user_model->add($data);
        if ($uid > 0) {
            $this->makeUploadDir();
            $this->makeDir(config_item('cache_path'));
            $this->makeDir(config_item('sess_save_path'));
            $this->makeDir(config_item('log_path'));
            $data_login["user_id"] = $uid;
            $data_login["username"] = $email;
            $data_login["password_date"] = date('Y-m-d H:i:s');
            $data_login["password"] = password_hash($password, PASSWORD_DEFAULT);
            $this->login_model->add($data_login);
            $data_newpic["createdby"] = $uid;
            $this->user_model->edit($uid, $data_newpic);
            $this->upload_type_model->make_dir($uid);
            $json["type_done"] = "redirect";
            $json["redirect_url"] = login_url();
            $json["msg"] = "Installatie is oke!";
            $json["status"] = "good";
            exit(json_encode($json));
        }
    }

    private function makeDir(string $path = "")
    {
        if (empty($path) === false && is_dir($path) === false) {
            mkdir($path, 0755, true);
            file_put_contents($path . DIRECTORY_SEPARATOR . '.htaccess', 'deny from all');
        }

        if (empty($path) === false && is_dir($path) === true) {
            file_put_contents($path . DIRECTORY_SEPARATOR . '.htaccess', 'deny from all');
        }
    }

    private function makeUploadDir()
    {
        $val = $this->upload_type_model->fetchData(1, 'allowed_types');
        $path = FCPATH . $this->upload_model->root_folder;
        if (empty($path) === false && is_dir($path) === false) {
            mkdir($path, 0755, true);
            file_put_contents($path . DIRECTORY_SEPARATOR . '.htaccess', 'SetEnvIfNoCase Request_URI "\\.(' . $val . ')$" let_me_in
Order Deny,Allow
Deny from All
Allow from env=let_me_in');
        }

        if (empty($path) === false && is_dir($path) === true) {
            file_put_contents($path . DIRECTORY_SEPARATOR . '.htaccess', 'SetEnvIfNoCase Request_URI "\\.(' . $val . ')$" let_me_in
Order Deny,Allow
Deny from All
Allow from env=let_me_in');
        }
    }
}
