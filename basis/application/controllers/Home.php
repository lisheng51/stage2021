<?php

class Home extends MX_Controller
{

    public function index()
    {
        if ($this->login_model->user_id() > 0) {
            redirect($this->access_check_model->redirect_url());
        }
        $this->access_check_model->startRedirect(login_url());
    }
}
