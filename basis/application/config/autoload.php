<?php

$autoload['libraries'] = [
  'session',
  'user_agent',
  'encryption',
  'helper/F_algorithm',
  'helper/F_array',
  'helper/F_asset',
  'helper/F_datetime',
  'helper/F_number',
  'helper/F_string'
];
$autoload['helper'] = ['xml', 'text', 'url', 'file', 'string', 'download', 'date', 'language', 'form', 'directory', 'global', 'trait'];
$autoload['config'] = [];
$autoload['language'] = [];
$autoload['model'] = [
  'access/access_check_model',
  'access/failcheck_model',
  'access/failcheck_type_model',
  'access/login_history_model',
  'access/login_model',
  'access/visitor_model',
  'api/api_log_model',
  'api/api_model',
  'api/api_method_model',
  'helper/aes_model',
  'helper/bookmark_model',
  'helper/address_type_model',
  'helper/gender_model',
  'helper/vat_model',
  'helper/barcode_model',
  'helper/environment_model',
  'helper/contact_type_model',
  'mail/mail_config_model',
  'mail/mail_model',
  'mail/mail_template_model',
  'mail/sendmail_model',
  'mail/send_mail_file_model',
  'notification/breadcrumb_model',
  'notification/notify_template_model',
  'notification/message_model',
  'notification/telegram_model',
  'notification/ajaxck_model',
  'notification/applog_model',
  'notification/history_url_model',
  'permission/module_model',
  'permission/permission_group_model',
  'permission/permission_model',
  'permission/permission_group_type_model',
  'upload/upload_model',
  'upload/upload_type_model',
  'system/global_model',
  'system/config_model',
  'system/spamword_model',
  'system/language_model',
  'system/language_info_model',
  'user/user_model'
];
