<?php

class Overzicht extends Back_Controller
 {

    public function index()
    {

        $data = $this->fib_model->get_all();

        $data3 = [];
        foreach($data as $data1){

            $data1['fibonacci_step'] = json_decode($data1['fibonacci_step']);
            $data1['fibonacci_input'] = json_decode($data1['fibonacci_input']);
            array_push($data3, $data1);
        }

        
        $data['data'] = $data3;
        $this->view_layout('overzicht', $data);
    }

    // search fibonacci by date
    public function searchbyDate()
    {
        $reportrange_end = $this->input->post("reportrange_end");
        $reportrange_start = $this->input->post("reportrange_start");
        if (empty($reportrange_start) === false && empty($reportrange_end) === true) {
            $data_where[] = [$this->fib_model->table . ".datetime >=" => date_format(date_create(trim($reportrange_start)), 'Y-m-d H:i:s')];
        }

        if (empty($reportrange_end) === false && empty($reportrange_start) === true) {
            $data_where[] = [$this->fib_model->table . ".datetime <=" => date_format(date_create(trim($reportrange_end)), 'Y-m-d H:i:s')];
        }

        if (empty($reportrange_start) === false && empty($reportrange_end) === false) {
            $data_where[] = [$this->fib_model->table . ".fibonacci_date >=" => date_format(date_create(trim($reportrange_start)), 'Y-m-d H:i:s')];
            $data_where[] = [$this->fib_model->table . ".fibonacci_date <=" => date_format(date_create(trim($reportrange_end)), 'Y-m-d H:i:s')];
        }

        $this->fib_model->setSqlWhere($data_where);
        $total = $this->fib_model->get_total();
        $data = $this->fib_model->get_all();        




        $data3 = [];
        foreach($data as $data1){
            $data1['fibonacci_step'] = json_decode($data1['fibonacci_step']);
            $data1['fibonacci_input'] = json_decode($data1['fibonacci_input']);
            array_push($data3, $data1);
        }
        $data['data'] = $data3;

        $data3 = [];
        $result = $this->view_layout_return("overzicht_content", $data);
        $output["result"] = $result;

        exit(json_encode($output));

    }   
}


?>