<div class="card m-5">
    <div class="card-header">
        <p> Fibonacci </p>

    </div>
    
    <table class="table">
        <thead>
        <form method="post" id="send"> 
            <input class="form-control" type="date" placeholder="Search" name="reportrange_start" aria-label="Search">
            <input class="form-control" type="date" placeholder="Search" name="reportrange_end" aria-label="Search">
            <?php echo add_csrf_value(); ?>
            <input type="submit"> 
        </form>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Input - 1</th>
                <th scope="col">Input - 2</th>
                <th scope="col">Fibonacci stap - 1</th>
                <th scope="col">Fibonacci stap - 2</th>
                <th scope="col">Fibonacci stap - 3</th>
                <th scope="col">Date</th>
            </tr>
        </thead>
        <tbody id ="ajax_search_content">
            <?php foreach($data as $data){ 
                // $steps = json_decode($data['fibonacci_step']);
                // $input = json_decode($data['fibonacci_input']);
                ?>
                
            <tr>
                <th scope="row"> <?php echo $data['fibonacci_id'] ?></th>
                <td ><?php echo $data['fibonacci_input']->input1 ?></td>
                <td style ="max-width: 30px; word-wrap: break-word;"><?php echo $data['fibonacci_input']->input2 ?></td>
                <td style ="max-width: 30px; word-wrap: break-word;"class="w-25"><?php echo implode(",", $data['fibonacci_step']->step1) ?></td>
                <td style ="max-width: 30px; word-wrap: break-word;" class="w-25"><?php echo implode(",", $data['fibonacci_step']->step2) ?></td>
                <td style ="max-width: 30px; word-wrap: break-word;" class="w-25"><?php echo $data['fibonacci_step']->step3 ?></td>
                <td style ="max-width: 30px; word-wrap: break-word;" class="w-25"><?php echo $data['fibonacci_date'] ?></td>
            </tr>
            <?php  } ?>
        </tbody>
    </table>

</div>


<script>
    let ajaxurl = module_url + 'back/overzicht/searchByDate';
    $("form#send").submit(function(e) {
        e.preventDefault();
        ajax_form_search($(this), ajaxurl);
    });


</script>