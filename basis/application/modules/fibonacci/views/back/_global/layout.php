<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $title ?></title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
   <?php  echo F_asset::jquery(); echo F_asset::bootstrap();   echo F_asset::sweetalert2();
    echo script_tag(site_url("asset/index/" . $module_url)); 
    echo script_tag(sys_asset_url("js/function.js")); 
   ?> 
</head>



<body id="page-top">
<?php echo $content ?>
</body>

</html>