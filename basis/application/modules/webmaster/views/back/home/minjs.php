<div class="row">
    <div class="col-md-12">
        <?php echo $this->global_model->alert() ?>
    </div>
</div>


<div class="card">
    <div class="card-header">Zip bestand upload</div>
    <div class="card-body">
        <form enctype="multipart/form-data" method="POST" id="send_multipart">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Bestand</label>
                        <input type="file" class="form-control" accept=".zip" required name="userFiles"/>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <?php echo add_csrf_value(); ?>
                        <input type="hidden" name="yes" value="yes">
                        <button class="btn btn-success" type="submit" id="submit_button">Download</button>    
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>




