<?php

class Home extends Back_Controller
{

    private $upload_folder = "minjs";

    private function __commentToArray(string $DocComment = ''): array
    {
        $sDocComment = preg_replace("/(^[\\s]*\\/\\*\\*)
                 |(^[\\s]\\*\\/)
                 |(^[\\s]*\\*?\\s)
                 |(^[\\s]*)
                 |(^[\\t]*)/ixm", "", $DocComment);

        $sDocComment = str_replace("\r", "", $sDocComment);
        $sDocComment = preg_replace("/([\\t])+/", "\t", $sDocComment);
        $aDocCommentLines = explode("\n", $sDocComment);

        return array_filter($aDocCommentLines);
    }

    private function getData()
    {
        $app_path = APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'helper' . DIRECTORY_SEPARATOR;
        $files = directory_map($app_path, 1);

        $matches = array_filter($files, function ($var) {
            return preg_match("/F_(.+?).php/is", $var);
        });

        $removeMethods = ["__construct", "__get"];
        $arrPathData = [];
        foreach ($matches as $value) {
            require_once($app_path . $value);
            $class = basename($value, '.php');

            // Get Reflection Class
            $classReflection = new ReflectionClass($class);
            $arrComments = $this->__commentToArray($classReflection->getDocComment());

            $arrMethod = get_class_methods($class);
            //$arrPathData[$class]["class " . $class] = $arrComments;
            foreach ($arrMethod as $method) {
                if (in_array($method, $removeMethods, true) === true) {
                    continue;
                }

                $methodReflection = $classReflection->getMethod($method);
                $arrComments = $this->__commentToArray($methodReflection->getDocComment());

                $string = "(";
                $paramString = [];
                $paramReturnString = "";
                foreach ($arrComments as $comment) {
                    if (strpos($comment, '@param ') !== false) {
                        $arrParam = explode("@param ", $comment);
                        $paramString[] = end($arrParam);
                    }

                    if (strpos($comment, '@return ') !== false) {
                        $arrParamReturn = explode("@return ", $comment);
                        $paramReturnString = end($arrParamReturn);
                    }
                }
                $string .= implode(", ", $paramString) . ')';
                $key = '<b>' . $method . '</b>' . $string . ': ' . $paramReturnString;
                $arrPathData[$class][$key] = $arrComments;
            }
        }

        return $arrPathData;
    }

    public function minJs()
    {
        if ($this->input->post()) {
            $this->minJsAction();
        }
        $data['breadcrumb'] = "";
        $data["title"] = 'Min js maken';
        $data["event_result_box"] = "";
        $this->view_layout("minjs", $data);
    }

    private function minJsAction()
    {
        if ($_FILES['userFiles']['error'] === 4) {
            $json["msg"] = "Geen bestand gevonden!";
            $json["status"] = "error";
            $this->ajaxck_model->add_session($json);
            redirect($this->controller_url);
        }

        $fileData = explode('.', $_FILES['userFiles']["name"]);
        $filename_to_action = url_title($fileData[0]) . '.' . $fileData[1];

        $path = $this->upload_model->root_folder . DIRECTORY_SEPARATOR . $this->upload_folder . DIRECTORY_SEPARATOR;
        $config['upload_path'] = $path;
        $config['allowed_types'] = "zip";
        $config['overwrite'] = true;
        $this->load->library('upload');
        $this->upload->initialize($config);
        if ($this->upload->do_upload('userFiles') === false) {
            $json["msg"] = $this->upload->display_errors();
            $json["status"] = "warning";
            $this->ajaxck_model->add_session($json);
            redirect($this->controller_url);
        }

        $zip = new ZipArchive;
        $res = $zip->open($path . $filename_to_action);

        $filename = [];
        if ($res === true) {
            for ($i = 0; $i < $zip->numFiles; $i++) {
                $model = FCPATH . $path . $zip->getNameIndex($i);

                $path_parts = pathinfo($model);

                if (isset($path_parts['extension'])) {
                    $extension = strtolower($path_parts['extension']);
                    if ($extension !== 'js') {
                        continue;
                    }
                    $filename[] = $model;
                }
            }


            $zip->extractTo($path);
            $zip->close();
        }

        if (empty($filename) === true) {
            $json["msg"] = "Geen JS bestand gevonden!";
            $json["status"] = "warning";
            $this->ajaxck_model->add_session($json);
            redirect($this->controller_url);
        }

        // foreach ($filename as $value) {
        //     $minifier = new MatthiasMullie\Minify\JS($value);
        //     $minifier->minify($value);
        // }

        $arrfolder = explode($path, $filename[0]);
        $arrfolder_zipname = explode('/', $arrfolder[1]);
        $this->load->library('zip');
        $this->zip->read_dir($path . $arrfolder_zipname[0]);
        @unlink($path . $filename_to_action);
        delete_files($path . $arrfolder_zipname[0]);
        $this->zip->download($filename_to_action);
    }

    private function multi2array(array $arr_model = []): array
    {
        static $arr_result = [];
        if (empty($arr_model) === true) {
            return $arr_result;
        }
        foreach ($arr_model as $model) {
            if (is_array($model) === true) {
                $this->multi2array($model);
            } else {
                $path_parts = pathinfo($model);
                $extension = strtolower($path_parts['extension']);
                if ($extension === 'php') {
                    $arr_result[] = $path_parts['filename'];
                }
            }
        }
        return $arr_result;
    }

    private function get_list(): array
    {
        $array_sys_model = directory_map(APPPATH . 'models');
        $arr_end = $this->multi2array($array_sys_model);

        $array_sub = directory_map(APPPATH . 'modules');
        if (empty($array_sub) === true) {
            return $arr_end;
        }
        $modules = array_keys($array_sub);
        foreach ($modules as $module) {
            $arr = directory_map(APPPATH . 'modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . 'models');
            if (empty($arr) === false) {
                $arr_end = $this->multi2array($arr);
            }
        }
        return $arr_end;
    }

    private function basic(): string
    {
        $string = '/**
* @property CI_DB_query_builder $db
* @property CI_DB_forge $dbforge
* @property CI_Benchmark $benchmark
* @property CI_Calendar $calendar
* @property CI_Config $config
* @property CI_Controller $controller
* @property CI_Encryption $encrypt
* @property CI_Ftp $ftp
* @property CI_Input $input
* @property CI_Loader $load
* @property CI_Model $model
* @property CI_Output $output
* @property CI_Pagination $pagination
* @property CI_Session $session
* @property CI_Table $table
* @property CI_URI $uri
* @property CI_User_agent $user_agent';
        return $string;
    }

    public function index()
    {
        $listdb = array_unique($this->get_list());
        $basic = $this->basic();
        $string = "<?php\r\n" . $basic;
        $customer_string = "\r\n";
        //dump($listdb); #
        if (empty($listdb) === false) {
            foreach ($listdb as $model_file) {
                $model_name = lcfirst($model_file);
                $customer_string .= "* @property $model_file $$model_name\r\n";
            }
            $customer_string .= "*/\r\n\r\n";
        }

        $string .= $customer_string;
        $string .= "class MX_Controller {}\r\n\r\n";
        $string .= $basic . $customer_string;
        $string .= "class CI_Model {}\r\n\r\n";

        $data["result"] = "Bestand is niet vernieuwd!";
        if (write_file(FCPATH . "CI_Autocomplete.php", $string) === true) {
            $data["result"] = "Bestand is vernieuwd!";
        }

        $data["title"] = 'Auto complete bestand';
        $data['breadcrumb'] = "";
        $data["listdb"] = $this->getData();
        $data["event_result_box"] = "";
        $this->view_layout("index", $data);
    }
}
