<?php
$lang["access_submit_button_text"] = "Login";
$lang["access_username_label_text"] = "Username";
$lang["access_password_label_text"] = "Password...";
$lang["access_forget_password_text"] = "Forget password ?";
$lang["access_login_title"] = "Login";
$lang["access_emailaddress_text"] = "Email";
$lang["access_code_send_text"] = "Your access code has been sent to your mailbox, enter your code";
$lang["access_code_label_text"] = "Access code";
$lang["access_login_with_qrcode"] = "Login with QR code";
