<?php
$lang["access_submit_button_text"] = "Inloggen";
$lang["access_username_label_text"] = "Gebruikersnaam";
$lang["access_password_label_text"] = "Wachtwoord...";
$lang["access_forget_password_text"] = "Wachtwoord vergeten ?";
$lang["access_login_title"] = "Inloggen";
$lang["access_emailaddress_text"] = "Emailadres";
$lang["access_code_send_text"] = "Uw toegangscode is verzonden, Voert deze code in";
$lang["access_code_label_text"] = "Toegangscode";
$lang["access_login_with_qrcode"] = "Inloggen met QR code";
$lang["access_2facode_label_text"] = "2FA code";
