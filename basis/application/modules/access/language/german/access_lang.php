<?php
$lang["access_submit_button_text"] = "Einloggen";
$lang["access_username_label_text"] = "Benutzername";
$lang["access_password_label_text"] = "Passwort...";
$lang["access_forget_password_text"] = "Passwort vergessen ?";
$lang["access_login_title"] = "Einloggen";
$lang["access_emailaddress_text"] = "Email";
$lang["access_code_send_text"] = "Ihr Zugangscode wurde an Ihre Mailbox gesendet. Geben Sie Ihren Code ein";
$lang["access_code_label_text"] = "Zugangscode";
$lang["access_login_with_qrcode"] = "Einloggen with QR code";
