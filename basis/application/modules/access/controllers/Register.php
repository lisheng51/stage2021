<?php

class Register extends Site_Controller
{

    private $tem_key = "register_form_send";

    public function index()
    {
        $this->access_check_model->ipAllow();
        if ($this->input->post()) {
            $this->add_action();
        }

        if ($this->login_model->user_id() > 0) {
            redirect($this->access_check_model->redirect_url());
        }

        $data["title"] = "Welkom bij registreren";
        $data["h4"] = "Registreren";
        $data["emailaddress"] = $this->input->get("emailaddress");
        $this->view_layout("index", $data);
    }

    public function active()
    {
        $key = rawurldecode($this->input->get('hashkey'));
        $arr_keys = explode("#_#", $this->encryption->decrypt($key));
        if (empty($arr_keys[0]) === true) {
            redirect(error_url());
        }

        $user_db = $this->login_model->get_one_by_username_email($arr_keys[1]);
        if (empty($user_db) === true) {
            $this->session->set_flashdata('page_error_type', "user_no_find");
            redirect(error_url());
        }

        if ($user_db["is_active"] > 0) {
            $this->session->set_flashdata('page_error_type', "is_active");
            redirect(error_url());
        }
        $this->login_model->update_password_reset_date($user_db["user_id"]);
        $encrypted_string = $this->encryption->encrypt($user_db["user_id"] . "#_#" . $user_db["emailaddress"]);
        $hashkey = rawurlencode($encrypted_string);
        redirect("access/password_reset/reset?hashkey=$hashkey");
    }

    private function get_postdata()
    {
        $arr_input = $this->input->post('user');
        $data["emailaddress"] = strtolower($arr_input["username"]);
        $data["permission_group_ids"] = 2;
        $this->ajaxck_model->ck_value('email', $data["emailaddress"]);
        return $data;
    }

    private function send_active_mail(array $arr_user = [])
    {
        if (empty($arr_user) === true) {
            return;
        }
        $this->sendmail_model->user_active($arr_user);
        $json["msg"] = "Bedankt voor het aanmelden, een activatiemail is naar uw mail gestuurd!";
        $json["status"] = "good";
        $this->session->set_tempdata($this->tem_key, $this->input->ip_address());
        exit(json_encode($json));
    }

    private function add_action()
    {
        $this->ajaxck_model->to_much_send_mail($this->tem_key);
        $data = $this->get_postdata();

        $arr_user = $this->login_model->get_one_by_username_email($data["emailaddress"]);
        if (empty($arr_user) === false && $arr_user["is_active"] > 0) {
            $json["msg"] = "Het emailadres bestaat al!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        if (empty($arr_user) === false && $arr_user["is_active"] == 0) {
            $this->send_active_mail($arr_user);
        }

        $uid = $this->user_model->add($data);
        if ($uid > 0) {
            $data_login["user_id"] = $uid;
            $data_login["username"] = $data["emailaddress"];
            $data_login["redirect_url"] = '';
            $this->login_model->add($data_login);
            $data_newpic["createdby"] = $uid;
            $this->user_model->edit($uid, $data_newpic);
            $this->upload_type_model->make_dir($uid);
            $this->send_active_mail($this->user_model->get_one_by_id($uid));
        }
    }
}
