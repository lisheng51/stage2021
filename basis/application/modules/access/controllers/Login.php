<?php

class Login extends Site_Controller
{

    public function direct()
    {
        $key = rawurldecode($this->input->get('hashkey'));
        $arr_keys = explode("#_#", $this->encryption->decrypt($key));
        if (empty($arr_keys[0]) === true) {
            redirect(error_url());
        }

        $userId = $arr_keys[0];
        $webapp_access_code = $arr_keys[1];

        $arr_rs = $this->user_model->get_one_by_id($userId);

        if (empty($arr_rs) === true || empty($arr_rs["access_code"]) === true || $arr_rs["is_active"] == 0 || $arr_rs["is_del"] == 1) {
            $this->session->set_flashdata('page_error_type', "user_no_find");
            redirect(error_url());
        }

        if (date("Y-m-d H:i:s") > $arr_rs["access_code_date"]) {
            $this->session->set_flashdata('page_error_type', "timeout");
            redirect(error_url());
        }

        if ($webapp_access_code !== $arr_rs["access_code"]) {
            $this->session->set_flashdata('page_error_type', "timeout");
            redirect(error_url());
        }

        $this->login_model->login_user_data = $arr_rs;
        $this->login_model->add_user_session();
        redirect($this->access_check_model->redirect_url());
    }

    public function directEasy()
    {
        $key = rawurldecode($this->input->get('key'));
        $arrTokenValue = json_decode($this->encryption->decrypt($key), true);
        $userId = $arrTokenValue[$this->user_model->primary_key];
        if ($userId <= 0) {
            redirect(login_url());
        }

        $token_expires = $arrTokenValue['datetime'] ?? null;
        if (date("Y-m-d H:i:s") > $token_expires) {
            $this->session->set_flashdata('page_error_type', "timeout");
            redirect(error_url());
        }


        $arr_rs = $this->user_model->get_one_by_id($userId);

        if (empty($arr_rs) === true || $arr_rs["is_active"] == 0 || $arr_rs["is_del"] == 1) {
            $this->session->set_flashdata('page_error_type', "user_no_find");
            redirect(error_url());
        }

        $this->login_model->login_user_data = $arr_rs;
        $this->login_model->add_user_session();
        redirect($this->access_check_model->redirect_url());
    }

    public function active()
    {
        $key = rawurldecode($this->input->get('hashkey'));
        $arr_keys = explode("#_#", $this->encryption->decrypt($key));
        if (empty($arr_keys[0]) === true) {
            redirect(error_url());
        }



        $user_db = $this->login_model->get_one_by_username_email($arr_keys[1]);
        if (empty($user_db) === true) {
            $this->session->set_flashdata('page_error_type', "user_no_find");
            redirect(error_url());
        }

        if ($user_db["is_active"] > 0) {
            $this->session->set_flashdata('page_error_type', "is_active");
            redirect(error_url());
        }
        $this->login_model->update_password_reset_date($user_db["user_id"]);
        $encrypted_string = $this->encryption->encrypt($user_db["user_id"] . "#_#" . $user_db["emailaddress"]);
        $hashkey = rawurlencode($encrypted_string);
        redirect("access/password_reset/reset?hashkey=$hashkey");
    }

    public function index_dev()
    {
        $this->access_check_model->ipAllow();


        if ($this->input->post()) {
            $this->index_dev_action();
        }

        if ($this->login_model->user_id() > 0) {
            redirect($this->access_check_model->redirect_url());
        }

        $data["title"] = lang('access_login_title');
        $data["select_user"] = $this->user_model->select();
        $this->view_layout('index_dev', $data);
    }

    private function index_dev_action()
    {
        $user_id = $this->input->post($this->user_model->primary_key) ?? 0;
        $arr_user = $this->user_model->get_one_by_id($user_id);
        if (empty($arr_user) === true) {
            $json["msg"] = "Verkeerde gebruikersnaam!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        if ($arr_user["is_active"] == 0 || $arr_user["is_del"] == 1) {
            $json["msg"] = "Verkeerde gebruikersnaam!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $this->login_model->login_user_data = $arr_user;
        $this->login_model->add_user_session();
        $json["msg"] = "Even geduld aub... U wordt automatisch doorgeschakeld";
        $json["status"] = "good";
        $json["type_done"] = "redirect";
        $json["redirect_url"] = $this->access_check_model->redirect_url();
        exit(json_encode($json));
    }

    public function index()
    {
        if ($this->input->post()) {
            $this->index_action();
        }

        if ($this->login_model->user_id() > 0) {
            redirect($this->access_check_model->redirect_url());
        }
        $data["title"] = lang('access_login_title');
        $this->view_layout("index", $data);
    }

    private function send_access_code($arr_user = [], $password = null)
    {
        if (empty($arr_user) === true) {
            return;
        }
        $this->sendmail_model->access_code_login($arr_user, $password);
        $json["msg"] = lang("access_code_send_text");
        $json["status"] = "good";
        $json["type_done"] = "show_access_code_input";
        $json["input_html"] = $this->view_layout_return('access_code_input');
        exit(json_encode($json));
    }

    private function index_action()
    {
        $arr_input = $this->input->post('user');
        $username = $arr_input["username"];
        $password = $arr_input["password"];

        $this->ajaxck_model->ck_value('Gebruikersnaam', $username);
        $this->ajaxck_model->ck_value('Wachtwoord', $password);


        $this->ajaxck_model->failcheck($this->failcheck_type_model->user_login_id);
        $json = [];

        $arr_user = $this->login_model->get_one($username, $password);
        if (empty($arr_user) === true) {
            $this->failcheck_model->update($this->failcheck_type_model->user_login_id);
            $json["msg"] = "Verkeerde gebruikersnaam of wachtwoord!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $onecode = $this->input->post('webapp_one_code');
        if ($this->login_model->check2FA($arr_user, $onecode) === false) {
            $this->failcheck_model->update($this->failcheck_type_model->user_login_id);
            $json["msg"] = lang('access_2facode_label_text') . " is niet juist!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        if ($this->login_model->check_password_date($arr_user["password_date"]) === false) {
            $json["msg"] = "Uw wachtwoord is verlopen, via wachtwoord vergeten kunt u nieuw wachtwoord aanmaken !";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $webapp_access_code = $this->input->post('webapp_access_code');
        if (empty($webapp_access_code) === true && $arr_user["with_access_code"] > 0) {
            $this->send_access_code($arr_user, $password);
        }

        if (empty($webapp_access_code) === false && $arr_user["with_access_code"] > 0) {
            $this->login_model->set_access_code($webapp_access_code);
        }

        if ($this->login_model->check_data($username, $password) === true) {
            $this->login_model->add_user_session();
            $this->failcheck_model->reset($this->failcheck_type_model->user_login_id);
            $json["msg"] = "Even geduld aub... U wordt automatisch doorgeschakeld";
            $json["status"] = "good";
            $json["type_done"] = "redirect";
            $json["redirect_url"] = $this->access_check_model->redirect_url();
            exit(json_encode($json));
        }

        $json["msg"] = "Verkeerde toegangscode!";
        $json["status"] = "error";
        exit(json_encode($json));
    }

    public function logout()
    {
        $this->login_model->logout();
        redirect(login_url());
    }
}
