<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg-12">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4"><?php echo $h4 ?></h1>
                    </div>
                    <form method="POST" id="send" class="user">
                        <div class="form-group">
                            <input type="email" name="user[username]" autofocus required placeholder="Emailadres..." class="form-control form-control-user">
                        </div>
                        <?php echo add_csrf_value(); ?>
                        <button type="submit" class="btn btn-primary  btn-user btn-block" id="submit_button"><?php echo lang('submit_button_text') ?></button>
                    </form>
                    <hr>
                    <div class="text-center">
                        <a href="<?php echo site_url("access/Password_reset") ?>" class="small">Wachtwoord vergeten ?</a>
                    </div>
                    <div class="text-center">
                        <a href="<?php echo login_url() ?>" class="small"><?php echo lang('access_login_title') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>