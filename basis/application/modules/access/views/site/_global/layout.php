<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php echo $title ?></title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <?php
        echo F_asset::favicon();
        echo F_asset::fontawesome();
        echo F_asset::jquery();
        echo F_asset::bootstrap();
        echo F_asset::sweetalert2();
        echo script_tag(base_url("asset"));
        echo script_tag(sys_asset_url("js/function.js"));
        echo link_tag(module_asset_url("css/access.css"));
        echo script_tag(module_asset_url("js/access.js"));
        ?>
    </head>
    <body class="bg-gradient-dark">
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-12 col-md-9">
                    <?php echo $content; ?>
                </div>
            </div>

            <!--                        <div class="row justify-content-center">
                                        <div class="col-xl-10 col-lg-12 col-md-9">
                                            <a href="<?php echo current_url() . '?language=dutch' ?>">Nederlands</a>
                                            <a href="<?php echo current_url() . '?language=german' ?>">Duits</a>
                                            <a href="<?php echo current_url() . '?language=english' ?>">Engels</a>
                                        </div>
                                    </div>-->
        </div>
    </body>
</html>