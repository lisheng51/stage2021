<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg-12">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4"><?php echo lang('access_login_title') ?></h1>
                    </div>
                    <form id="send" class="user">
                        <div class="form-group">
                            <input type="email" name="user[username]" autofocus required placeholder="<?php echo lang('access_username_label_text') ?>" class="form-control form-control-user">
                        </div>
                        <div class="form-group">
                            <input type="password" name="user[password]" required placeholder="<?php echo lang('access_password_label_text') ?>" class="form-control form-control-user">
                        </div>
                        <div class="form-group">
                            <input type="text" name="webapp_one_code" maxlength="6" class="form-control form-control-user" value="" placeholder="<?php echo lang('access_2facode_label_text') ?>" />
                        </div>
                        <div class="form-group">
                            <div class="show_access_code_input"></div>
                        </div>
                        <?php echo add_csrf_value(); ?>
                        <button type="submit" class="btn btn-primary btn-user btn-block" id="submit_button"><?php echo lang('access_submit_button_text') ?></button>
                    </form>
                    <hr>
                    <div class="text-center">
                        <a href="<?php echo site_url("access/Password_reset") ?>" class="small">Wachtwoord vergeten ?</a>
                    </div>
                    <?php if (ENVIRONMENT === 'development'): ?>
                        <div class="text-center">
                            <a href="<?php echo login_url("/index_dev") ?>" class="small">Ik ben webmaster</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>