$(function () {
    send_form();
});


function send_form()
{
    var submit_button = $('#submit_button');
    var submit_button_text = submit_button.html();
    $("form#send").submit(function (e) {
        if ($('input#webapp_access_code').length)
        {
            if ($('#webapp_access_code').val().length === 0) {
                handle_info_box("error", "Uw toegangscode is leeg");
                return false;
            }

        }

        $.ajax({
            data: $(this).serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                submit_button.attr("disabled", "disabled");
                submit_button.html('<i class="fa fa-spinner fa-pulse"></i> ');
            }
        }).done(function (json) {
            if (json.type_done) {
                switch (json.type_done) {
                    case 'redirect':
                        if (json.redirect_url) {
                            window.location.href = json.redirect_url;
                        }
                        break;
                    case 'show_access_code_input':
                        if (json.input_html) {
                            $('.show_access_code_input').html(json.input_html);
                        }
                        break;
                    default:
                        break;
                }
            }
            handle_info_box(json.status, json.msg);
        }).always(function () {
            submit_button.html(submit_button_text);
            submit_button.removeAttr("disabled");
        }).fail(function (jqxhr) {
            message_ajax_fail_show(jqxhr);
        });
        e.preventDefault();
    });
}