<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title><?php echo $title ?></title>
    <?php
    echo F_asset::favicon();
    echo F_asset::fontawesome();
    echo F_asset::jquery();
    echo F_asset::bootstrap();
    echo script_tag(site_url("asset/index/" . $module_url));
    ?>
</head>

<body>
    <div class="container-fluid">
        <div class="card mt-3">
            <?php echo $content; ?>
        </div>
    </div>
</body>

</html>