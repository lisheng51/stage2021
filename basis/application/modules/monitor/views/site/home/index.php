<div class="progress" style="height: 5px;">
    <div class="progress-bar progress-bar-striped progress-bar-animated"></div>
</div>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">

    <div class="carousel-item" data-interval="20000">
            <!-- Kandidaat In behandeling Aangeboden Intake Afwijzingen Succes -->
            <div class="card">
                <div class="card-body">
                    <div id="sbi_content"></div>
                </div>
            </div>
        </div>

        <div class="carousel-item" data-interval="10000">
        
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <div class="card">
                                <h5 class="card-header">KVA</h5>
                                <div class="card-body">
                                    <p>Vandaag: <span id="result_kva_1_today">0</span></p>
                                    <p>Totaal: <span id="result_kva_1_all">0</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mt-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <div class="card">
                                <h5 class="card-header">Uniekdating</h5>
                                <div class="card-body">
                                    <p>Vandaag (niet verzonden): <span id="result_mail_today_to_send">0</span></p>
                                    <p>Vandaag (verzonden): <span id="result_mail_today_is_send">0</span></p>
                                    <p>Totaal: <span id="result_mail_total">0</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mt-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <h5 class="card-header">Logipoort</h5>
                                <div class="card-body">
                                    <p>Aantal klanten: <span class="badge badge-success" id="logi_total_company">0</span></p>
                                    <p>Aantal keer opgestart (Vandaag): <span class="badge badge-info" id="logi_total_start_today">0</span></p>
                                    <p>Aantal keer opgestart (Totaal): <span class="badge badge-info" id="logi_total_start">0</span> (sinds 03-06-2019)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="carousel-item" data-interval="10000">
            <!-- Server status en Concurrent connection-->
            <div id="serverStatus_content"></div>
        </div>

        <div class="carousel-item active" data-interval="20000">
            <!-- Server status en Concurrent connection-->
            <div id="calendar_content"></div>
        </div>
    </div>
</div>

<script>
    $.fn.carousel.Constructor.prototype.cycle = function(event) {
        if (!event) {
            this._isPaused = false;
        }

        if (this._interval) {
            clearInterval(this._interval)
            this._interval = null;
        }

        if (this._config.interval && !this._isPaused) {
            var $ele = $('.carousel-item-next');
            var newInterval = $ele.data('interval') || this._config.interval;
            var sec = newInterval / 100;
            var width = 0;
            var width_sec = 100 / sec;

            function doSomething() {
                width = width + width_sec;
                sec = sec - 1;
                if (sec >= 0) {
                    $(".progress-bar").css('width', width + "%");
                    //$(".progress-bar").text(sec/10);
                    //$(".progress-bar").text(width.toFixed(0) + '%');
                    setTimeout(doSomething, 100);
                }
            }

            setTimeout(doSomething, 100);
            this._interval = setInterval(
                (document.visibilityState ? this.nextWhenVisible : this.next).bind(this),
                newInterval + 1000
            );

        }
    };
</script>


<?php echo script_tag(module_asset_url("js/hosting.js"));
echo script_tag(module_asset_url("js/home.js"));
echo script_tag(module_asset_url("js/ud.js"));
echo script_tag(module_asset_url("js/kva.js"));
