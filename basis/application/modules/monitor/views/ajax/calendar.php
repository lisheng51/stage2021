<?php
$len = count($listdb);
$listdb1 = array_slice($listdb, 0, 16);
$listdb2 = array_slice($listdb, 17, $len);
?>

<div class="card">
    <div class="card-header">Agenda <?php echo date("Y") ?></div>
    <div class="card-body">


        <div class="row">
            <div class="col-6">


                <table class="table table-bordered table-hover float-left w-40">
                    <thead>
                        <tr>
                            <th>Datum</th>
                            <th>Titel</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($listdb1 as $value) : ?>
                            <tr>
                                <td><?php echo $value["date"]; ?></td>
                                <td><?php echo $value["title"]; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
             
            </div>

            <div class="col-6">


                <table class="table table-bordered table-hover w-40">
                    <thead>
                        <tr>
                            <th>Datum</th>
                            <th>Titel</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($listdb2 as $value) : ?>
                            <tr>
                                <td><?php echo $value["date"]; ?></td>
                                <td><?php echo $value["title"]; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>