<div class="card">
    <h5 class="card-header"><?php echo $title ?></h5>
    <div class="card-body">
        <div class="row">
            <?php foreach ($listdb as $rs) : ?>
                <div class="col-12">
                    <div class="card mt-1">
                        <h5 class="card-header"><?php echo $rs['date'] ?> <?php echo $rs['title'] ?></h5>
                        <div class="card-body">
                            <p><?php echo $rs['description'] ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>   
        </div>
    </div>
</div>



