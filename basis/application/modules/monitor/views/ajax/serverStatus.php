<div class="row">
    <?php foreach ($listdb as $rs) : ?>
        <div class="col-6">
            <div class="card mt-3">
                <h5 class="card-header"><?php echo $rs['appName'] ?></h5>
                <div class="card-body">
                    <h1>Server status <?php echo $rs['appStatus'] ?></h1>
                </div>
            </div>
        </div>
    <?php endforeach; ?>   
</div>