<?php

class Global_m extends CI_Model
{

    public function curl($url = "", $params = [], $methode = "POST", $CURLOPT_HTTPHEADER = [])
    {
        if (empty($url) === true || filter_var($url, FILTER_VALIDATE_URL) === false) {
            return;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        switch ($methode) {
            case "POST":
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                break;
            default:
                break;
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $CURLOPT_HTTPHEADER);
        $response = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpCode == 404) {
            return;
        }

        if (curl_errno($ch)) {
            return;
        }
        curl_close($ch);
        if ($response === false || empty($response) === true) {
            return;
        }
        return $response;
    }

    public function filterError(array $listdb = [])
    {
        $arrResult = $arr = $temp = [];
        foreach ($listdb as $value) {
            preg_match('/log-(.+?).php/i', $value, $arr);
            if (empty($arr) === true) {
                continue;
            }
            $filename = end($arr);
            $temp["active"] = '';
            $temp["date"] = $filename;
            if ($filename === date('Y-m-d')) {
                $temp["active"] = 'list-group-item-info';
            }
            $arrResult[] = $temp;
        }
        usort($arrResult, function ($b, $a) {
            return strtotime($a["date"]) - strtotime($b["date"]);
        });

        return array_slice($arrResult, 0, 10);
    }
}
