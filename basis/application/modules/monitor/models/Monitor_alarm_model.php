<?php

class Monitor_alarm_model extends MY_Model
{

    public $table = "monitor_alarm";
    public $primary_key = "id";
    public $select_order_by = [];

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'id#desc' => 'ID (aflopend)',
            'ck_udate#desc' => 'Tijd (aflopend)',
            'ck_udate#asc' => 'Tijd (oplopend)',
        ];
    }

    public function update(array $postData = []): int
    {
        if (empty($postData) === true) {
            return 0;
        }
        $arrFind = $this->get_one_by_field("ck_udate", $postData["ck_udate"]);
        if (empty($arrFind) === true) {
            return $this->add($postData);
        }
        return 0;
    }
}
