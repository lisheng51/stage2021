<?php

class Ud_processlist_m extends CI_Model {

    public $table = "monitor_ud_processlist";
    public $primary_key = "pid";
    public $sql_where = [];
    public $sql_where_in = [];
    public $sql_like = [];
    public $sql_or_like = [];
    public $sql_order_by = [];

    public function add(int $total = 0) {
        if ($total <= 0) {
            return;
        }
        $data["total"] = $total;
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function del(int $id = 0) {
        $this->db->from($this->table)->where($this->primary_key, $id)->limit(1)->delete();
    }

    public function select_max() {
        $this->db->from($this->table);
        $this->db->select_max("total");
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }
        $query = $this->db->get();
        $array = $query->row_array();
        return $array["total"];
    }

    public function get_all(): array {
        $limit = $this->get_total();
        return $this->get_list($limit);
    }

    public function get_total(): int {
        $this->db->select($this->primary_key);
        $this->db->from($this->table);
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_or_like) === false) {
            foreach ($this->sql_or_like as $field => $value) {
                $this->db->or_like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        return $this->db->count_all_results();
    }

    public function get_list(int $limit = 1, int $page = 0): array {
        $this->db->from($this->table);
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_or_like) === false) {
            foreach ($this->sql_or_like as $field => $value) {
                $this->db->or_like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        if (empty($this->sql_order_by) === false) {
            foreach ($this->sql_order_by as $field => $value) {
                $this->db->order_by($field, $value);
            }
        }
        $this->db->order_by($this->primary_key, "desc");
        $this->db->limit($limit, $page);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_one(): array {
        $arr_result = $this->get_list();
        if (empty($arr_result) === false) {
            return current($arr_result);
        }
        return [];
    }

    public function get_one_by_id(int $id = 0): array {
        $data[$this->primary_key] = $id;
        $this->sql_where = $data;
        return $this->get_one();
    }

}
