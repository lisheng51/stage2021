$(function () {
    //errorlogs_kva();
    //processlist_kva();
    result_kva_1_today();
    result_kva_1_all();
});

function result_kva_1_today()
{
    var ajaxurl = module_url + "ajax/Kva/result_rdw_today";
    $.getJSON(ajaxurl, function (json) {
        $("#result_kva_1_today").html(json.total);
    });
    setInterval(function () {
        $.getJSON(ajaxurl, function (json) {
            $("#result_kva_1_today").html(json.total);
        });
    }, 60000);
}

function result_kva_1_all()
{
    var ajaxurl = module_url + "ajax/Kva/result_rdw";
    $.getJSON(ajaxurl, function (json) {
        $("#result_kva_1_all").html(json.total);
    });
    setInterval(function () {
        $.getJSON(ajaxurl, function (json) {
            $("#result_kva_1_all").html(json.total);
        });
    }, 60000);
}


function errorlogs_kva()
{
    var ajaxurl = module_url + "ajax/Kva/error";
    $.getJSON(ajaxurl, function (json) {
        if (json.result) {
            $("#error_content_kva").html(json.result);
        }
    });
    setInterval(function () {
        $.getJSON(ajaxurl, function (json) {
            if (json.result) {
                $("#error_content_kva").html(json.result);
            }
        });
    }, 60000);
}

function processlist_kva()
{
    var ajaxurl = module_url + "ajax/Kva/last_10_chart";
    $.getJSON(ajaxurl, function (json) {
        line_chart_kva('line-chart_kva', json.result);
    });
}

function line_chart_kva(divid, json_result)
{
    var json = json_result;
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    Highcharts.chart(divid, {
        chart: {
            type: 'line',
            marginRight: 20,
            events: {
                load: function () {
                    var series_data = this.series;
                    setInterval(function () {
                        var ajaxurl = module_url + "ajax/Kva/last_1_chart";
                        $.getJSON(ajaxurl, function (json) {
                            series_data[0].addPoint([json.x, json.y], true, true);
                            var maxnow = json.maxnow;
                            if (maxnow > 25) {
                                $("span#maxnow_kva").removeClass('text-success');
                                $("span#maxnow_kva").addClass('text-danger').text(json.maxnow);
                            } else
                            {
                                $("span#maxnow_kva").removeClass('text-danger');
                                $("span#maxnow_kva").addClass('text-success').text(json.maxnow);
                            }
                        });
                    }, 60000);
                }
            }
        },

        title: {
            text: ''
        },

        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: json.text
            }
        },
        legend: {
            enabled: true
        },

        plotOptions: {
            line: {
                dataLabels: {
                    enabled: false
                },
                enableMouseTracking: true
            }
        },
        series: json.data
    });
}



