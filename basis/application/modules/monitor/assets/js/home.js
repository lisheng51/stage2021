$(function () {
    sbi();
    calendar();
    //project_show();
    //project_showAll();
    //project_showPrio();
    company_info();
});

function calendar() {
    var ajaxurl = module_url + "ajax/his/calendar";
    $.getJSON(ajaxurl, function (json) {
        if (json.result) {
            $("#calendar_content").html(json.result);
        }
    });
    setInterval(function () {
        $.getJSON(ajaxurl, function (json) {
            if (json.result) {
                $("#calendar_content").html(json.result);
            }
        });
    }, 60000);
}

function company_info() {
    var ajaxurl = module_url + "ajax/his/company_info";
    $.getJSON(ajaxurl, function (json) {
        if (json.result) {
            $("#logi_total_company").text(json.result.total_company);
            $("#logi_total_start").text(json.result.total_start);
            $("#logi_total_start_today").text(json.result.total_start_today);
        }
    });
    setInterval(function () {
        $.getJSON(ajaxurl, function (json) {
            if (json.result) {
                $("#logi_total_company").text(json.result.total_company);
                $("#logi_total_start").text(json.result.total_start);
                $("#logi_total_start_today").text(json.result.total_start_today);
            }
        });
    }, 60000);
}

function project_show() {
    var ajaxurl = module_url + "ajax/his/project_show";
    $.getJSON(ajaxurl, function (json) {
        if (json.result) {
            $("#project_show_content").html(json.result);
        }
    });
    setInterval(function () {
        $.getJSON(ajaxurl, function (json) {
            if (json.result) {
                $("#project_show_content").html(json.result);
            }
        });
    }, 60000);
}

function project_showAll() {
    var ajaxurl = module_url + "ajax/his/project_showAll";
    $.getJSON(ajaxurl, function (json) {
        if (json.result) {
            $("#project_showAll_content").html(json.result);
        }
    });
    setInterval(function () {
        $.getJSON(ajaxurl, function (json) {
            if (json.result) {
                $("#project_showAll_content").html(json.result);
            }
        });
    }, 60000);
}

function project_showPrio() {
    var ajaxurl = module_url + "ajax/his/project_showPrio";
    $.getJSON(ajaxurl, function (json) {
        if (json.result) {
            $("#project_showPrio_content").html(json.result);
        }
    });
    setInterval(function () {
        $.getJSON(ajaxurl, function (json) {
            if (json.result) {
                $("#project_showPrio_content").html(json.result);
            }
        });
    }, 60000);
}

function sbi() {
    var ajaxurl = module_url + "ajax/his/sbi";
    $.getJSON(ajaxurl, function (json) {
        if (json.result) {
            $("#sbi_content").html(json.result);
        }
    });
    setInterval(function () {
        $.getJSON(ajaxurl, function (json) {
            if (json.result) {
                $("#sbi_content").html(json.result);
            }
        });
    }, 60000);
}


