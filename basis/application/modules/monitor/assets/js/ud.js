$(function () {
    //errorlogs();
    //processlist();
    result_mail();
    result_directions();
});

function result_directions()
{
    var ajaxurl = module_url + "ajax/Ud/directions";
    $.getJSON(ajaxurl, function (json) {
        $("#ud_directions_today").html(json.directions_today);
        $("#ud_directions_apicall").html(json.directions_apicall);
        $("#ud_directions_total").html(json.directions_total);
    });
    setInterval(function () {
        $.getJSON(ajaxurl, function (json) {
            $("#ud_directions_today").html(json.directions_today);
            $("#ud_directions_apicall").html(json.directions_apicall);
            $("#ud_directions_total").html(json.directions_total);
        });
    }, 60000);
}


function result_mail()
{
    var ajaxurl = module_url + "ajax/Ud/result";
    $.getJSON(ajaxurl, function (json) {
        $("#result_mail_total").html(json.totalmail);
        $("#result_mail_today_to_send").html(json.totaltodaytosend);
        $("#result_mail_today_is_send").html(json.totaltodayissend);
    });
    setInterval(function () {
        $.getJSON(ajaxurl, function (json) {
            $("#result_mail_total").html(json.totalmail);
            $("#result_mail_today_to_send").html(json.totaltodaytosend);
            $("#result_mail_today_is_send").html(json.totaltodayissend);
        });
    }, 60000);
}

function errorlogs()
{
    var ajaxurl = module_url + "ajax/Ud/error";
    $.getJSON(ajaxurl, function (json) {
        if (json.result) {
            $("#error_content").html(json.result);
        }
    });
    setInterval(function () {
        $.getJSON(ajaxurl, function (json) {
            if (json.result) {
                $("#error_content").html(json.result);
            }
        });
    }, 60000);
}

function processlist()
{
    var ajaxurl = module_url + "ajax/Ud/last_10_chart";
    $.getJSON(ajaxurl, function (json) {
        line_chart_min('line-chart', json.result);
    });
}

function line_chart_min(divid, json_result)
{
    var json = json_result;
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    Highcharts.chart(divid, {
        chart: {
            type: 'line',
            marginRight: 20,
            events: {
                load: function () {
                    var series_data = this.series;
                    setInterval(function () {
                        var ajaxurl = module_url + "ajax/Ud/last_1_chart";
                        $.getJSON(ajaxurl, function (json) {
                            series_data[0].addPoint([json.x, json.y], true, true);
                            var maxnow = json.maxnow;
                            if (maxnow > 25) {
                                $("span#maxnow").removeClass('text-success');
                                $("span#maxnow").addClass('text-danger').text(json.maxnow);
                            } else
                            {
                                $("span#maxnow").removeClass('text-danger');
                                $("span#maxnow").addClass('text-success').text(json.maxnow);
                            }
                        });
                    }, 60000);
                }
            }
        },

        title: {
            text: ''
        },

        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: json.text
            }
        },
        legend: {
            enabled: true
        },

        plotOptions: {
            line: {
                dataLabels: {
                    enabled: false
                },
                enableMouseTracking: true
            }
        },
        series: json.data
    });
}



