$(function () {
    //hosting();
    serverStatus();
});

function hosting() {
    var ajaxurl = module_url + "ajax/Hosting/neostrada";
    $.getJSON(ajaxurl, function (json) {
        if (json.result) {
            $("#neostrada_content").html(json.result);
        }
    });
    setInterval(function () {
        $.getJSON(ajaxurl, function (json) {
            if (json.result) {
                $("#neostrada_content").html(json.result);
            }
        });
    }, 600000);
}



function serverStatus() {
    var ajaxurl = module_url + "ajax/Hosting/serverStatus";
    $.getJSON(ajaxurl, function (json) {
        if (json.result) {
            $("#serverStatus_content").html(json.result);
        }
    });
    setInterval(function () {
        $.getJSON(ajaxurl, function (json) {
            if (json.result) {
                $("#serverStatus_content").html(json.result);
            }
        });
    }, 300000); //300s
}