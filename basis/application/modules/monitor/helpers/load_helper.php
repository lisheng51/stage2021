<?php

if (!function_exists('loadModuleModel')) {

    function loadModuleModel()
    {
        $CI = &get_instance();
        $CI->load->model('kva_processlist_m');
        $CI->load->model('ud_processlist_m');
        $CI->load->model('vd_processlist_m');
        $CI->load->model('monitor_alarm_model');
        $CI->load->model('global_m');
    }
}
