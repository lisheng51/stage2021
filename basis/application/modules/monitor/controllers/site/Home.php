<?php

class Home extends Site_Controller
{

    public function index()
    {
        $ip = $this->input->ip_address();
        $allowIps = ['::1', '127.0.0.1', '188.202.95.230'];
        if (in_array($ip, $allowIps) === false) {
            $this->session->set_flashdata('page_error_type', "no_authorized");
            redirect(error_url());
        }

        $data["title"] = "Monitor";
        $this->view_layout("index", $data);
    }
}
