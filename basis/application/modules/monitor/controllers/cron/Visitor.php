<?php

class Visitor extends Cron_Controller {

    private function check(string $ip = "") {
        $url = 'https://api.abuseipdb.com/api/v2/check?' . http_build_query(['ipAddress' => $ip]);
        $result = $this->curl($url);
        if ($result !== null) {
            $response = json_decode($result, true);
            return $response;
        }

        return [];
    }

    private function whitelist() {
        $url = 'https://www.uniekdating.nl/api/Database/whitelist';
        $json = $this->curl($url);
        $whitePaths = json_decode($json, true);
        //$whitePaths[] = '22';
        return $whitePaths;
    }

    private function strposa($haystack, $needle, $offset = 0): bool {
        if (!is_array($needle)) {
            $needle = array($needle);
        }
        foreach ($needle as $query) {
            if (strpos($haystack, $query, $offset) !== false) {
                return true;
            }
        }
        return false;
    }

    private function curl(string $url = "", array $postdata = []) {
        if (empty($url) === true) {
            return null;
        }

        $httpheaders = array(
            'Accept: application/json',
            'Key: e795c73005be04c7e6cf4e7d98ae70ea89c7ffcf76486f9f844ae70fa451703cb667c9066bd596be',
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0); //return url reponse header
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpheaders);
        if (empty($postdata) === false) {
            $fields_string = http_build_query($postdata);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100020);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            return null;
        }
        curl_close($ch);
        if ($response === false || empty($response) === true) {
            return null;
        }
        return $response;
    }

    private function filename(string $name = "") {
        $this->load->library('ftp');
        $config['hostname'] = '185.56.145.184';
        $config['username'] = 'onderhoud@uniekdating.nl';
        $config['password'] = '!7$ZHS0zCU@W';
        $config['port'] = 21;
        if ($this->ftp->connect($config) === false) {
            $this->output->set_status_header(404);
            die("Login is fout!");
        }

        $path = 'uploads' . DIRECTORY_SEPARATOR . 'uniekdating.nl';
        $arrFiles = $this->ftp->list_files();
        if (in_array($name, $arrFiles) === false || empty($name) === true) {
            die(0);
        }

        $filename = $path . DIRECTORY_SEPARATOR . $name;
        $status = $this->ftp->download($name, $filename);
        $this->ftp->close();
        if ($status === true) {
            $this->output->set_status_header();
            return $filename;
        }
        $this->output->set_status_header(404);
        die(0);
    }

    public function index(int $day = 1) {
        $this->load->library('ftp');
        $config['hostname'] = '185.56.145.184';
        $config['username'] = 'onderhoud@uniekdating.nl';
        $config['password'] = '!7$ZHS0zCU@W';
        $config['port'] = 21;
        if ($this->ftp->connect($config) === false) {
            $this->output->set_status_header(404);
            die("Login is fout!");
        }
        $html = '';
        $listdb = $this->ftp->list_files();
        foreach ($listdb as $value) {
            $arr = [];
            preg_match('/^uniekdating.nl-(.+?).gz/i', $value, $arr);
            if (empty($arr) === true) {
                continue;
            }

            $url = site_url('cron/Visitor/search?type=html&day=' . $day . '&name=' . $value);
            $a = '<a href=' . $url . '>' . $value . '</a><hr>';
            $html .= $a;
        }
        exit($html);
    }

    public function search(string $set_name = "", int $set_day = 1) {
        $name = $this->input->get('name') ?? $set_name;
        $day = $this->input->get('day') ?? $set_day;
        $type = $this->input->get('type') ?? 'raw';
        $filename = $this->filename($name);
        $handle = gzopen($filename, "r") or die(0);

        $whitePaths = $this->whitelist();

        $date = new DateTime();
        $date->modify('-' . $day . 'days');
        $ckDate = $date->format('Y-m-d');
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $ipPath = [];
        while (($row = fgets($handle, 4000)) !== false) {
            $data = explode(' - - ', $row);
            $matchesDateTime = $matchesPath = [];
            preg_match('/\[(.*?)\]/', $data[1], $matchesDateTime);
            $datetime = new DateTime(end($matchesDateTime));
            $findDate = $datetime->format('Y-m-d');
            if ($findDate === $ckDate) {
                preg_match('/"GET \/(.*?) HTTP/', $data[1], $matchesPath);
                $path = end($matchesPath);
                if (empty($path) === false) {
                    $ip_address = $data[0];
                    $datetime = $datetime->format('Y-m-d H:i:s');
                    $findStatus = $this->strposa($path, $whitePaths);
                    if ($findStatus === false) {
                        $ipPath[$ip_address][] = substr($datetime, 11) . ' ' . $path;
                    }
                }
            }
        }
        fclose($handle);

        $arrayResult = $rs = [];
        foreach ($ipPath as $ip => $arrayPath) {
            $response = $this->check($ip);
            if (empty($response) === false && $response['data']['abuseConfidenceScore'] > 0) {
                $rs['ipAddress'] = $response['data']['ipAddress'];
                $rs['countryCode'] = $response['data']['countryCode'];
                $rs['abuseConfidenceScore'] = $response['data']['abuseConfidenceScore'];
                $rs['path'] = implode(PHP_EOL, $arrayPath);
                $arrayResult[] = $rs;
            }
        }

        $subject = 'Uniekdating visitor log ' . date('d-m-Y', strtotime('-' . $day . ' days'));
        if ($type === 'html') {
            $content = '<h3>' . $subject . '</h3>';
            foreach ($arrayResult as $value) {
                $content .= $value['ipAddress'] . ' - ' . $value['countryCode'] . ' - ' . $value['abuseConfidenceScore'] . '<br>' . $value['path'] . '<hr>';
            }
            exit($content);
        }
        if (empty($arrayResult) === false) {
            $path = FCPATH . 'uploads' . DIRECTORY_SEPARATOR . 'uniekdating.nl';
            $file_name = date('d-m-Y', strtotime('-1 days')) . '.csv';
            $header = ["ipAddress", "countryCode", "abuseConfidenceScore", "path"];
            $fp = fopen($path . DIRECTORY_SEPARATOR . $file_name, "w");
            fputcsv($fp, $header, ";");
            foreach ($arrayResult as $item_result) {
                fputcsv($fp, $item_result, ";");
            }
            fclose($fp);
            $content = "";
            foreach ($arrayResult as $value) {
                $content .= $value['ipAddress'] . ' - ' . $value['countryCode'] . ' - ' . $value['abuseConfidenceScore'] . PHP_EOL . $value['path'] . PHP_EOL . PHP_EOL;
            }
            $this->mail($subject, $content);
        }
    }

    private function mail(string $subject = "", string $message = "", string $attach = "") {
        $dataMessage["content"] = $subject;
        $dataMessage["title"] = $message;
        $this->sendmail_model->add($dataMessage, "lisheng51@gmail.com", $attach);
    }

}
