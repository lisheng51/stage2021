<?php

class Processlist extends Cron_Controller {

    public function doAll() {
        loadModuleModel();
        $this->kva();
        $this->ud();
        //$this->vd();
    }

    private function kva() {
        $methode = 'GET';
        $url = 'https://www.klantvraaganalyse.nl/api/database/processlist';
        $params = [];
        $response = $this->global_m->curl($url, $params, $methode);
        $obj = json_decode($response);
        $total = $obj->total;
        if (empty($total) === false && $total > 0) {
            $this->kva_processlist_m->add($total);
            if ($total > 15) {
                $message = "Kva database connectie: " . $total . PHP_EOL;
                $this->telegram_model->log($message);
            }
        }
    }

    private function ud() {
        $methode = 'GET';
        $url = 'https://www.uniekdating.nl/api/database/processlist';
        $params = [];
        $response = $this->global_m->curl($url, $params, $methode);
        $obj = json_decode($response);
        $total = $obj->total;
        if (empty($total) === false && $total > 0) {
            $this->ud_processlist_m->add($total);
            if ($total > 15) {
                $message = "Ud database connectie: " . $total . PHP_EOL;
                $this->telegram_model->log($message);
            }
        }
    }

    private function bc() {
        $methode = 'GET';
        $url = 'https://www.bloemendaalconsultancy.nl/api/database/processlist';
        $params = [];
        $response = $this->global_m->curl($url, $params, $methode);
        $obj = json_decode($response);
        $total = $obj->total;
        if (empty($total) === false && $total > 0) {
            $this->bc_processlist_m->add($total);
            if ($total > 15) {
                $message = "Bc database connectie: " . $total . PHP_EOL;
                $this->telegram_model->log($message);
            }
        }
    }

    private function vd() {
        $methode = 'GET';
        $url = 'https://www.vloeddefensie.com/api/database/processlist';
        $params = [];
        $response = $this->global_m->curl($url, $params, $methode);
        $obj = json_decode($response);
        $total = $obj->total;
        if (empty($total) === false && $total > 0) {
            $this->vd_processlist_m->add($total);
            if ($total > 15) {
                $message = "Vd database connectie: " . $total . PHP_EOL;
                $this->telegram_model->log($message);
            }
        }
    }

    private function pp() {
        $methode = 'GET';
        $url = 'https://www.plugandplay.live/api/database/processlist';
        $params = [];
        $response = $this->global_m->curl($url, $params, $methode);
        $obj = json_decode($response);
        $total = $obj->total;
        if (empty($total) === false && $total > 0) {
            $this->pp_processlist_m->add($total);
            if ($total > 15) {
                $message = "Pp database connectie: " . $total . PHP_EOL;
                $this->telegram_model->log($message);
            }
        }
    }

}
