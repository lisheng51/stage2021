<?php

class Mailbox extends Cron_Controller
{
    const Hostname = '{outlook.office365.com}';
    const Username = 'development@bloemendaalconsultancy.nl';
    const Password = 'Saf63009';

    public function alarm(string $setPassword = '', string $setUsername = 'l.ye@bloemendaalconsultancy.nl')
    {
        $password = self::Password;
        $username = self::Username;
        if (!empty($setPassword) && !empty($setUsername)) {
            $password = $setPassword;
            $username = $setUsername;
        }

        $imap_stream = imap_open(self::Hostname . 'INBOX', $username,  $password) or die("can't connect: " . imap_last_error());

        $result = imap_search(
            $imap_stream,
            'SUBJECT "Alarmboodschap van uw systeem Bloemendaal" FROM "noreply@riscocloud.com"'
        );

        $msgno = 0;
        $date_format = "Y-m-d H:i:s";
        if ($result !== false) {
            $msgno = end($result);
        }

        if ($msgno > 0) {
            $header = imap_headerinfo($imap_stream, $msgno);
            $arrResultItem["datetime"] = date($date_format, $header->udate);  //$d->format('Y-m-d H:i:s');  //$d->format('Y-m-d\TH:i:s.u'); 
            $arrResultItem["is_open"] = 0;
            $arrResultItem["ck_udate"] = $header->udate;
            $arrResultItem["message_id"] = $header->message_id;
            $body = imap_body($imap_stream, $msgno);
            // $structure = imap_fetchstructure($imap_stream, $msgno);
            // if (!empty($structure->parts)) {
            //     for ($j = 0, $k = count($structure->parts); $j < $k; $j++) {
            //         $part = $structure->parts[$j];
            //         if ($part->subtype == 'PLAIN') {
            //             $body = imap_fetchbody($imap_stream, $msgno, $j + 1);
            //         }
            //     }
            // }
            if (str_contains($body, 'Uitgeschakeld')) {
                $arrResultItem["is_open"] = 1;
            }

            $id = $this->monitor_alarm_model->update($arrResultItem);
            if ($id > 0) {
                $message = $arrResultItem["is_open"] > 0 ? "Kantoor is open" : "Kantoor is dicht";
                $this->telegram_model->log($message, $this->telegram_model->default_chat_id);
            }
        }

        imap_close($imap_stream);
    }
    public function index(string $setPassword = '', string $setUsername = 'development@bloemendaalconsultancy.nl')
    {
        $password = self::Password;
        $username = self::Username;
        if (!empty($setPassword) && !empty($setUsername)) {
            $password = $setPassword;
            $username = $setUsername;
        }

        $allBox = $this->list($password, $username);
        //$allBox = ["INBOX", "Ongewenste e-mail"];
        if (is_array($allBox) && !empty($allBox)) {
            $tagTotal = [];
            foreach ($allBox as $tag) {
                $resultTotal = $this->unSeenTotal(self::Hostname . $tag, $username, $password);
                if ($resultTotal > 0) {
                    $tagTotal[] = $tag . ": " . $resultTotal;
                }
            }

            if (!empty($tagTotal)) {
                $message = "Aantal ongelezen in " . $username . PHP_EOL . implode(PHP_EOL, $tagTotal);
                $this->telegram_model->log($message);
            }
        }
    }

    private function unSeenTotal(string $mailbox = "", string $username = "", string $password = " "): int
    {
        $imap = imap_open($mailbox, $username, $password);
        $result = imap_search($imap, 'UNSEEN');
        $total = 0;
        imap_close($imap);
        if ($result !== false) {
            $total = count($result);
        }
        return $total;
    }

    private function list(string $password = "", string $username = '')
    {
        $mbox = imap_open(self::Hostname, $username,  $password, OP_HALFOPEN) or die("can't connect: " . imap_last_error());
        $list = imap_list($mbox, self::Hostname, "*");
        imap_close($mbox);
        $arrResult = [];
        if (is_array($list)) {
            foreach ($list as $val) {
                $fullname = imap_utf7_decode($val);
                $arrResult[] =  str_replace(self::Hostname, "", $fullname);
            }
        }
        return $arrResult;
    }
}
