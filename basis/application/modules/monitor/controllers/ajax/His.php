<?php

class His extends Ajax_Controller
{

    private $url = "https://www.bloemendaalconsultancy.nl/";

    private function curl(string $url = "", array $params = [], string $apiId = "2", string $apiKey = "b89f0f6ec9189c8485f1d68a131c6702")
    {
        $httpheaders = array(
            "apiId:  $apiId",
            "apiKey: $apiKey"
        );
        
        $fields_string = http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0); //return url reponse header
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpheaders);
        curl_setopt($ch, CURLOPT_URL, $this->url . $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100020);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            return null;
        }
        curl_close($ch);
        if ($response === false || empty($response) === true) {
            return null;
        }
        return $response;
    }

    public function calendar()
    {
        $url = 'my/api/calendar/index';
        $response = $this->curl($url);
        $obj = json_decode($response, true);
        if ($obj === null) {
            $json["result"] = "";
            $json["status"] = 'error';
            exit(json_encode($json));
        }
        $message = $obj["message"];
        $data['listdb'] = $message["listdb"];
        $result = $this->load->view('ajax/calendar', $data, true);
        $json["result"] = $result;
        $json["status"] = 'good';
        exit(json_encode($json));
    }

    public function company_info()
    {
        $url = 'company/api/Monitor/info_count';
        $response = $this->curl($url);
        $obj = json_decode($response);
        $json["result"] = $obj->message;
        exit(json_encode($json));
    }

    public function sbi()
    {
        $url = 'tender/api/Tender/show';
	$url = 'sbi/api/sbi/show';
        $response = $this->curl($url);
        $obj = json_decode($response);
        $json["result"] = $obj->message;
        exit(json_encode($json));
    }

    // public function scrumboard() {
    //     $url = 'scrumboard/api/Scrumboard/show';
    //     $response = $this->curl($url);
    //     $obj = json_decode($response);
    //     $json["result"] = $obj->message;
    //     exit(json_encode($json));
    // }

    // public function scrumboard_doing() {
    //     $url = 'scrumboard/api/Scrumboard/show_5';
    //     $response = $this->curl($url);
    //     $obj = json_decode($response);
    //     $json["result"] = $obj->message;
    //     exit(json_encode($json));
    // }

    public function project_show()
    {
        $url = 'project/api/Project/show';
        $response = $this->curl($url);
        $obj = json_decode($response);
        $json["result"] = $obj->message;
        exit(json_encode($json));
    }

    public function project_showAll()
    {
        $url = 'project/api/Project/showAll';
        $response = $this->curl($url);
        $obj = json_decode($response);
        $json["result"] = $obj->message;
        exit(json_encode($json));
    }

    public function project_showPrio()
    {
        $url = 'project/api/Project/showPrio';
        $response = $this->curl($url);
        $obj = json_decode($response);
        $json["result"] = $obj->message;
        exit(json_encode($json));
    }

    // public function opentime_show()
    // {
    //     $url = 'opentime/api/Opentime/show';
    //     $response = $this->curl($url);
    //     $obj = json_decode($response);
    //     $json["result"] = $obj->message;
    //     exit(json_encode($json));
    // }
}
