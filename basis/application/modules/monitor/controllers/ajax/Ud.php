<?php

class Ud extends Ajax_Controller
{

    private $url = 'https://www.uniekdating.nl/api/';

    private function processlist()
    {
        $methode = 'GET';
        $url = $this->url . 'Database/processlist';
        $params = [];
        $response = $this->global_m->curl($url, $params, $methode);
        $obj = json_decode($response);
        $total = $obj->total;
        if (empty($total) === false && $total > 0) {
            $this->ud_processlist_m->add($total);
            if ($total > 15) {
                $message = "Ud database connectie: " . $total . PHP_EOL;
                $this->telegram_model->log($message);
            }
        }
    }

    public function last_1_chart()
    {
        $this->processlist();
        $year = date('Y');
        $day = date('j');
        $month = date('n');
        $data_where["YEAR(datecreated)"] = $year;
        $data_where["MONTH(datecreated)"] = $month;
        $data_where["DAY(datecreated)"] = $day;

        $this->ud_processlist_m->sql_where = $data_where;
        $value = $this->ud_processlist_m->get_one();
        $ret["x"] = strtotime($value["datecreated"]) * 1000;
        $ret["y"] = (float) $value["total"];
        $ret["maxnow"] = $this->ud_processlist_m->select_max();
        exit(json_encode($ret));
    }

    public function directions()
    {
        $methode = 'GET';
        $url = $this->url . 'Directions/summary';
        $params = [];
        $response = $this->global_m->curl($url, $params, $methode);
        $obj = json_decode($response);
        $json["directions_today"] = $obj->message->directions_today ?? 0;
        $json["directions_apicall"] = $obj->message->directions_apicall ?? 0;
        $json["directions_total"] = $obj->message->directions_total ?? 0;
        exit(json_encode($json));
    }

    public function select_max()
    {
        $year = date('Y');
        $day = date('j');
        $month = date('n');
        $data_where["YEAR(datecreated)"] = $year;
        $data_where["MONTH(datecreated)"] = $month;
        $data_where["DAY(datecreated)"] = $day;

        $this->ud_processlist_m->sql_where = $data_where;
        $json["maxnow"] = $this->ud_processlist_m->select_max();
        $json["maxlimit"] = 30;
        exit(json_encode($json));
    }

    public function last_10_chart()
    {
        $this->processlist();
        $year = date('Y');
        $day = date('j');
        $month = date('n');
        $data_where["YEAR(datecreated)"] = $year;
        $data_where["MONTH(datecreated)"] = $month;
        $data_where["DAY(datecreated)"] = $day;

        $this->ud_processlist_m->sql_where = $data_where;
        $listdb = $this->ud_processlist_m->get_list(20);
        usort($listdb, function ($a, $b) {
            return strtotime($a["datecreated"]) - strtotime($b["datecreated"]);
        });
        foreach ($listdb as $value) {
            $x = strtotime($value["datecreated"]) * 1000;
            $arr_soure_data[] = [$x, (float) $value["total"]];
        }

        $json = [];
        $json["result"]["data"][] = array("name" => "Aantal connecties", "data" => $arr_soure_data);
        exit(json_encode($json));
    }

    public function error()
    {
        $methode = 'GET';
        $url = $this->url . 'logs/error';
        $params = [];
        $response = $this->global_m->curl($url, $params, $methode);
        $obj = json_decode($response);
        $data["listdb"] = [];
        if ($obj !== null) {
            $list = $obj->listdb;
            $data["listdb"] = $this->global_m->filterError($list);
        }
        $json["result"] = $this->load->view('ajax/error', $data, true);
        $json["status"] = "good";
        exit(json_encode($json));
    }

    public function result()
    {
        $methode = 'GET';
        $url = $this->url . 'database/result/1';
        $params = [];
        $response = $this->global_m->curl($url, $params, $methode);
        exit($response);
    }
}
