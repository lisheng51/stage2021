<?php

class Hosting extends Ajax_Controller
{
    private function servers(string $key = "")
    {
        $servers = [
            'bc' => 'www.bloemendaalconsultancy.nl',
            'kva' => 'www.klantvraaganalyse.nl',
            'ud' => 'www.uniekdating.nl',
            'vd' => 'www.vloeddefensie.com',
            'pv' => 'www.proveiling.nl',
            'vv' => 'www.vlavem.com',
        ];

        if (empty($key) === false) {
            return $servers[$key];
        }
        return $servers;
    }

    public function neostrada()
    {
        $module = $this->router->module;
        require_once APPPATH . 'modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . 'libraries' . DIRECTORY_SEPARATOR . 'Feed.php';
        $rss = Feed::loadRss('https://status.neostrada.nl/feed.rss');
        $data['title'] = str_replace('Status Status', 'Status', $rss->description);
        $data['lastupdate'] = $rss->pubDate;
        $data['listdb'] = $this->filter($rss->item);
        $result = $this->load->view('ajax/neostrada', $data, true);
        $json["result"] = $result;
        $json["status"] = 'good';
        exit(json_encode($json));
    }

    private function filter($listdb): array
    {
        $arrResult = [];
        foreach ($listdb as $value) {
            $rs['title'] = $this->titleExtra($value->title);
            $rs['date'] = date('d-m-Y', intval($value->timestamp));
            $rs['description'] = $value->description;
            $arrResult[] = $rs;
        }
        return $arrResult;
    }

    private function titleExtra(string $title = ""): string
    {
        $ckstring = strtolower($title);
        $pos = strpos($ckstring, "www96");
        $project = [];
        if ($pos !== false) {
            $project[] = 'UD';
        }

        $pos3 = strpos($ckstring, "www31");
        if ($pos3 !== false) {
            $project[] = 'LiveView';
        }

        $pos2 = strpos($ckstring, "www84");
        if ($pos2 !== false) {
            $project[] = 'KVA';
        }

        $pos1 = strpos($ckstring, "www99");
        if ($pos1 !== false) {
            $project[] = 'BC/Vloeddefensie';
        }
        //
        //        $pos2 = strpos($ckstring, "premium12");
        //        if ($pos2 !== false) {
        //            $project[] = 'Uniek12dating';
        //        }

        if (empty($project) === false) {
            return $title . ': ' . implode(" en ", $project);
        }
        return $title;
    }

    // private function exportPvCheck()
    // {
    //     $methode = 'GET';
    //     $url = 'https://www.proveiling.nl/export/health';
    //     $params = [];
    //     $response = $this->global_m->curl($url, $params, $methode);
    //     if ($response !== 'Healthy') {
    //         $message = 'Proveiling export is offline';
    //         $this->telegram_model->log($message);
    //     }
    // }


    public function serverStatus(): string
    {
        //$this->exportPvCheck();

        $listdb = [];
        $ch = [];
        $mh = curl_multi_init();
        foreach ($this->servers() as $type => $url) {
            $ch[$type] = curl_init();
            curl_setopt($ch[$type], CURLOPT_URL, 'https://' . $url . '/robots.txt');
            curl_setopt($ch[$type], CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch[$type], CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch[$type], CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch[$type], CURLOPT_SSL_VERIFYPEER, 0);
            curl_multi_add_handle($mh, $ch[$type]);
        }

        $running = 0;
        do {
            curl_multi_exec($mh, $running);
            curl_multi_select($mh);
        } while ($running > 0);

        foreach (array_keys($ch) as $key) {
            $http_code = curl_getinfo($ch[$key], CURLINFO_HTTP_CODE);
            $typeString = $this->servers($key);
            $listdb[$key]['appName'] = $typeString;
            $response = curl_multi_getcontent($ch[$key]);
            $appStatus = '<span class="badge badge-success"><i class="far fa-arrow-alt-circle-up"></i></span>';
            if ($http_code !== 200 || $response === null) {
                $appStatus = '<span class="badge badge-danger"><i class="far fa-arrow-alt-circle-down"></i></span>';
                $message = $typeString . ' is offline';
                $this->telegram_model->log($message);
            }
            $listdb[$key]['appStatus'] = $appStatus;
            curl_multi_remove_handle($mh, $ch[$key]);
            curl_close($ch[$key]);
        }
        curl_multi_close($mh);

        $data['listdb'] = $listdb;
        $result = $this->load->view('ajax/serverStatus', $data, true);
        $json["result"] = $result;
        $json["status"] = 'good';
        exit(json_encode($json));
    }

    public function serverStatus3(): string
    {
        $this->benchmark->mark('code_start');
        $listdb = [];
        $ch = [];
        $mh = curl_multi_init();
        foreach ($this->servers() as $type => $url) {
            $ch[$type] = curl_init();
            curl_setopt($ch[$type], CURLOPT_URL, 'https://' . $url . '/robots.txt');
            curl_setopt($ch[$type], CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch[$type], CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch[$type], CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch[$type], CURLOPT_SSL_VERIFYPEER, 0);
            curl_multi_add_handle($mh, $ch[$type]);
        }

        $running = 0;
        do {
            curl_multi_exec($mh, $running);
            curl_multi_select($mh);
        } while ($running > 0);

        foreach (array_keys($ch) as $key) {
            $http_code = curl_getinfo($ch[$key], CURLINFO_HTTP_CODE);
            $typeString = $this->servers($key);
            $listdb[$key]['appName'] = $typeString;
            $response = curl_multi_getcontent($ch[$key]);
            $appStatus = 'Online';
            if ($http_code !== 200 || $response === null) {
                $appStatus = 'Offline';
            }
            $listdb[$key]['appStatus'] = $appStatus;
            curl_multi_remove_handle($mh, $ch[$key]);
            curl_close($ch[$key]);
        }
        curl_multi_close($mh);
        $this->benchmark->mark('code_end');
        var_dump($listdb);
        echo $this->benchmark->elapsed_time('code_start', 'code_end');
        exit;
    }
}
