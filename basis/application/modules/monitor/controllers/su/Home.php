<?php

class Home extends Su_Controller
{

    public function sql()
    {
        $module = $this->router->module;
        $add_insert_list = [];
        exit($this->global_model->makeSql($module, $add_insert_list));
    }


    public function url()
    {
        $servers = [
            'bc' => 'https://diflex.bloemendaalconsultancy.nl/robots.txt',
            'kva' => 'https://www.klantvraaganalyse.nl/robots.txt',
            'pp' => 'https://www.plugandplay.live/robots.txt',
            'ud' => 'https://www.uniekdating.nl/robots.txt',
            'vd' => 'https://www.vloeddefensie.com/robots.txt',
        ];
        $listdb = url_exists($servers);
        $new = [];
        foreach ($listdb as $type => $status) {
            $typeString = $this->fetchTypeString($type);
            $new[$type]['appName'] = $typeString;

            $appStatus = 'Online';
            if ($status === false) {
                $appStatus = 'Offline';
            }

            $new[$type]['appStatus'] = $appStatus;
        }
        var_dump($new);
    }

    private function fetchTypeString(string $type = "bc"): string
    {
        switch ($type) {
            case 'bc':
                return 'bloemendaalconsultancy.nl';
            case 'kva':
                return 'klantvraaganalyse.nl';
            case 'pp':
                return 'plugandplay.live';
            case 'ud':
                return 'uniekdating.nl';
            case 'vd':
                return 'vloeddefensie.com';
            default:
                return "";
        }

        return "";
    }
}
