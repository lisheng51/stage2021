#
# TABLE STRUCTURE FOR: bc_monitor_alarm
#

CREATE TABLE `bc_monitor_alarm` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_open` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `datetime` datetime DEFAULT NULL,
  `message_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ck_udate` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `ck_udate` (`ck_udate`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE bc_monitor_alarm AUTO_INCREMENT = 1;