#
# TABLE STRUCTURE FOR: bc_monitor_ud_processlist
#

CREATE TABLE `bc_monitor_ud_processlist` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `total` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `datecreated` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE bc_monitor_ud_processlist AUTO_INCREMENT = 1;