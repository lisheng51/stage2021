<div role="main" class="main">
    <section class="page-header">
        <div class="container">
            <?php echo $breadcrumb ?>
            <div class="row">
                <div class="col-md-12">
                    <h1><?php echo $title ?></h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <?php echo $rsdb["content"] ?>
            </div>
        </div>
    </div>
</div>