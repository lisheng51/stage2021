<div class="card">
    <h5 class="card-header">Algemene informatie</h5>
    <div class="card-body">
        <form method="POST" id="send">
            <div class="row">
                <div class="col-2">
                    <label>Categorie*</label>
                    <div class="form-group">
                        <?php echo $this->article_cate_model->select($rsdb["cate_id"] ?? 0); ?>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label>Datum*</label>
                        <input type="text" class="form-control" name="created_at" maxlength="19" value="<?php echo $created_at; ?>" />
                    </div>
                </div>
                <div class="col-8">
                    <div class="form-group">
                        <label>Titel*</label>
                        <input type="text" class="form-control" name="title" required maxlength="80" value="<?php echo $rsdb["title"] ?? ""; ?>" />
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Omschrijving*</label>
                        <textarea class="form-control" rows="4" maxlength="250" name="description"><?php echo $rsdb["description"] ?? ""; ?></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <textarea name="content" class="form-control tinymce"><?php echo $rsdb["content"] ?? ""; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <input type="hidden" name="article_id" value="<?php echo $rsdb["article_id"] ?? 0; ?>" />
                        <?php echo add_csrf_value(); ?>
                        <?php echo add_submit_button($rsdb) ?>
                        <?php echo add_reset_button() ?>
                        <?php echo $delButton ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <?php echo $this->global_model->edit_time_info($rsdb) ?>
</div>

<script>
    $("form#send").submit(function(e) {
        tinyMCE.triggerSave();
        e.preventDefault();
        ajax_form_search($(this));
    });
    input_datewithtime('input[name="created_at"]');
</script>