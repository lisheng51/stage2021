<?php

if (!function_exists('loadModuleModel')) {

    function loadModuleModel()
    {
        $CI = &get_instance();
        $CI->load->model('article_cate_model');
        $CI->load->model('article_model');
        $CI->load->model('article_status_model');
    }
}
