<?php

class Article_model extends MY_Model
{

    public $table = "article";
    public $primary_key = "article_id";
    public $select_order_by = [];

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'article_id#desc' => 'ID (aflopend)',
            'title#desc' => 'Titel (aflopend)',
            'title#asc' => 'Titel (oplopend)'
        ];
    }

    public function update_hits(array $rsdb = [])
    {
        $tem_key = "is_view_article_" . $rsdb[$this->primary_key];
        $session = $this->session->tempdata($tem_key);
        if (empty($session) === TRUE) {
            $data["hits"] = $rsdb["hits"] + 1;
            $this->edit($rsdb[$this->primary_key], $data);
            $this->session->set_tempdata($tem_key, $this->input->ip_address(), 60);
        }

        return;
    }

    public function get_postdata(): array
    {
        $data["description"] = $this->input->post("description");
        $data["title"] = $this->input->post("title");
        $data["cate_id"] = $this->input->post("cate_id");
        $data["content"] = $this->input->post("content");
        $data["status_id"] = 1;
        $data["url_title"] = url_title($data["title"]);
        $data["created_at"] = oneStepCkDatetime('created_at');
        $this->ajaxck_model->ck_value('description', $data["description"]);
        $this->ajaxck_model->ck_value('datetime', $data["created_at"], "Datum is niet juist");
        $this->ajaxck_model->ck_value('title', $data["title"]);
        $this->ajaxck_model->ck_value('cate_id', $data["cate_id"]);
        $this->ajaxck_model->ck_value('status_id', $data["status_id"]);
        $this->ajaxck_model->ck_value('content', $data["content"]);
        return $data;
    }
}
