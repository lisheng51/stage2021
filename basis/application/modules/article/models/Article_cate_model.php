<?php

class Article_cate_model extends MY_Model
{

    public $table = "article_cate";
    public $primary_key = "cate_id";
    public $select_order_by = [];

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'cate_id#desc' => 'ID (aflopend)',
            'name#desc' => 'Naam (aflopend)',
            'name#asc' => 'Naam (oplopend)'
        ];
    }

    public function select(int $id = 0, bool $with_empty = false, string $name = ''): string
    {
        $select_name = empty($name) === false ? $name : $this->primary_key;
        if ($id === 0) {
            $id = $this->input->post_get($select_name) ?? 0;
        }
        $this->sqlReset();
        $data[$this->_field_is_deleted] = 0;
        $this->sql_where = $data;
        //$this->sql_order_by = ['sort_list' => 'asc'];
        $select = '<select name=' . $select_name . ' class="form-control selectpicker" data-live-search="true">';
        $select .= $with_empty === true ? "<option value='' >------</option>" : "";
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = $id == $rs[$this->primary_key] ? "selected" : '';
            $select .= '<option value=' . $rs[$this->primary_key] . ' ' . $ckk . '>' . $rs["name"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function selectMultiple(array $haystack = [], string $name = ''): string
    {
        $select_name = empty($name) === false ? $name : $this->primary_key;
        if (empty($haystack) === true) {
            $haystack = $this->input->post_get($select_name) ?? [];
            if (is_array($haystack) === false) {
                $haystack = [$haystack];
            }
        }
        $this->sqlReset();
        $data[$this->_field_is_deleted] = 0;
        $this->sql_where = $data;
        //$this->sql_order_by = ['sort_list' => 'asc'];
        $select = '<select name="' . $select_name . '[]" class="form-control selectpicker" data-selected-text-format="count > 3" data-live-search="true" multiple title="------">';
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = (empty($haystack) === false && in_array($rs[$this->primary_key], $haystack) === true) ? 'selected' : '';
            $select .= '<option value=' . $rs[$this->primary_key] . ' ' . $ckk . '>' . $rs["name"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function get_postdata(): array
    {
        $data["description"] = $this->input->post("description");
        $data["name"] = $this->input->post("name");
        $this->ajaxck_model->ck_value('name', $data["name"]);
        return $data;
    }
}
