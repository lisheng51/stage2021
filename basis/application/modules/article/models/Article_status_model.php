<?php

class Article_status_model extends MY_Model
{

    public $table = "article_status";
    public $primary_key = "status_id";

    public function select(int $id = 0, bool $with_empty = false): string
    {
        $select = '<select name=' . $this->primary_key . ' class="form-control selectpicker">';
        $select .= $with_empty === true ? "<option value='' >------</option>" : "";
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = $id == $rs[$this->primary_key] ? "selected" : '';
            $select .= '<option value=' . $rs[$this->primary_key] . '' . $ckk . '>' . $rs["label"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }
}
