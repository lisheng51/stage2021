<?php

class Article_cate extends Back_Controller
{

    protected static $title_module = 'Artikel catergorie';

    public function add()
    {
        if ($this->input->post()) {
            $this->addAction();
        }
        $data["rsdb"] = null;
        $data["title"] = self::$title_module . ' toevoegen';
        $data["delButton"] = null;
        $this->view_layout("edit", $data);
    }

    public function del()
    {
        $id = $this->input->post("del_id");
        $rsdb = $this->article_cate_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            $json["msg"] = self::$title_module . ' is niet gevonden!';
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $data["is_del"] = 1;
        $this->article_cate_model->edit($id, $data);
        $json["msg"] = self::$title_module . ' is verwijderd!';
        $json["status"] = "good";
        $json["type_done"] = "redirect";
        $json["redirect_url"] = site_url($this->controller_url);
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $this->editAction();
        }

        $rsdb = $this->article_cate_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            redirect($this->controller_url);
        }

        $data["title"] = self::$title_module . ' wijzigen';
        $data["rsdb"] = $rsdb;
        $data["delButton"] = delButton($this->controller_name . '.del', $id);
        $this->view_layout("edit", $data);
    }

    public function index()
    {
        $data_where[] = [$this->article_cate_model->table . ".is_del" => 0];
        $data_where[] = setFieldAndOperator('search_title', $this->article_cate_model->table . '.name');
        $data_where[] = setFieldAndOperator('search_content', $this->article_cate_model->table . '.description');
        $this->article_cate_model->setSqlWhere($data_where);
        $this->article_cate_model->sql_order_by = setFieldOrderBy();
        $total = $this->article_cate_model->get_total();
        $data["listdb"] = $this->getData();
        $data["total"] = $total;
        $data["pagination"] = $this->global_model->show_page($total);
        $data["result"] = $this->view_layout_return("ajax_list", $data);
        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }

        $data["title"] = self::$title_module . " overzicht";
        $data["addButton"] = addButton($this->controller_name . '.add', $this->controller_url . '/add');
        $this->view_layout("index", $data);
    }

    private function getData()
    {
        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;

        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;

        $arr_result = [];
        $listdb = $this->article_cate_model->get_list($limit, $page);
        foreach ($listdb as $rs) {
            $rs["edit_url"] = site_url($this->controller_url . "/edit/{$rs[$this->article_cate_model->primary_key]}");
            $arr_result[] = $rs;
        }

        return $arr_result;
    }

    private function addAction()
    {
        $data = $this->getPostdata();
        $check_double = $this->article_cate_model->get_one_by_field('name', $data['name']);
        if (empty($check_double) === false && $check_double["is_del"] > 0) {
            $data_reset["is_del"] = 0;
            $this->article_cate_model->edit($check_double[$this->article_cate_model->primary_key], $data_reset);
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            $json["msg"] = self::$title_module . ' is aangemaakt!';
            $json["status"] = "good";
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }

        if (empty($check_double) === false) {
            $json["msg"] = self::$title_module . ' bestaat al!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $insert_id = $this->article_cate_model->add($data);
        if ($insert_id > 0) {
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            $json["msg"] = self::$title_module . ' is toegevoegd!';
            $json["status"] = "good";
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    private function editAction()
    {
        $data = $this->getPostdata();
        $id = $this->input->post($this->article_cate_model->primary_key);
        $existdb = $this->article_cate_model->get_one_by_field('name', $data['name']);
        if (empty($existdb) === false && $id != $existdb[$this->article_cate_model->primary_key]) {
            $json["msg"] = self::$title_module . ' bestaat al!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $rsdb = $this->article_cate_model->get_one_by_id($id);
        if (empty($rsdb) === false) {
            $this->article_cate_model->edit($id, $data);
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            $json["msg"] = self::$title_module . ' is bijgewerkt!';
            $json["status"] = "good";
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    private function getPostdata()
    {
        if ($this->input->post("del_id")) {
            $this->del();
        }
        $data = $this->article_cate_model->get_postdata();
        return $data;
    }
}
