#
# TABLE STRUCTURE FOR: bc_alarm
#

CREATE TABLE `bc_alarm` (
  `datetime` datetime NOT NULL,
  `lockedunlocked` tinyint(1) NOT NULL,
  PRIMARY KEY (`datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE bc_alarm AUTO_INCREMENT = 1;