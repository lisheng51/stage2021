<?php



class Home extends Cron_Controller
{
    private static $serverUrl = "{outlook.office365.com:993/ssl}";
    private static $skipFolders = ["KPC"];
    private static $extraFolders = ['Ongewenste e-mail'];

    public function index($password = "Saf63009", $email = "development@bloemendaalconsultancy.nl")
    {

        $this->checkAndSendMail($password, $email);
    }


    private function checkAndSendMail($password, $email)
    {
        $server = self::$serverUrl . "INBOX";
        $connection = imap_open($server, $email, $password);
        if (!$connection) {
            $this->telegram_model->log("E-mail connectie mislukt. Controleer het wachtwoord");
            exit;
        }
        $message = "";

        $emails = imap_search($connection, 'UNSEEN');
        $unread = 0;

        $folders = imap_list($connection, self::$serverUrl, "*");
        $aantalOngelezeninFolder = [];

        $folders = $this->returnNeededMailFolders($folders);

        foreach ($folders as $folder) {

            $count = 0;
            $connection = imap_open(self::$serverUrl . $folder, $email, $password);
            $emails = imap_search($connection, 'UNSEEN');
            if ($emails) {
                $mails = [];
                foreach ($emails as $mail) {
                    $count = $count + 1;
                    $overview = imap_fetch_overview($connection, $mail, 0);
                    if (!$overview[0]->seen) {
                        $unread = $unread + 1;
                        $mailObject = [];
                        $mailObject['subject'] = $overview[0]->subject;
                        $mailObject['folder'] = $folder;
                        // $mailObject['from'] = $overview[0]->from;
                        // $mailObject['date'] = $overview[0]->date;
                        // $mailObject['message'] = imap_fetchbody($connection, $mail, "", FT_PEEK);

                        imap_clearflag_full($connection, $mail, "//Seen");
                        array_push($mails, $mailObject);
                    }
                }
                $aantalOngelezen = [];
                $aantalOngelezen['mails'] = $mails;
                $aantalOngelezen['folder'] = $folder;
                $aantalOngelezen['aantalOngelezen'] = $count;

                array_push($aantalOngelezeninFolder, $aantalOngelezen);
            }
        }
        $aantal = 0;
        foreach ($aantalOngelezeninFolder as $folder) {
            $aantal = $aantal + $folder['aantalOngelezen'];
            $message = $message . $folder['folder'] . ":" . $folder['aantalOngelezen'] . "\n";
        }

        imap_close($connection);
        if ($message !== "") {
            $message = "Aantal ongelezen berichten in " . $email . "\n\n" . $message;
            $this->telegram_model->log($message, "1027598459);
        }
    }


    private function returnNeededMailFolders($folders) // array
    {
        $neededFolders = [];
        foreach ($folders as $folder) {
            $needed = false;
            // INBOX e-mails always needed
            if (str_contains($folder, "INBOX")) {
                $needed = True;
            }
            // Extra folders outside of INBOX
            foreach (self::$extraFolders as $fol) {
                if (str_contains($folder, $fol)) {
                    $needed = True;
                }
            }
            // Exclude folders in INBOX
            if (in_array(trim($folder, self::$serverUrl . "INBOX"), self::$skipFolders)) {
                $needed = false;
            }
            if ($needed) {
                array_push($neededFolders, $folder);
            }
        }
        return str_replace(self::$serverUrl, "", $neededFolders);
    }
}
