<?php


class Alarm extends Cron_Controller
{
    private static $serverUrl = "{outlook.office365.com:993/ssl}";

    public function check()
    {
        // $data = $this->alarm_model->get_all();
        $email = "development@bloemendaalconsultancy.nl";
        $password = "Saf63009";

        $server = self::$serverUrl . "INBOX/Alarm";

        $now = new DateTime();
        $lastUpdate = $this->db->query("SELECT * FROM `bc_alarm` ORDER BY `bc_alarm`.`datetime` DESC LIMIT 1");
        $lastUpdate = $lastUpdate->row();
        if(empty($lastUpdate))
        {
            $this->checkEmail($server, $email, $password);
            exit();
        }
        $lastUpdateDatetime = new DateTime($lastUpdate->datetime);

        // If unlocked today

        if ($lastUpdate->lockedunlocked = 1 && $lastUpdateDatetime->format('Y-m-d') === $now->format('Y-m-d')) {
            // if current time before 17:00
            if (date('H') > 17) {
                $this->checkEmail($server, $email, $password);
            } else {
                exit();
            }
        } else {
            $this->checkEmail($server, $email, $password);
        }
    }


    protected function checkEmail($server, $email, $password)
    {
        $connection = imap_open($server, $email, $password);
        if (!$connection) {
            $this->telegram_model->log("E-mail connectie mislukt. Controleer het wachtwoord");
            exit;
        }

        $emails = imap_search($connection, 'SEEN');
        if ($emails) {
            foreach ($emails as $email) {
                // var_dump($this->alarm_model->get_all());
                $header = imap_fetch_overview($connection, $email, 0);
                $email_content = imap_body($connection, $email);
                $date = $header[0]->date;
                $date = new DateTime($date);
                $date = $date->add(new DateInterval('PT' . 60 . 'M'));

                $data_where[] = [$this->alarm_model->table . ".datetime =" => $date->format("Y-m-d H:i:s")];
                $this->alarm_model->setSQlWhere($data_where);

                $data = $this->alarm_model->get_all();
                if (empty($data)) {
                    $data = [];
                    $data['datetime'] = $date->format("Y-m-d H:i:s");
                    $this->alarm_model->add($data);
                    if (strpos($email_content, "Volledig ingeschakeld")) {
                        $this->telegram_model->log("Het alarm is ingeschakeld");
                    } elseif (strpos($email_content, "Uitgeschakeld")) {
                        $this->telegram_model->log("Het alarm is uitgeschakeld");
                    }
                }

                if (strpos($email_content, "Volledig ingeschakeld")) {
                    $data = [];
                    $data['lockedunlocked'] = 0;
                    $data['datetime'] = $date->format("Y-m-d H:i:s");
                    $this->alarm_model->edittwo($date->format("Y-m-d H:i:s"), $data);
                } elseif (strpos($email_content, "Uitgeschakeld")) {
                    $data = [];
                    $data['lockedunlocked'] = 1;
                    $data['datetime'] = $date->format("Y-m-d H:i:s");
                    $this->alarm_model->edittwo($date->format("Y-m-d H:i:s"), $data);
                }
            }
        }
    }

}
