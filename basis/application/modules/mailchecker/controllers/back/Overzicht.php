<?php


class Overzicht extends Back_Controller 
{
    public function index()
    {
        $data_where  =[];
        $reportrange_end = $this->input->post("reportrange_end");
        $reportrange_start = $this->input->post("reportrange_start");
        if (empty($reportrange_start) === false && empty($reportrange_end) === true) {
            $data_where[] = [$this->alarm_model->table . ".datetime >=" => date_format(date_create(trim($reportrange_start)), 'Y-m-d H:i:s')];
        }

        if (empty($reportrange_end) === false && empty($reportrange_start) === true) {
            $data_where[] = [$this->alarm_model->table . ".datetime <=" => date_format(date_create(trim($reportrange_end)), 'Y-m-d H:i:s')];
        }

        if (empty($reportrange_start) === false && empty($reportrange_end) === false) {
            $data_where[] = [$this->alarm_model->table . ".datetime >=" => date_format(date_create(trim($reportrange_start)), 'Y-m-d H:i:s')];
            $data_where[] = [$this->alarm_model->table . ".datetime <=" => date_format(date_create(trim($reportrange_end)), 'Y-m-d H:i:s')];
        }

        $this->alarm_model->setSqlWhere($data_where);
       $this->alarm_model->sql_order_by = setFieldOrderBy();
     
        $total = $this->alarm_model->get_total();

        $data["total"] = $total;
        $data["pagination"] = $this->global_model->show_page($total);
        $data['data'] = $this->getData();
        $data["result"] = $this->view_layout_return("ajax-return", $data);
    
        if ($this->input->post()) {
            sleep(5);
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }

        $this->view_layout("Overzicht", $data);
    }


    protected function getData()
    {
        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;

        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;

        $listdb = $this->alarm_model->get_list($limit, $page);
        return $listdb;
    }
}