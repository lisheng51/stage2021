<?php

$path = explode(DIRECTORY_SEPARATOR, dirname(__FILE__));
$data["path"] = end($path);
$data["use_path"] = false;
$data["path_description"] = 'MailChecker';
$data["arrPermission"] = [
    'Home' => [
        [
            'index' => 'MailChecker'
        ]
    ]
];
