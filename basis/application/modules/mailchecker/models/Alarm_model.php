<?php

class Alarm_model extends MY_Model
{

    public $select_order_by = [];

    public $table = "alarm";
    public $primary_key = "datetime";
   
    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'datetime#desc' => 'Datum (aflopend)',
            'datetime#asc' => 'Datum (oplopend)',
        ];
    }


    public function edittwo(string $id, array $data = [])
    {
        if ($this->db->field_exists($this->_field_modifiedby, $this->table) === true) {
            $data[$this->_field_modifiedby] = $this->login_model->user_id();
        }

        $this->db->set($data);
        if (empty($this->sql_set_raw) === false) {
            foreach ($this->sql_set_raw as $field => $value) {
                $this->db->set($field, $value, false);
            }
        }
        $this->db->where($this->primary_key, $id);
        return $this->db->update($this->table);
    }
}
