<table class="table">
    <thead>
        <tr>
            <th scope="col">Datum</th>
            <th scope="col">Tijd</th>
            <th scope="col">Gebeurtenis slot</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $gebeurtenis) { ?>
            <tr>
                <td><?php echo substr($gebeurtenis['datetime'], 0, 10) ?></th>
                <td><?php echo substr($gebeurtenis['datetime'], 11, 20) ?></th>
                <?php
                if ($gebeurtenis['lockedunlocked'] == 0) {
                    echo "<td> <span class='badge badge-warning'> Locked </span> </td>";
                } else {
                    echo "<td> <span class='badge badge-danger'> Unlocked </span> </td>";
                }
            }
                ?>
            </tr>

    </tbody>
</table>
<div class="card-footer">
    <?php echo $pagination; ?>
</div>

<script>
    var pagination = document.querySelector(".pagination");
    pagination.onclick = function(){
        pagination.setAttribute("style", "pointer-events: none;");
    }

</script>