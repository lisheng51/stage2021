<?php

$path = explode(DIRECTORY_SEPARATOR, dirname(__FILE__));
$data["path"] = end($path);
$data["use_path"] = false;
$data["path_description"] = 'Systeem';
$data["arrPermission"] = [
    'Config' => [
        [
            'index' => 'Instelling'
        ]
    ],
    'Language_info' => [
        [
            'index' => 'Taal tekst'
        ]
    ],
    'Mail_template_info' => [
        [
            'index' => 'Mail template'
        ]
    ],
    'Spamword' => [
        [
            'index' => 'Spamword'
        ]
    ],
    'Mail_config' => [
        [
            'index' => 'Mail config'
        ]
    ],
    'Module' => [
        [
            'index' => 'Module'
        ]
    ],
    'Permission' => [
        [
            'index' => 'Toestemming'
        ]
    ],
    'Permission_group' => [
        [
            'index' => 'Groep',
            'add' => 'Toevoegen'
        ]
    ]
];
