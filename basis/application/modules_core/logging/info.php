<?php

$path = explode(DIRECTORY_SEPARATOR, dirname(__FILE__));
$data["path"] = end($path);
$data["use_path"] = false;
$data["path_description"] = 'Logging';
$data["arrPermission"] = [
    'Applog' => [
        [
            'index' => 'Systeem Log'
        ]
    ],
    'Visitor' => [
        [
            'index' => 'Bezoeker Log'
        ]
    ],
    'Api' => [
        [
            'log' => 'Api Log'
        ]
    ],
    'Error_log' => [
        [
            'index' => 'Error Log'
        ]
    ],
    'Change_log' => [
        [
            'index' => 'Change Log'
        ]
    ],
    'Sendmail' => [
        [
            'index' => 'Sendmail Log'
        ]
    ],
    'History_url' => [
        [
            'index' => 'Geschiedenis'
        ]
    ]
];
