<?php

$path = explode(DIRECTORY_SEPARATOR, dirname(__FILE__));
$data["path"] = end($path);
$data["use_path"] = false;
$data["path_description"] = 'Account';
$data["arrPermission"] = [
    'User' => [
        [
            'index' => 'Overzicht',
            'add' => 'Toevoegen',
            'profile' => 'Profiel',
            'edit_email' => 'Email Wijzigen'
        ]
    ],
    'File' => [
        [
            'index' => 'Bestand',
            'add' => 'Bestand toevoegen',
        ]
    ],
    'Bookmark' => [
        [
            'index' => 'Bookmark'
        ]
    ],
    'Login_history' => [
        [
            'index' => 'Login geschiedenis'
        ]
    ],
    'Message' => [
        [
            'index' => 'Inbox',
            'my' => 'Outbox',
            'add' => 'Toevoegen'
        ]
    ]
];
