<script>
    $(function () {
        $("form#send").submit(function (e) {
            ajax_form_search($(this));
            e.preventDefault();
        });
    });
</script>
<div class="card-header"><?php echo $h4 ?></div>
<div class="card-body">
    <form method="POST" id="send">
        <div class="form-group">
            <input type="email" name="email" autofocus required placeholder="Emailadres..." class="form-control">
        </div>

        <div class="form-group">
            <input type="password" name="password" required placeholder="Wachtwoord..." class="form-control">
        </div>
        <div class="form-group">
            <?php echo add_csrf_value(); ?>
            <button type="submit" class="btn btn-primary btn-block" id="submit_button">Installeren</button>
        </div>
    </form>
</div>


