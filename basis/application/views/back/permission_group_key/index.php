<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
<div class="row">
    <div class="col-md-12">
        <form method="POST" id="send">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Module</th>
                            <th>Toestemming</th>
                            <th>Link title</th>
                            <th>Beschrijving</th>
                            <?php foreach ($listdb_group as $value) : ?>
                                <th><?php echo $value["name"]; ?></th>
                            <?php endforeach; ?>
                        </tr>
                    </thead>
                    <tbody id="itemContainer">
                        <?php foreach ($listdb_permission as $value) : ?>
                            <tr>
                                <td><?php echo $value["path_description"]; ?></td>
                                <td><?php echo $value["object"] . '::' . $value["method"]; ?></td>
                                <td><?php echo $value["link_title"]; ?></td>
                                <td><?php echo $value["description"]; ?></td>
                                <?php foreach ($value["checkboxdb"] as $ck) : ?>
                                    <td><?php echo $ck["checkbox_is_check"]; ?></td>
                                <?php endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <?php echo add_csrf_value(); ?>
                        <button class="btn btn-success" type="submit" id="submit_button">Opslaan</button>
                        <?php echo add_reset_button() ?>

                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <div class='checkbox checkbox-primary'><input type='checkbox'><label>Niet toegestaan</label></div>
                        <div class='checkbox checkbox-primary'><input type='checkbox' checked><label>Toegestaan</label></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $("form#send").submit(function(e) {
        e.preventDefault();
        ajax_form_search($(this));
    });
    //ajax_checkbox();
    function ajax_checkbox() {
        $("input[type='checkbox']").click(function() {
            var value;
            var checkbox_name = $(this).attr('name');
            var arr_name = checkbox_name.split('_');
            var type = arr_name[0];
            var group_id = arr_name[1];
            var permission_id = arr_name[2];
            var ajaxurl = site_url + "back/Permission_group_key/checkbox";
            switch (type) {
                case "check":
                    if ($(this).is(":checked")) {
                        value = "1";
                    } else {
                        value = "0";
                    }
                    break;
            }
            var ajaxdata = {
                type: type,
                group_id: group_id,
                permission_id: permission_id,
                value: value
            };
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data: ajaxdata
            }).done(function(json) {

            }).fail(function(jqxhr) {
                message_ajax_fail_show(jqxhr);
            }).always(function() {

            });
        });
    }
</script>