<?php foreach ($listdb as $value) : ?>
    <tr class="treegrid-<?php echo $value[$this->bookmark_model->primary_key]; ?> treegrid-parent-<?php echo $value["parent_bookmark_id"]; ?> child" id="<?php echo $value[$this->bookmark_model->primary_key]; ?>" data-f_question_id="<?php echo $value["parent_bookmark_id"]; ?>">
        <input type="hidden" name="sort_list[<?php echo $value[$this->bookmark_model->primary_key] ?>]" value="<?php echo $value['order_list'] ?>">
        <td><span id="<?php echo $value[$this->bookmark_model->primary_key]; ?>_name_text_color"><?php echo $value["name"]; ?></span></td>
        <td><?php echo $value["description"] ?></td>
        <td>
            <button type="button" class="btn btn-primary btn-sm" id="start_ajax_sort"><i class="fa fa-arrows-alt"></i></button>
            <button type="button" class="btn btn-primary btn-sm" data-search_data="<?php echo $value[$this->bookmark_model->primary_key]; ?>" data-edit_link="<?php echo $value["edit_link"] ?>" data-toggle="modal" data-target="#Modal_question_edit"><i class="fa fa-pencil-alt"></i></button>
            <?php echo $value["change_button"]; ?>
            <a class="btn btn-success btn-sm" href="<?php echo $value["url"] ?>"><?php echo lang('view_icon') ?></a>
        </td>
    </tr>
<?php endforeach; ?>