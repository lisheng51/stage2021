<td><span id="<?php echo $bookmark_id; ?>_name_text_color"><?php echo $name; ?></span></td>
<td><?php echo $description; ?></td>
<td>
    <button type="button" class="btn btn-primary btn-sm" id="start_ajax_sort"><i class="fa fa-arrows-alt"></i></button>
    <button type="button" class="btn btn-primary btn-sm" data-search_data="<?php echo $bookmark_id ?>" data-edit_link="<?php echo $edit_link ?>" data-toggle="modal" data-target="#Modal_question_edit"><i class="fa fa-pencil-alt"></i></button>
    <?php echo $change_button; ?>
    <?php echo $add_child_link ?>
    <?php echo $add_view_link ?>
</td>