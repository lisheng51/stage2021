<ul class="nav nav-tabs">
    <?php
    foreach ($ul as $key => $value) :
        $status = $key === 0 ? "active" : null;
    ?>
        <li class="nav-item"><a class="nav-link <?php echo $status ?>" href="#<?php echo $value ?>_lang" data-toggle="tab"><?php echo $value ?></a></li>
    <?php endforeach; ?>
</ul>

<form method="POST" id="send">
    <div class="tab-content">
        <?php
        foreach ($listdb as $key => $value) :
            $status = $key === 0 ? "active" : null;
            foreach ($value as $did => $data) :
        ?>
                <div id="<?php echo $did ?>_lang" class="tab-pane <?php echo $status ?>">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <?php foreach ($data as $key => $value) : ?>
                                    <div class="col-3">
                                        <label><?php echo $key ?></label>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="4" name="<?php echo $did . '[' . $key . ']' ?>"><?php echo stripslashes($value) ?></textarea>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
        <?php
            endforeach;
        endforeach;
        ?>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <div class="form-group">
                <?php echo add_csrf_value(); ?>
                <?php echo add_submit_button('config') ?>
            </div>
        </div>
    </div>
</form>

<script>
    $("form#send").submit(function(e) {
        ajax_form_search($(this));
        e.preventDefault();
    });
</script>