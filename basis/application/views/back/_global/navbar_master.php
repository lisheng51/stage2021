<ul class="navbar-nav bg-gray-900 sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo site_url($this->login_model->redirect_url()) ?>">
        <div class="sidebar-brand-text"><?php echo c_key('webapp_name') ?></div>
    </a>
    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Logging">
            <i class="fas fa-book fa-fw"></i>
            <span>Logging</span>
        </a>
        <div id="Logging" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Api/log') ?>">API</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Sendmail/index') ?>">Sendmail</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Visitor/index') ?>">Bezoeker</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Applog/index') ?>">Systeem</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/History_url/index') ?>">Geschiedenis</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Login_history/index') ?>">Login</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Error_log/index') ?>">Error</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Change_log/index') ?>">Change</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Bericht">
            <i class="fas fa-envelope fa-fw"></i>
            <span>Bericht</span>
        </a>
        <div id="Bericht" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Message/index') ?>">Inbox</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Message/my') ?>">Outbox</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Account">
            <i class="fas fa-user fa-fw"></i>
            <span>Account</span>
        </a>
        <div id="Account" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Api/index') ?>">API</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/User/index') ?>">Overzicht</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/File/index') ?>">Bestand</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Bookmark/index') ?>">Bookmark</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Toestemming">
            <i class="fas fa-lock fa-fw"></i>
            <span>Toestemming</span>
        </a>
        <div id="Toestemming" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Permission_group/index') ?>">Groep</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Permission/index') ?>">Overzicht</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Systeem">
            <i class="fas fa-cog fa-fw"></i>
            <span>Systeem</span>
        </a>
        <div id="Systeem" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Module/index') ?>">Module</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Config/index') ?>">Instelling</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Language_info/index') ?>">Taal tekst</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Mail_template_info/index') ?>">Mail template</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Spamword/index') ?>">Spamword</a>
                <a class="collapse-item" href="<?php echo site_url($path_name . '/Mail_config/index') ?>">Mail config</a>
            </div>
        </div>
    </li>

    <?php echo $this->module_model->autoNavbar(); ?>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>