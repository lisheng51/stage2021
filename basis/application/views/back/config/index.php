<ul class="nav nav-tabs">
    <li class="nav-item"><a class="nav-link active" href="#default" data-toggle="tab">Algemeen</a></li>
    <li class="nav-item"><a class="nav-link" href="#email" data-toggle="tab">Email</a></li>
    <li class="nav-item"><a class="nav-link" href="#notify" data-toggle="tab">Notificatie</a></li>
</ul>

<form method="POST" id="send">
    <div class="tab-content">
        <div id="default" class="tab-pane active">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_name', 'Applicatie naam') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_title', 'Applicatie titel') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_keywords', 'Applicatie keywoord') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_description', 'Applicatie beschrijving') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_default_url', 'Applicatie standaard pagina') ?>
                        </div>
                        <div class="col-3">
                            <label>Dnkere modus</label>
                            <?php echo select_boolean('webapp_option_dark_mode', intval($this->config_model->configNow["webapp_option_dark_mode"] ?? 0)) ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_default_show_per_page', 'Aantal items per pagina', 'number') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_ck_pass_unuse_day', 'Wachtwoord verloop aantal dagen', 'number') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_ck_pass_notify_day', 'Waarschuwing wachtwoord verloop aantal dagen', 'number') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_ck_pass_reset_hour', 'Wachtwoord herstellink verloop aantal uur', 'number') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_fail_limit_num', 'Na aantal poging login blokkeren', 'number') ?>
                        </div>

                        <div class="col-12">
                            <?php echo $this->config_model->textarea('webapp_domain_redirect', 'Domein redirect (www.company.com@access,sub.company.com@web)') ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-3">
                            <label>Site url</label>
                            <div class="form-group">
                                <input type="text" class="form-control" disabled value="<?php echo ENVIRONMENT_BASE_URL ?>">
                            </div>
                        </div>
                        <div class="col-3">
                            <label>Database server</label>
                            <div class="form-group">
                                <input type="text" class="form-control" disabled value="<?php echo ENVIRONMENT_HOSTNAME ?>">
                            </div>
                        </div>
                        <div class="col-3">
                            <label>Database gebruiker</label>
                            <div class="form-group">
                                <input type="text" class="form-control" disabled value="<?php echo ENVIRONMENT_USERNAME ?>">
                            </div>
                        </div>
                        <div class="col-3">
                            <label>Database naam</label>
                            <div class="form-group">
                                <input type="text" class="form-control" disabled value="<?php echo ENVIRONMENT_DATABASE ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="email" class="tab-pane">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_noreply_email_address', 'Noreply email') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_noreply_email_name', 'Noreply email naam') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_master_email_address', 'Web master email') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_smtp_host', 'SMTP server') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_smtp_port', 'SMTP poort') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_smtp_crypto', 'SMTP crypto') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_smtp_user_name', 'SMTP afzender naam') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_smtp_user', 'SMTP gebruikersnaam') ?>
                        </div>
                        <div class="col-3">
                            <label>SMTP wachtwoord*</label>
                            <div class="form-group">
                                <input type="password" class="form-control" name="webapp_smtp_pass" value="<?php echo $webapp_smtp_pass ?>">
                            </div>
                        </div>
                        <div class="col-3">
                            <label>Met webversie link</label>
                            <?php echo select_boolean('webapp_sendmail_with_webversionurl', intval($this->config_model->configNow["webapp_sendmail_with_webversionurl"] ?? 0)) ?>
                        </div>
                        <div class="col-3">
                            <label>Met check open image</label>
                            <?php echo select_boolean('webapp_sendmail_with_check_open_img', intval($this->config_model->configNow["webapp_sendmail_with_check_open_img"] ?? 0)) ?>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label>Test instelling</label>
                                <div class="input-group">
                                    <input type="text" id="to_email" placeholder="..@.." class="form-control">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-success" id="send_mail_test"><i class="fas fa-paper-plane fa-fw"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="notify" class="tab-pane">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_telegram_group_chat_id', 'Telegram groep ID') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_telegram_user_chat_id', 'Telegram user ID') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_telegram_bot_token', 'Telegram bot token') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_messagebird_originator', 'Messagebird nummer') ?>
                        </div>
                        <div class="col-3">
                            <?php echo $this->config_model->input('webapp_messagebird_api_key', 'Messagebird key') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <div class="form-group">
                <?php echo add_csrf_value(); ?>
                <?php echo add_submit_button('config') ?>
            </div>
        </div>
    </div>
</form>

<script>
    $("form#send").submit(function(e) {
        ajax_form_search($(this));
        e.preventDefault();
    });

    $("button#send_mail_test").click(function() {
        let host = $('input[name=webapp_smtp_host]').val();
        let user = $('input[name=webapp_smtp_user]').val();
        let name = $('input[name=webapp_smtp_user_name]').val();
        let pass = $('input[name=webapp_smtp_pass]').val();
        let port = $('input[name=webapp_smtp_port]').val();
        let crypto = $('input[name=webapp_smtp_crypto]').val();
        let to_email = $('input#to_email').val();

        let ajaxurl = site_url + "back/Config/sendMailTest";
        let senddata = {
            host: host,
            user: user,
            name: name,
            pass: pass,
            port: port,
            crypto: crypto,
            to_email: to_email,
        };
        axios_search(ajaxurl, senddata, $(this));
    });
</script>