<div class="card">
    <div class="card-header">Algemeen</div>
    <div class="card-body">
        <form method="POST" id="send">
            <div class="row">
                <div class="col-4">
                    <div class="form-group">
                        <label>Naam</label>
                        <input type="text" class="form-control" maxlength="200" name="path_description" value="<?php echo $rsdb["path_description"]; ?>" />
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>Path*</label>
                        <input type="text" class="form-control" maxlength="50" name="path" readonly value="<?php echo $rsdb["path"]; ?>" />
                    </div>
                </div>

                <div class="col-2">
                    <div class="form-group">
                        <label>Is actief</label>
                        <?php echo select_boolean('is_active', intval($rsdb["is_active"] ?? 0)); ?>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <input type="hidden" name="<?php echo $this->module_model->primary_key ?>" value="<?php echo $rsdb[$this->module_model->primary_key]; ?>" />
                        <?php echo add_csrf_value(); ?>
                        <?php echo add_submit_button($rsdb) ?>
                        <?php echo add_reset_button() ?>
                        <?php echo $delButton; ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $("form#send").submit(function(e) {
        e.preventDefault();
        ajax_form_search($(this));
    });
</script>