<script>
    $(function() {
        ajax_sort(site_url + "back/Module/sortList");
        $('.updatemodule').click(function() {
            var moduleID = $(this).data('search_data');
            var button = $(this);
            var or_text = button.html();
            $.ajax({
                url: site_url + "back/Module/update",
                type: 'POST',
                dataType: 'json',
                data: {
                    id: moduleID,
                    [csrf_token_name]: csrf_hash
                },
                beforeSend: function() {
                    button.html('<i class="fa fa-fw fa-spinner fa-pulse"></i> ');
                }
            }).done(function(json) {
                handle_info_box(json.status, json.msg);
                //                if (json.status == "good") {
                //                    location.reload();
                //                }
            }).fail(function(jqxhr) {
                message_ajax_fail_show(jqxhr);
            }).always(function() {
                button.html(or_text);
            });
        });

    });
</script>
<div class="card">
    <div class="card-header">Resultaten - Totaal gevonden: <span class="totalcount"><?php echo $total ?></span></div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Naam</th>
                        <th>Reinstall</th>
                        <th>Is actief</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="itemContainer">
                    <?php foreach ($listdb as $value) : ?>
                        <tr>
                            <td><button type="button" class="btn btn-info btn-sm" id="start_ajax_sort"><?php echo lang("sort_icon") ?></button> <?php echo $value["path_description"]; ?> <input type="hidden" name="sort_list[<?php echo $value['module_id'] ?>]" value="<?php echo $value['sort_list'] ?>"></td>
                            <td>
                                <button type="button" class="btn btn-warning btn-sm updatemodule" data-search_data="<?php echo $value["module_id"]; ?>"><?php echo lang("reset_icon") ?></button>
                            </td>
                            <td><?php echo $value["is_active"] > 0 ? "Ja" : "Nee"; ?></td>
                            <td>
                                <?php echo $value["editButton"] ?>
                                <a href="<?php echo $value["changelog_url"] ?>" class="btn btn-info btn-sm"><?php echo lang("info_icon") ?></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>