<div class="card">
    <div class="card-header"><?php echo lang('search_box_header_text') ?></div>
    <div class="card-body">
        <form method="POST" id="form_search">
            <div class="row">
                <div class="col-3">
                    <?php echo labelSelectInput('Naam', 'path_description') ?>
                </div>
                <div class="col-3">
                    <label>Is actief</label>
                    <div class="form-group">
                        <?php echo select_boolean("is_active", 2, true) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?php echo add_csrf_value(); ?>
                    <?php echo search_button() ?>
                    <?php echo reset_button() ?>
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-2">
                <?php echo select_order_by($this->module_model->select_order_by, 'sort_list#asc'); ?>
            </div>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-12" id="ajax_search_content">
        <?php echo $result; ?>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Nieuw - Totaal gevonden: <span class="totalcount"><?php echo $new["total"] ?></span></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Naam</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="itemContainer">
                            <?php foreach ($new["listdb"] as $value) : ?>
                                <tr>
                                    <td><?php echo $value["path_description"]; ?></td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-sm addmodule" data-search_data="<?php echo $value["path"]; ?>"><?php echo lang("add_icon") ?></button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $("form#form_search").submit(function(e) {
        ajax_form_search($(this));
        e.preventDefault();
    });

    $('.addmodule').click(function() {
        var modulepath = $(this).data('search_data');
        var button = $(this);
        $.ajax({
            url: site_url + "back/Module/add",
            type: 'POST',
            dataType: 'json',
            data: {
                path: modulepath,
                [csrf_token_name]: csrf_hash
            },
            beforeSend: function() {
                button.html('<i class="fa fa-fw fa-spinner fa-pulse"></i> ');
            }
        }).done(function(json) {
            handle_info_box(json.status, json.msg);
            if (json.status == "good") {
                location.reload();
            }
        }).fail(function(jqxhr) {
            message_ajax_fail_show(jqxhr);
        }).always(function() {
            button.html('<i class="fa fa-fw fa-plus"></i> ');
        });
    });
</script>