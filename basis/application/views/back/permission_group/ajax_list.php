<div class="card">
    <div class="card-header">Resultaten - Totaal gevonden: <?php echo $total ?></div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Info</th>
                        <th>Naam</th>
                        <th>Type</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="itemContainer">
                    <?php foreach ($listdb as $value) : ?>
                        <tr>
                            <td><button class="btn btn-info btn-sm" data-toggle="collapse" data-target="#Info_<?php echo $value[$this->permission_group_model->primary_key]; ?>">Info</button> </td>
                            <td><?php echo $value["name"]; ?></td>
                            <td><?php echo $value["type_name"]; ?></td>
                            <td><?php echo $value["editButton"] ?></td>
                        </tr>

                        <tr>
                            <td colspan="4">
                                <ul class="list-group collapse" id="Info_<?php echo $value[$this->permission_group_model->primary_key]; ?>">
                                    <?php foreach ($value["listdbPermission"] as $module => $listdb2) : list($name, $toggleId) = explode("#_#", $module); ?>
                                        <li class="list-group-item"><?php echo $name ?>:<br>
                                            <?php foreach ($listdb2 as $value2) : ?>
                                                <span class="badge badge-info"><?php echo $value2['checkbox_label'] ?></span>
                                            <?php endforeach; ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer">
        <?php echo $pagination; ?>
    </div>
</div>
<script>
    ajax_inline_edit(site_url + 'back/Permission_group/editInline');
</script>