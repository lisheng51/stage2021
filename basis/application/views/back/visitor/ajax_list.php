<div v-html="result">
    <div class="card">
        <div class="card-header">Resultaten - Totaal gevonden: <span class="totalcount"><?php echo $total ?></span></div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Datum & Tijd</th>
                            <th>Wie</th>
                            <th>IP</th>
                            <th>Path</th>
                            <th>Browser</th>
                            <th>Platform</th>
                        </tr>
                    </thead>
                    <tbody id="itemContainer">
                        <?php foreach ($listdb as $value) : ?>
                            <tr>
                                <td><?php echo $value["datetime"] ?></td>
                                <td><?php echo $value["display_info"] ?></td>
                                <td><?php echo $value["ip_address"] ?></td>
                                <td><?php echo $value["path"] ?></td>
                                <td><?php echo $value["browser"] ?></td>
                                <td><?php echo $value["platform"] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card-footer">
            <?php echo $pagination; ?>
        </div>
    </div>
</div>