<div class="card">
    <div class="card-header"><?php echo $rsdb["title"] ?></div>
    <div class="card-body">
        <?php echo $rsdb["content"] ?>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-md-4">
                Van: <?php echo $rsdb["from_user_name"] ?>
            </div>
            <div class="col-md-4">
                Naar: <?php echo $rsdb["to_user_name"] ?>
            </div>
            <div class="col-md-4">
                Tijd aangemaakt: <?php echo $rsdb["date"] ?>
            </div>
        </div>
    </div>
</div>


<div class="card mt-3 <?php echo $reply_status ?>">
    <div class="card-header">
        <button class="btn btn-info" id="reply_button" data-toggle="collapse" data-target="#reply_content"><i class="fa-fw fas fa-plus"></i> Beantwoord</button> <?php echo $delButton ?>
    </div>
    <div class="card-body">
        <div id="reply_content" class="panel-collapse collapse">
            <form method="POST" id="send">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Inhoud*</label>
                            <textarea class="form-control tinymce_noxss_clean" name="content"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo add_csrf_value(); ?>
                            <input type="hidden" name="title" value="<?php echo $rsdb["title"]; ?>" />
                            <input type="hidden" name="to_user_id" value="<?php echo $rsdb["from_user_id"]; ?>" />
                            <?php echo add_submit_button() ?>
                            <?php echo add_reset_button() ?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    setup_tinymce_noxss_clean();
    $("form#send").submit(function(e) {
        tinymce.triggerSave();
        e.preventDefault();
        ajax_form_search($(this));
    });
    $("button#reply_button").click(function() {
        var $this = $(this);
        var icon = $this.find('i');
        if (icon.hasClass('fa-plus')) {
            $this.find('i').removeClass('fa-plus').addClass('fa-minus');
        } else {
            $this.find('i').removeClass('fa-minus').addClass('fa-plus');
        }
    });
</script>