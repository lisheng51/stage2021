<div class="card">
    <div class="card-header">Resultaten - Totaal gevonden: <span class="totalcount"><?php echo $total ?></span></div>
    <div class="card-body">
        <div class="row">
            <?php foreach ($listdb as $value) : ?>
                <div class="col-md-3" id="<?php echo $value["user_id"]; ?>">
                    <div class="card">
                        <div class="card-body">
                            <h4><?php echo $value["display_info"]; ?></h4>
                            <p>Email: <?php echo $value["emailaddress"]; ?></p>
                            <p><?php echo $value["addButton"] ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>   
        </div>
    </div>
    <div class="card-footer">
        <?php echo $pagination; ?>
    </div>
</div>
