<div class="card">
    <div class="card-header">Resultaten - Totaal gevonden: <span class="totalcount"><?php echo $total ?></span></div>
    <div class="card-body">
        <div class="table-responsive">  
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Type</th>
                        <th>Bestand</th>
                        <th>Datum</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="itemContainer">
                    <?php foreach ($listdb as $value) : ?>
                        <tr id="<?php echo $value["upload_id"]; ?>">  
                            <td><?php echo $value["title"]; ?></td>
                            <td><?php echo $value["type_name"]; ?></td>
                            <td><a href="<?php echo $value["path"]; ?>"><?php echo $value["file_name"]; ?></a></td>
                            <td><?php echo $value["created_at"]; ?></td>
                            <td><?php echo $value["editButton"] ?></td> 
                        </tr>
                    <?php endforeach; ?>   
                </tbody>
            </table>
        </div>
    </div>

    <div class="card-footer">
        <?php echo $pagination; ?>
    </div>
</div>