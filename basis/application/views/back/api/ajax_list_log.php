<div class="card">
    <div class="card-header">Resultaten - Totaal gevonden: <span class="totalcount"><?php echo $total ?></span></div>
    <div class="card-body">
        <div class="table-responsive">  
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Datum & Tijd</th>
                        <th>API naam</th>
                        <th>IP</th>
                        <th>Path/url</th>
                        <th>Post</th>
                        <th>Get</th>
                        <th>Header</th>
                        <th>Out</th>
                    </tr>
                </thead>
                <tbody id="itemContainer">
                    <?php foreach ($listdb as $value) : ?>
                        <tr>  
                            <td><?php echo F_datetime::convert_datetime($value["datetime"]) ?></td>
                            <td><?php echo $value["name"] ?></td>
                            <td><?php echo $value["ip_address"] ?></td>
                            <td><?php echo $value["path"] ?></td>
                            <td><?php echo $value["post_value"] ?></td>
                            <td><?php echo $value["get_value"] ?></td>
                            <td><?php echo $value["header_value"] ?></td>
                            <td><?php echo $value["out_value"] ?></td>
                        </tr>
                    <?php endforeach; ?>   
                </tbody>
            </table>
        </div>
    </div>

    <div class="card-footer">
        <div class="row">
            <div class="col-md-12">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>
</div>