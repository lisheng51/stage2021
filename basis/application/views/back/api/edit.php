<div class="card">
    <div class="card-header">Algemeen</div>
    <div class="card-body">
        <form method="POST" id="send">
            <div class="row">
                <div class="col-3">
                    <label>Naam*</label>
                    <div class="form-group">
                        <input type="text" maxlength="100" class="form-control" name="name" required value="<?php echo $rsdb["name"] ?? "" ?>">
                    </div>
                </div>
                <div class="col-3">
                    <label>Key*</label>
                    <div class="form-group">

                        <input type="text" maxlength="32" class="form-control" name="secret" required value="<?php echo $rsdb["secret"] ?? md5(time()) ?>">
                    </div>
                </div>
                <div class="col-2">
                    <label>Token life time(min)*</label>
                    <div class="form-group">
                        <input type="number" min=15 max="1440" class="form-control" name="token_min" required value="<?php echo $rsdb["token_min"] ?? 30 ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <label>Toestemming groep*</label>
                    <div class="form-group">
                        <?php echo $this->permission_group_model->selectMultiple(explode(',', $rsdb["permission_group_ids"] ?? ""), "", 2); ?>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <input type="hidden" name="<?php echo $this->api_model->primary_key ?>" value="<?php echo $rsdb[$this->api_model->primary_key] ?? 0; ?>" />
                        <?php echo add_csrf_value(); ?>
                        <?php echo add_submit_button($rsdb) ?>
                        <?php echo add_reset_button() ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <?php echo $this->global_model->edit_time_info($rsdb) ?>
</div>

<script>
    $("form#send").submit(function(e) {
        ajax_form_search($(this));
        e.preventDefault();
    });
</script>