<div class="card">
    <div class="card-header">Algemeen</div>
    <div class="card-body">
        <form method="POST" id="send">
            <div class="row">
                <div class="col-2">
                    <label>Module*</label>
                    <div class="form-group">
                        <?php echo $this->module_model->select() ?>
                    </div>
                </div>
                <div class="col-2">
                    <label>Link dir*</label>
                    <div class="form-group">
                        <?php echo $this->permission_group_type_model->selectPath(); ?>
                    </div>
                </div>
                <div class="col-2">
                    <label>Object*</label>
                    <div class="form-group">
                        <input type="text" class="form-control" maxlength="50" name="object" readonly value="<?php echo $rsdb["object"]; ?>" />
                    </div>
                </div>
                <div class="col-2">
                    <label>Method*</label>
                    <div class="form-group">
                        <input type="text" class="form-control" maxlength="50" name="method" readonly value="<?php echo $rsdb["method"]; ?>" />
                    </div>
                </div>
                <div class="col-2">
                    <label>Is link</label>
                    <div class="form-group">
                        <?php echo select_boolean('has_link', intval($rsdb["has_link"])); ?>
                    </div>
                </div>
                <div class="col-2">
                    <label>Link titel</label>
                    <div class="form-group">
                        <input type="text" class="form-control" maxlength="50" name="link_title" value="<?php echo $rsdb["link_title"]; ?>" />
                    </div>
                </div>
                <div class="col-12">
                    <label>Beschrijving</label>
                    <div class="form-group">
                        <input type="text" class="form-control" maxlength="200" name="description" value="<?php echo $rsdb["description"]; ?>" />
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <input type="hidden" name="permission_id" value="<?php echo $rsdb["permission_id"]; ?>" />
                        <?php echo add_csrf_value(); ?>
                        <?php echo add_submit_button($rsdb) ?>
                        <?php echo add_reset_button() ?>
                        <?php echo $delButton; ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('select[name=link_dir]').prop('disabled', true);
    $('select[name=module_id]').prop('disabled', true);
    $("form#send").submit(function(e) {
        ajax_form_search($(this));
        e.preventDefault();
    });
</script>