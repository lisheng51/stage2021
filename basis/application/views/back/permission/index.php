<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header"><?php echo lang('search_box_header_text') ?></div>
            <div class="card-body">
                <form method="POST" id="form_search">
                    <div class="row">
                        <div class="col-3">
                            <?php echo labelSelectSelectMulti('Module', $this->module_model->primary_key, $this->module_model->selectMultiple()) ?>
                        </div>
                        <div class="col-3">
                            <?php echo labelSelectInput('Beschrijving', 'description') ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <?php echo add_csrf_value(); ?>
                            <?php echo search_button() ?>
                            <?php echo reset_button() ?>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-2">
                        <?php echo select_order_by($this->permission_model->select_order_by); ?>
                    </div>
                    <div class="col-2">
                        <?php echo select_page_limit() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-12" id="ajax_search_content">
        <?php echo $result; ?>
    </div>
</div>

<script>
    $("form#form_search").submit(function(e) {
        e.preventDefault();
        ajax_form_search($(this));
    });
</script>