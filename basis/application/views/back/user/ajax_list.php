<div class="card">
    <div class="card-header">Resultaten - Totaal gevonden: <span class="totalcount"><?php echo $total ?></span></div>
    <div class="card-body">
        <div class="row">
            <?php foreach ($listdb as $value) : ?>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <h4><?php echo $value["display_info"]; ?></h4>
                            <p><?php echo $value["editButton"]; ?> <?php echo $value["emailaddress"]; ?></p>
                            <?php foreach ($value['permissionGroupDb'] as $rs) : ?>
                                <span class="badge badge-info"><?php echo $rs["name"]; ?></span>
                                <!--                                <a href="<?php echo site_url($path_name . "/User/index?permission_group_id[]=" . $rs["permission_group_id"]); ?>" class="badge badge-info"><?php echo $rs["name"]; ?></a>-->
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="card-footer">
        <?php echo $pagination; ?>
    </div>
</div>