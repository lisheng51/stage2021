<form method="POST" id="form_search">
    <div class="card">
        <div class="card-header"><?php echo lang('search_box_header_text') ?><div class="float-right"><?php echo $addButton ?></div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-4">
                    <?php echo labelSelectInput('Email', 'search_email') ?>
                </div>
                <div class="col-2">
                    <label>Is active</label>
                    <div class="form-group">
                        <?php echo select_boolean("is_active", 2, true); ?>
                    </div>
                </div>
                <div class="col-6">
                    <?php echo labelSelectSelectMulti('Toestemming groep', 'permission_group_id', $this->permission_group_model->selectMultiple(loadPostGet('permission_group_id', 'array'))) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?php echo add_csrf_value(); ?>
                    <?php echo search_button() ?>
                    <?php echo reset_button() ?>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-2">
                    <?php echo select_order_by($this->user_model->select_order_by); ?>
                </div>
                <div class="col-2">
                    <?php echo select_page_limit() ?>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="row mt-3">
    <div class="col-12" id="ajax_search_content">
        <?php echo $result; ?>
    </div>
</div>

<script>
    $(function() {
        $("form#form_search").find('select').selectpicker('refresh');
        ajax_form_search($("form#form_search"));
    });
    $("form#form_search").submit(function(e) {
        ajax_form_search($(this));
        e.preventDefault();
    });
</script>