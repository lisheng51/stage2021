<div class="card">
    <div class="card-header">Resultaten - Totaal gevonden: <span class="totalcount"><?php echo $total ?></span></div>
    <div class="card-body">
        <div class="table-responsive">  
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>SMTP server</th>
                        <th>SMTP gebruikersnaam</th>
                        <th>SMTP afzender naam</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="itemContainer">
                    <?php foreach ($listdb as $value) : ?>
                        <tr>  
                            <td><?php echo $value["host"]; ?></td>
                            <td><?php echo $value["user"]; ?></td>
                            <td><?php echo $value["name"]; ?></td>
                            <td><?php echo $value["editButton"] ?></td> 
                        </tr>
                    <?php endforeach; ?>   
                </tbody>
            </table>
        </div>
    </div>

    <div class="card-footer">
        <?php echo $pagination; ?>
    </div>
</div>