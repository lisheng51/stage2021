<div class="card">
    <div class="card-header"><?php echo $title ?></div>
    <div class="card-body">
        <form method="POST" id="send">
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label>SMTP gebruikersnaam*</label>
                        <input type="email" class="form-control" name="user" required value="<?php echo $rsdb["user"] ?? ""; ?>" />
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>SMTP afzender naam*</label>
                        <input type="text" class="form-control" name="name" required value="<?php echo $rsdb["name"] ?? ""; ?>" />
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>SMTP server*</label>
                        <input type="text" class="form-control" name="host" required value="<?php echo $rsdb["host"] ?? ""; ?>" />
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>SMTP wachtwoord*</label>
                        <input type="password" class="form-control" name="pass" required value="<?php echo $rsdb["pass"] ?? ""; ?>" />
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>SMTP poort*</label>
                        <input type="text" class="form-control" name="port" required value="<?php echo $rsdb["port"] ?? ""; ?>" />
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>SMTP crypto</label>
                        <input type="text" class="form-control" name="crypto" value="<?php echo $rsdb["crypto"] ?? ""; ?>" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <input type="hidden" name="mail_config_id" value="<?php echo $rsdb["mail_config_id"] ?? 0; ?>" />
                        <?php echo add_csrf_value(); ?>
                        <?php echo add_submit_button($rsdb) ?>
                        <?php echo add_reset_button() ?>
                        <?php echo $delButton; ?>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>

<script>
    $("form#send").submit(function(e) {
        e.preventDefault();
        ajax_form_search($(this));

    });
</script>