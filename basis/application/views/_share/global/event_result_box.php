<div class="row mb-3">
    <div class="col-md-12">
        <div class="card" id="event_result_box">
            <div class="card-header"><?php echo $title ?></div>
            <div class="card-body"><?php echo $body ?></div>
        </div>
    </div>
</div>
