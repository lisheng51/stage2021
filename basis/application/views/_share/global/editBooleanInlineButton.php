<label class="ms-switch">
    <input type="checkbox" <?php echo $value > 0 ? "checked" : "" ?>>
    <span class="ms-switch-slider ms-switch-<?php echo $style ?> round inline_boolean" data-field="<?php echo $field ?>" data-edit-id="<?php echo $editId; ?>" data-value="<?php echo $value; ?>"></span>
</label>