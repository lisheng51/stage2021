<div class="form-group">
    <label><?php echo $label ?></label>
    <div class="input-group">
        <div class="input-group-prepend">
            <?php echo $select ?>
        </div>
        <input name="<?php echo $inputname ?>" type="text" class="form-control" value="<?php echo $inputname_value ?>" />
    </div>
</div>