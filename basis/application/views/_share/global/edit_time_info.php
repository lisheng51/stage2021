<div class="card-footer">
    <div class="row">
        <div class="col-md-6">
            <span class="text-primary"><i class="fa-fw fas fa-history"></i> aangemaakt op </span><?php echo $created_at; ?> - <?php echo $createdby ?>
        </div>
        <div class="col-md-6">
            <span class="text-primary"><i class="fa-fw fas fa-history"></i> gewijzigd op </span><?php echo $modified_at; ?> - <?php echo $modifiedby ?>
        </div>
    </div>
</div>