<?php

class API_Controller extends MX_Controller
{

    protected $apiId = ENVIRONMENT !== 'development' ? 0 : 1;
    protected $checkCodeAccessDatetime = false;
    protected $usingBodySecurity = false;
    protected $usingBodyOutSecurity = false;
    protected $checkPermission = false;
    protected $checkUserPermission = false;
    protected $checkMethodPost = false;
    protected $arrPost = [];
    protected $version = '';
    protected $arrUserdb = [];

    public function __construct()
    {
        parent::__construct();
        if ($this->checkMethodPost === true && isset($_SERVER['REQUEST_METHOD'])) {
            $method = $_SERVER['REQUEST_METHOD'];
            if (strtoupper($method) !== 'POST') {
                $this->api_model->out('HTTP methode is geen POST', 90);
            }
        }
        $this->load->database();
        $this->language_model->set_language();
        $this->api_model->usingBodyOutSecurity = $this->usingBodyOutSecurity;
        $this->version = $this->api_model->getVersion();

        $rsdb =  $this->api_model->get_one_by_request();
        if ($this->apiId > 0) {
            $rsdb = $this->api_model->get_one_by_id($this->apiId);
        }

        if (empty($rsdb) === true) {
            $this->api_model->out('API is niet geldig', 99);
        }

        $this->apiId = $rsdb[$this->api_model->primary_key];

        if ($this->checkCodeAccessDatetime === true) {
            $this->api_model->checkCodeAccessDatetime($rsdb);
        }

        if ($this->checkPermission === true) {
            $this->api_model->checkPermission($rsdb["permission_group_ids"], 92);
        }

        if ($this->checkUserPermission === true) {
            $userDB = $this->api_model->checkFetchByJWT();
            $this->arrUserdb = $userDB;
            if ($userDB[$this->user_model->primary_key] !== $this->user_model->secure_id) {
                $this->api_model->checkPermission($userDB["permission_group_ids"], 91);
            }
        }

        if ($this->usingBodySecurity === true) {
            $inputBody = $this->input->post('body') ?? null;
            if ($inputBody === null || empty($inputBody) === true) {
                $this->api_model->out('Geen body gevonden', 98);
            }
            $body = $this->aes_model->decrypt(rawurldecode($inputBody));
            $postData = json_decode($body, true);
            if ($postData === null || empty($postData) === true) {
                $this->api_model->out('Geen body gevonden', 98);
            }
            $this->arrPost = $postData;
        }

        if (empty($this->router->module) === false && $this->router->module !== 'api') {
            $lang = $this->language_model->get_language();
            $this->module_model->language($lang);
            loadModuleModel();
        }
    }

    protected function addLog(string $msg = '')
    {
        return $this->api_log_model->add($msg, $this->apiId);
    }
}

class Cron_Controller extends MX_Controller
{

    protected $site_url = "";
    public function __construct()
    {
        parent::__construct();
        if (is_cli() === false && ENVIRONMENT !== 'development') {
            exit;
        }
        if (ENVIRONMENT !== 'development') {
            $this->config->set_item('base_url', $this->site_url);
        }
        $this->load->database();
        if (empty($this->router->module) === false && $this->router->module !== 'cron') {
            loadModuleModel();
        }
    }
}

class Su_Controller extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->access_check_model->ipAllow();
        $this->access_check_model->authorized();
        $this->user_model->secure_id();
        $this->load->database();
        $this->language_model->set_language();

        if (empty($this->router->module) === false && $this->router->module !== 'su') {
            $lang = $this->language_model->get_language();
            $this->module_model->language($lang);
            loadModuleModel();
        }
    }
}

class Ajax_Controller extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->language_model->set_language();

        if (empty($this->router->module) === false && $this->router->module !== 'ajax') {
            $lang = $this->language_model->get_language();
            $this->module_model->language($lang);
            loadModuleModel();
        }
    }
}

class Site_Controller extends MX_Controller
{

    protected $controller_name;
    protected $controller_url;
    protected $path_name = 'site';
    protected $module_path_name;
    protected $module_url = "";

    public function __construct()
    {
        parent::__construct();
        $this->access_check_model->maintenance();
        $this->controller_name = get_class($this);
        $this->controller_url = $this->path_name . "/" . $this->controller_name;
        $this->language_model->set_language();

        if (empty($this->router->module) === false && $this->router->module !== $this->path_name) {
            $this->module_url = $this->router->module;
            $this->module_path_name = $this->router->module . "/" . $this->path_name;
            $this->controller_url = $this->module_path_name . "/" . $this->controller_name;
            $this->load->database();
            $lang = $this->language_model->get_language();
            $this->visitor_model->insert();
            $this->module_model->language($lang);
            loadModuleModel();
        }
    }

    protected function view_layout_return(string $view_file = "index", array $data = [])
    {
        $data["path_name"] = $this->path_name;
        $data["module_path_name"] = $this->module_path_name;
        $data["module_url"] = $this->module_url;
        $view_path_name = $this->path_name;
        return $this->load->view($this->path_name . '/' . $this->access_check_model->view_file($this->controller_name, $view_path_name, $view_file), $data, true);
    }

    protected function view_layout(string $view_file = "index", array $data = [])
    {
        $dataLayout["title"] = null;
        $dataLayout["description"] = null;
        $dataLayout["keywords"] = null;
        $dataLayout["path_name"] = $this->path_name;
        $dataLayout["module_path_name"] = $this->module_path_name;
        $dataLayout["module_url"] = $this->module_url;

        if (isset($data['title']) === true && empty($data['title']) === false) {
            $dataLayout["title"] = $data["title"];
        }

        if (isset($data['description']) === true && empty($data['description']) === false) {
            $dataLayout["description"] = $data["description"];
        }

        if (isset($data['keywords']) === true && empty($data['keywords']) === false) {
            $dataLayout["keywords"] = $data["keywords"];
        }

        $view_path_name = $this->path_name;
        $this->breadcrumb_model->set_template($this->path_name . '/' . $this->access_check_model->view_file("_global", $view_path_name, "breadcrumb"));
        $this->breadcrumb_model->set_data(["name" => "Home", "url" => null, "active_status" => null]);
        $breadcrumbTitle = "Overzicht";
        if (isset($data['title']) === true && empty($data['title']) === false) {
            $breadcrumbTitle = $data["title"];
        }
        $this->breadcrumb_model->set_data(["name" => $breadcrumbTitle, "url" => $this->controller_url, "active_status" => "active"]);
        if (isset($data['breadcrumbData']) === true && empty($data['breadcrumbData']) === false) {
            $this->breadcrumb_model->setMultidata($data['breadcrumbData']);
        }

        if (isset($data['breadcrumbDataReset']) === true && empty($data['breadcrumbDataReset']) === false) {
            $this->breadcrumb_model->empty_data();
            $this->breadcrumb_model->setMultidata($data['breadcrumbDataReset']);
        }
        $data["breadcrumb"] = $this->breadcrumb_model->show();
        $dataLayout['content'] = $this->view_layout_return($view_file, $data);
        if ($this->input->get('format') === "json") {
            $json["success"] = true;
            $json["layout"] = $dataLayout['content'];
            exit(json_encode($json));
        }
        $this->load->view($this->path_name . '/' . $this->access_check_model->view_file("_global", $view_path_name, "layout"), $dataLayout);
    }
}

class Back_Controller extends MX_Controller
{

    protected $arr_userdb;
    protected $controller_name;
    protected $controller_url;
    protected $path_name;
    protected $module_path_name;
    protected $module_url = "";
    protected static $title_module = '';

    public function __construct()
    {
        parent::__construct();
        $this->access_check_model->maintenance();
        $this->access_check_model->authorized();

        $this->load->database();

        $this->visitor_model->insert();
        $this->path_name = $this->access_check_model->backPath;
        $this->controller_name = get_class($this);
        $this->controller_url = $this->path_name . "/" . $this->controller_name;
        $this->arr_userdb = $this->user_model->get_one_by_id($this->login_model->user_id());
        $this->language_model->set_language();

        if ($this->login_model->user_id() !== $this->user_model->secure_id) {
            $this->permission_model->setForUser($this->arr_userdb);
            $this->permission_model->checkForUser();
        }

        //is module path
        if (empty($this->router->module) === false && $this->router->module !== $this->access_check_model->backPath) {
            $this->module_url = $this->router->module;
            $this->module_path_name = $this->router->module . "/" . $this->path_name;
            $this->controller_url = $this->module_path_name . "/" . $this->controller_name;
            $lang = $this->language_model->get_language();
            $this->module_model->language($lang);
            loadModuleModel();

            if (empty(self::$title_module) === true) {
                $class = $this->router->class;
                self::$title_module = $this->lang->line($this->router->module . '_' . $this->access_check_model->backPath . '_' . strtolower($class) . '_title', false);
            }
        }
    }

    protected function view_layout_return(string $view_file = "index", array $data = [])
    {
        $data["path_name"] = $this->path_name;
        $data["module_path_name"] = $this->module_path_name;
        $data["module_url"] = $this->module_url;
        $view_path_name = $this->access_check_model->backPath;
        return $this->load->view($this->path_name . '/' . $this->access_check_model->view_file($this->controller_name, $view_path_name, $view_file), $data, true);
    }

    protected function view_layout(string $view_file = "index", array $data = [])
    {
        $dataLayout["title"] = c_key('webapp_title');
        $arr_no_open_message = $this->message_model->get_no_open_message();
        $dataLayout["listdb_message"] = $arr_no_open_message["listdb"];
        $dataLayout["total_new_message"] = $arr_no_open_message["total"];
        $dataLayout["show_total_new_message"] = "d-none";
        if ($dataLayout["total_new_message"] > 0) {
            $dataLayout["show_total_new_message"] = "";
        }

        $dataLayout["path_name"] = $this->path_name;
        $dataLayout["module_path_name"] = $this->module_path_name;
        $dataLayout["module_url"] = $this->module_url;

        $breadcrumbTitle = $dataLayout["title"];
        if (isset($data['title']) === true && empty($data['title']) === false) {
            $dataLayout["title"] .= ' - ' . $data['title'];
            $breadcrumbTitle = $data['title'];
        }

        $this->history_url_model->update($breadcrumbTitle);
        $dataLayout["listdb_history_url"] = $this->history_url_model->get_last();

        $view_path_name = $this->access_check_model->backPath;

        $this->breadcrumb_model->set_template($this->path_name . '/' . $this->access_check_model->view_file("_global", $view_path_name, "breadcrumb"));
        $this->breadcrumb_model->set_data(["name" => lang("back_breadcrumb_home_text"), "url" => $this->login_model->redirect_url()]);
        $this->breadcrumb_model->set_data(["name" => lang("back_breadcrumb_controller_url_text"), "url" => $this->controller_url]);
        if (isset($data['breadcrumbData']) === true && empty($data['breadcrumbData']) === false) {
            $this->breadcrumb_model->empty_data();
            $this->breadcrumb_model->set_data(["name" => lang("back_breadcrumb_home_text"), "url" => $this->login_model->redirect_url()]);
            $this->breadcrumb_model->setMultidata($data['breadcrumbData']);
        }

        if (isset($data['breadcrumbDataReset']) === true && empty($data['breadcrumbDataReset']) === false) {
            $this->breadcrumb_model->empty_data();
            $this->breadcrumb_model->setMultidata($data['breadcrumbDataReset']);
        }

        $breadcrumb = $this->breadcrumb_model->show();
        if (isset($data['breadcrumb']) === true) {
            $breadcrumb = $data['breadcrumb'];
        }
        $dataLayout["breadcrumb"] = $breadcrumb;
        $dataLayout["breadcrumbTitle"] = $breadcrumbTitle;

        $event_result_box = $this->global_model->event_result_box();
        if (isset($data['event_result_box']) === true) {
            $event_result_box = $data['event_result_box'];
        }
        $dataLayout["event_result_box"] = $event_result_box;

        $dataLayout["asset"] = $this->load->view($this->path_name . '/' . $this->access_check_model->view_file("_global", $view_path_name, "asset"), $dataLayout, true);
        $navbarViewFile = $this->user_model->navbarViewFile($this->arr_userdb);
        $dataLayout["navbar"] = $this->load->view($this->path_name . '/' . $this->access_check_model->view_file("_global", $view_path_name, $navbarViewFile), $dataLayout, true);
        $dataLayout["display_info"] = $this->arr_userdb["display_info"];
        $dataLayout['content'] = $this->view_layout_return($view_file, $data);

        if ($this->input->get('format') === "json") {
            $json["success"] = true;
            $json["layout"] = $this->load->view($this->path_name . '/' . $this->access_check_model->view_file("_global", $view_path_name, "layout_json"), $dataLayout, true);
            exit(json_encode($json));
        }
        $this->load->view($this->path_name . '/' . $this->access_check_model->view_file("_global", $view_path_name, "layout"), $dataLayout);
    }
}
