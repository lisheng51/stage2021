<?php

class MY_Exceptions extends CI_Exceptions
{

    public function __construct()
    {
        parent::__construct();
    }

    public function log_exception($severity, $message, $filepath, $line)
    {
        $severity = isset($this->levels[$severity]) ? $this->levels[$severity] : $severity;
        log_message('error', 'Severity: ' . $severity . ' --> ' . $message . ' ' . $filepath . ' ' . $line);
        if (ENVIRONMENT !== 'development') {
            $CI = &get_instance();
            $CI->telegram_model->log($message . ' ' . $filepath . ' ' . $line, $CI->telegram_model->default_chat_id);
        }
    }
}
