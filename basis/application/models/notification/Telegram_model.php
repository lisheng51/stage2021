<?php

class Telegram_model extends CI_Model
{

    public $groupChatId = '-1001386091311'; //group bc notificatie
    public $default_chat_id = '273053602'; //beheerder lisheng ye
    public $logDirnameFile = false;

    public function loadObject(string $setToken = "")
    {
        require_once APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'My_telegram.php';
        $bot_token = empty(c_key('webapp_telegram_bot_token')) === false ? c_key('webapp_telegram_bot_token') : '363080218:AAH62zqTe-BJgipb7weRRgPylljCnbO_4AQ';
        if (empty($setToken) === false) {
            $bot_token = $setToken;
        }
        $object = new My_telegram($bot_token);
        return $object;
    }

    public function log(string $text = "", string $setChatId = '')
    {
        if (empty($text) === true) {
            return "";
        }

        $chat_id = $this->get_chat_id($setChatId);
        $text .= PHP_EOL . 'datetime: ' . date('d-m-Y H:i:s');
        $text .= PHP_EOL . 'site: ' . ENVIRONMENT_BASE_URL;
        $text .= PHP_EOL . 'database: ' . ENVIRONMENT_DATABASE;
        if ($this->logDirnameFile === true) {
            $text .= PHP_EOL . 'path: ' . dirname(__FILE__);
        }
        $content = ['parse_mode' => 'HTML', 'chat_id' => $chat_id, 'text' => $text];
        $object = $this->loadObject();
        $res = $object->sendMessage($content);
        return $res["ok"];
    }

    private function get_chat_id(string $setChatId = '')
    {
        if (empty($setChatId) === false) {
            return $setChatId;
        }
        $groupChatId = empty(c_key('webapp_telegram_group_chat_id')) === false ? c_key('webapp_telegram_group_chat_id') : $this->groupChatId;
        $userChatId = empty(c_key('webapp_telegram_user_chat_id')) === false ? c_key('webapp_telegram_user_chat_id') : $this->default_chat_id;
        return (ENVIRONMENT === 'development') ? $userChatId : $groupChatId;
    }

    public function callback_chatids()
    {
        $object = $this->loadObject();
        //$object->endpoint('deleteWebhook', [], false);
        return $object->getUpdates();
    }

    public function sms(string $recipients = "", $text = "")
    {
        $originator = empty(c_key('webapp_messagebird_originator')) === false ? c_key('webapp_messagebird_originator') : '+31621255392';
        $params = [
            'recipients' => $recipients,
            'originator' => $originator,
            'body' => $text
        ];

        if (ENVIRONMENT !== 'development') {
            $response = $this->curlSMS('https://rest.messagebird.com/messages', $params);
            $object = json_decode($response);
            if (isset($object->id)) {
                return true;
            }
        }

        return false;
    }

    private function curlSMS(string $url = "", array $params = [])
    {
        if (empty($url) === true || filter_var($url, FILTER_VALIDATE_URL) === false) {
            return;
        }
        $apiKey = empty(c_key('webapp_messagebird_api_key')) === false ? c_key('webapp_messagebird_api_key') : 'LE0Gh7iXb0hrL47PfOs8cqKut';
        $httpheaders = array(
            "Authorization:  AccessKey $apiKey"
        );
        $fields_string = http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0); //return url reponse header
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpheaders);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100020);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            return null;
        }
        curl_close($ch);
        if ($response === false || empty($response) === true) {
            return null;
        }
        return $response;
    }
}
