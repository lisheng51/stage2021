<?php

class Breadcrumb_model extends CI_Model
{

    private $arr_data = [];
    private $template = "breadcrumb";

    public function set_data(array $data = [])
    {
        $this->arr_data[] = $data;
    }

    public function empty_data()
    {
        $this->arr_data = [];
    }

    public function setMultidata(array $data = [])
    {
        foreach ($data as $value) {
            $this->arr_data[] = $value;
        }
    }

    public function set_template(string $value = "")
    {
        $this->template = $value;
    }

    public function show(): string
    {
        if (empty($this->arr_data) === true) {
            return "";
        }

        $viewPath = str_replace("/", DIRECTORY_SEPARATOR, $this->template);
        $defaultCk = APPPATH . 'views' . DIRECTORY_SEPARATOR . $viewPath . '.php';
        if (empty($this->router->module) === false) {
            $modulesName = $this->router->module;
            $defaultCk = APPPATH . 'modules' . DIRECTORY_SEPARATOR . $modulesName . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . $viewPath . '.php';
            if (file_exists($defaultCk) === false) {
                $defaultCk = APPPATH . 'views' . DIRECTORY_SEPARATOR . $viewPath . '.php';
            }
        }

        if (file_exists($defaultCk) === false) {
            return "";
        }

        $listdb = [];
        foreach ($this->arr_data as $value) {
            $value["string"] = $value["string"] = '<li class="breadcrumb-item active">' . $value["name"] . '</li>';
            if (isset($value["url"]) && empty($value["url"]) === false) {
                $value["string"] = '<li class="breadcrumb-item"><a href=' . site_url($value["url"]) . '>' . $value["name"] . '</a></li>';
            }
            if (isset($value["event"]) && empty($value["event"]) === false) {
                $value["string"] = '<li class="breadcrumb-item"><a href=' . $value["event"] . '>' . $value["name"] . '</a></li>';
            }
            $listdb[] = $value;
        }
        $data["listdb"] = $listdb;
        return $this->load->view($this->template, $data, true);
    }
}
