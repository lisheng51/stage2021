<?php

class Applog_model extends MY_Model
{

    public $table = "app_log";
    public $primary_key = "log_id";
    public $select_order_by = [];

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'log_id#desc' => 'ID (aflopend)',
            'date#desc' => 'Datum (aflopend)',
            'date#asc' => 'Datum (oplopend)',
            'path#desc' => 'Path (aflopend)',
            'path#asc' => 'Path (oplopend)',
        ];
    }

    public function insert(string $description = "", int $uid = 0)
    {
        if (empty($description) === true) {
            return;
        }
        $data["description"] = $description;
        if ($uid > 0) {
            $data["user_id"] = $uid;
        } else {
            $data["user_id"] = $this->login_model->user_id();
        }
        $data["path"] = uri_string();
        $insertId = $this->db->insert($this->table, $data);

        return $insertId;
    }

    public function get_total(): int
    {
        $this->db->select($this->primary_key);
        $this->db->from($this->table);
        $this->db->join($this->user_model->table, $this->user_model->primary_key, 'LEFT');
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_or_like) === false) {
            foreach ($this->sql_or_like as $field => $value) {
                $this->db->or_like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        return $this->db->count_all_results();
    }

    public function get_list(int $limit = 1, int $page = 0): array
    {
        $this->db->select($this->table . '.*');
        $this->db->select($this->user_model->table . '.emailaddress');
        $this->db->select($this->user_model->table . '.display_info');
        $this->db->from($this->table);
        $this->db->join($this->user_model->table, $this->user_model->primary_key, 'LEFT');
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_or_like) === false) {
            foreach ($this->sql_or_like as $field => $value) {
                $this->db->or_like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        if (empty($this->sql_order_by) === false) {
            foreach ($this->sql_order_by as $field => $value) {
                $this->db->order_by($field, $value);
            }
        }

        $this->db->order_by($this->primary_key, "desc");
        if ($page <= 0) {
            $page = 0;
        }
        $this->db->limit($limit, $page);
        $query = $this->db->get();
        return $query->result_array();
    }
}
