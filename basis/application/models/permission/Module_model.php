<?php

class Module_model extends MY_Model
{

    public $table = "module";
    public $primary_key = "module_id";
    public $sql_order_by = ['sort_list' => "asc"];
    public $select_order_by = [];

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'module_id#desc' => 'ID (aflopend)',
            'path_description#desc' => 'Naam (aflopend)',
            'path_description#asc' => 'Naam (oplopend)',
            'sort_list#desc' => 'Volgorde (aflopend)',
            'sort_list#asc' => 'Volgorde (oplopend)',
        ];
    }

    public function select(int $id = 0, bool $with_empty = false, string $name = ''): string
    {
        $select_name = empty($name) === false ? $name : $this->primary_key;
        if ($id === 0) {
            $id = $this->input->post_get($select_name) ?? 0;
        }
        $this->sqlReset();
        $this->sql_order_by = ['sort_list' => 'asc'];
        $select = '<select name=' . $select_name . ' class="form-control selectpicker" data-live-search="true">';
        $select .= $with_empty === true ? "<option value='' >------</option>" : "";
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = $id == $rs[$this->primary_key] ? "selected" : '';
            $select .= '<option value=' . $rs[$this->primary_key] . ' ' . $ckk . '>' . $rs["path_description"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function selectMultiple(array $haystack = [], string $name = ''): string
    {
        $select_name = empty($name) === false ? $name : $this->primary_key;
        if (empty($haystack) === true) {
            $haystack = $this->input->post_get($select_name) ?? [];
            if (is_array($haystack) === false) {
                $haystack = [$haystack];
            }
        }
        $this->sqlReset();
        $this->sql_order_by = ['sort_list' => 'asc'];
        $select = '<select name="' . $select_name . '[]" class="form-control selectpicker" data-selected-text-format="count > 3" data-live-search="true" multiple title="------">';
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = (empty($haystack) === false && in_array($rs[$this->primary_key], $haystack) === true) ? 'selected' : '';
            $select .= '<option value=' . $rs[$this->primary_key] . ' ' . $ckk . '>' . $rs["path_description"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function get_one_by_path(string $path = ''): array
    {
        $data['path'] = $path;
        $this->sql_where = $data;
        return $this->get_one();
    }

    public function getInfo(string $app_path = "")
    {
        $data = [];
        $data_file = APPPATH . "modules_core/$app_path/info.php";
        if (file_exists($data_file) === false) {
            $data_file = APPPATH . "modules/$app_path/info.php";
        }
        if (file_exists($data_file) === true) {
            require($data_file);
        }
        return $data;
    }

    public function check_exist(array $data = [])
    {
        $arrList = array_keys($data["arrPermission"]);
        $backPath = $this->access_check_model->backPath;
        foreach ($arrList as $object) {
            $filename = "controllers/" . $backPath . '/' . $object . '.php';
            $path = $filename;

            if (isset($data['use_path']) && $data['use_path'] === true) {
                $path = "modules/" . $data['path'] . '/' . $filename;
            }

            if (file_exists(APPPATH . $path) === false) {
                $json["status"] = "error";
                $json['msg'] = 'Geen bestand gevonden!';
                exit(json_encode($json));
            }
        }
    }

    public function get_postdata(): array
    {
        $data["is_active"] = $this->input->post("is_active");
        $data["path_description"] = $this->input->post("path_description");
        return $data;
    }

    public function update(array $fromdata = [])
    {
        $data["path"] = $fromdata["path"];
        $data["use_path"] = intval($fromdata["use_path"]);
        $data["path_description"] = $fromdata["path_description"];
        $rsdb = $this->get_one_by_path($data["path"]);

        if (empty($rsdb) === true) {
            $data['is_active'] = 1;
            $module_id = $this->add($data);
            $this->permission_model->update($module_id, $fromdata);
        } else {
            $this->edit($rsdb[$this->primary_key], $data);
            $this->permission_model->update($rsdb[$this->primary_key], $fromdata);
        }
    }

    public function setup(array $fromdata = [])
    {
        if (isset($fromdata['use_path']) && $fromdata['use_path'] === true) {
            $module = $fromdata["path"];
            $this->upload_model->make_dir($module);
            $this->insertSqlFile($module);
        }

        if (isset($fromdata['arrUploadFolder']) && empty($fromdata['arrUploadFolder']) === false) {
            foreach ($fromdata['arrUploadFolder'] as $path) {
                $this->upload_model->make_dir($path);
            }
        }
    }

    public function copieLiveDataUrl(string $module = "", string $liveUrl = ''): string
    {
        if (empty($module) === true) {
            return "";
        }
        $linesFilePath = APPPATH . 'modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . 'sql' . DIRECTORY_SEPARATOR;
        $arrSqlFile = directory_map($linesFilePath);

        if (empty($arrSqlFile) === true) {
            return "";
        }

        foreach ($arrSqlFile as $name) {
            $fileList[] = basename($name, ".sql");
        }
        $tables = implode(',', $fileList);
        $url = site_url("su/CopieTest/module/yes?url=$liveUrl&tables=$tables");
        return $url;
    }

    private function insertSqlFile(string $module = "")
    {
        $fileList = [];
        $linesFilePath = APPPATH . 'modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . 'sql' . DIRECTORY_SEPARATOR;
        $arrSqlFile = directory_map($linesFilePath);
        if (empty($arrSqlFile) === false) {
            foreach ($arrSqlFile as $name) {
                $fileList[] = basename($name, ".sql");
            }
            foreach ($fileList as $tableName) {
                if ($this->db->table_exists($tableName) === false) {
                    $templine = '';
                    $lines = file(APPPATH . '/modules/' . $module . '/sql/' . $tableName . '.sql');
                    foreach ($lines as $line) {
                        if (substr($line, 0, 2) == '--' || $line == '') {
                            continue;
                        }
                        $templine .= $line;
                        if (substr(trim($line), -1, 1) == ';') {
                            $this->db->query($templine);
                            $templine = '';
                        }
                    }
                }
            }
        }
    }

    public function updateSql(array $sqlList = [])
    {
        if (empty($sqlList) === true) {
            return;
        }
        foreach ($sqlList as $tableName => $arrValue) {
            if ($this->db->table_exists($tableName) === true && empty($arrValue) === false) {
                foreach ($arrValue as $val) {
                    if ($this->db->field_exists($val["field"], $tableName) === $val["exists"]) {
                        $this->db->query($val["sql"]);
                    }
                }
            }
        }
    }

    public function navbar(string $moduleName = '', string $filename = '_navbar')
    {
        if (empty($moduleName) === true) {
            return "";
        }
        $existInstall = $this->get_one_by_path($moduleName);
        $backPath = $this->access_check_model->backPath;
        $ck_file_exist_path = APPPATH . "modules/$moduleName/" . "views/" . $backPath . "/_global/" . $filename . ".php";
        if (file_exists($ck_file_exist_path) === true && empty($existInstall) === false && $existInstall["is_active"] > 0) {
            $data['path_name'] = $moduleName . '/' . $backPath;
            $data['moduleName'] = $existInstall["path_description"];
            $data['targetName'] = $existInstall["path"];
            $content = $this->load->view($moduleName . '/' . $backPath . '/_global/' . $filename, $data, true);
            return $content;
        }

        return "";
    }

    public function autoNavbar(string $filename = '_navbar')
    {
        $content = "";
        $data_where["is_active"] = 1;
        $this->sql_where = $data_where;
        $modulesmap = $this->get_all();
        $backPath = $this->access_check_model->backPath;
        foreach ($modulesmap as $module) {
            $moduleName = $module["path"];
            $ck_file_exist_path = APPPATH . "modules/$moduleName/" . "views/" . $backPath . "/_global/" . $filename . ".php";
            if (file_exists($ck_file_exist_path) === true) {
                $data['path_name'] = $moduleName . '/' . $backPath;
                $data['moduleName'] = $module["path_description"];
                $data['targetName'] = $moduleName;
                $content .= $this->load->view($moduleName . '/' . $backPath . '/_global/' . $filename, $data, true);
            }
        }
        return $content;
    }

    public function language(string $lang = "")
    {
        $modulesmapAll = directory_map(APPPATH . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR, 1);

        foreach ($modulesmapAll as $module) {
            $moduleName = rtrim($module, DIRECTORY_SEPARATOR);
            if ($this->router->module == $moduleName) {
                $modulesmap = directory_map(APPPATH . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $moduleName . DIRECTORY_SEPARATOR . 'language' . DIRECTORY_SEPARATOR . $lang, 1);
                if (empty($modulesmap) === false) {
                    foreach ($modulesmap as $filenameLong) {
                        $filename = str_replace('_lang.php', '', rtrim($filenameLong, DIRECTORY_SEPARATOR));
                        $ck_file_default = APPPATH . "modules/$moduleName/language/$lang/" . $filename . '_lang.php';
                        if (file_exists($ck_file_default) === true) {
                            $this->lang->load($filename, $lang, false, true, APPPATH . "modules/$moduleName/");
                        }
                    }
                }
            }
        }
    }

    public function loadLanguageFile(string $moduleName = "", string $filename = "", string $setLang = "")
    {
        $lang = empty($setLang) === true ? $this->language_model->get_language() : $setLang;
        $modulesmap = directory_map(APPPATH . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $moduleName . DIRECTORY_SEPARATOR . 'language' . DIRECTORY_SEPARATOR . $lang, 1);
        if (empty($modulesmap) === false) {
            $ck_file_default = APPPATH . "modules/$moduleName/language/$lang/" . $filename . '_lang.php';
            if (file_exists($ck_file_default) === true) {
                $this->lang->load($filename, $lang, false, true, APPPATH . "modules/$moduleName/");
            }
        }
    }

    public function selectModule($choose_id = "")
    {
        $select = '<select name="moduleName" class="form-control selectpicker">';
        $select .= "<option value=''>Core</option>";
        foreach ($this->get_all() as $rs) {
            $ckk = $choose_id == $rs['path'] ? "selected" : '';
            $select .= "<option value={$rs["path"]} $ckk >{$rs["path_description"]}</option>";
        }
        $select .= '</select>';
        return $select;
    }
}
