<?php

class Permission_group_type_model extends CI_Model
{

    public $primary_key = "permission_group_type_id";
    public $group_id_1 = 1;
    public $group_id_2 = 2;

    public function select(int $id = 0, bool $with_empty = false): string
    {
        if ($id === 0) {
            $id = $this->input->post_get($this->primary_key) ?? 0;
        }

        $select = '<select name=' . $this->primary_key . ' class="form-control selectpicker" data-live-search="true">';
        $select .= $with_empty === true ? "<option value='' >------</option>" : "";
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = $id == $rs[$this->primary_key] ? "selected" : '';
            $select .= '<option value=' . $rs[$this->primary_key] . ' ' . $ckk . '>' . $rs["name"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function selectMultiple(array $haystack = []): string
    {
        if (empty($haystack) === true) {
            $haystack = $this->input->post_get($this->primary_key) ?? [];
            if (is_array($haystack) === false) {
                $haystack = [$haystack];
            }
        }
        $listdb = $this->get_all();
        $select = '<select name="' . $this->primary_key . '[]" class="form-control selectpicker" data-selected-text-format="count > 3" data-live-search="true" multiple title="------">';
        foreach ($listdb as $rs) {
            $ckk = (empty($haystack) === false && in_array($rs[$this->primary_key], $haystack) === true) ? 'selected' : '';
            $select .= '<option value=' . $rs[$this->primary_key] . ' ' . $ckk . '>' . $rs["name"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function selectPath(string $path = "back", string $name = 'link_dir'): string
    {
        $select = '<select name=' . $name . ' class="form-control selectpicker">';
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = $path == $rs['path'] ? "selected" : '';
            $select .= '<option value=' . $rs['path'] . ' ' . $ckk . '>' . $rs["path"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function get_all(): array
    {
        $data[] = $this->group_id_1();
        $data[] = $this->group_id_2();
        return $data;
    }

    private function group_id_1(string $key = '')
    {
        $data[$this->primary_key] = $this->group_id_1;
        $data['name'] = 'Back';
        $data['path'] = 'back';
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    private function group_id_2(string $key = '')
    {
        $data[$this->primary_key] = $this->group_id_2;
        $data['name'] = 'API';
        $data['path'] = 'api';
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    public function fetchField(int $id = 0, string $field = 'name'): string
    {
        switch ($id) {
            case $this->group_id_1:
                $value = $this->group_id_1($field);
                break;
            case $this->group_id_2:
                $value = $this->group_id_2($field);
                break;
            default:
                $value = '';
                break;
        }
        return $value;
    }
}
