<?php

class Permission_model extends MY_Model
{

    public $table = "permission";
    public $primary_key = "permission_id";
    public $permisson = [];
    public $permissonNav = [];
    private $removeMethods = ["__construct", "_remap", "__get"];
    public $session_key = "permission";
    public $select_order_by = [];

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'permission_id#desc' => 'ID (aflopend)',
            'description#desc' => 'Beschrijving (aflopend)',
            'description#asc' => 'Beschrijving (oplopend)',
        ];
    }

    public function getPostdata(): array
    {
        $data["description"] = $this->input->post("description");
        $data["link_title"] = $this->input->post("link_title");
        $data["has_link"] = $this->input->post("has_link");
        return $data;
    }

    public function checkHas(string $ck_permission = "", string $setPath = ""): bool
    {
        if (ENVIRONMENT_PERMISSION_CHECK === false) {
            return true;
        }
        $path = $this->access_check_model->backPath;
        if (empty($this->router->module) === false && $this->router->module !== $this->access_check_model->backPath) {
            $path = $this->router->module;
        }

        if (empty($setPath) === false) {
            $path = $setPath;
        }

        $ck = in_array($path . '.' . $ck_permission, $this->permisson, true);
        if ($ck === true || $this->login_model->user_id() === $this->user_model->secure_id) {
            return true;
        }
        return false;
    }

    public function checkForUser()
    {
        if (ENVIRONMENT_PERMISSION_CHECK === false) {
            return;
        }
        $path = $this->access_check_model->backPath;
        if (empty($this->router->module) === false && $this->router->module !== $this->access_check_model->backPath) {
            $path = $this->router->module;
        }
        $class = ucfirst($this->router->class);
        $method = $this->router->method;
        $ckPermission = in_array($path . '.' . $class . '.' . $method, $this->permisson, true);
        if ($ckPermission === false) {
            if ($this->input->post()) {
                $json["msg"] = 'U bent niet gemachtigd om hier te komen!';
                $json["status"] = "error";
                exit(json_encode($json));
            }

            $this->session->set_flashdata('page_error_type', "no_authorized");
            redirect(error_url());
        }
    }

    public function setForUser(array $userDB = [])
    {
        if (empty($userDB) === true) {
            redirect(login_url());
        }

        $session_data = $this->session->userdata($this->session_key);
        if (empty($session_data) === false && ENVIRONMENT !== 'development') {
            $this->permissonNav = $session_data["permissionsNav"];
            $this->permisson = $session_data["permisson"];
            return $session_data;
        }

        $ids = $userDB["permission_group_ids"];
        if (empty($ids) === true) {
            $this->session->set_flashdata('page_error_type', "no_authorized");
            redirect(error_url());
        }

        $listdbGroup = $this->permission_group_model->get_all_group($ids);
        $stringCompares = array_column($listdbGroup, 'permission_ids');

        $permissionArray = $all_permissions = $permissions = $permissionsNav = [];
        foreach ($stringCompares as $stringCompare) {
            $permissionArray[] = explode(',', $stringCompare);
        }
        $all_permissions = array_reduce($permissionArray, 'array_merge', []);
        $all_permissions = array_unique($all_permissions);

        if (empty($all_permissions) === false) {
            $this->sql_where = [$this->module_model->table . ".is_active" => 1];
            $this->sql_where_in = [$this->table . "." . $this->primary_key => $all_permissions];
            $listdb = $this->get_all();

            foreach ($listdb as $rsdb) {
                $path = $rsdb['use_path'] > 0 ? $rsdb['path'] : $this->access_check_model->backPath;
                $permissions[] = $path . '.' . $rsdb['object'] . '.' . $rsdb['method'];
                if ($rsdb["has_link"] > 0) {
                    $startPath = $rsdb["use_path"] > 0 ? $rsdb["path"] . '/' : "";
                    $data["url"] = $startPath . $rsdb["link_dir"] . '/' . $rsdb['object'] . '/' . $rsdb['method'];
                    $data["name"] = $rsdb["link_title"];
                    $keyString = $rsdb["path"] . '#' . $rsdb["path_description"];
                    $permissionsNav[$keyString][] = $data;
                }
            }
        }

        $dataSession["permissionsNav"] = $permissionsNav;
        $dataSession["permisson"] = array_unique($permissions);
        $this->permissonNav = $dataSession["permissionsNav"];
        $this->permisson = $dataSession["permisson"];
        $this->session->set_userdata($this->session_key, $dataSession);
    }

    public function select(int $id = 0, bool $with_empty = false): string
    {
        $select = '<select name=' . $this->primary_key . ' class="form-control selectpicker">';
        $select .= $with_empty === true ? "<option value='' >------</option>" : "";
        foreach ($this->get_all() as $rs) {
            $ckk = $id == $rs[$this->primary_key] ? "selected" : '';
            $select .= "<option value={$rs[$this->primary_key]} $ckk >{$rs["description"]}</option>";
        }
        $select .= '</select>';
        return $select;
    }

    public function get_total(): int
    {
        $this->db->select($this->primary_key);
        $this->db->from($this->table);
        $this->db->join($this->module_model->table, $this->module_model->primary_key);
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        return $this->db->count_all_results();
    }

    public function get_list(int $limit = 1, int $page = 0): array
    {
        $this->db->from($this->table);
        $this->db->join($this->module_model->table, $this->module_model->primary_key);
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        if (empty($this->sql_order_by) === false) {
            foreach ($this->sql_order_by as $field => $value) {
                $this->db->order_by($field, $value);
            }
        }

        $this->db->order_by('sort_list', "asc");
        $this->db->order_by('order_num', "asc");
        if ($page <= 0) {
            $page = 0;
        }
        $this->db->limit($limit, $page);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update(int $module_id = 0, array $moduleInfoData = [])
    {
        $arrPermission = $moduleInfoData['arrPermission'];
        if (empty($arrPermission) === true || $module_id <= 0) {
            return;
        }

        $backPath = $this->access_check_model->backPath;
        $app_path = 'controllers' . DIRECTORY_SEPARATOR . $backPath . DIRECTORY_SEPARATOR;

        if (isset($moduleInfoData['use_path']) && $moduleInfoData['use_path'] === true) {
            $app_path = "modules" . DIRECTORY_SEPARATOR . $moduleInfoData['path'] . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR . $backPath . DIRECTORY_SEPARATOR;
        }

        $arrResultBack = [];
        foreach ($arrPermission as $object => $pathfileinfo) {
            $matches = [];
            $fileContent = file_get_contents(APPPATH . $app_path . $object . '.php');
            preg_match_all('/public function (.*?)\(/', $fileContent, $matches);
            $arrMethod = end($matches);
            $arrPathData = [];
            foreach ($arrMethod as $method) {
                if (in_array($method, $this->removeMethods, true) === true) {
                    continue;
                }

                $data["module_id"] = $module_id;
                $data["object"] = $object;
                $data["method"] = $method;
                $data["description"] = $module_id . '.' . $backPath . '.' . $object . '.' . $method;
                $data["link_dir"] = $backPath;
                $data["link_title"] = "";
                $data["has_link"] = 0;
                foreach ($pathfileinfo as $arrMethodCk) {
                    foreach ($arrMethodCk as $ck_method => $link_title) {
                        if ($ck_method == $method) {
                            $data["link_title"] = $link_title;
                            $data["has_link"] = 1;
                        }
                    }
                }

                $arrPathData[] = $data;
            }

            $arrResultBack[] = $arrPathData;
        }

        $arrResult = $arrResultBack;
        if (isset($moduleInfoData['arrApiPermission'])) {
            $arrResultApi =  $this->updateArrApiPermission($module_id, $moduleInfoData);
            $arrResult = array_merge($arrResultBack, $arrResultApi);
        }

        if (empty($arrResult) === false) {
            foreach ($arrResult as $arrMethod) {
                foreach ($arrMethod as $data) {
                    $ckData["method"] = $data["method"];
                    $ckData["object"] = $data["object"];
                    $ckData["module_id"] = $data["module_id"];
                    $ckData["link_dir"] = $data["link_dir"];
                    $this->sql_where = $ckData;
                    $rsdb = $this->get_one();
                    if (empty($rsdb) === true) {
                        $data["order_num"] = 0;
                        $this->add($data);
                    } else {
                        $this->edit($rsdb[$this->primary_key], $data);
                    }
                }
            }
        }
    }

    private function updateArrApiPermission(int $module_id = 0, array $moduleInfoData = []): array
    {
        $arrPermission = $moduleInfoData['arrApiPermission'];
        if (empty($arrPermission) === true || $module_id <= 0) {
            return [];
        }

        $backPath = "api";
        $app_path = 'controllers' . DIRECTORY_SEPARATOR . $backPath . DIRECTORY_SEPARATOR;

        if (isset($moduleInfoData['use_path']) && $moduleInfoData['use_path'] === true) {
            $app_path = "modules" . DIRECTORY_SEPARATOR . $moduleInfoData['path'] . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR . $backPath . DIRECTORY_SEPARATOR;
        }

        $arrResult = [];
        foreach ($arrPermission as $object => $pathfileinfo) {
            $matches = [];
            $fileContent = file_get_contents(APPPATH . $app_path . $object . '.php');
            preg_match_all('/public function (.*?)\(/', $fileContent, $matches);
            $arrMethod = end($matches);
            $arrPathData = [];
            foreach ($arrMethod as $method) {
                if (in_array($method, $this->removeMethods, true) === true) {
                    continue;
                }

                $data["module_id"] = $module_id;
                $data["object"] = $object;
                $data["method"] = $method;
                $data["description"] = $module_id . '.' . $backPath . '.' . $object . '.' . $method;
                $data["link_dir"] = $backPath;
                $data["link_title"] = "";
                $data["has_link"] = 0;
                foreach ($pathfileinfo as $arrMethodCk) {
                    foreach ($arrMethodCk as $ck_method => $link_title) {
                        if ($ck_method == $method) {
                            $data["link_title"] = $link_title;
                            $data["has_link"] = 1;
                        }
                    }
                }

                $arrPathData[] = $data;
            }

            $arrResult[] = $arrPathData;
        }

        return $arrResult;
    }
}
