<?php

class Permission_group_model extends MY_Model
{

    public $table = "permission_group";
    public $primary_key = "permission_group_id";
    public $select_order_by = [];

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'permission_group_id#desc' => 'ID (aflopend)',
            'name#desc' => 'Naam (aflopend)',
            'name#asc' => 'Naam (oplopend)',
            'permission_group_type_id#desc' => 'Type (aflopend)',
            'permission_group_type_id#asc' => 'Type (oplopend)',
        ];
    }

    public function select(int $id = 0, bool $with_empty = false, string $name = '', int $type_id = 1): string
    {
        $select_name = empty($name) === false ? $name : $this->primary_key;
        if ($id === 0) {
            $id = $this->input->post_get($select_name) ?? 0;
        }
        $this->sqlReset();
        $data[$this->_field_is_deleted] = 0;
        $data[$this->permission_group_type_model->primary_key] = $type_id;
        $this->sql_where = $data;
        $this->sql_order_by = ['sort_list_group' => 'asc'];
        $select = '<select name=' . $select_name . ' class="form-control selectpicker" data-live-search="true">';
        $select .= $with_empty === true ? "<option value='' >------</option>" : "";
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = $id == $rs[$this->primary_key] ? "selected" : '';
            $select .= '<option value=' . $rs[$this->primary_key] . ' ' . $ckk . '>' . $rs["name"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function selectMultiple(array $haystack = [], string $name = '', int $type_id = 1): string
    {
        $select_name = empty($name) === false ? $name : $this->primary_key;
        if (empty($haystack) === true) {
            $haystack = $this->input->post_get($select_name) ?? [];
            if (is_array($haystack) === false) {
                $haystack = [$haystack];
            }
        }
        $this->sqlReset();
        $data[$this->_field_is_deleted] = 0;
        $data[$this->permission_group_type_model->primary_key] = $type_id;
        $this->sql_where = $data;
        $this->sql_order_by = ['sort_list_group' => 'asc'];
        $select = '<select name="' . $select_name . '[]" class="form-control selectpicker" data-selected-text-format="count > 3" data-live-search="true" multiple title="------">';
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = (empty($haystack) === false && in_array($rs[$this->primary_key], $haystack) === true) ? 'selected' : '';
            $select .= '<option value=' . $rs[$this->primary_key] . ' ' . $ckk . '>' . $rs["name"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function get_one_by_name(string $subject = ""): array
    {
        $data["name"] = $subject;
        $this->sql_where = $data;
        return $this->get_one();
    }

    public function getPostdata(): array
    {
        $data["name"] = $this->input->post("name");
        $data[$this->permission_group_type_model->primary_key] = $this->input->post($this->permission_group_type_model->primary_key);
        $permission_ids = $this->input->post($this->permission_model->primary_key);
        if (empty($permission_ids) === true) {
            $json["msg"] = 'Toestemming is leeg!';
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $data["is_lock"] = $this->input->post("is_lock") ?? 0;
        $data["permission_ids"] = implode(',', $permission_ids);
        $this->ajaxck_model->ck_value('name', $data["name"]);
        return $data;
    }

    public function get_all_group(string $string = ""): array
    {
        $arr_result = [];
        if (empty($string) === true) {
            return $arr_result;
        }
        $array_in = explode(',', $string);
        $this->sql_where_in = [$this->primary_key => $array_in];
        $result = $this->get_all();
        $this->sql_where_in = [];
        return $result;
    }
}
