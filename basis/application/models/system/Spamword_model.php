<?php

class Spamword_model extends MY_Model
{

    public $table = "spam_word";
    public $primary_key = "word_id";
    public $limit = 1;
    public $doCheck = true;
    public $select_order_by = [];
    protected $using_softDel = true;

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'word_id#desc' => 'ID (aflopend)',
            'word#desc' => 'Woord (aflopend)',
            'word#asc' => 'Woord (oplopend)',
        ];
    }

    public function get_one_by_word(string $value = ''): array
    {
        $data['word'] = $value;
        $this->sql_where = $data;
        return $this->get_one();
    }

    private function make_string()
    {
        $this->sql_where = ["is_del" => 0];
        $arr_result = $this->get_all();
        if (empty($arr_result) === false) {
            $new_array = [];
            foreach ($arr_result as $value) {
                $new_array[] = $value["word"];
            }
            return implode("|", $new_array);
        }
        return;
    }

    public function check(array $data = []): bool
    {
        if (empty($data) === true || $this->doCheck === false) {
            return true;
        }
        $total_find = 0;
        $matches = [];
        $string = $this->make_string();
        if (empty($string) === true) {
            return true;
        }
        foreach ($data as $value) {
            preg_match_all('/' . $string . '/i', trim($value), $matches);
            $total_find += count($matches[0]);
        }
        if ($total_find >= $this->limit) {
            return false;
        }
        return true;
    }

    public function check_one(string $content = ""): bool
    {
        if (empty($content) === true || $this->doCheck === false) {
            return true;
        }
        $matches = [];
        $string = $this->make_string();
        if (empty($string) === true) {
            return true;
        }
        preg_match_all('/' . $string . '/i', $content, $matches);
        if (count($matches[0]) >= $this->limit) {
            return false;
        }
        return true;
    }

    public function getPostdata(): array
    {
        $data["word"] = $this->input->post("word");
        $this->ajaxck_model->ck_value('word', $data["word"]);
        return $data;
    }
}
