<?php

class Config_model extends CI_Model
{

    public $table = "config";
    public $session_key = "config";
    public $upload_folder = 'config';
    public $configNow = [];

    public function make_dir()
    {
        $path = FCPATH . $this->upload_model->root_folder . DIRECTORY_SEPARATOR . $this->upload_folder;
        if (is_dir($path) === false) {
            mkdir($path, 0755, true);
        }
    }

    public function input(string $key = "", string $label = "", string $type = 'text', array $config = [])
    {
        if (empty($label) === true) {
            $label = $key;
        }
        if (empty($config) === true) {
            $config = $this->configNow;
        }

        $value = null;
        if (isset($config[$key])) {
            $value = $config[$key];
        }
        return '<div class="form-group"><label>' . $label . '</label><input type="' . $type . '" class="form-control" name="' . $key . '" value="' . $value . '"></div>';
    }

    public function textarea(string $key = "", string $label = "", int $row = 8, array $config = [])
    {
        if (empty($label) === true) {
            $label = $key;
        }
        if (empty($config) === true) {
            $config = $this->configNow;
        }

        $value = null;
        if (isset($config[$key])) {
            $value = $config[$key];
        }
        return '<div class="form-group"><label>' . $label . '</label><textarea class="form-control" rows="' . $row . '" name="' . $key . '"  >' . $value . '</textarea></div>';
    }

    public function get_all(): array
    {
        $this->db->from($this->table);
        $query = $this->db->get();
        $listdb = $query->result_array();
        $result = [];
        if (empty($listdb) === false) {
            foreach ($listdb as $v) {
                $result[$v['c_key']] = $v['c_value'];
            }
        }
        return $result;
    }

    public function fetch(string $key = "")
    {
        $config = $this->get_session();
        if (empty($key) === true) {
            return $config;
        }
        if (isset($config[$key])) {
            return $config[$key];
        }
        return null;
    }

    public function set_session(): array
    {
        $array = $this->get_all();
        $this->session->set_userdata($this->session_key, $array);
        return $array;
    }

    public function get_session(): array
    {
        $session_data = $this->session->userdata($this->session_key);
        if (empty($session_data) === false) {
            return $session_data;
        }
        return $this->set_session();
    }

    public function get_one_by_key(string $key = ''): string
    {
        $this->db->from($this->table);
        $this->db->where('c_key', $key);
        $query = $this->db->get();
        $rsdb = $query->row_array();
        return $rsdb["c_value"] ?? "";
    }

    public function update(array $webdbs = []): bool
    {
        $result = false;
        if (is_array($webdbs)) {
            $insertResult = $ckIn = $data = [];
            foreach ($webdbs as $key => $value) {
                $ckIn[] = $key;
                $insertResult['c_key'] = $key;
                $insertResult['c_value'] = $value;
                $data[] = $insertResult;
            }

            if (empty($ckIn) === false) {
                $this->db->from($this->table)->where_in('c_key', $ckIn)->delete();
                if (empty($data) === false) {
                    $this->db->insert_batch($this->table, $data);
                }
                $this->load->dbutil();
                $this->dbutil->repair_table($this->table);
                $this->dbutil->optimize_table($this->table);
                $result = true;
            }
        }

        return $result;
    }

    public function decryptData(string $result = ""): string
    {
        return $this->encryption->decrypt($result);
    }

    public function encryptData(string $result = ""): string
    {
        return $this->encryption->encrypt($result);
    }
}
