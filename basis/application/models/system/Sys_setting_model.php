<?php

/**
 * 
 * 
 * WARNING: This feature is DEPRECATED and currently available only
  |          for backwards compatibility purposes!
 */
class Sys_setting_model extends CI_Model
{

    protected function file_path(): string
    {
        return APPPATH . 'config/' . $this->filename() . '.php';
    }

    protected function keys(): array
    {
        $keys = [
            "webapp_title" => "webapp_title",
            "webapp_default_show_per_page" => "webapp_default_show_per_page",
            "webapp_limit_word" => "webapp_limit_word",
            "webapp_noreply_email_address" => "webapp_noreply_email_address",
            "webapp_noreply_email_name" => "webapp_noreply_email_name",
            "webapp_master_email_address" => "webapp_master_email_address",
            "webapp_keywords" => "webapp_keywords",
            "webapp_description" => "webapp_description",
            "webapp_ck_pass_unuse_day" => "webapp_ck_pass_unuse_day",
            "webapp_ck_pass_notify_day" => "webapp_ck_pass_notify_day",
            "webapp_ck_pass_reset_hour" => "webapp_ck_pass_reset_hour",
            "webapp_ck_recaptcha_num" => "webapp_ck_recaptcha_num",
            "webapp_smtp_host" => "webapp_smtp_host",
            "webapp_smtp_port" => "webapp_smtp_port",
            "webapp_smtp_crypto" => "webapp_smtp_crypto",
            "webapp_smtp_user" => "webapp_smtp_user",
            "webapp_smtp_user_name" => "webapp_smtp_user_name",
            "webapp_smtp_pass" => "webapp_smtp_pass"
        ];
        return $keys;
    }

    protected function filename(): string
    {
        return 'sys_setting';
    }

    protected function autoFieldClass(): string
    {
        return 'col-md-3';
    }

    public function update(array $data = [])
    {
        if (empty($data) === true) {
            $json["msg"] = "Geen data gevonden!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $file_path = $this->file_path();

        if (file_exists($file_path) === false) {
            $json["msg"] = "$file_path kan het niet vinden!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $writefile = "<?php\r\n";
        foreach ($data as $key => $value) {
            $writefile .= '$config["' . $key . '"] = "' . addslashes($value) . '";' . PHP_EOL;
        }

        if (write_file($file_path, $writefile) === false) {
            $json["msg"] = "$file_path heeft geen toegang!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $json["msg"] = "Instelling is succes opgeslagen";
        $json["status"] = "good";
        exit(json_encode($json));
    }

    public function initialize()
    {
        $this->update($this->keys());
    }

    public function autoField(): string
    {
        $config = $this->fetch();
        $writefile = '<div class="row">';

        foreach ($this->keys() as $key => $label) {
            $input = "text";
            if ($key === 'password') {
                $input = "password";
            }
            $writefile .= '<div class=' . $this->autoFieldClass() . '><div class="form-group"><label>' . $label . '</label><input type="' . $input . '" class="form-control" name="' . $key . '" value="' . $config[$key] . '"></div></div>';
        }

        $writefile .= '</div>';
        return $writefile;
    }

    public function fetch(string $key = "")
    {
        $config = $this->load->config($this->filename(), true);
        if (empty($key) === false) {
            return $config[$key];
        }
        return $config;
    }
}
