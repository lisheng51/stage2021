<?php

class Language_model extends MY_Model
{

    public $table = "language";
    public $primary_key = "language_id";
    public $default_lang = ENVIRONMENT_DEFAULT_LANGUAGE;
    public $select_order_by = [];

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'language_id#desc' => 'ID (aflopend)',
            'name#desc' => 'Naam (aflopend)',
            'name#asc' => 'Naam (oplopend)',
        ];
    }

    public function selectFolder(string $folder = "", string $name = ''): string
    {
        $select_name = empty($name) === false ? $name : 'defaultLanguage';
        $select = '<select name="' . $select_name . '" class="form-control selectpicker" data-live-search="true">';
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = $folder == $rs['folder'] ? "selected" : '';
            $select .= '<option value="' . $rs["folder"] . '" ' . $ckk . '>' . $rs["folder"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }


    public function selectList(string $moduleName = ''): array
    {
        $folderPath = DIRECTORY_SEPARATOR . 'language' . DIRECTORY_SEPARATOR;
        if (empty($moduleName) === false) {
            $folderPath = 'modules' . DIRECTORY_SEPARATOR . $moduleName . DIRECTORY_SEPARATOR . 'language' . DIRECTORY_SEPARATOR;
        }
        $modulesmap = directory_map(APPPATH . $folderPath, 1);
        $arrResult = [];
        if (empty($modulesmap) === false) {
            foreach ($modulesmap as $module) {
                $arrResult[] = rtrim($module, DIRECTORY_SEPARATOR);
            }
        }
        return $arrResult;
    }

    public function select(int $id = 0, bool $with_empty = false, string $name = ''): string
    {
        $select_name = empty($name) === false ? $name : $this->primary_key;
        if ($id === 0) {
            $id = $this->input->post_get($select_name) ?? 0;
        }
        $this->sqlReset();
        $data[$this->_field_is_deleted] = 0;
        $this->sql_where = $data;
        $this->sql_order_by = ['order_list' => 'asc'];
        $select = '<select name=' . $select_name . ' class="form-control selectpicker" data-live-search="true">';
        $select .= $with_empty === true ? "<option value='' >------</option>" : "";
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = $id == $rs[$this->primary_key] ? "selected" : '';
            $select .= '<option value=' . $rs[$this->primary_key] . ' ' . $ckk . '>' . $rs["name"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function selectMultiple(array $haystack = [], string $name = ''): string
    {
        $select_name = empty($name) === false ? $name : $this->primary_key;
        if (empty($haystack) === true) {
            $haystack = $this->input->post_get($select_name) ?? [];
            if (is_array($haystack) === false) {
                $haystack = [$haystack];
            }
        }
        $this->sqlReset();
        $data[$this->_field_is_deleted] = 0;
        $this->sql_where = $data;
        $this->sql_order_by = ['order_list' => 'asc'];
        $select = '<select name="' . $select_name . '[]" class="form-control selectpicker" data-selected-text-format="count > 3" data-live-search="true" multiple title="------">';
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = (empty($haystack) === false && in_array($rs[$this->primary_key], $haystack) === true) ? 'selected' : '';
            $select .= '<option value=' . $rs[$this->primary_key] . ' ' . $ckk . '>' . $rs["name"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function set_language()
    {
        $lang = $this->get_language();

        $arrResult = $this->language_info_model->files($lang);
        if (empty($arrResult) === false) {
            foreach ($arrResult as $file) {
                $this->lang->load($file, $lang);
            }
        }
    }

    public function get_language()
    {
        $lang = $this->input->get($this->table);
        if (empty($lang) === false && in_array($lang, $this->selectList()) === true) {
            $this->session->set_userdata($this->table, $lang);
            return $lang;
        }
        $session_data = $this->session->userdata($this->table);
        if (empty($session_data) === false) {
            $lang = $session_data;
            return $lang;
        }
        return $this->default_lang;
    }

    public function get_postdata(): array
    {
        $data["name"] = $this->input->post("name");
        $data["folder"] = $this->input->post("folder");
        $data["code"] = $this->input->post("code");
        $data["icon"] = $this->input->post("icon");
        $this->ajaxck_model->ck_value('name', $data["name"]);
        return $data;
    }

    public function getFieldData(string $field = 'code'): array
    {
        $listdb = $this->get_all();
        $arrResult = [];
        foreach ($listdb as $value) {
            $arrResult[] = $value[$field];
        }

        return $arrResult;
    }

    public function fetch_folder(int $id = 0): string
    {
        return $this->fetch_field($id, 'folder');
    }

    public function fetch_code(int $id = 0): string
    {
        return $this->fetch_field($id, 'code');
    }

    public function fetch_icon(int $id = 0): string
    {
        return $this->fetch_field($id, 'icon');
    }

    public function fetch_name(int $id = 0): string
    {
        return $this->fetch_field($id, 'name');
    }

    public function fetch_id(string $setLang = ""): string
    {
        $lang = empty($setLang) === true ? $this->get_language() : $setLang;
        $arr_data = $this->get_one_by_field('folder', $lang);
        if (empty($arr_data) === true) {
            return 1;
        }
        return $arr_data[$this->primary_key];
    }

    public function fieldByLanguage(string $field = "name", string $setLang = "")
    {
        $language = empty($setLang) === true ? $this->get_language() : $setLang;
        $fieldName = $field;

        $arr_data = $this->get_one_by_field('folder', $language);
        if (empty($arr_data) === false && $language != $this->default_lang) {
            $fieldName = $field . '_' . $arr_data["code"];
        }
        return $fieldName;
    }
}
