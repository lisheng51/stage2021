<?php

class Global_model extends CI_Model
{

    public $_formPostSession = '_formPostSession';
    public $_querySegmentPageNumber = 'page_number';

    public function savePostGet()
    {
        $sessionKey = uri_string() . $this->_formPostSession;
        $formSession = $this->input->post();
        $formSession[$this->_querySegmentPageNumber] = $this->input->get($this->_querySegmentPageNumber);
        $this->session->set_userdata($sessionKey, $formSession);
    }


    public function loadPostGet(string $key = '', $valueType = 'string')
    {
        $sessionKey = uri_string() . $this->_formPostSession;
        $formSession = $this->session->userdata($sessionKey);
        $result = null;
        if (empty($formSession) === false && empty($key) === false && isset($formSession[$key]) === true) {
            $result =  $formSession[$key];
        }

        if ($valueType === 'float') {
            return (float) $result;
        }

        if ($valueType === 'int') {
            return (int) $result;
        }

        if ($valueType === 'string') {
            return (string) $result;
        }
        if ($valueType === 'array') {
            return (array) $result;
        }
    }


    public function showPostGet()
    {
        $sessionKey = uri_string() . $this->_formPostSession;
        $formSession = $this->session->userdata($sessionKey);
        if (empty($formSession) === false) {
            return $formSession;
        }
        return null;
    }

    public function redirectWithPageNumber()
    {
        if (!$this->input->get() && !$this->input->post()) {
            $query_string_segment = $this->_querySegmentPageNumber;
            $pageNumberSession = $this->loadPostGet($query_string_segment, 'int');
            if (!isset($_GET[$query_string_segment]) && $pageNumberSession > 1) {
                $fields_string = http_build_query([
                    $query_string_segment => $pageNumberSession
                ]);
                $url = current_url() . '?' . $fields_string;
                $sessionKey = uri_string() . $this->_formPostSession;
                $formSession[$this->_querySegmentPageNumber] = 0;
                $this->session->set_userdata($sessionKey, $formSession);
                redirect($url);
            }
        }
    }

    public function edit_time_info($rsdb = null)
    {
        if (empty($rsdb) === true) {
            return "";
        }

        $date_start = $rsdb["datecreated"] ?? $rsdb["created_at"] ?? null;
        $date_end = $rsdb["datemodified"] ?? $rsdb["modified_at"] ?? null;

        $createdby = $rsdb["createdby"] ?? 0;
        $modifiedby = $rsdb["modifiedby"] ?? 0;

        $data["created_at"] = F_datetime::convert_datetime($date_start);
        $data["createdby"] = $this->user_model->fetch_name($createdby);
        $data["modified_at"] = null;
        $data["modifiedby"] = null;
        if (empty($date_end) === false) {
            $data["modified_at"] = F_datetime::convert_datetime($date_end);
            $data["modifiedby"] = $this->user_model->fetch_name($modifiedby);
        }

        return $this->load->view('_share/global/edit_time_info', $data, true);
    }

    public function event_result_box(string $body = "", string $title = "", string $extrabody = "")
    {
        if (empty($body) === true) {
            $class = $this->router->class;
            $method = $this->router->method;
            $body = $this->lang->line('event_result_box_' . strtolower($class . '_' . $method) . '_body', false);
        }

        if (empty($title) === true) {
            $title = "Toelichting";
        }

        $data["title"] = $title;
        $data["body"] = $body . $extrabody;
        return $this->load->view("_share/global/event_result_box", $data, true);
    }

    public function show_page(int $total = 0, int $per_page = 0, string $url = "")
    {
        $this->load->library('pagination');
        $query_string_segment = $this->_querySegmentPageNumber;
        $config = [];
        $page_limit = $this->input->post("page_limit");
        $config["per_page"] = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;
        if ($per_page > 0) {
            $config["per_page"] = $per_page;
        }

        $config["base_url"] = current_url();
        if (empty($url) === false) {
            $config["base_url"] = site_url($url);
        }
        $config["total_rows"] = $total;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = $query_string_segment;
        $config["use_page_numbers"] = true;
        $query_string = $_GET;
        if (isset($query_string[$query_string_segment])) {
            unset($query_string[$query_string_segment]);
        }

        if (count($query_string) > 0) {
            $config['suffix'] = '&' . http_build_query($query_string, '', "&");
            $config['first_url'] = $config['base_url'] . '?' . http_build_query($query_string, '', "&");
        }
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = lang('show_page_first_link_text');
        $config['last_link'] = lang('show_page_last_link_text');
        $config['num_links'] = 5;
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tag_close'] = '</span></li>';
        $config['prev_link'] = lang('show_page_prev_link_text');
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tag_close'] = '</span></li>';
        $config['next_link'] = lang('show_page_next_link_text');
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tag_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    public function check_password_date(string $password_date = "")
    {
        $data["webapp_ck_pass_notify_day"] = c_key("webapp_ck_pass_notify_day");
        if ($this->login_model->check_password_date($password_date) === false) {
            return $this->load->view('_share/global/check_password_date', $data, true);
        }
    }

    public function alert(string $moduleName = ""): string
    {
        $data = $this->ajaxck_model->get_session("", true);
        if ($data === null) {
            return "";
        }
        switch ($data["status"]) {
            case "error":
                $data["className"] = "danger";
                break;
            case "good":
                $data["className"] = "success";
                break;
            case "warning":
                $data["className"] = "warning";
                break;
            default:
                $data["className"] = "info";
                break;
        }

        if (empty($moduleName) === false) {
            $ck_file_exist_path = APPPATH . "modules/$moduleName/" . "views/_share/global/alert.php";
            if (file_exists($ck_file_exist_path) === true) {
                return $this->load->view($moduleName . '/_share/global/alert', $data, true);
            }
        }
        return $this->load->view('_share/global/alert', $data, true);
    }

    public function get_info(string $path = "")
    {
        $key = 'changelog';
        $data_file = APPPATH . 'info.php';
        require($data_file);

        if (empty($path) === false) {
            $data = $this->module_model->getInfo($path);
        }

        if (isset($data[$key]) === false) {
            return [
                [
                    "name" => "Informatie is niet gevonden",
                    "created_at" => date("Y-m-d H:i:s"),
                    "info" => [
                        "Contact met opnemen met 0614304050"
                    ]
                ]
            ];
        }
        return $data[$key];
    }

    public function last_version(string $path = "")
    {
        $listdb = $this->get_info($path);
        $lastArr = end($listdb);
        return $lastArr["name"];
    }

    public function makeSql(string $module = "", array $add_insert_list = [], string $setDatabaseName = "")
    {
        $tables = $this->db->list_tables();
        $linesFilePath = APPPATH . 'modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . 'sql' . DIRECTORY_SEPARATOR;

        $databaseName = "";
        if (empty($setDatabaseName) === false) {
            $databaseName = $setDatabaseName;
        }

        if (empty($databaseName) === false) {
            $dsn = $this->db->dbdriver . '://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database . "_" . $databaseName;
            $dbcollat = $this->db->dbcollat;
            $char_set = $this->db->char_set;
            $this->db = $this->load->database($dsn, true);
            $this->db->dbcollat = $dbcollat;
            $this->db->char_set = $char_set;
            $tables = $this->db->list_tables();
        }

        if (empty($module) === true) {
            $linesFilePath = APPPATH . 'sql' . DIRECTORY_SEPARATOR;
        }

        $fileList = [];
        $arrSqlFile = directory_map($linesFilePath);
        if (empty($arrSqlFile) === false && empty($tables) === false) {
            foreach ($arrSqlFile as $name) {
                $fileList[] = basename($name, ".sql");
            }

            $this->load->dbutil($this->db);
            foreach ($tables as $table) {
                if (in_array($table, $fileList) === false) {
                    continue;
                }
                $add_insert = false;
                $extraString = 'ALTER TABLE ' . $table . ' AUTO_INCREMENT = 1;';
                if (in_array($table, $add_insert_list) === true) {
                    $add_insert = true;
                    $extraString = '';
                }

                $prefs = array(
                    'tables' => [$table],
                    'format' => 'txt',
                    'add_drop' => false,
                    'add_insert' => $add_insert,
                    'newline' => "\n"
                );

                $backup = $this->dbutil->backup($prefs);
                write_file($linesFilePath . $table . '.sql', $backup . $extraString);
            }
        }
        return $linesFilePath;
    }
}
