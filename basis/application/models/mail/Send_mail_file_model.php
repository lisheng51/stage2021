<?php

class Send_mail_file_model extends MY_Model
{

    public $table = "send_mail_file";
    public $primary_key = "mail_file_id";
    public $select_order_by = [];

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'mail_file_id#desc' => 'ID (aflopend)',
            'file_name#desc' => 'Naam (aflopend)',
            'file_name#asc' => 'Naam (oplopend)',
            'created_at#desc' => 'Tijd (aflopend)',
            'created_at#asc' => 'Tijd (oplopend)',
        ];
    }

    public function getExist(array $input = []): array
    {
        $data['file_name'] = $input['file_name'];
        $data[$this->sendmail_model->primary_key] = $input[$this->sendmail_model->primary_key];
        $this->sql_where = $data;
        return $this->get_one();
    }

    public function upload(int $mail_id = 0, array $files = [])
    {
        if ($mail_id > 0 && empty($files) === false) {
            foreach ($files as $rsdb) {
                $filename = $rsdb['file_name'];
                $data['file_name'] = $filename;
                $data[$this->sendmail_model->primary_key] = $mail_id;
                $rsdbFile = $this->getExist($data);
                if (empty($rsdbFile) === true) {
                    $base64String = $rsdb['base64'];
                    $data['base64'] = $base64String;
                    $this->add($data);
                }
            }
        }
    }
}
