<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Mail_model extends CI_Model
{

    public $template = "_share/sendmail/default/template";
    public $button_template = "_share/sendmail/default/template_button";
    public $alwaysSend = false;

    public function set_template(string $filename = 'template')
    {
        if (empty($filename) === FALSE && file_exists(APPPATH . "views/_share/sendmail/default/$filename.php") === TRUE) {
            $this->template = "_share/sendmail/default/$filename";
        }
    }

    public function previewTemplate(array $data = []): string
    {
        $message["check_open_image"] = "";
        $message["content"] = $data["content"];
        $message["title"] = $data["title"];
        $message["webversion_button"] = "";
        $html = stripslashes($this->load->view($this->template, $message, true));
        return $html;
    }

    public function create_event(array $insert = [], string $path = "sys_open")
    {
        if (empty($insert) === true) {
            return;
        }
        $encrypted_string = $this->encryption->encrypt($insert["event_value"] . "#_#" . $insert["to_email"]);
        $hashkey = rawurlencode($encrypted_string);
        return site_url("site/Mail/" . $path . "?hashkey=$hashkey");
    }

    public function send_action(array $data = []): bool
    {
        $from_email = "";
        if (isset($data['from_email']) === true && empty($data['from_email']) === false) {
            $from_email = $data["from_email"];
        }

        if (empty($from_email) === true) {
            return false;
        }

        $to_email = null;
        if (isset($data['to_email']) === true && empty($data['to_email']) === false) {
            $to_email = $data["to_email"];
        }
        if (empty($to_email) === true) {
            return false;
        }
        $subject = $data["subject"];
        $message = $data["message"];

        $mail = new PHPMailer(true);
        try {
            //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                   
            $mail->isSMTP();
            $mail->SMTPAuth = true;
            $mail->CharSet = PHPMailer::CHARSET_UTF8;
            $mailConfig = $this->mail_config_model->sendMailData($from_email);
            $mail->Host = $mailConfig['Host'];                    // Set the SMTP server to send through
            $mail->Username = $mailConfig['Username'];                     // SMTP username
            $mail->Password = $mailConfig['Password'];
            $mail->SMTPSecure = $mailConfig['SMTPSecure'];
            $mail->Port = $mailConfig['Port'];                                           // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
            $mail->setFrom($mailConfig['Username'], $mailConfig['Name']);
            $mail->addAddress($to_email);

            if (isset($data['reply_to_json']) === true && empty($data['reply_to_json']) === false) {
                $listReplyTo = json_decode($data["reply_to_json"], true);
                if (empty($listReplyTo) === false) {
                    foreach ($listReplyTo as $email => $name) {
                        $mail->addReplyTo($email, $name);
                    }
                }
            }

            if (isset($data['cc_json']) === true && empty($data['cc_json']) === false) {
                $listCc = json_decode($data["cc_json"], true);
                if (empty($listCc) === false) {
                    foreach ($listCc as $email => $name) {
                        $mail->addCC($email, $name);
                    }
                }
            }

            if (isset($data['bcc_json']) === true && empty($data['bcc_json']) === false) {
                $listBcc = json_decode($data["bcc_json"], true);
                if (empty($listBcc) === false) {
                    foreach ($listBcc as $email => $name) {
                        $mail->addBCC($email, $name);
                    }
                }
            }

            $files = $this->send_mail_file_model->get_all_by_field($this->sendmail_model->primary_key, $data[$this->sendmail_model->primary_key]);
            if (empty($files) === false) {
                foreach ($files as $rsdb) {
                    $file_content = base64_decode($rsdb["base64"]);
                    $filename = $rsdb["file_name"];
                    $mail->addStringAttachment($file_content, $filename);
                }
            }

            if (isset($data['attach']) === true && empty($data['attach']) === false) {
                $multiAttach = explode(",", $data['attach']);
                if (empty($multiAttach) === true) {
                    $mail->addAttachment($data["attach"]);
                } else {
                    foreach ($multiAttach as $value) {
                        $mail->addAttachment($value);
                    }
                }
            }
            $mail->isHTML();
            $mail->Subject = $subject;
            $mail->Body = $message;
            if (ENVIRONMENT !== 'development' || $this->alwaysSend === true) {
                return $mail->send();
            }
            return true;
        } catch (Exception $e) {
            $debug_result = $from_email . ': ' . $mail->ErrorInfo;
            $this->telegram_model->log($debug_result);
            log_message('error', $debug_result);
            return false;
        }
    }
}
