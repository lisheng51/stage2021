<?php

class Api_model extends MY_Model
{

    public $table = "api";
    public $primary_key = "api_id";
    protected $using_softDel = false;

    public $select_order_by = [];
    public $usingBodyOutSecurity = false;

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'api_id#desc' => 'ID (aflopend)',
            'name#desc' => 'Naam (aflopend)',
            'name#asc' => 'Naam (oplopend)',
        ];
    }

    public function select(int $id = 0, bool $with_empty = false, string $name = '', array $dataWhere = []): string
    {
        $select_name = empty($name) === false ? $name : $this->primary_key;
        if ($id === 0) {
            $id = $this->input->post_get($select_name) ?? 0;
        }
        $this->sqlReset();
        $data[$this->_field_is_deleted] = 0;
        $this->sql_where = array_merge($data, $dataWhere);
        $this->sql_order_by = ['name' => 'asc'];
        $select = '<select name=' . $select_name . ' class="form-control selectpicker" data-live-search="true">';
        $select .= $with_empty === true ? "<option value='' >------</option>" : "";
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = $id == $rs[$this->primary_key] ? "selected" : '';
            $select .= '<option value=' . $rs[$this->primary_key] . ' ' . $ckk . '>' . $rs["name"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function selectMultiple(array $haystack = [], string $name = '', array $dataWhere = []): string
    {
        $select_name = empty($name) === false ? $name : $this->primary_key;
        if (empty($haystack) === true) {
            $haystack = $this->input->post_get($select_name) ?? [];
            if (is_array($haystack) === false) {
                $haystack = [$haystack];
            }
        }
        $this->sqlReset();
        $data[$this->_field_is_deleted] = 0;
        $this->sql_where = array_merge($data, $dataWhere);
        $this->sql_order_by = ['name' => 'asc'];
        $select = '<select name="' . $select_name . '[]" class="form-control selectpicker" data-selected-text-format="count > 3" data-live-search="true" multiple title="------">';
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = (empty($haystack) === false && in_array($rs[$this->primary_key], $haystack) === true) ? 'selected' : '';
            $select .= '<option value=' . $rs[$this->primary_key] . ' ' . $ckk . '>' . $rs["name"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function get_one_by_request(): array
    {
        $headers = apache_request_headers();
        $apiId = $headers['Api-Id'] ?? $headers['api-id'] ?? $headers['Api-id'] ?? $headers['apiId'] ?? $headers['Apiid'] ?? $headers['apiid'] ?? 0;
        $apiSecret = $headers['Api-Key'] ?? $headers['api-key'] ?? $headers['Api-key'] ?? $headers['apiKey'] ?? $headers['Apikey'] ?? $headers['apikey'] ?? null;
        if (empty($apiId) === true || empty($apiSecret) === true) {
            return [];
        }

        $data[$this->primary_key] = $apiId;
        $data['secret'] = $apiSecret;
        $data['is_del'] = 0;
        $this->sql_where = $data;
        $arrApi = $this->get_one();
        return $arrApi;
    }

    public function getVersion(string $v = "1.0"): string
    {
        $headers = apache_request_headers();
        return $headers['version'] ?? $v;
    }

    public function get_one_by_name(string $name = ''): array
    {
        $data['name'] = $name;
        $this->sql_where = $data;
        return $this->get_one();
    }

    public function fetchTokenMin(int $id = 0)
    {
        $much_strtotime = '+15 minutes';
        $rsdb = $this->get_one_by_id($id);
        if (empty($rsdb) === false) {
            $much_strtotime = '+' . $rsdb['token_min'] . ' minutes';
        }
        return date('Y-m-d H:i:s', strtotime($much_strtotime));
    }

    public function getPostdata(): array
    {
        $data["name"] = $this->input->post("name");
        $data["max"] = $this->input->post("max") ?? 0;
        $data["secret"] =  $this->input->post("secret");
        $data["token_min"] = $this->input->post("token_min") ?? 15;
        $permission_group_ids = $this->input->post("permission_group_id");
        if (empty($permission_group_ids) === true) {
            $json["msg"] = 'Toestemming groep is leeg!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $data["permission_group_ids"] = implode(',', $permission_group_ids);
        $this->ajaxck_model->ck_value('name', $data["name"]);
        return $data;
    }


    public function out($message = null, int $statusCode = 0, int $apiLogId = 0)
    {
        $output["message"] = $message;
        $output["statusCode"] = $statusCode;
        $this->api_log_model->updateOut($apiLogId, $output);
        $headers = apache_request_headers();
        $type = $headers['type'] ?? $headers['Type'] ?? 'json';

        if ($this->usingBodyOutSecurity === true) {
            $encrypted_string = $this->aes_model->encrypt(json_encode($output));
            $body = rawurlencode($encrypted_string);
            $output = [];
            $output["body"] = $body;
        }

        if ($type === 'json') {
            header('Content-Type: application/json');
            exit(json_encode($output));
        }

        $xml_data = new SimpleXMLElement('<?xml version="1.0"?><xmldata></xmldata>');
        $this->array_to_xml($output, $xml_data);
        header('Content-Type: application/xml; charset=utf-8');
        exit($xml_data->asXML());
    }

    public function outOK($message = 'good', int $apiLogId = 0)
    {
        $this->out($message, 100, $apiLogId);
    }

    public function outNOK(int $statusCode = 99, $message = 'error', int $apiLogId = 0)
    {
        $this->out($message, $statusCode, $apiLogId);
    }

    private function array_to_xml($data, &$xml_data)
    {
        foreach ($data as $key => $value) {
            if (is_numeric($key)) {
                $key = 'item';
                //$key = 'item' . $key; //dealing with <0/>..<n/> issues
            }
            if (is_array($value)) {
                $subnode = $xml_data->addChild($key);
                $this->array_to_xml($value, $subnode);
            } else {
                $xml_data->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }

    public function checkCodeAccessDatetime(array $rsdb = [])
    {
        $headers = apache_request_headers();
        $code = $headers['code'] ?? $headers['Code'] ?? "";
        $apiId = $rsdb[$this->primary_key];
        if (empty($code) === true) {
            $this->api_model->outNOK(99, 'Code header is niet gevonden');
        }
        $key = rawurldecode($code);
        $arrTokenValue = json_decode($this->encryption->decrypt($key), true);
        if ($arrTokenValue[$this->api_model->primary_key] != $apiId) {
            $this->api_model->outNOK(97, "Code kan niet verifiëren!");
        }

        $token_expires = $arrTokenValue['datetime'] ?? null;
        if (date("Y-m-d H:i:s") > $token_expires) {
            $this->api_model->outNOK(95, "Code is verlopen!");
        }
    }

    public function checkPermission(string $ids = "", int $statusCode = 92)
    {
        $listdbGroup = $this->permission_group_model->get_all_group($ids);
        $stringCompares = array_column($listdbGroup, 'permission_ids');

        $permisson = $permissionArray = $all_permissions = $permissions = [];
        foreach ($stringCompares as $stringCompare) {
            $permissionArray[] = explode(',', $stringCompare);
        }
        $all_permissions = array_reduce($permissionArray, 'array_merge', []);
        $all_permissions = array_unique($all_permissions);

        if (empty($all_permissions) === false) {
            $this->permission_model->sql_where = [$this->module_model->table . ".is_active" => 1];
            $this->permission_model->sql_where_in = [$this->permission_model->table . "." . $this->permission_model->primary_key => $all_permissions];
            $listdb = $this->permission_model->get_all();
            foreach ($listdb as $rsdb) {
                $path = $rsdb['use_path'] > 0 ? $rsdb['path'] : null;
                $permissions[] = $path . '.' . $rsdb['object'] . '.' . $rsdb['method'];
            }
        }

        $permisson = array_unique($permissions);
        $path = null;
        if (empty($this->router->module) === false && $this->router->module !== 'api') {
            $path = $this->router->module;
        }
        $class = ucfirst($this->router->class);
        $method = $this->router->method;
        $ckPath = $path . '.' . $class . '.' . $method;
        //$this->api_model->out($ckPath, 90);
        $ckPermission = in_array($ckPath, $permisson);
        if ($ckPermission === false) {
            $this->api_model->out('Niet gemachtigd', $statusCode);
        }
    }

    public function checkFetchByJWT(bool $CheckExpires = true)
    {
        $headers = apache_request_headers();
        $apiId = $headers['Api-Id'] ?? $headers['api-id'] ?? $headers['Api-id'] ?? $headers['apiId'] ?? $headers['Apiid'] ?? $headers['apiid'] ?? 0;
        $token = $headers['token'] ?? $headers['Token'] ?? null;
        if (empty($token) === true) {
            $this->api_model->outNOK(99, 'Token is niet gevonden');
        }
        $arrTokenValue = json_decode($this->encryption->decrypt(rawurldecode($token)), true);

        if ($arrTokenValue === null || empty($arrTokenValue) === true) {
            $this->api_model->outNOK(97, "Token is niet juist");
        }

        $token_expires = $arrTokenValue['expires'] ?? null;
        if ($CheckExpires === true && date("Y-m-d H:i:s") > $token_expires) {
            $this->api_model->outNOK(95, "Token is verlopen (tijd)");
        }

        $userId = $arrTokenValue[$this->user_model->primary_key] ?? 0;
        $dataUser = $this->login_model->getByField($this->user_model->primary_key, $userId);
        if (empty($dataUser) === true) {
            $this->api_model->outNOK(97, "Geen gebruiker gevonden");
        }

        $password_date = $arrTokenValue['password_date'] ?? null;
        if ($CheckExpires === true && $dataUser["password_date"] != $password_date) {
            $this->api_model->outNOK(95, "Token is verlopen (wachtwoord)");
        }

        if ($arrTokenValue[$this->api_model->primary_key] != $apiId) {
            $this->api_model->outNOK(96, "Token misbruik(api)");
        }

        $ip_address = $arrTokenValue['ip_address'] ?? null;
        if ($ip_address !== null && $dataUser["ip_address"] != $ip_address) {
            $this->api_model->outNOK(96, "Token misbruik(ip)");
        }

        //        $browser = $arrTokenValue['browser'] ?? null;
        //        if ($browser !== null && $dataUser["browser"] != $browser) {
        //            $this->api_model->outNOK(96, "Token misbruik(browser)!");
        //        }
        //
        //        $platform = $arrTokenValue['platform'] ?? null;
        //        if ($platform !== null && $dataUser["platform"] != $platform) {
        //            $this->api_model->outNOK(96, "Token misbruik(platform)!");
        //        }

        return $dataUser;
    }
}
