<?php

class Api_log_model extends CI_Model
{

    public $table = "api_log";
    public $primary_key = "log_id";
    public $sql_where = [];
    public $sql_where_in = [];
    public $sql_like = [];
    public $sql_or_like = [];
    public $sql_order_by = [];
    public $select_order_by = [];

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'log_id#desc' => 'ID (aflopend)',
            'datetime#desc' => 'Datum (aflopend)',
            'datetime#asc' => 'Datum (oplopend)',
        ];
    }

    public function setSqlWhere(array $whereSet = [])
    {
        $newArray = [];
        foreach ($whereSet as $array) {
            if (is_array($array)) {
                foreach ($array as $k => $v) {
                    if (!is_null($v) && $v !== '') {
                        $newArray[$k] = $v === 'value_is_null' ? null : $v;
                    }
                }
            }
        }
        $this->sql_where = $newArray;
    }

    public function add(string $msg = "", int $api_id = 0): int
    {
        if ($api_id > 0) {
            $data['api_id'] = $api_id;
            $data['msg'] = $msg;
            $data["ip_address"] = $this->input->ip_address();
            $data["browser"] = $this->agent->browser();
            $data["platform"] = $this->agent->platform();
            $data["path"] = uri_string();
            $data['post_value'] = json_encode($this->input->post());
            $data['get_value'] = json_encode($this->input->get());
            $data['header_value'] = json_encode(apache_request_headers());
            $this->db->insert($this->table, $data);
            //$this->telegram_model->log($api_id . ' => ' . $msg);
            return $this->db->insert_id();
        }

        return 0;
    }

    public function updateOut(int $id = 0, $out = null)
    {
        if ($id > 0) {
            if (is_array($out) === true) {
                $out = json_encode($out);
            }
            $data['out_value'] = $out;
            $this->db->where($this->primary_key, $id)->update($this->table, $data);
        }
    }

    public function edit(int $id = 0, array $data = [])
    {
        $this->db->where($this->primary_key, $id)->update($this->table, $data);
    }

    public function del(int $id = 0)
    {
        $this->db->from($this->table)->where($this->primary_key, $id)->limit(1)->delete();
    }

    public function get_all(): array
    {
        $limit = $this->get_total();
        return $this->get_list($limit);
    }

    public function get_total(): int
    {
        $this->db->select($this->primary_key);
        $this->db->from($this->table);
        $this->db->join($this->api_model->table, $this->api_model->primary_key);
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_or_like) === false) {
            foreach ($this->sql_or_like as $field => $value) {
                $this->db->or_like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        return $this->db->count_all_results();
    }

    public function get_list(int $limit = 1, int $page = 0): array
    {
        $this->db->from($this->table);
        $this->db->join($this->api_model->table, $this->api_model->primary_key);
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_or_like) === false) {
            foreach ($this->sql_or_like as $field => $value) {
                $this->db->or_like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        if (empty($this->sql_order_by) === false) {
            foreach ($this->sql_order_by as $field => $value) {
                $this->db->order_by($field, $value);
            }
        }

        $this->db->order_by($this->primary_key, "desc");
        if ($page <= 0) {
            $page = 0;
        }
        $this->db->limit($limit, $page);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_one(): array
    {
        $arr_result = $this->get_list();
        if (empty($arr_result) === false) {
            return current($arr_result);
        }
        return [];
    }

    public function get_one_by_id(int $id = 0): array
    {
        $data[$this->primary_key] = $id;
        $this->sql_where = $data;
        return $this->get_one();
    }
}
