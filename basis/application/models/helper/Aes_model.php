<?php

class Aes_model extends CI_Model
{

    public $password = 'D1hwvBC';
    public $key = 'dcefec265e57f64fb6a9362ee831e48e';

    private function setOject(): AesEncryption
    {
        require_once APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'AES-Encryption' . DIRECTORY_SEPARATOR . 'AesEncryption.php';
        $aes = new AesEncryption();
        $aes->setMasterKey($this->key);
        return $aes;
    }

    public function encrypt(string $text = "")
    {
        $aes = $this->setOject();
        if (empty($text) === true) {
            return "";
        }
        return $aes->encrypt($text, $this->password);
    }

    public function decrypt(string $hash = "")
    {
        $aes = $this->setOject();
        if (empty($hash) === true) {
            return "";
        }
        return $aes->decrypt($hash, $this->password);
    }
}
