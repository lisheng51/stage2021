<?php

class Vat_model extends CI_Model
{

    public $primary_key = "vat_id";
    public $id_1 = 1;
    public $id_2 = 2;
    public $id_3 = 3;

    public function select(int $id = 0, bool $with_empty = false, string $name = ''): string
    {
        $select_name = empty($name) === false ? $name : $this->primary_key;
        if ($id === 0) {
            $id = $this->input->post_get($select_name) ?? 0;
        }
        $listdb = $this->getAll();
        $select = '<select name=' . $select_name . ' class="form-control">';
        $select .= $with_empty === true ? "<option value='' >------</option>" : "";

        foreach ($listdb as $rs) {
            $ckk = $id == $rs[$this->primary_key] ? "selected" : '';
            $select .= "<option value={$rs[$this->primary_key]} $ckk >{$rs["name"]}</option>";
        }
        $select .= '</select>';
        return $select;
    }

    public function getAll(): array
    {
        $data[] = $this->id_1();
        $data[] = $this->id_2();
        $data[] = $this->id_3();
        return $data;
    }

    private function id_1(string $key = '')
    {
        $data[$this->primary_key] = $this->id_1;
        $data['name'] = '0%';
        $data['description'] = null;
        $data['number'] = 0.00;
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    private function id_2(string $key = '')
    {
        $data[$this->primary_key] = $this->id_2;
        $data['name'] = '21%';
        $data['description'] = null;
        $data['number'] = 1.21;
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    private function id_3(string $key = '')
    {
        $data[$this->primary_key] = $this->id_3;
        $data['name'] = '9%';
        $data['description'] = null;
        $data['number'] = 1.09;
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    public function fetchData(int $id = 0, string $field = '')
    {
        switch ($id) {
            case $this->id_1:
                $value = $this->id_1($field);
                break;
            case $this->id_2:
                $value = $this->id_2($field);
                break;
            case $this->id_3:
                $value = $this->id_3($field);
                break;
            default:
                $value = null;
                break;
        }
        return $value;
    }

    public function fetchName(int $id = 0)
    {
        return $this->fetchData($id, 'name');
    }

    public function fetchNumber(int $id = 0)
    {
        return $this->fetchData($id, 'number');
    }

    public function calAmount(int $amount = 0, int $id = 0, bool $with_vat = true)
    {
        if ($amount < 0.01) {
            return 0;
        }

        $cal_number = $this->fetchNumber($id);
        if ($cal_number < 0.01) {
            return 0;
        }
        if ($with_vat === false) {
            $cal_number = $cal_number - 1;
            return round($amount - $amount * $cal_number, 2);
        }
        return round($amount - $amount / $cal_number, 2);
    }
}
