<?php

class Bookmark_model extends MY_Model
{

    public $table = "bookmark";
    public $primary_key = "bookmark_id";
    public $select_order_by = [];
    public $sql_order_by = ["order_list" => 'asc'];
    protected $using_softDel = true;

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'bookmark_id#desc' => 'ID (aflopend)',
            'path#desc' => 'Path (aflopend)',
            'path#asc' => 'Path (oplopend)',
            'order_list#desc' => 'Volgorde (aflopend)',
            'order_list#asc' => 'Volgorde (oplopend)',
        ];
    }

    public function selectParent(int $id = 0, int $uid = 0, bool $with_empty = false, string $name = 'parent_bookmark_id'): string
    {
        $userId = $uid > 0 ? $uid : $this->login_model->user_id();
        $select_name = empty($name) === false ? $name : $this->primary_key;
        if ($id === 0) {
            $id = $this->input->post_get($select_name) ?? 0;
        }
        $this->sqlReset();
        $data[$this->_field_is_deleted] = 0;
        $data['is_sort'] = 1;
        $data[$this->user_model->primary_key] = $userId;
        $this->sql_where = $data;
        $this->sql_order_by = ['order_list' => 'asc'];
        $select = '<select name=' . $select_name . ' class="form-control selectpicker" data-live-search="true">';
        $select .= $with_empty === true ? "<option value='' >------</option>" : "";
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = $id == $rs[$this->primary_key] ? "selected" : '';
            $select .= '<option value=' . $rs[$this->primary_key] . ' ' . $ckk . '>' . $rs["name"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }


    public function getPostdata(): array
    {
        $data["path"] = $this->input->post("path") ?? "";
        $data["name"] = $this->input->post("name");
        $data["description"] = $this->input->post("description");
        $data["icon"] = $this->input->post("icon");
        $data["open_new"] = $this->input->post("open_new");
        $data["is_extern"] = $this->input->post("is_extern");
        $data[$this->user_model->primary_key] = $this->input->post($this->user_model->primary_key);
        $data["parent_bookmark_id"] = $this->input->post("parent_bookmark_id") ?? 0;
        $data["is_sort"]  = $this->input->post("is_sort");
        $this->ajaxck_model->ck_value('name', $data["name"]);
        return $data;
    }

    private function autoChildId(string $name = "", string $path = "", int $parentId = 0, int $uid = 0)
    {
        if (empty($name) === true) {
            return 0;
        }

        $data["is_sort"] = 0;
        $userId = $uid > 0 ? $uid : $this->login_model->user_id();
        $data[$this->user_model->primary_key] = $userId;
        $data["path"] = $path;
        $this->sql_where = $data;
        $exist = $this->get_one();
        if (empty($exist) === true) {
            $data["open_new"] = 0;
            $data["parent_bookmark_id"] = $parentId;
            $data["description"] = "auto update";
            $data["is_extern"] = 0;
            $data["name"] = $name;
            return $this->add($data);
        }
        $this->edit($exist[$this->primary_key], $data);
        return $exist[$this->primary_key];
    }

    private function autoParentId(string $name = "", int $uid = 0)
    {
        if (empty($name) === true) {
            return 0;
        }
        $data["name"] = $name;
        $data["is_sort"] = 1;
        $userId = $uid > 0 ? $uid : $this->login_model->user_id();
        $data[$this->user_model->primary_key] = $userId;
        $this->sql_where = $data;
        $exist = $this->get_one();
        if (empty($exist) === true) {
            $data["description"] = "auto update";
            $data["parent_bookmark_id"] = 0;
            $data["is_extern"] = 0;
            $data["open_new"] = 0;
            return $this->add($data);
        }
        $this->edit($exist[$this->primary_key], $data);
        return $exist[$this->primary_key];
    }

    public function resetAllByPermission(int $id = 0): bool
    {
        $userDB = $this->user_model->get_one_by_id(intval($id));
        $ids = $userDB["permission_group_ids"];
        $status = false;
        if (empty($ids) === false) {
            $listdbGroup = $this->permission_group_model->get_all_group($ids);
            $stringCompares = array_column($listdbGroup, 'permission_ids');

            $permissionsNav = $permissionArray = $all_permissions = [];
            foreach ($stringCompares as $stringCompare) {
                $permissionArray[] = explode(',', $stringCompare);
            }
            $all_permissions = array_reduce($permissionArray, 'array_merge', []);
            $all_permissions = array_unique($all_permissions);

            if (empty($all_permissions) === false) {
                $this->permission_model->sql_where = [$this->module_model->table . ".is_active" => 1];
                $this->permission_model->sql_where_in = [$this->permission_model->table . "." . $this->permission_model->primary_key => $all_permissions];
                $listdb = $this->permission_model->get_all();
                foreach ($listdb as $rsdb) {
                    if ($rsdb["has_link"] > 0) {
                        $startPath = $rsdb["use_path"] > 0 ? $rsdb["path"] . '/' : "";
                        $data["url"] = $startPath . $rsdb["link_dir"] . '/' . $rsdb['object'] . '/' . $rsdb['method'];
                        $data["name"] = $rsdb["link_title"];
                        $keyString = $rsdb["path_description"];
                        $permissionsNav[$keyString][] = $data;
                    }
                }
            }

            if (empty($permissionsNav) === false) {
                foreach ($permissionsNav as $name => $arr_menu) :
                    $arr_menu_unique = F_array::unique_multidim($arr_menu, ["name", "url"]);
                    $sortId = $this->autoParentId($name, $id);
                    foreach ($arr_menu_unique as $value) :
                        $this->autoChildId($value["name"], $value["url"], $sortId, $id);
                    endforeach;

                endforeach;
                $status = true;
            }
        }

        return $status;
    }

    public function navBarData(int $uid = 0)
    {
        $userId = $uid > 0 ? $uid : $this->login_model->user_id();
        $this->sqlReset();
        $data_where[] = [$this->table . '.' . $this->_field_is_deleted => 0];
        $data_where[] = [$this->table . '.' . $this->user_model->primary_key => $userId];
        $this->setSqlWhere($data_where);
        $this->sql_order_by = ['order_list' => 'asc'];
        $arrResult = [];
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            if ($rs['parent_bookmark_id'] == 0) {
                $rs["navBarDataChild"] = null;
                if ($rs["is_sort"] > 0) {
                    $rs["navBarDataChild"] = $this->navBarDataChild($rs[$this->primary_key], $listdb);
                }
                $arrResult[] = $rs;
            }
        }
        return $arrResult;
    }


    private function navBarDataChild($parent_id = 0, array $listdb = [])
    {
        $arrResult = [];
        foreach ($listdb as $rs) {
            if ($rs['parent_bookmark_id'] == $parent_id) {
                $rs["target"] = $rs["open_new"] > 0 ? "_blank" : "_self";
                $rs["url"] = $rs["is_extern"] > 0 ? $rs["path"] : site_url($rs["path"]);
                $arrResult[] = $rs;
            }
        }
        return $arrResult;
    }
}
