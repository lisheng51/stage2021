<?php

class Login_history_model extends MY_Model
{

    public $table = "login_history";
    public $primary_key = "login_history_id";
    public $select_order_by = [];

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'login_history_id#desc' => 'ID (aflopend)',
            'created_at#desc' => 'Datum (aflopend)',
            'created_at#asc' => 'Datum (oplopend)',
        ];
    }

    public function insert(int $userId = 0)
    {
        $data["ip_address"] = $this->input->ip_address();
        $data["browser"] = $this->agent->browser() . ' ' . $this->agent->version();
        $data["platform"] = $this->agent->platform();
        $data[$this->user_model->primary_key] = $userId;
        $insertId = $this->db->insert($this->table, $data);
        return $insertId;
    }

    public function last(int $setUserId = 0)
    {
        $userId = $this->login_model->user_id();
        if ($setUserId > 0) {
            $userId = $setUserId;
        }
        $data[$this->user_model->primary_key] = $userId;

        $this->sql_where = $data;
        $listdb = $this->get_list(2);
        $last = end($listdb);
        return $last;
    }

    public function get_total(): int
    {
        $this->db->select($this->primary_key);
        $this->db->from($this->table);
        $this->db->join($this->user_model->table, $this->user_model->primary_key);
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        return $this->db->count_all_results();
    }

    public function get_list(int $limit = 1, int $page = 0): array
    {
        $this->db->select($this->table . '.*');
        $this->db->select($this->user_model->table . '.display_info');
        $this->db->select($this->user_model->table . '.emailaddress');
        $this->db->from($this->table);
        $this->db->join($this->user_model->table, $this->user_model->primary_key);
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        if (empty($this->sql_order_by) === false) {
            foreach ($this->sql_order_by as $field => $value) {
                $this->db->order_by($field, $value);
            }
        }

        $this->db->order_by($this->primary_key, "desc");
        if ($page <= 0) {
            $page = 0;
        }
        $this->db->limit($limit, $page);
        $query = $this->db->get();
        return $query->result_array();
    }
}
