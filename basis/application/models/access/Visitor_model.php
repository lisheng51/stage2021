<?php

class Visitor_model extends MY_Model
{

    public $table = "visitor";
    public $primary_key = "visitor_id";
    public $select_order_by = [];

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'visitor_id#desc' => 'ID (aflopend)',
            'datetime#desc' => 'Datum (aflopend)',
            'datetime#asc' => 'Datum (oplopend)',
            'path#desc' => 'Path (aflopend)',
            'path#asc' => 'Path (oplopend)',
        ];
    }

    public function insert()
    {
        if (ENVIRONMENT_VISITOR_LOG === true) {
            $data["ip_address"] = $this->input->ip_address();
            $data["browser"] = $this->agent->browser() . ' ' . $this->agent->version();
            $data["platform"] = $this->agent->platform();
            $data["path"] = uri_string();
            $data["user_id"] = $this->login_model->user_id();
            $insertId = $this->db->insert($this->table, $data);
            return $insertId;
        }
    }

    public function get_total(): int
    {
        $this->db->select($this->primary_key);
        $this->db->from($this->table);
        $this->db->join($this->user_model->table, $this->user_model->primary_key, 'LEFT');
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        return $this->db->count_all_results();
    }

    public function get_list(int $limit = 1, int $page = 0): array
    {
        $this->db->select($this->table . '.*');
        $this->db->select($this->user_model->table . '.display_info');
        $this->db->from($this->table);
        $this->db->join($this->user_model->table, $this->user_model->primary_key, 'LEFT');
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        if (empty($this->sql_order_by) === false) {
            foreach ($this->sql_order_by as $field => $value) {
                $this->db->order_by($field, $value);
            }
        }

        $this->db->order_by($this->primary_key, "desc");
        if ($page <= 0) {
            $page = 0;
        }
        $this->db->limit($limit, $page);
        $query = $this->db->get();
        return $query->result_array();
    }
}
