<?php

class Failcheck_type_model extends CI_Model
{

    public $primary_key = "type_id";
    public $user_login_id = 1;
    public $password_reset_id = 2;
    public $user_register_id = 3;

    private function user_login_id(string $key = '')
    {
        $data[$this->primary_key] = $this->user_login_id;
        $data['name'] = 'user_login';
        $data['description'] = null;
        $data['key'] = 'fail_user_login';
        $data['lock_time'] = 10;
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    private function password_reset_id(string $key = '')
    {
        $data[$this->primary_key] = $this->password_reset_id;
        $data['name'] = 'password_reset';
        $data['description'] = null;
        $data['key'] = 'fail_password_reset';
        $data['lock_time'] = 300;
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    private function user_register_id(string $key = '')
    {
        $data[$this->primary_key] = $this->user_register_id;
        $data['name'] = 'user_register';
        $data['description'] = null;
        $data['key'] = 'fail_user_register';
        $data['lock_time'] = 600;
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    public function fetchField(int $id = 0, string $field = '')
    {
        switch ($id) {
            case $this->user_login_id:
                $value = $this->user_login_id($field);
                break;
            case $this->password_reset_id:
                $value = $this->password_reset_id($field);
                break;
            case $this->user_register_id:
                $value = $this->user_register_id($field);
                break;
            default:
                $value = null;
                break;
        }
        return $value;
    }
}
