<?php

class Access_check_model extends CI_Model
{

    public $backPath = "back";

    public function ipAllow()
    {
        $ip = $this->input->ip_address();
        $allowIps = ['::1', '127.0.0.1'];
        if (in_array($ip, $allowIps) === false || ENVIRONMENT !== 'development') {
            $this->session->set_flashdata('page_error_type', "no_authorized");
            redirect(error_url());
        }
    }

    public function maintenance()
    {
        $filename = 'maintenance.txt';
        $defaultpath = $this->backPath;
        $ck_file_exist_path = APPPATH . $filename;
        $out = '';
        if (empty($this->router->module) === false && $this->router->module !== $defaultpath) {
            $modulesName = $this->router->module;
            $ck_file_exist_path = APPPATH . "modules/$modulesName/" . $filename;
        }

        if (file_exists($ck_file_exist_path) === true) {
            $out = $this->load->file($ck_file_exist_path, true);
        }

        if (empty($out) === false) {
            $data["msg"] = $out;
            exit($this->load->view('_share/global/maintenance', $data, true));
        }
    }

    public function permissionGroup(array $ckPermissionGroupIds = [])
    {
        $this->authorized();
        $userdb = $this->user_model->get_one_by_id($this->login_model->user_id());
        if (empty($userdb) === true) {
            $this->session->set_flashdata('page_error_type', "no_authorized");
            redirect(error_url());
        }

        $arrayIds = explode(',', $userdb["permission_group_ids"]);
        if (empty($arrayIds) === true) {
            $this->session->set_flashdata('page_error_type', "no_authorized");
            redirect(error_url());
        }

        $result = array_diff($ckPermissionGroupIds, $arrayIds);
        if (empty($result) === false) {
            $this->session->set_flashdata('page_error_type', "no_authorized");
            redirect(error_url());
        }
    }

    public function authorized()
    {
        if ($this->input->post() && $this->login_model->user_id() <= 0) {
            $json["type_done"] = "redirect";
            $json["redirect_url"] = login_url('?redirect_url=' . current_url());
            $json["msg"] = anchor($json["redirect_url"], 'Uw sessie is verlopen!');
            $json["status"] = "error";
            exit(json_encode($json));
        }

        if ($this->login_model->user_id() <= 0) {
            redirect(login_url('?redirect_url=' . current_url()));
        }
    }

    public function view_file($controller_name = "", $view_path_name = "", $filename = "")
    {
        $defaultpath = $view_path_name; //$this->backPath;
        $controller = lcfirst($controller_name);
        $viewFile = "$controller/$filename";

        $defaultCk1 = APPPATH . "views/$defaultpath";
        if (empty($this->router->module) === false && $this->router->module !== $defaultpath) {
            $modulesName = $this->router->module;
            $defaultCk1 = APPPATH . "modules/$modulesName/views/$defaultpath";
        }

        if ($defaultpath !== $this->backPath) {
            $modulesName = $this->router->module;
            $defaultCk1 = APPPATH . "modules/$modulesName/views/back/$controller/$view_path_name/$filename.php";
            if (file_exists($defaultCk1) === true) {
                return "$controller/$view_path_name/$filename";
            }
        }

        $defaultCk2 = "/$filename.php";
        $defaultCkFolderController = "/$controller";
        $ck_file_exist_path = $defaultCk1 . $defaultCkFolderController . $defaultCk2;
        if (file_exists($ck_file_exist_path) === true) {
            $viewFile = "$controller/$filename";
        }

        return $viewFile;
    }

    public function double()
    {
        if ($this->login_model->is_double_login() === true) {
            $this->login_model->logout();
            redirect(login_url());
        }
    }

    public function redirect_url()
    {
        $redirect_url = $this->input->get('redirect_url');
        if (empty($redirect_url) === false) {
            $parse = parse_url($redirect_url);
            if ($_SERVER['HTTP_HOST'] !== $parse['host']) {
                return site_url();
            }
            return $redirect_url;
        }

        $session_data = $this->session->userdata('redirect_url');
        if (empty($session_data) === false && $session_data !== null) {
            $this->session->set_userdata('redirect_url', null);
            $parse = parse_url($session_data);
            if ($_SERVER['HTTP_HOST'] !== $parse['host']) {
                return site_url();
            }
            return $session_data;
        }

        return site_url($this->login_model->redirect_url());
    }

    public function back_url(string $backUrl = "")
    {
        if (empty($backUrl) === false) {
            $this->session->set_userdata('back_url', $backUrl);
            return site_url($backUrl);
        }

        $session_data = $this->session->userdata('back_url');
        if (empty($session_data) === false) {
            $this->session->unset_userdata('back_url');
            return site_url($session_data);
        }
        return $this->agent->referrer();
    }

    public function startRedirect(string $defaultUrl = "")
    {
        $this->load->database();
        $domainRedirect = c_key('webapp_domain_redirect') ?? "";
        if (empty($domainRedirect) === false) {
            $domeinList = explode(',', $domainRedirect);
            if (empty($domeinList) === false) {
                $httphost = $_SERVER['HTTP_HOST'];
                foreach ($domeinList as $value) {
                    $domeinRedirectList = explode('@', $value);
                    if (current($domeinRedirectList) === $httphost) {
                        redirect(end($domeinRedirectList));
                    }
                }
            }
        }

        $defaultRedirect = c_key('webapp_default_url') ?? "";
        if (empty($defaultRedirect) === false) {
            redirect($defaultRedirect);
        }

        if (empty($defaultUrl) === false) {
            redirect($defaultUrl);
        }
    }
}
