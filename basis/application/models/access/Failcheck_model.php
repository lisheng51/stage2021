<?php

class Failcheck_model extends CI_Model
{

    public $table = "fail_check";
    public $primary_key = "fail_id";

    private function get_one(int $type_id = 0)
    {
        $this->db->from($this->table)->where("ip_address", $this->input->ip_address())->where("platform", $this->agent->platform())->where("date", date('Y-m-d'))->where("type_id", $type_id);
        $this->db->where("browser", $this->agent->browser());
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return [];
    }

    public function getTryNum(int $type_id = 0): int
    {
        $arr_data = $this->get_one($type_id);
        if (empty($arr_data) === false) {
            return (int) $arr_data["num"];
        }
        return 0;
    }

    private function get(string $key = ''): string
    {
        $value = $this->session->tempdata($key);
        if (empty($value) === true) {
            return "";
        }
        return $value;
    }

    public function check(int $type_id = 0): bool
    {
        $arr_type = $this->failcheck_type_model->fetchField($type_id);
        if (empty($arr_type) === true) {
            return true;
        }
        $check_ip = $this->get($arr_type["key"]);
        if (empty($check_ip) === true) {
            return true;
        }
        if ($this->input->ip_address() == $check_ip) {
            return false;
        }

        return true;
    }

    public function update(int $type_id = 0)
    {
        $arr_type = $this->failcheck_type_model->fetchField($type_id);
        if (empty($arr_type) === true) {
            return;
        }

        $arr_data = $this->get_one($type_id);
        if (empty($arr_data) === true) {
            return $this->add($type_id);
        }

        $webapp_fail_limit_num = c_key("webapp_fail_limit_num") > 0 ? c_key("webapp_fail_limit_num") : 5;
        $mod = $arr_data["num"] % $webapp_fail_limit_num;
        if ($mod === 0) {
            $this->session->set_tempdata($arr_type["key"], $arr_data["ip_address"], $arr_type["lock_time"]);
        } else {
            $data["num"] = $arr_data["num"] + 1;
            $this->edit($arr_data[$this->primary_key], $data);
        }
    }

    public function reset(int $type_id = 0)
    {
        $arr_type = $this->failcheck_type_model->fetchField($type_id);
        if (empty($arr_type) === true) {
            return;
        }

        $arr_data = $this->get_one($type_id);
        if (empty($arr_data) === true) {
            return;
        }

        $data["num"] = 0;
        $this->edit($arr_data[$this->primary_key], $data);
    }

    private function add(int $type_id = 0): int
    {
        if ($type_id > 0) {
            $data["ip_address"] = $this->input->ip_address();
            $data["browser"] = $this->agent->browser();
            $data["platform"] = $this->agent->platform();
            $data["date"] = date('Y-m-d');
            $data["type_id"] = $type_id;
            return $this->db->insert($this->table, $data);
        }
        return 0;
    }

    private function edit(int $id = 0, array $data = [])
    {
        $this->db->where($this->primary_key, $id)->update($this->table, $data);
    }
}
