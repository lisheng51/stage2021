<?php

class Upload_model extends MY_Model
{

    public $root_folder = ENVIRONMENT_UPLOAD_PATH;
    public $folder = "files";
    private $file_name = "";
    public $only_file_name = null;
    private $base64code = "";
    public $table = "upload";
    public $primary_key = "upload_id";
    public $select_order_by = [];
    public $usingResize = true;

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'upload_id#desc' => 'ID (aflopend)',
            'title#desc' => 'Title (aflopend)',
            'title#asc' => 'Title (oplopend)',
            'file_name#desc' => 'Bestand (aflopend)',
            'file_name#asc' => 'Bestand (oplopend)',
        ];
    }

    public function make_dir(string $dir = "", bool $withIndexFile = true, bool $withLockFile = false)
    {
        $path = FCPATH . $this->root_folder;
        if (empty($dir) === false) {
            $path .= DIRECTORY_SEPARATOR . $dir;
        }
        if (is_dir($path) === false) {
            mkdir($path, 0755, true);
        }

        if ($withIndexFile === true) {
            file_put_contents($path . DIRECTORY_SEPARATOR . 'index.html', '<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body>Directory access is forbidden</body></html>');
        }

        if ($withLockFile === true) {
            file_put_contents($path . DIRECTORY_SEPARATOR . '.htaccess', 'deny from all');
        }
    }

    public function get_one_by_title(string $value = ''): array
    {
        $data['title'] = $value;
        $this->sql_where = $data;
        return $this->get_one();
    }

    public function add_batch(array $data = [])
    {
        $this->db->insert_batch($this->table, $data);
    }

    public function get_postdata(): array
    {
        $data[$this->upload_type_model->primary_key] = $this->input->post($this->upload_type_model->primary_key);
        $data["title"] = $this->input->post("title");
        $this->ajaxck_model->spamword($data);
        return $data;
    }

    public function from_string($base64code = "", $file_name_format = 'uploadfile')
    {
        if (empty($base64code) === true) {
            return null;
        }

        $this->file_name = $file_name_format;
        $this->base64code = $base64code;
        if (preg_match("/^data:image\/png;base64/i", $this->base64code)) {
            return $this->make_file('png');
        } elseif (preg_match("/^data:application\/pdf;base64/i", $this->base64code)) {
            return $this->make_file('pdf');
        } elseif (preg_match("/^data:image\/jpeg;base64/i", $this->base64code)) {
            return $this->make_file('jpeg');
        } elseif (preg_match("/^data:image\/jpg;base64/i", $this->base64code)) {
            return $this->make_file('jpg');
        } elseif (preg_match("/^data:text\/xml;base64/i", $this->base64code)) {
            return $this->make_file('xml');
        } elseif (preg_match("/^data:image\/gif;base64/i", $this->base64code)) {
            return $this->make_file('gif');
        } elseif (preg_match("/^data:application\/vnd.openxmlformats-officedocument.wordprocessingml.document;base64/i", $this->base64code)) {
            return $this->make_file('docx');
        } elseif (preg_match("/^data:application\/msword;base64/i", $this->base64code)) {
            return $this->make_file('doc');
        } else {
            return $this->base64code;
        }
    }

    public function show_file($file = null, $only_file_path = false)
    {
        if (empty($file) === true) {
            return $only_file_path === false ? sys_asset_url('img/0.png') : "asset/load/assets/img/0.png";
        }
        $folder = $this->root_folder . DIRECTORY_SEPARATOR;
        $file_path = $folder . $this->folder . DIRECTORY_SEPARATOR . $file;
        return $only_file_path === false ? base_url($file_path) : $file_path;
    }

    private function make_file(string $ext = 'png')
    {
        switch ($ext) {
            case 'png':
            case 'jpeg':
            case 'jpg':
            case 'gif':
                $imagestrng = str_replace('data:image/' . $ext . ';base64,', "", $this->base64code);
                break;
            case 'pdf':
                $imagestrng = str_replace('data:application/' . $ext . ';base64,', "", $this->base64code);
                break;
            case 'xml':
                $imagestrng = str_replace('data:text/' . $ext . ';base64,', "", $this->base64code);
                break;
            case 'docx':
                $imagestrng = str_replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,', "", $this->base64code);
                break;
            case 'doc':
                $imagestrng = str_replace('data:application/msword;base64,', "", $this->base64code);
                break;
        }

        $decode_string = str_replace(' ', '+', $imagestrng);
        $folder = $this->root_folder . DIRECTORY_SEPARATOR;
        file_put_contents($folder . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR . $this->file_name . '.' . $ext, base64_decode($decode_string));
        if ($ext === 'png' || $ext === 'jpeg' || $ext === 'jpg' || $ext === 'gif') {
            $this->resize($folder . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR . $this->file_name . '.' . $ext);
        }
        return $this->file_name . '.' . $ext;
    }

    public function resize(string $source_image = '')
    {
        if ($this->usingResize === true) {
            $this->load->library('image_lib');
            $config['image_library'] = 'gd2';
            $config['source_image'] = $source_image;
            $config['create_thumb'] = false;
            $config['maintain_ratio'] = true;
            $config['width'] = 450;
            $config['height'] = 450;
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }
            $this->image_lib->clear();
        }
    }

    public function convertBase64($file = null)
    {
        if (empty($file) === true) {
            return "";
        }
        $type = pathinfo($file, PATHINFO_EXTENSION);
        $data = @file_get_contents($file);
        if ($data === false) {
            return "";
        }
        return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }

    public function replace_site_url($content = null, $search = null, $replace = "")
    {
        if (empty($replace) === true) {
            $replace = base_url();
        }
        //replace_site_url($content, http://localhost/uniekdating');
        return preg_replace('/' . preg_quote($search, '/') . '/', $replace, $content);
    }
}
