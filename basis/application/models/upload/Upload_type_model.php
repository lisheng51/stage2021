<?php

class Upload_type_model extends CI_Model
{

    public $primary_key = "type_id";
    public $id_1 = 1;
    public $id_2 = 2;
    public $id_3 = 3;

    public function select(int $id = 0, bool $with_empty = false): string
    {
        if ($id === 0) {
            $id = $this->input->post_get($this->primary_key) ?? 0;
        }

        $select = '<select name=' . $this->primary_key . ' class="form-control selectpicker" data-live-search="true">';
        $select .= $with_empty === true ? "<option value='' >------</option>" : "";
        $listdb = $this->listDB();
        foreach ($listdb as $rs) {
            $ckk = $id == $rs[$this->primary_key] ? "selected" : '';
            $select .= '<option value=' . $rs[$this->primary_key] . ' ' . $ckk . '>' . $rs["name"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function selectMultiple(array $haystack = []): string
    {
        if (empty($haystack) === true) {
            $haystack = $this->input->post_get($this->primary_key) ?? [];
            if (is_array($haystack) === false) {
                $haystack = [$haystack];
            }
        }
        $listdb = $this->listDB();
        $select = '<select name="' . $this->primary_key . '[]" class="form-control selectpicker" data-selected-text-format="count > 3" data-live-search="true" multiple title="------">';
        foreach ($listdb as $rs) {
            $ckk = (empty($haystack) === false && in_array($rs[$this->primary_key], $haystack) === true) ? 'selected' : '';
            $select .= '<option value=' . $rs[$this->primary_key] . ' ' . $ckk . '>' . $rs["name"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function listDB(): array
    {
        $data[] = $this->id_1();
        $data[] = $this->id_2();
        $data[] = $this->id_3();
        return $data;
    }

    private function id_1(string $key = '')
    {
        $data[$this->primary_key] = $this->id_1;
        $data['name'] = 'Any';
        $data['path_name'] = 'any';
        $data['allowed_types'] = 'mp3|wma|wmv|mp4|doc|docx|pdf|gif|xlsx|csv|jpeg|jpg|png|txt|zip|bmp|css|js|woff2|ttf|woff|svg|otf';
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    private function id_2(string $key = '')
    {
        $data[$this->primary_key] = $this->id_2;
        $data['name'] = 'Media';
        $data['path_name'] = 'media';
        $data['allowed_types'] = 'wmv|mp4|gif|jpeg|jpg|png|mp3|wma|bmp|ogg|wav';
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    private function id_3(string $key = '')
    {
        $data[$this->primary_key] = $this->id_3;
        $data['name'] = 'Office';
        $data['path_name'] = 'office';
        $data['allowed_types'] = 'doc|docx|pdf|xlsx|csv|txt|ppt';
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    public function fetchData(int $id = 0, string $field = '')
    {
        switch ($id) {
            case $this->id_1:
                $value = $this->id_1($field);
                break;
            case $this->id_2:
                $value = $this->id_2($field);
                break;
            case $this->id_3:
                $value = $this->id_3($field);
                break;
            default:
                $value = null;
                break;
        }
        return $value;
    }

    public function fetch_path_name(int $id = 0, string $field = 'path_name')
    {
        return $this->fetchData($id, $field);
    }

    public function show_dir(string $path_name = "", int $user_id = 0): string
    {
        if ($user_id <= 0) {
            $user_id = $this->login_model->user_id();
        }
        return $this->upload_model->root_folder . DIRECTORY_SEPARATOR . $this->upload_model->folder . DIRECTORY_SEPARATOR . $user_id . DIRECTORY_SEPARATOR . $path_name;
    }

    public function make_dir(int $user_id = 0)
    {
        $this->upload_model->folder = 'files';
        $listdb = $this->listDB();
        foreach ($listdb as $rs) {
            $path = FCPATH . $this->show_dir($rs["path_name"], $user_id);
            if (is_dir($path) === false) {
                mkdir($path, 0755, true);
            }
        }
    }
}
