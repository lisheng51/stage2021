<?php

class User_model extends MY_Model
{

    public $table = "user";
    public $primary_key = "user_id";
    public $upload_folder = 'images' . DIRECTORY_SEPARATOR . 'user';
    public $secure_id = 1;
    public $select_order_by = [];
    protected $using_softDel = true;

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'user_id#desc' => 'ID (aflopend)',
            'display_info#desc' => 'Naam (aflopend)',
            'display_info#asc' => 'Naam (oplopend)',
        ];
    }

    public function select(int $id = 0, bool $with_empty = false): string
    {
        $where[$this->_field_is_deleted] = 0;
        $this->sql_where = $where;
        $select = '<select name=' . $this->primary_key . ' class="form-control selectpicker">';
        $select .= $with_empty === true ? "<option value='' >------</option>" : "";
        foreach ($this->get_all() as $rs) {
            $ckk = $id == $rs[$this->primary_key] ? "selected" : '';
            $name = $rs["display_info"];
            $select .= "<option value={$rs[$this->primary_key]} $ckk >$name</option>";
        }
        $select .= '</select>';
        return $select;
    }

    public function make_dir()
    {
        $path = FCPATH . $this->upload_model->root_folder . DIRECTORY_SEPARATOR . $this->upload_folder;
        if (is_dir($path) === false) {
            mkdir($path, 0755, true);
        }
    }

    public function get_postdata(): array
    {
        $data["display_info"] = $this->input->post("display_info");
        $data["phone"] = $this->input->post("phone");
        $data["nav_bookmark"] = $this->input->post("nav_bookmark");
        return $data;
    }

    public function navbarViewFile(array $userDB = [])
    {
        $viewFile = 'navbar_master';
        if ($this->login_model->user_id() !== $this->user_model->secure_id) {
            $viewFile = "navbar";

            if ($userDB["nav_bookmark"] > 0) {
                $viewFile = "navbar_bookmark";
            }
        }

        return $viewFile;
    }

    public function secure_id(int $id = 1)
    {
        if ($this->login_model->user_id() !== $this->secure_id && $id === $this->secure_id) {
            $this->session->set_flashdata('page_error_type', "no_authorized");
            redirect(error_url());
        }
    }

    public function get_total(): int
    {
        $this->db->select($this->primary_key);
        $this->db->from($this->table);
        $this->db->join($this->login_model->table, $this->primary_key);
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        return $this->db->count_all_results();
    }

    public function get_list(int $limit = 1, int $page = 0): array
    {
        $this->db->from($this->table);
        $this->db->join($this->login_model->table, $this->primary_key);
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        if (empty($this->sql_order_by) === false) {
            foreach ($this->sql_order_by as $field => $value) {
                $this->db->order_by($field, $value);
            }
        }

        $this->db->order_by($this->primary_key, "desc");
        if ($page <= 0) {
            $page = 0;
        }
        $this->db->limit($limit, $page);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function fetch_name(int $uid = 0): string
    {
        $this->db->from($this->table);
        $this->db->join($this->login_model->table, $this->primary_key);
        $this->db->where($this->primary_key, $uid);
        $query = $this->db->limit(1)->get();
        $arr_user = [];
        if ($query->num_rows() > 0) {
            $arr_user = $query->row_array();
        }
        if (empty($arr_user) === true) {
            return "Systeem";
        }
        return $arr_user["display_info"] ?? "";
    }
}
