<?php


class Api_service
{

    public static $url = "https://api.bloemendaalconsultancy.nl/";
    public static $id = "4";
    public static $key = "ca63be25600ead9dd7d7d588ab4ee425";

    private static function curl(string $path = "", array $params = [])
    {
        $apiId = self::$id;
        $apiKey = self::$key;

        $httpheaders = array(
            "api-id:  $apiId",
            "api-key: $apiKey"
        );
        $toUrl = self::$url . $path;
        $fields_string = http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0); //return url reponse header
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpheaders);
        curl_setopt($ch, CURLOPT_URL, $toUrl);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            return null;
        }
        curl_close($ch);
        if ($response === false || empty($response) === true) {
            return null;
        }
        return $response;
    }

    public static function selectNamePrefix(string $value = "", string $name = ''): string
    {
        $select_name = empty($name) === false ? $name : "prefix";
        $select = '<select name="' . $select_name . '" class="form-control selectpicker" data-live-search="true">';
        $select .= "<option value='' ></option>";
        $listdb = self::allNamePrefix();
        foreach ($listdb as $rs) {
            $ckk = $value == $rs['value'] ? "selected" : '';
            $select .= '<option value="' . $rs["value"] . '" ' . $ckk . '>' . $rs["value"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public static function selectNationality(string $value = "", string $name = ''): string
    {
        $select_name = empty($name) === false ? $name : "nationality";
        $select = '<select name="' . $select_name . '" class="form-control selectpicker" data-live-search="true">';
        $select .= "<option value='' ></option>";
        $listdb = self::allNationality();
        foreach ($listdb as $rs) {
            $ckk = $value == $rs['nationality'] ? "selected" : '';
            $select .= '<option value="' . $rs["nationality"] . '" ' . $ckk . '>' . $rs["nationality"] . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public static function allNationality()
    {
        $arrResult = [];
        $response = self::curl('country/api/home/all');
        if ($response !== null) {
            $response_result = json_decode($response, true);
            if ($response_result['statusCode'] === 100) {
                $arrResult = $response_result['message']["listdb"];
            }
        }
        return $arrResult;
    }

    public static function allNamePrefix()
    {
        $arrResult = [];
        $response = self::curl('name_prefix/api/home/all');
        if ($response !== null) {
            $response_result = json_decode($response, true);
            if ($response_result['statusCode'] === 100) {
                $arrResult = $response_result['message']["listdb"];
            }
        }
        return $arrResult;
    }


    public static function addressSearch(string $zipcode = "", string $houseNumber = "", string $housenr_addition = "")
    {
        $input["zipcode"] = $zipcode;
        $input["housenr"] = $houseNumber;
        $input["housenr_addition"] = $housenr_addition;

        $arrResult = [];
        $response = self::curl('address/api/home/search', $input);
        if ($response !== null) {
            $response_result = json_decode($response, true);
            if ($response_result['statusCode'] === 100) {
                $arrResult = $response_result['message'];
            }
        }
        return $arrResult;
    }
}
