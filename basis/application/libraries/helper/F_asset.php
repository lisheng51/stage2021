<?php

/**
 * F_asset
 */
class F_asset
{

    public static $nodeModules = 'node_modules';

    /**
     * 
     * @param string $iconName
     * @return string
     */
    public static function favicon(string $iconName = "favicon.ico"): string
    {
        return link_tag(base_url($iconName), 'rel="shortcut icon" type="image/x-icon"');
    }

    /**
     * 
     * @return string
     */
    public static function jquery(): string
    {
        return self::axios() . script_tag(base_url(self::$nodeModules . "/jquery/dist/jquery.min.js"));
    }

    /**
     * 
     * @return string
     */
    public static function axios(): string
    {
        return script_tag(base_url(self::$nodeModules . "/axios/dist/axios.min.js"));
    }

    /**
     * 
     * @return string
     */
    public static function jqueryUI(): string
    {
        return script_tag(base_url(self::$nodeModules . "/jquery-ui-dist/jquery-ui.min.js"));
    }

    /**
     * 
     * @return string
     */
    public static function socket(): string
    {
        return script_tag(base_url(self::$nodeModules . "/socket.io-client/dist/socket.io.js"));
    }

    /**
     * 
     * @return string
     */
    public static function cropper(): string
    {
        return link_tag(base_url(self::$nodeModules . "/cropper/dist/cropper.min.css")) .
            script_tag(base_url(self::$nodeModules . "/cropper/dist/cropper.min.js"));
    }

    /**
     * 
     * @return string
     */
    public static function sweetalert2(): string
    {
        return link_tag(base_url(self::$nodeModules . "/sweetalert2/dist/sweetalert2.min.css")) .
            script_tag(base_url(self::$nodeModules . "/sweetalert2/dist/sweetalert2.min.js"));
    }

    /**
     * 
     * @return string
     */
    public static function jqueryMask(): string
    {
        return script_tag(base_url(self::$nodeModules . "/jquery-mask-plugin/dist/jquery.mask.min.js"));
    }

    /**
     * 
     * @return string
     */
    public static function cookie(): string
    {
        return script_tag(base_url(self::$nodeModules . "/js-cookie/src/js.cookie.js"));
    }

    /**
     * 
     * @return string
     */
    public static function signature(): string
    {
        return script_tag(base_url(self::$nodeModules . "/jsignature/src/jSignature.js"));
    }

    /**
     * 
     * @return string
     */
    public static function fontawesome(): string
    {
        return link_tag(base_url(self::$nodeModules . "/@fortawesome/fontawesome-free/css/all.min.css"));
    }

    /**
     * 
     * @return string
     */
    public static function bootstrap(): string
    {
        return link_tag(base_url(self::$nodeModules . "/bootstrap/dist/css/bootstrap.min.css")) .
            script_tag(base_url(self::$nodeModules . "/bootstrap/dist/js/bootstrap.bundle.min.js"));
    }

    /**
     * 
     * @return string
     */
    public static function selectpicker(): string
    {
        return link_tag(base_url(self::$nodeModules . "/bootstrap-select/dist/css/bootstrap-select.min.css")) .
            script_tag(base_url(self::$nodeModules . "/bootstrap-select/dist/js/bootstrap-select.min.js")) .
            script_tag(base_url(self::$nodeModules . "/bootstrap-select/dist/js/i18n/defaults-nl_NL.min.js"));
    }

    /**
     * 
     * @return string
     */
    public static function datatables(): string
    {
        return link_tag(base_url(self::$nodeModules . "/datatables.net-bs4/css/dataTables.bootstrap4.min.css")) .
            script_tag(base_url(self::$nodeModules . "/datatables.net/js/jquery.dataTables.min.js")) .
            script_tag(base_url(self::$nodeModules . "/datatables.net-bs4/js/dataTables.bootstrap4.min.js"));
    }

    /**
     * 
     * @return string
     */
    public static function tinymce(): string
    {
        return script_tag(base_url(self::$nodeModules . "/tinymce/tinymce.min.js"));
    }

    /**
     * 
     * @return string
     */
    public static function highcharts(): string
    {
        return script_tag(base_url(self::$nodeModules . "/highcharts/highcharts.js"));
    }

    /**
     * 
     * @return string
     */
    public static function moment(): string
    {
        return script_tag(base_url(self::$nodeModules . "/moment/min/moment.min.js"));
    }

    /**
     * 
     * @return string
     */
    public static function print(): string
    {
        return script_tag(base_url(self::$nodeModules . "/jQuery.print/jQuery.print.js"));
    }

    /**
     * 
     * @return string
     */
    public static function touchPunch(): string
    {
        return script_tag(base_url(self::$nodeModules . "/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"));
    }

    /**
     * 
     * @return string
     */
    public static function typeahead(): string
    {
        return script_tag(base_url(self::$nodeModules . "/typeahead.js/dist/typeahead.bundle.min.js"));
    }

    /**
     * 
     * @return string
     */
    public static function tagsinput(): string
    {
        return link_tag(base_url(self::$nodeModules . "/bootstrap-tagsinput/dist/bootstrap-tagsinput.css")) .
            link_tag(base_url(self::$nodeModules . "/bootstrap-tagsinput/dist/bootstrap-tagsinput-typeahead.css")) .
            script_tag(base_url(self::$nodeModules . "/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"));
    }

    /**
     * 
     * @return string
     */
    public static function daterangepicker(): string
    {
        return link_tag(base_url(self::$nodeModules . "/daterangepicker/daterangepicker.css")) .
            script_tag(base_url(self::$nodeModules . "/daterangepicker/daterangepicker.js"));
    }

    /**
     * 
     * @return string
     */
    public static function fullcalendar(): string
    {
        return link_tag(base_url(self::$nodeModules . "/fullcalendar/main.min.css")) .
            script_tag(base_url(self::$nodeModules . "/fullcalendar/main.min.js")) .
            script_tag(base_url(self::$nodeModules . "/fullcalendar/locales-all.min.js"));
    }

    /**
     * 
     * @return string
     */
    public static function fileinput(): string
    {
        return link_tag(base_url(self::$nodeModules . "/bootstrap-fileinput/css/fileinput.min.css")) .
            script_tag(base_url(self::$nodeModules . "/bootstrap-fileinput/js/fileinput.min.js")) .
            script_tag(base_url(self::$nodeModules . "/bootstrap-fileinput/js/locales/nl.js")) .
            script_tag(base_url(self::$nodeModules . "/bootstrap-fileinput/themes/fas/theme.min.js"));
    }
}
