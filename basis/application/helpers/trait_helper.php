<?php
$traitFolder = 'traits';
$files = directory_map(__DIR__ . DIRECTORY_SEPARATOR . $traitFolder);
foreach ($files as $filename) {
    include $traitFolder . DIRECTORY_SEPARATOR . $filename;
}
