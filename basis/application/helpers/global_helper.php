<?php

if (!function_exists('debug_as_file')) {

    function debug_as_file($postdata = null)
    {
        if (is_array($postdata) === true) {
            $postdata = json_encode($postdata);
        }

        $CI = &get_instance();
        $path = $CI->config->item('cache_path');
        write_file($path . 'debug.txt', $postdata);
    }
}

if (!function_exists('isJSON')) {

    function isJSON($string = null)
    {
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }
}

if (!function_exists('script_tag')) {

    function script_tag(string $path = "", string $attribute = "")
    {
        return '<script src="' . $path . '" ' . $attribute . '></script>';
    }
}

if (!function_exists('link_tag')) {

    function link_tag(string $path = "", string $attribute = 'rel="stylesheet" type="text/css"')
    {
        return '<link href="' . $path . '" ' . $attribute . '>';
    }
}

if (!function_exists('sys_asset_url')) {

    function sys_asset_url(string $path = ""): string
    {
        if (empty($path) === true) {
            return "";
        }
        return site_url("asset/load/assets/$path");
    }
}

if (!function_exists('module_url')) {

    function module_url(string $path = "", string $modulename = ""): string
    {
        if (empty($path) === true) {
            return "";
        }
        $module = $modulename;
        if (empty($modulename) === true) {
            $CI = &get_instance();
            $module = $CI->router->module;
        }
        return site_url($module . '/' . $path);
    }
}

if (!function_exists('module_asset_url')) {

    function module_asset_url(string $filename = "", string $modulename = "", string $path = ""): string
    {
        if (empty($filename) === true) {
            return "";
        }
        $module = $modulename;
        if (empty($modulename) === true) {
            $CI = &get_instance();
            $module = $CI->router->module;
        }
        return site_url('asset/load/' . $module . '/' . $filename . '?path=' . $path);
    }
}

if (!function_exists('route_url')) {

    function route_url(string $url = '', string $extendUrl = ''): string
    {
        $CI = &get_instance();
        $urls = array_flip($CI->router->routes);
        if (isset($urls[$url]) === false || empty($url) === true) {
            return site_url($url);
        }
        if (empty($extendUrl) === false) {
            return site_url($urls[$url] . $extendUrl);
        }
        return site_url($urls[$url]);
    }
}

if (!function_exists('login_url')) {

    function login_url(string $path = ''): string
    {
        return site_url(ENVIRONMENT_ACCESS_URL . "/Login" . $path);
    }
}

if (!function_exists('error_url')) {

    function error_url(): string
    {
        return site_url(ENVIRONMENT_ERROR_URL);
    }
}

if (!function_exists('dump')) {

    function dump($val, $vardump = false)
    {
        if ($vardump === true) {
            var_dump($val);
            exit;
        }
        echo '<pre>';
        print_r($val);
        echo '</pre>';
        exit;
    }
}

if (!function_exists('add_csrf_value')) {

    function add_csrf_value()
    {
        if (config_item('csrf_protection')) {
            $CI = &get_instance();
            $csrf = array(
                'name' => $CI->security->get_csrf_token_name(),
                'hash' => $CI->security->get_csrf_hash()
            );

            return '<input type="hidden" name="' . $csrf["name"] . '" value="' . $csrf["hash"] . '" />';
        }
    }
}

if (!function_exists('add_submit_button')) {

    function add_submit_button($search_data = null, $only_submit_text = false)
    {
        $CI = &get_instance();
        $submit_button_text = lang('change_button_text');
        if (empty($search_data) === true && $only_submit_text === false) {
            $submit_button_text = lang('add_button_text');
        }

        if (empty($search_data) === true && $only_submit_text === true) {
            $submit_button_text = lang('submit_button_text');
        }
        $data["text"] = $submit_button_text;
        $data["rsdb"] = $search_data;
        return $CI->load->view("_share/global/submitButton", $data, true);
    }
}


if (!function_exists('search_button')) {

    function search_button(string $text = "")
    {
        $CI = &get_instance();
        $data["rsdb"] = null;
        $data["text"] = empty($text) === true ? lang('search_icon') : $text;
        return $CI->load->view("_share/global/submitButton", $data, true);
    }
}

if (!function_exists('reset_button')) {

    function reset_button(string $text = "")
    {
        $CI = &get_instance();
        $data["text"] = empty($text) === true ? lang('reset_icon') : $text;
        $data["type"] = 'button';
        $data["extraClass"] = 'reset';
        return $CI->load->view("_share/global/resetButton", $data, true);
    }
}

if (!function_exists('add_reset_button')) {

    function add_reset_button(string $text = "")
    {
        $CI = &get_instance();
        $data["text"] = empty($text) === true ? lang('reset_icon') : $text;
        $data["type"] = 'reset';
        $data["extraClass"] = '';
        return $CI->load->view("_share/global/resetButton", $data, true);
    }
}
if (!function_exists('add_back_button')) {

    function add_back_button(string $url = "", string $text = "")
    {
        $CI = &get_instance();
        $defaultUrl = $CI->access_check_model->back_url();
        $data["url"] = empty($url) === true ? $defaultUrl : site_url($url);
        $data["text"] = empty($text) === true ? lang('back_button_text') : $text;
        return $CI->load->view("_share/global/backButton", $data, true);
    }
}

if (!function_exists('get_curl')) {

    function get_curl($url = "", $params = [], $methode = "POST", $CURLOPT_HTTPHEADER = [])
    {
        if (empty($url) === true || filter_var($url, FILTER_VALIDATE_URL) === false) {
            return;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        switch ($methode) {
            case "POST":
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                break;
            default:
                break;
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $CURLOPT_HTTPHEADER);
        $response = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpCode == 404) {
            return;
        }

        if (curl_errno($ch)) {
            return;
        }
        curl_close($ch);
        if ($response === false || empty($response) === true) {
            return;
        }
        return $response;
    }
}

if (!function_exists('add_app_log')) {

    function add_app_log($description = "", $uid = 0)
    {
        if (empty($description) === true) {
            return;
        }
        $CI = &get_instance();
        $CI->applog_model->insert($description, $uid);
    }
}

if (!function_exists('select_page_limit')) {

    function select_page_limit(int $setLimit = 0, bool $withLabel = true, array $listdb = [20 => "20", 50 => "50", 100 => "100", 200 => "200"], string $name = 'page_limit')
    {
        $label = "";
        if ($withLabel === true) {
            $label = lang("select_page_limit_label", "select_page_limit_label");
        }

        $CI = &get_instance();
        $limit = $CI->input->post_get($name) ?? loadPostGet($name, 'int');

        if ($setLimit > 0 && empty($limit) === true) {
            $limit = $setLimit;
        }

        $select = $label . '<select name="' . $name . '" class="form-control selectpicker">';
        $select .= "<option value=''>------</option>";
        foreach ($listdb as $key => $rs) {
            $ckk = $limit == $key ? "selected" : '';
            $select .= "<option value={$key} $ckk >$rs</option>";
        }
        $select .= '</select>';
        $html = '<div class="form-group">' . $select . '</div>';
        return $html;
    }
}

if (!function_exists('span_tooltip')) {

    function span_tooltip(string $tooltip = "")
    {
        $CI = &get_instance();
        $data["msg"] = $tooltip;
        return $CI->load->view("_share/global/span_tooltip", $data, true);
    }
}

if (!function_exists('invalid_feedback')) {

    function invalid_feedback(string $class = "invalid-feedback", string $text = "")
    {
        $msg = empty($text) === true ? lang('required_field_text_global') : $text;
        return '<div class="' . $class . '">' . $msg . '</div>';
    }
}

if (!function_exists('select_boolean')) {

    function select_boolean(string $name = 'is_boolean', int $setId = 0, bool $with_empty = false, array $listdb = [0 => "Nee", 1 => "Ja"])
    {
        $CI = &get_instance();
        $id = $CI->input->post_get($name) ?? loadPostGet($name) ?? $setId;
        if ($id != "" && $setId === 2) {
            $id = intval($id);
        }

        if ($setId < 2) {
            $id = $setId;
        }

        $select = '<select name="' . $name . '" class="form-control selectpicker">';
        $select .= $with_empty === true ? "<option value='' >------</option>" : "";
        foreach ($listdb as $key => $value) {
            $ckk = $id === $key ? "selected" : '';
            $select .= "<option value={$key} $ckk >$value</option>";
        }
        $select .= '</select>';
        $html = '<div class="form-group">' . $select . '</div>';
        return $html;
    }
}

if (!function_exists('select')) {

    function select(string $name = '', array $listdb = [], string $setValue = "", bool $with_empty = false): string
    {
        $select = '<select name="' . $name . '" class="form-control selectpicker">';
        $select .= $with_empty === true ? "<option value='' >------</option>" : "";
        foreach ($listdb as $value => $label) {
            $ckk = $setValue == $value ? "selected" : '';
            $select .= '<option value="' . $value . '" ' . $ckk . '>' . $label . '</option>';
        }
        $select .= '</select>';
        return $select;
    }
}

if (!function_exists('oneStepCkDatetime')) {

    function oneStepCkDatetime(string $inputname = 'start', bool $is_empty = false)
    {
        $CI = &get_instance();
        $startInputData = $CI->input->post($inputname) ?? "";
        if (empty($startInputData) === true && $is_empty === true) {
            return null;
        }
        if (strlen($startInputData) !== 19) {
            $json["msg"] = "Er is geen datum tijd gevonden!";
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $CI->ajaxck_model->ck_input_datetime($startInputData);
        $value = F_datetime::convert_datetime($startInputData, 'en');
        $CI->ajaxck_model->ck_value('datetime', $value, "Datum tijd is niet juist");
        return $value;
    }
}

if (!function_exists('oneStepCkDate')) {

    function oneStepCkDate(string $inputname = 'start', bool $is_empty = false)
    {
        $CI = &get_instance();
        $startInputData = $CI->input->post($inputname) ?? "";
        if (empty($startInputData) === true && $is_empty === true) {
            return null;
        }
        if (strlen($startInputData) !== 10) {
            $json["msg"] = "Er is geen datum gevonden!";
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $CI->ajaxck_model->ck_input_date($startInputData);
        $value = F_datetime::convert_date($startInputData, 'en');
        $CI->ajaxck_model->ck_value('date', $value, "Datum is niet juist");
        return $value;
    }
}

if (!function_exists('c_key')) {

    function c_key(string $key = "")
    {
        $CI = &get_instance();
        $value = $CI->config_model->fetch($key) ?? "";
        if ($key === 'webapp_default_show_per_page' && empty($value)) {
            return "50";
        }
        return $value;
    }
}

if (!function_exists('lang')) {

    function lang(string $line = "")
    {
        $str = get_instance()->lang->line($line);
        return stripslashes($str);
    }
}


if (!function_exists('isInternet')) {

    function isInternet(string $url = 'www.google.nl'): bool
    {
        $connected = @fsockopen($url, 443);
        $isConn = false;
        if ($connected) {
            $isConn = true;
            fclose($connected);
        }
        return $isConn;
    }
}
if (!function_exists('url_exists')) {

    function url_exists(array $servers = []): array
    {
        $listdb = [];
        if (empty($servers) === false) {
            $ch = [];
            $mh = curl_multi_init();
            foreach ($servers as $type => $url) {
                $ch[$type] = curl_init();
                curl_setopt($ch[$type], CURLOPT_URL, $url);
                curl_setopt($ch[$type], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch[$type], CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch[$type], CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch[$type], CURLOPT_SSL_VERIFYPEER, 0);
                curl_multi_add_handle($mh, $ch[$type]);
            }

            $running = 0;
            do {
                curl_multi_exec($mh, $running);
                curl_multi_select($mh);
            } while ($running > 0);

            foreach (array_keys($ch) as $key) {
                $http_code = curl_getinfo($ch[$key], CURLINFO_HTTP_CODE);
                $response = curl_multi_getcontent($ch[$key]);
                $appStatusOnline = true;
                if ($http_code !== 200 || $response === null) {
                    $appStatusOnline = false;
                }
                $listdb[$key] = $appStatusOnline;
                curl_multi_remove_handle($mh, $ch[$key]);
                curl_close($ch[$key]);
            }
            curl_multi_close($mh);
        }
        return $listdb;
    }
}
if (!function_exists('setFieldAndOperator')) {

    function setFieldAndOperator(string $inputname = '', string $field = "", $defaultKeyValue = null, bool $toConvertDateTime = false, string $defaultOperator = "=", string $setTableFix = "")
    {
        $CI = &get_instance();

        $setOperator = $CI->input->post_get($inputname . '_operator') ?? $defaultOperator;
        $searchKey = $CI->input->post_get($inputname) ?? $defaultKeyValue;

        if (empty($searchKey) === false || $searchKey !== null) {
            $operator = "=";
            if ($toConvertDateTime === true) {
                $regEx = '/(\d{2})-(\d{2})-(\d{4})/';
                if (preg_match($regEx, $searchKey) > 0) {
                    $searchKey = @date_format(date_create($searchKey), 'Y-m-d');
                }

                $regEx2 = '/(\d{2})-(\d{2})-(\d{4}) (\d{2}):(\d{2}):(\d{2})/';
                if (preg_match($regEx2, $searchKey) > 0) {
                    $searchKey = @date_format(date_create($searchKey), 'Y-m-d H:i:s');
                }
            }
            $searchContent = $searchKey;
            if (is_array($CI->input->post($inputname)) && $setOperator === '') {
                $setOperator = 'in';
            }
            if (is_array($CI->input->get($inputname))) {
                $setOperator = 'in';
            }

            $tableFix = $CI->db->dbprefix($field);
            if (empty($setTableFix) === false) {
                $tableFix = $setTableFix;
            }
            switch ($setOperator) {
                case '=':
                    $operator = '=';
                    break;
                case '!=':
                    $operator = '!=';
                    break;
                case '>':
                    $operator = '>';
                    break;
                case '>=':
                    $operator = '>=';
                    break;
                case '<':
                    $operator = '<';
                    break;
                case '<=':
                    $operator = '<=';
                    break;
                case 'notregexp':
                    $operator = 'NOT REGEXP "(' . $searchKey . ')"';
                    $searchContent = 'value_is_null';
                    break;
                case 'regexp':
                    $operator = 'REGEXP "(' . $searchKey . ')"';
                    $searchContent = 'value_is_null';
                    break;
                case 'in':
                    $elements = [];
                    foreach ($searchKey as $element) {
                        if (is_numeric($element)) {
                            $elements[] = $element;
                        } else {
                            $elements[] = '"' . $element . '"';
                        }
                    }
                    $operator = 'IN (' . implode(', ', $elements) . ')';
                    $searchContent = 'value_is_null';
                    break;
                case 'notin':
                    $elements = [];
                    foreach ($searchKey as $element) {
                        if (is_numeric($element)) {
                            $elements[] = $element;
                        } else {
                            $elements[] = '"' . $element . '"';
                        }
                    }
                    $operator = 'NOT IN (' . implode(', ', $elements) . ')';
                    $searchContent = 'value_is_null';
                    break;
                case 'find':
                    $elements = [];
                    foreach ($searchKey as $element) {
                        if (is_numeric($element)) {
                            $elements[] = 'FIND_IN_SET (' . $element . ',' . $tableFix . ')';
                            // $elements[] = $element;
                        } else {
                            $elements[] = 'FIND_IN_SET (' . '"' . $element . '"' . ',' . $tableFix . ')';
                            // $elements[] = '"' . $element . '"';
                        }
                    }
                    $operator = implode(' OR ', $elements);
                    //$operator = 'IN (' . implode(', ', $elements) . ')';
                    $searchContent = 'value_is_null';
                    return [$operator => $searchContent];
                case 'not_find':
                    $elements = [];
                    foreach ($searchKey as $element) {
                        if (is_numeric($element)) {
                            $elements[] = 'NOT FIND_IN_SET (' . $element . ',' . $tableFix . ')';
                            // $elements[] = $element;
                        } else {
                            $elements[] = 'NOT FIND_IN_SET (' . '"' . $element . '"' . ',' . $tableFix . ')';
                            // $elements[] = '"' . $element . '"';
                        }
                    }
                    $operator = implode(' AND ', $elements);
                    //$operator = 'NOT IN (' . implode(', ', $elements) . ')';
                    $searchContent = 'value_is_null';
                    return [$operator => $searchContent];
            }
            return [$tableFix . ' ' . $operator => $searchContent];
        }
    }
}

if (!function_exists('labelSelectInput')) {

    function labelSelectInput(string $label = "", string $inputname = "", string $defaultKeyValue = "", string $defaultOperator = "")
    {
        $CI = &get_instance();
        $data["label"] = $label;
        $operatorName = $inputname . '_operator';
        $fixValue = empty(loadPostGet($operatorName) === true) ? 'regexp' : loadPostGet($operatorName);
        $operatorName_value = $CI->input->post_get($operatorName) ?? $fixValue;
        if (empty($defaultOperator) === false) {
            $operatorName_value = $defaultOperator;
        }
        $data["select"] = select($operatorName, ['regexp' => 'Bevat', '' => '=', '!=' => '<>', '>' => '>', '>=' => '>=', '<' => '<', '<=' => '<=', 'notregexp' => 'Bevat niet'], $operatorName_value);
        $data["inputname_value"] = $CI->input->post_get($inputname) ?? loadPostGet($inputname) ?? $defaultKeyValue;
        $data["inputname"] = $inputname;
        return $CI->load->view("_share/global/label_select_input", $data, true);
    }
}


if (!function_exists('labelSelectSelectMulti')) {

    function labelSelectSelectMulti(string $label = "", string $inputname = "", string $selectMulti = "", string $defaultOperator = "")
    {
        $CI = &get_instance();
        $data["label"] = $label;
        $operatorName = $inputname . '_operator';
        $operatorName_value = $CI->input->post_get($operatorName) ?? loadPostGet($operatorName) ??  $defaultOperator;
        $data["select"] = select($operatorName, ['' => '=', 'notin' => '<>', 'find' => 'Bevat', 'not_find' => 'Bevat niet'], $operatorName_value);
        $data["selectMulti"] = $selectMulti;
        return $CI->load->view("_share/global/label_select_selectmulti", $data, true);
    }
}

if (!function_exists('select_order_by')) {

    function select_order_by(array $listdb = [], string $setSelected = '', string $setDefault = "", bool $withLabel = true, string $selectName = 'sql_orderby_field'): string
    {
        $CI = &get_instance();
        $label = "";
        if ($withLabel === true) {
            $label = lang("select_order_by_label", "select_order_by_label");
        }
        $idNow = $CI->input->post_get($selectName) ?? loadPostGet($selectName);

        if (empty($setSelected) === false && empty($idNow) === true) {
            $idNow = $setSelected;
        }

        $default = empty($setDefault) === false ? $setDefault : array_key_first($listdb);
        $select = $label . '<select name=' . $selectName . ' class="form-control selectpicker">';
        $select .= '<option value=' . $default . ' >------</option>';
        foreach ($listdb as $key => $value) {
            $ckk = $idNow === $key ? "selected" : '';
            $select .= "<option value={$key} $ckk >$value</option>";
        }
        $select .= '</select>';
        $html = '<div class="form-group">' . $select . '</div>';
        return $html;
    }
}

if (!function_exists('setFieldOrderBy')) {

    function setFieldOrderBy(string $setSelected = '', string $selectName = 'sql_orderby_field')
    {
        $CI = &get_instance();
        $orderby_field = $CI->input->post_get($selectName) ?? loadPostGet($selectName);

        if (empty($setSelected) === false && empty($orderby_field) === true) {
            $orderby_field = $setSelected;
        }

        $data_order_by = [];
        if (empty($orderby_field) === false) {
            if (strpos($orderby_field, '&&') !== false) {
                $array = explode("&&", $orderby_field);
                foreach ($array as $value) {
                    $orderbyItem = explode("#", $value);
                    if (isset($orderbyItem[0]) && isset($orderbyItem[1])) {
                        $data_order_by[$orderbyItem[0]] = $orderbyItem[1];
                    }
                }
            }

            if (strpos($orderby_field, '&&') === false) {
                $orderby_field_data = explode("#", $orderby_field);
                if (isset($orderby_field_data[0]) && isset($orderby_field_data[1])) {
                    $data_order_by[$orderby_field_data[0]] = $orderby_field_data[1];
                }
            }
        }
        return $data_order_by;
    }
}


if (!function_exists('addButton')) {

    function addButton(string $permission = "", string $url = "", string $text = "", string $setPath = "")
    {
        $CI = &get_instance();
        $checkHas = $CI->permission_model->checkHas($permission, $setPath);
        $data["disabled"] = 'disabled';
        if ($checkHas === true && $url !== "") {
            $data["disabled"] = '';
        }
        $path = $url;
        if (empty($setPath) === false) {
            $path = $setPath . '/' . $CI->access_check_model->backPath . '/' . $url;
        }
        $data["url"] = site_url($path);
        $data["text"] = empty($text) === true ? lang('add_icon') : $text;
        return $CI->load->view("_share/global/addButton", $data, true);
    }
}

if (!function_exists('delButton')) {

    function delButton(string $permission = "", $searchData = 0, string $text = "", string $setPath = "")
    {
        $CI = &get_instance();
        $checkHas = $CI->permission_model->checkHas($permission, $setPath);
        $data["disabled"] = 'disabled';
        if ($checkHas === true && $searchData > 0) {
            $data["disabled"] = '';
        }
        $data["searchData"] = $searchData;
        $data["text"] = empty($text) === true ? lang('del_icon') : $text;
        return $CI->load->view("_share/global/delButton", $data, true);
    }
}


if (!function_exists('editButton')) {

    function editButton(string $permission = "", string $url = "", string $text = "", string $setPath = "")
    {
        $CI = &get_instance();
        $checkHas = $CI->permission_model->checkHas($permission, $setPath);
        $data["disabled"] = 'disabled';
        if ($checkHas === true && $url !== "") {
            $data["disabled"] = '';
        }
        $path = $url;
        if (empty($setPath) === false) {
            $path = $setPath . '/' . $CI->access_check_model->backPath . '/' . $url;
        }
        $data["url"] = site_url($path);
        $data["text"] = empty($text) === true ? lang('edit_icon') : $text;
        return $CI->load->view("_share/global/editButton", $data, true);
    }
}


if (!function_exists('viewButton')) {

    function viewButton(string $permission = "", string $url = "", string $text = "", string $setPath = "")
    {
        $CI = &get_instance();
        $checkHas = $CI->permission_model->checkHas($permission, $setPath);
        $data["disabled"] = 'disabled';
        if ($checkHas === true && $url !== "") {
            $data["disabled"] = '';
        }
        $path = $url;
        if (empty($setPath) === false) {
            $path = $setPath . '/' . $CI->access_check_model->backPath . '/' . $url;
        }
        $data["url"] = site_url($path);
        $data["text"] = empty($text) === true ? lang('view_icon') : $text;
        return $CI->load->view("_share/global/viewButton", $data, true);
    }
}


if (!function_exists('inlineEditButton')) {

    function editInlineButton(string $permission = "", int $editId = 0, string $field = "", string $text = "", string $setPath = "")
    {
        $CI = &get_instance();
        $checkHas = $CI->permission_model->checkHas($permission, $setPath);
        if ($checkHas === true && $editId > 0 && empty($field) === false) {
            $data["editId"] = $editId;
            $data["field"] = $field;
            $data["text"] = $text;
            return $CI->load->view("_share/global/editInlineButton", $data, true);
        }
        return $text;
    }
}


if (!function_exists('editBooleanInlineButton')) {

    function editBooleanInlineButton(string $permission = "", int $editId = 0, string $field = "", int $value = 0, string $style = "primary",  string $setPath = "")
    {
        $CI = &get_instance();
        $checkHas = $CI->permission_model->checkHas($permission, $setPath);
        if ($checkHas === true && $editId > 0 && empty($field) === false) {
            $data["editId"] = $editId;
            $data["field"] = $field;
            $data["value"] = $value;
            $data["style"] = $style;
            return $CI->load->view("_share/global/editBooleanInlineButton", $data, true);
        }
        return $value;
    }
}

if (!function_exists('defaultFieldByLanguage')) {

    function defaultFieldByLanguage(string $field = "name", string $setLanguage = ""): string
    {
        $CI = &get_instance();
        return $CI->language_model->fieldByLanguage($field, $setLanguage);
    }
}

if (!function_exists('app_form_action_status')) {

    function app_form_action_status()
    {
        $CI = &get_instance();
        if ($CI->input->get('format') === "json") {
            return "no";
        }
        return "yes";
    }
}

if (!function_exists('referrer_url')) {

    function referrer_url(bool $set = false)
    {
        $CI = &get_instance();
        if ($set === true) {
            $value = current_url() . '?' . $_SERVER['QUERY_STRING'];
            return $CI->session->set_userdata('referrer_url', $value);
        }

        $session_data = $CI->session->userdata('referrer_url');
        if (empty($session_data) === false && $session_data !== null) {
            $CI->session->set_userdata('referrer_url', null);
            return $session_data;
        }

        return null;
    }
}

if (!function_exists('loadPostGet')) {

    function loadPostGet(string $key = '', $valueType = 'string')
    {
        $CI = &get_instance();
        return $CI->global_model->loadPostGet($key, $valueType);
    }
}
