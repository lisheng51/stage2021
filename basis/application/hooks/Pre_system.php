<?php
class Pre_system
{

    public function webmaster_vendor_autoload()
    {
        $filename = APPPATH . 'modules' . DIRECTORY_SEPARATOR . 'webmaster' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
        if (file_exists($filename)) {
            require_once($filename);
        }
    }
}
