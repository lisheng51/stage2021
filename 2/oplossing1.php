<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</head>
<?php 


// stap 1
$getallen = [];
$stap1 = []; // array
foreach(range(1, $_GET['number-1'] - 2) as $i){
    if(!isset($getallen[0]))
    {
        array_push($getallen, 0);
        array_push($stap1, 0);
    }
    if(!isset($getallen[1]))
    {
        array_push($getallen, 1);
        array_push($stap1, 1);
    }

    $antwoord = $getallen[0] + $getallen[1];
    array_shift($getallen);
    array_push($getallen, $antwoord);
    array_push($stap1, $antwoord);

}   
// var_dump($stap1);




// stap 2
$stap2 = []; // array
foreach($stap1 as $getal)
{
    if($getal % $_GET['number-2'] === 0){
        array_push($stap2, $getal);
    }
}
// var_dump($stap2);


// stap 3 
$stap3 = 0; // int
foreach($stap2 as $getal)
{
    $stap3 = $stap3 + $getal;
}
// var_dump($stap3);

?>



<div class="d-flex card m-3">
    <div class="card-header">
        Fibonacci
    </div>

    <form method="GET"> 
        <div class="d-flex m-3">
            <input class="input-group-number me-1" name="number-1" type="number">
            <input class="input-group-number me-1" name="number-2" type="number">
            <input class="btn btn-primary" type="submit">
        </div>

    </form>

    <div class="d-flex m-5">
        <div class="w-25 me-3">
        <h1> Stap 1 </h1>
        <?php foreach($stap1 as $i) { ?>
            <span class="badge badge-secondary badge-pill bg-primary"><?php echo $i ?> </span> 
            <?php } ?>
        </div>

        <div class="w-25">
        <h1> Stap 2 </h1>
        <?php foreach($stap2 as $i) { ?>
            <span class="badge badge-secondary badge-pill bg-primary"><?php echo $i ?> </span> 
            <?php } ?>
        </div>

        <div class="w-25">
        <h1> Stap 3 </h1>
            <span class="badge badge-secondary badge-pill bg-primary"><?php echo $stap3 ?> </span> 
        </div>
    </div>

    <p class="text-center"> &copy Copyright 2019 </p>
</div>

<script>



</script>