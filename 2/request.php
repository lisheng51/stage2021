<?php 
    $input = $_POST;
    $input1 = intval($input['number-1']);
    $input2 = intval($input['number-2']);
    
    if(!is_numeric($input1))
    {
        // throw new Exception('Geen getal ingevoerd');
        $terugkoppeling['error'] = "Onjuist getal";
        echo json_encode($terugkoppeling);
        exit;
    }
    else if(!is_numeric($input2))
    {
        // throw new Exception('Geen getal ingevoerd');
        $terugkoppeling['error'] = "Onjuist getal";
        echo json_encode($terugkoppeling);
        exit;
    }
    else if($input1 >50)
    {
        // throw new Exception('Getal hoger dan 50');
        $terugkoppeling['error'] = "Getal is hoger dan 50";
        echo json_encode($terugkoppeling);
        exit;
    }


    // stap 1
$getallen = [];
$stap1 = []; // array
foreach(range(1, $input1 - 2) as $i){
    if(!isset($getallen[0]))
    {
        array_push($getallen, 0);
        array_push($stap1, 0);
    }
    if(!isset($getallen[1]))
    {
        array_push($getallen, 1);
        array_push($stap1, 1);
    }

    $antwoord = $getallen[0] + $getallen[1];
    array_shift($getallen);
    array_push($getallen, $antwoord);
    array_push($stap1, $antwoord);

}   
// var_dump($stap1);




// stap 2
$stap2 = []; // array
foreach($stap1 as $getal)
{
    if($getal % $input2 === 0){
        array_push($stap2, $getal);
    }
}
// var_dump($stap2);


// stap 3 
$stap3 = 0; // int
foreach($stap2 as $getal)
{
    $stap3 = $stap3 + $getal;
}
// var_dump($stap3);

$terugkoppeling = [];

$htmlstap1 = "";
foreach($stap1 as $i) {
    $htmlstap1 .='<span class="badge badge-secondary badge-pill bg-primary m-1">'.$i.'</span>';
}
$htmlstap2 = "";
foreach($stap2 as $i){
    $htmlstap2 .='<span class="badge badge-secondary badge-pill bg-primary m-1">'.$i.'</span>';
}

$htmlstap3 = '<span class="badge badge-secondary badge-pill bg-primary m-1">'.$stap3.'</span>';




$terugkoppeling["stap1html"] = $htmlstap1;
$terugkoppeling['stap2html'] = $htmlstap2;
$terugkoppeling['stap3html'] = $htmlstap3;
array_push($terugkoppeling, $stap1);
array_push($terugkoppeling, $stap2);
array_push($terugkoppeling, $stap3);
json_encode($terugkoppeling);
echo json_encode($terugkoppeling);



?>