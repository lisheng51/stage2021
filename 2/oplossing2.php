<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>

<div class="d-flex card m-3">
    <div class="card-header">
        Fibonacci
    </div>
    <form id="send" method="POST">
            <div class="d-flex m-3">
                <input class="input-group-number me-1" name="number-1" id="input1" type="number">
                <input class="input-group-number me-1" name="number-2" id="input2" type="number">
                <button type="button" class="btn btn-primary" onclick="submitData()">Verzenden</button>
            </div>
    </form>

        <p id='inputmessage'>  </p>


    <div class="d-flex m-5">
        <div class="w-25 me-3">
        <h1> Stap 1 </h1>
            <div id="stap1"> </div>
        </div>

        <div class="w-25">
        <h1> Stap 2 </h1>
            <div id="stap2"> </div>
        </div>

        <div class="w-25">
        <h1> Stap 3 </h1>
            <div id="stap3"> </div>
        </div>
    </div>

    <p class="text-center"> &copy Copyright 2019 </p>
</div>


<script>
    var stap1 = [];
    var stap2 = [];
    var stap3;


function submitData()
{
    var input1 = document.querySelector('#input1');
    var input2 = document.querySelector('#input2');
    let message = document.querySelector('#inputmessage');

    input = $('#send')[0];
    var data = new FormData(input);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "request.php",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 800000,
        success: function (data) {
            console.log("SUCCESS : ", data);
            var gegevens = JSON.parse(data);
            var stap1 = document.getElementById("stap1");
            var stap2 = document.getElementById("stap2");
            var stap3 = document.getElementById("stap3");
            if(gegevens.hasOwnProperty('error')){
                message.innerHTML = gegevens.error;
                stap1.innerHTML = "";
                stap2.innerHTML = "";
                stap3.innerHTML = "";
            }
            else{
                stap1.innerHTML = gegevens.stap1html;
                stap2.innerHTML = gegevens.stap2html;
                stap3.innerHTML = gegevens.stap3html;
            }
            }
    });
}


</script>