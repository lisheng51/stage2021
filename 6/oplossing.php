
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="Description" content="Enter your description here"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
<link rel="stylesheet" href="assets/css/style.css">
<title>Title</title>
</head>
<body>


<div class="card m-5">
  <div class="card-header">
        <select onchange='changeWeek()' class="form-control" id="year">
            <?php 
            $year = intval(date("Y"));
            foreach (range($year-80, $year+80) as $number){
                $year = intval(date("Y"));
                if($number === $year){
                    echo "<option selected value='".$number."'> ".$number." </option>";
                }
                else{
                    echo "<option value='".$number."'> ".$number." </option>";
                }
            }
            ?>
        </select>
  </div>
    
    <div class="table-responsive">
        <div class="column"> 
            <table class="table table-bordered" id='content1'>
            </table>
        </div>

        <div class="column"> 
            <tbody id="month">
                <table class="table table-bordered">
                    <tbody  id='content2'>

                    </tbody>
                    <tfoot id="dagenvandeweek">


                    </tfoot>
                
                </table>
            </tbody>
        </div>
    </div>

    <table class="table table-bordered">
                <tbody id="week">
                        
                 </tbody>
            </table>

            <div class="card-footer text-muted" id="hergebruiken">
                
    <!-- Te hergebruiken jaren:  -->
  </div>
</div>




</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>




<style>
    .column {
        float: left;
        width: 50%;

    }
    .bold{
        font-weight: bold;
    }

</style>

<script>
    var data;
    $('document').ready(function(){
        var year = document.querySelector("#year").value;
        
        $.ajax({
        type: "POST",
        url: "request.php",
        data: {
            'year': year,
        },
        cache: false,
        timeout: 800000,
        success: function (data) {
                let gegevens = JSON.parse(data);
                document.querySelector('#content1').innerHTML = gegevens['content1'];
                document.querySelector('#content2').innerHTML = gegevens['content2'];
                document.querySelector('#week').innerHTML = gegevens['aantalweken'];
                document.querySelector('#dagenvandeweek').innerHTML = gegevens['dagenvandeweek'];
                document.querySelector('#hergebruiken').innerHTML = gegevens['hergebruiken'];
            }
    });
    });


    function changeWeek()
    {
        var jaren = [];
        var year = document.querySelector("#year").value;
        
        $.ajax({
        type: "POST",
        url: "request.php",
        data: {
            'year': year,
        },
        cache: false,
        timeout: 800000,
        success: function (data) {
                var gegevens = JSON.parse(data);
                var jsongegevens = data;
                document.querySelector('#content1').innerHTML = gegevens['content1'];
                document.querySelector('#content2').innerHTML = gegevens['content2'];
                document.querySelector('#week').innerHTML = gegevens['aantalweken'];
                document.querySelector('#dagenvandeweek').innerHTML = gegevens['dagenvandeweek'];
                document.querySelector('#hergebruiken').innerHTML = gegevens['hergebruiken'];
            }
        });
        
    }



</script>