<?php

$year = $_POST['year'];
// $year = 2019;


$jaren = [];
foreach(range($year - 80, $year + 80) as $i){
    $tijdelijkeday = date('l',strtotime(date('01-01-'.$year)));

    if($tijdelijkeday === date('l',strtotime(date('01-01-'.$i)))){
        if($year % 4 === 0){
            if($i % 4 === 0){
                array_push($jaren, $i);
            }
            else{
            }
        }
        else{
            if($i % 4 === 0){
            }
            else{
                array_push($jaren, $i);
            }
        }
    }
}



setlocale(LC_TIME, 'NL_nl');

// Aantal weken
$date = $year."-12-28";
$date = strtotime($date);
$totalweeks = idate('W', $date);
//
// Welk weeknummer
$date = date('d-m-'.$year);
$date = strtotime($date);
$weeknummer = idate('W', $date);
//
// Welk dagnummer
$dagnummer = sprintf('%00d', date("d"));
$restwaarde = $dagnummer % 7;


// Welk maandnummer
$month = date('m');
// Aantal dagen in de maand;
$days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
// Huidige maand in het nederlands
$dateObj   = DateTime::createFromFormat('!m', $month);
$currentMaand = strftime('%B', $dateObj->getTimestamp());
$DateTime = new DateTime();
$DateTime = $DateTime->setDate($year, $month, $dagnummer);
$VandaagDutch = strftime('%A', $DateTime->getTimeStamp());
// welke dag van de week









$api = file_get_contents("https://api.openweathermap.org/data/2.5/weather?id=2754669&lang=nl&units=metric&APPID=389161d37f9217a9f6d846380862a778");
$gegevens = json_decode($api, true);

$html = "";


// Table row 
$html = $html . "<tr> <td colspan='5'>".$gegevens['main']['temp_min']. "&degC / ". $gegevens['main']['temp_max']."&degC ". $gegevens['weather'][0]['description']. "</td> </tr>";

$html = $html . "<tr>  <td colspan='5'>". strftime('%e %B %Y om %H:%M',time()). "</td> </tr>";

$html = $html . "<tr> <td colspan='5'> 'Humidity: ". $gegevens['main']['humidity']. "&percnt; | Wind: ". $gegevens['wind']['speed']. " km/h </td> </tr>";


for ($x = 1; $x <= 7; $x++) {
    // als 28 dagen is kan het in 4 kolommen anders in 5;
    if($days === 28){
        $aantalKolommen = 4;
        if($x == $dagnummer){$html = $html . "<tr> <td class='daynumber table-success bold'>". str_pad($x, 2, '0', STR_PAD_LEFT) . "</td> ";} else{        $html = $html . "<tr> <td class='daynumber bold'>". str_pad($x, 2, '0', STR_PAD_LEFT) . "</td> ";}
        if($x +7 == $dagnummer){$html = $html . " <td class='daynumber table-success bold'>". str_pad($x+7, 2, '0', STR_PAD_LEFT) . "</td> ";} else{        $html = $html . " <td class='daynumber bold'>". str_pad($x+7, 2, '0', STR_PAD_LEFT) . "</td> ";}
        if($x +14== $dagnummer){$html = $html . " <td class='daynumber table-success bold'>". $x+14 . "</td> ";} else{        $html = $html . "<td class='daynumber bold'>". $x+14 . "</td> ";}
        if($x +21 == $dagnummer){$html = $html . "<td class='daynumber table-success bold'>". $x+21 . "</td> ";} else{        $html = $html . "<td class='daynumber bold'>". $x+21  . "</td> ";}
        
    }
    else{
        $aantalKolommen = 5;
        if($x == $dagnummer){$html = $html . "<tr> <td class='daynumber table-success bold'>". str_pad($x, 2, '0', STR_PAD_LEFT) . "</td> ";} else{        $html = $html . "<tr> <td class='daynumber bold'>". str_pad($x, 2, '0', STR_PAD_LEFT) . "</td> ";}
        if($x +7 == $dagnummer){$html = $html . " <td class='daynumber table-success bold'>". $x+7 . "</td> ";} else{        $html = $html . " <td class='daynumber bold'>". $x+7 . "</td> ";}
        if($x +14== $dagnummer){$html = $html . " <td class='daynumber table-success bold'>". $x+14 . "</td> ";} else{        $html = $html . "<td class='daynumber bold'>". $x+14 . "</td> ";}
        if($x +21 == $dagnummer){$html = $html . "<td class='daynumber table-success bold'>". $x+21 . "</td> ";} else{        $html = $html . "<td class='daynumber bold'>". $x+21  . "</td> ";}
        if($x + 28 <= $days){
            if($x +28 == $dagnummer){$html = $html . "<td class='daynumber table-success bold'>". $x+28 . "</td> ";} else{        $html = $html . "<td class='daynumber bold'>". $x+28  . "</td> ";}
        }
        
        }
    }
    $html = $html . "</tr>";
  
  $html . $html . "</div>";
  $terugkoppeling['content1'] = $html; 
  $html = "";

  $maanden = [];
  
  for($x = 1; $x <= 7; $x++){
      $maanden[$x] = [];
  }
  setlocale(LC_TIME, 'NL_nl');
  for ($x = 1; $x <= 12; $x++) {
        $maand = sprintf("%02d", $x);
        $datum = $year."-".$maand."-01";
        $dag = date('N', strtotime($datum));
        $datum = strtotime($datum);
        array_push($maanden[$dag], strftime('%B', $datum));
  }
    $html = $html . "<tr class='bold'>";
        foreach($maanden as $dag)
        {
            if(isset($dag[0])){
                if($dag[0] === $currentMaand){
                    $html = $html . "<td class='table-success'>". ucfirst($dag[0]). "</td>";
                }
                else{
                    $html = $html . "<td>". ucfirst($dag[0]). "</td>";
                }
            }
            else{
                $html = $html . "<td> </td>";
            }
        }
    $html = $html . "</tr>";

    $html = $html . "<tr class='bold'>";
        foreach($maanden as $dag)
        {
            if(isset($dag[1])){
                if($dag[1] === $currentMaand){
                    $html = $html . "<td class='table-success'>". ucfirst($dag[1]). "</td>";
                }
                else{
                    $html = $html . "<td>". ucfirst($dag[1]). "</td>";
                }
            }
            else{
                $html = $html . "<td> </td>";
            }
        }
    $html = $html . "</tr>";


    $html = $html . "<tr class='bold'>";
        foreach($maanden as $dag)
        {
            if(isset($dag[2])){
                if($dag[2] === $currentMaand){
                    $html = $html . "<td class='table-success'>". ucfirst($dag[2]). "</td>";
                }
                else{
                    $html = $html . "<td>". ucfirst($dag[2]). "</td>";
                }
            }
            else{
                $html = $html . "<td> </td>";
            }
        }
    $html = $html . "</tr>";



  $terugkoppeling['content2'] = $html;
  $html = "";

  for($x = 1; $x <= $totalweeks; $x++)
  {
      if($x === 1)
      {
          if($x === $weeknummer)
          {
            $html = $html . "<tr class='bold'> <td class='table-success'>". str_pad($x, 2, '0', STR_PAD_LEFT) . "</td>";
          }
          else{
            $html = $html . "<tr class='bold'> <td>". str_pad($x, 2, '0', STR_PAD_LEFT) . "</td>";
          }
      }
      elseif($x % 14 === 0){
        if($x === $weeknummer)
        {
            $html = $html . "<td class='table-success bold'>". str_pad($x, 2, '0', STR_PAD_LEFT) . "</td> </tr> ";
        }
        else{
            $html = $html . "<td class='bold'>". str_pad($x, 2, '0', STR_PAD_LEFT) . "</td> </tr> ";
        }
      }
      else{
          if($x === $weeknummer)
          {
            $html = $html . "<td class='table-success bold'>".str_pad($x, 2, '0', STR_PAD_LEFT)."</td>";
          }
          else{
            $html = $html . "<td class='bold'>".str_pad($x, 2, '0', STR_PAD_LEFT)."</td>";
          }
      }
  }

  $terugkoppeling['aantalweken'] = $html;
  $html = "";


    if($restwaarde === 0){
        $restwaarde = 7;
    }
    $dagenvandeweek = ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag', 'Zondag'];

    for($x=1; $x <=7; $x++){
        $html = $html . "<tr>";
        
        foreach($dagenvandeweek as $dag){
                if($restwaarde === $x && $VandaagDutch === strtolower($dag)){
                    $html = $html . "<td class='table-success'> ". $dag ." </td>";
                }
                else{
                    $html = $html . "<td> ". $dag ." </td>";
                }
                
        }
        $dagtopush = array_shift($dagenvandeweek);
        array_push($dagenvandeweek, $dagtopush);
        $html = $html. "</tr>";
    }

    // if( date('N', $date) == 7) { $html = $html . "<td class='table-success'> Zondag </td>"; }else{ $html = $html . "<td> Zondag </td> ";}
    // if( date('N', $date) == 1) { $html = $html . "<td class='table-success'> Maandag </td>"; }else{ $html = $html . "<td> Maandag </td> ";}
    // if( date('N', $date) == 2) { $html = $html . "<td class='table-success'> Dinsdag </td>"; }else{ $html = $html . "<td> Dinsdag </td> ";}
    // if( date('N', $date) == 3) { $html = $html . "<td class='table-success'> Woensdag </td>"; }else{ $html = $html . "<td> Woensdag </td> ";}
    // if( date('N', $date) == 4) { $html = $html . "<td class='table-success'> Donderdag </td>"; }else{ $html = $html . "<td> Donderdag </td> ";}
    // if( date('N', $date) == 5) { $html = $html . "<td class='table-success'> Vrijdag </td>"; }else{ $html = $html . "<td> Vrijdag </td> ";}
    // if( date('N', $date) == 6) { $html = $html . "<td class='table-success'> Zaterdag </td>"; }else{ $html = $html . "<td> Zaterdag </td> ";}
    
    $terugkoppeling['dagenvandeweek'] = $html;
    $html = "<p> Te hergebruiken jaren: ";

    foreach($jaren as $jaar)
    {
        $html = $html . $jaar . " ,";
    }

    $html = $html . "</p>";

    $terugkoppeling['hergebruiken'] = $html;


  
  
?>
<?php  
    echo json_encode($terugkoppeling);
?>

