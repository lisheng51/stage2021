<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="Description" content="Enter your description here"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
<link rel="stylesheet" href="assets/css/style.css">
<title>Title</title>
</head>
<body>

    <div class="d-flex">  
        <div class="w-50 m-5">
            <p> Jaar </p>
            <select class="form-select w-100" id="year" onchange="changeWeek()">
                <?php
                    $year = date("Y");
                    $year = $year - 2;
                    $count = -2;
                    $html = "";
                    while ($count < 3){
                        if($count === 0){
                            $html = $html . "<option selected value='".$year."' id='-2'> ".$year." </option>";
                        }
                        else{
                            $html = $html . "<option value='".$year."' id='-2'> ".$year." </option>";
                        }   
                        $count +=1;
                        $year +=1;
                    }
                    echo $html;
                ?>
            </select>
        </div>

        <div class="w-50 m-5">
            <p> Week </p>
            <select class="form-select w-100" id="week" >
            </select>
        </div>

    </div>


</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
</body>
</html>


<script>
    $('document').ready(function(){
        var year = document.querySelector("#year").value;
        
        $.ajax({
        type: "POST",
        url: "request.php",
        data: {
            'year': year,
        },
        cache: false,
        timeout: 800000,
        success: function (data) {
                let html = document.querySelector("#week").innerHTML = data;
            }
    });
    });

    function changeWeek()
    {
        var year = document.querySelector("#year").value;
        $.ajax({
        type: "POST",
        url: "request.php",
        data: {
            'year': year,
        },
        cache: false,
        timeout: 800000,
        success: function (data) {
                let html = document.querySelector("#week").innerHTML = data;
            }
    });
    }

</script>