<?php
// $year = $_POST['year'];
$year = 2021;
$html = "";

$date = $year."-12-28";
$date = strtotime($date);
$totalweeks = idate('W', $date);

$week = 0;
$count = 0;
while($week < $totalweeks)
{
    $newoptgroup = false;
    $week += 1;
    $count +=1;
    $data = getStartEndDate($week, $year);

    $start = new DateTime($data['start_date']);
    $start_maand = $start->format('F');

    $eind = new DateTime($data['end_date']);
    $eind_maand = $eind->format('F');

    if($week === 1)
    {
        $html = $html . "<optgroup label='" . $eind_maand . "'>";
    }
    else if($week === 53){
        //
    }
    else if($week === 52){
        $jaar = $eind->format('Y');
        if($year + 1 === $jaar){
            $week = 55;
        }
    }
    else if($start_maand != $eind_maand || $count > 4){
        $newoptgroup = true;
        $count = 0;
        
        if($start_maand !== $eind_maand)
        {
            $html = $html . "<option value='" . $week . "'>" . $week . " (" . $data['start_date'] . " t/m " . $data['end_date']. ")" . "</option>";
        }
    }
    if($newoptgroup === true)
    {
        $html = $html = $html . "</optgroup>";
        $html = $html . "<optgroup label='" . $eind_maand . "'>";
    }
    $html = $html . "<option value='" . $week . "'>" . $week . " (" . $data['start_date'] . " t/m " . $data['end_date']. ")" . "</option>";

}
echo $html;

function getStartEndDate($week, $year)
{
    $dateTime = new DateTime();
    $dateTime->setIsoDate($year, $week);
    $return['start_date'] = $dateTime->format('d-m-Y');
    $dateTime->modify('+6 days');
    $return['end_date'] = $dateTime->format('d-m-Y');

    return $return;
}

?>